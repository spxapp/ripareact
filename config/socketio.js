module.exports = function(socketio) {
    socketio.on('connection', function(socket) {
        console.log("socket connected")
      socket.address = socket.request.connection.remoteAddress +
        ':' + socket.request.connection.remotePort;
   
      socket.connectedAt = new Date();
   
      socket.log = function(data) {
        console.log(`SocketIO  ${socket.nsp.name} [${socket.address}]`, data);
      };
   
      socket.on('disconnect', function() {
        socket.log('Disconnected...');
      });
   
      socket.on('room', function(room) {
        console.log('Connected room is ', room)
        socket.join(room);
      });
    });
   }