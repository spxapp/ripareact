const JwtAdminStrategy = require('passport-jwt').Strategy;
const ExtractJwt  = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const AdminUsers = mongoose.model('adminusers');
const keys = require('../../config/keys');

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretKey;

module.exports = passportAdmin =>{
	passportAdmin.use(
		'admin-role',
		new JwtAdminStrategy(opts, (jwt_payload, done) => {
			AdminUsers.findById(jwt_payload.id)
			.then(user => {
				if(user){
					return done(null, user);
				} 
				return done(null, false);				 
			})
			.catch(err => console.log(err));
		})
	);
};