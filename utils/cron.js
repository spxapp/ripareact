const User = require('../models/Users');  
const Vehicles = require('../models/Vehicles');
const Bids = require('../models/Bids');
const cron = require('node-cron');

const keys = require('../config/keys');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(keys.SENDGRID_API_KEY); 


const cronJob =  () => {
    
    // run cron everyday at 8 am
    cron.schedule('0 8 * * *',  async () => {
          
        var startDate = new Date();  

        startDate.setSeconds(0);
        startDate.setHours(0);
        startDate.setMinutes(0);

        var dateMidnight = new Date(startDate);
        dateMidnight.setHours(23);
        dateMidnight.setMinutes(59);
        dateMidnight.setSeconds(59);

        const vehiclesFive = await Vehicles.find(
        {   "accepted":true,  
            "accepted_bid": {$exists: true },     
            "email_click": {$exists: false },  
            "date": 
            {
                $gte: new Date(new Date(startDate).getTime() - (5 * 24 * 60 * 60 * 1000)),
                $lte: new Date(new Date(dateMidnight).getTime() - (5 * 24 * 60 * 60 * 1000)) 
            }
        }
        ).limit(100);

        const vehiclesTwo = await Vehicles.find(
        {   "accepted":true,  
            "accepted_bid": {$exists: true },   
            "email_click": {$exists: false },   
            "date": 
            {
                $gte: new Date(new Date(startDate).getTime() - (2 * 24 * 60 * 60 * 1000)),
                $lte: new Date(new Date(dateMidnight).getTime() - (2 * 24 * 60 * 60 * 1000)) 
            }
        }
        ).limit(100);
 
         
        if(vehiclesTwo!==null)
        {
            vehiclesTwo.map(async vehicle => {
                
                // send followup email  
                let user = await User.findOne({_id: vehicle.user}); 
                let vehicle_name = vehicle.year+" "+vehicle.make+" "+vehicle.model;
                var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

                var last_user_bid = await Bids.findOne({vehicle: vehicle._id}).sort({_id:-1});
                let buyer  = last_user_bid.user;  
 
                let buyer_details = await User.findOne({_id: buyer}); 
          
                let date = new Date(vehicle.date);
                let dateFormat = dateForm(date);
                
                const msg = {
                    to: user.email,
                    from: 'Ripa <info@ripa.app>',
                    subject: 'Vehicle Auction Transaction Status.', 
                    templateId: '1c8eb39c-182c-48d0-89c7-7c522acc8b29',
                    substitutions: {
                        yes_url: Base64.encode('user='+buyer+'&vehicle='+vehicle._id+'&status=yes'),
                        no_url: Base64.encode('user='+buyer+'&vehicle='+vehicle._id+'&status=no'), 
                        seller_name: user.name, 
                        vehicle_name: vehicle_name,
                        date: dateFormat,
                        buyer_name: buyer_details.name, 
						SITE_URL: "https://www.ripa.co.nz/contact/" 
                    },
                };
                sgMail.send(msg);
            });
        }

        if(vehiclesFive!==null)
        {
            vehiclesFive.map(async vehicle => {
              
               // send followup email  
               let user = await User.findOne({_id: vehicle.user}); 
               let vehicle_name = vehicle.year+" "+vehicle.make+" "+vehicle.model;
               var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
               var last_user_bid = await Bids.findOne({vehicle: vehicle._id}).sort({_id:-1});
               let buyer  = last_user_bid.user;

               
               let buyer_details = await User.findOne({_id: buyer}); 
 
              let date = new Date(vehicle.date);
              let dateFormat = dateForm(date);

               const msg = {
                   to: user.email,
                   from: 'Ripa <info@ripa.app>',
                   subject: 'Vehicle Auction Transaction Status.', 
                   templateId: 'e22b225b-9e6f-4e6d-a78e-1479b73fb34c',
                   substitutions: {  
                       yes_url: Base64.encode('user='+buyer+'&vehicle='+vehicle._id+'&status=yes'),
                       no_url: Base64.encode('user='+buyer+'&vehicle='+vehicle._id+'&status=no'), 
                       seller_name: user.name, 
                       vehicle_name: vehicle_name,
                       date: dateFormat,
                       buyer_name: buyer_details.name, 
                       SITE_URL: keys.SITE_URL 
                   },
               };
               sgMail.send(msg); 

            });
        }
         

    }, {
        scheduled: true,
        timezone: "Pacific/Auckland"
    }); 
};


const dateForm = (date) => { 
          
    let day   = (date.getDate()< 10) ? '0'+date.getDate() : date.getDate();
    let month = (((date.getMonth() + 1)< 10) ? '0'+ (date.getMonth() + 1) : date.getMonth() + 1);
    let year  = date.getFullYear();
    let hours = date.getHours();
    let mins  = date.getMinutes(); 

    return day + "-" + month + "-" + year + " at " + hours + ":" + mins;

}

module.exports = cronJob;