const request = require("request");

const sendRequest = function(URL, SOAPAction, body) {
    return new Promise((resolve, reject) => {
        const headers = {
            SOAPAction: SOAPAction,          
            "Content-Type": "text/xml"
        };
        const options = {
            method: "POST",
            url: URL,
            headers: headers,
            body: body     
        };
        console.log("=================");
        console.log(options.url);
        console.log("=================");
        request(options, function(error, response, body) {
            // console.log(response.statusCode, body);
            if (error) reject(error);
            resolve({statusCode :response.statusCode, body});
        });
    })
};

module.exports = sendRequest;
