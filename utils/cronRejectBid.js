const User = require("../models/Users");
const Profile = require("../models/Profile");
const Vehicles = require("../models/Vehicles");
const Bids = require("../models/Bids");

const cron = require("node-cron");
const request = require("request");
const keys = require("../config/keys");

const stripe = require("stripe")(keys.StripeKey);

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(keys.SENDGRID_API_KEY);

const cronRejectBid = () => {
  // run cron in every 15 minutes
  cron.schedule(
    "*/15 * * * *",
    async () => {
      //find expired unaccepted/unrejected vehicles of last 1.15-1.20 hours
      const vehicles = await Vehicles.find({
        expired: true,
        accepted_bid: { $exists: false },
        current_bid: {
          $gt: 1
        },
        date: {
          $gt: new Date(new Date().getTime() - 90 * 60 * 1000),
          $lte: new Date(new Date().getTime() - 75 * 60 * 1000)
        }
      }).limit(100);

      if (vehicles !== null) {
        vehicles.map(async vehicle => {
            //console.log(vehicle);

          // get winner user
          var last_user_bid = await Bids.findOne({ vehicle: vehicle._id }).sort(
            {
              _id: -1
            }
          );

          let winBid = last_user_bid.id;
          var seller = vehicle.user;
          var buyer = last_user_bid.user;

          await Vehicles.findOneAndUpdate(
            { _id: vehicle._id, accepted: { $exists: false } },
            {
              $set: {
                accepted: false,
                accepted_bid: winBid
              }
            }
          );

          if (last_user_bid !== null) {
            //get seller user
            var seller_user = await User.findOne({ _id: seller });
            var buyer_user = await User.findOne({ _id: buyer });

            var buyer_details = await Profile.findOne({ user: buyer });
            var seller_details = await Profile.findOne({ user: seller });

            //negative feedback
            if (typeof seller_user.seller_feedback !== "undefined") {
              var seller_feedback = parseInt(
                seller_user.seller_feedback - 1,
                10
              );
            } else {
              var seller_feedback = 100 - 10;
            }

            // Deduct seller $1 for rejection and credit to the top bidder
            var sellerDetails = await User.findOne({ _id: seller_user._id });

            var get_bid_user_token = await User.distinct(
              "notifications_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            );
            
            get_bid_user_token = get_bid_user_token.concat(await User.distinct(
              "web_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ),
            await User.distinct(
              "ios_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ))

            var sellerVirtualCredits = 0;
            var amount = 1;

            if (
              typeof sellerDetails.virtual_credit !== "undefined" &&
              sellerDetails.virtual_credit > 0
            ) {
              sellerVirtualCredits = parseFloat(
                sellerDetails.virtual_credit,
                2
              );
            }
            let totalAmountCheck = amount * 1.15;
            // check if the buyer have the virtual credits available
            if (sellerVirtualCredits > 0) {
              // check if the buyer virtual credit less or equal to 1
              if (sellerVirtualCredits < totalAmountCheck) {
                let totalAmount = amount * 1.15;

                amount = totalAmount - sellerVirtualCredits;
                virtual_credits = 0;

                // Deduct from seller and update
                User.findOneAndUpdate(
                  { _id: sellerDetails._id },
                  {
                    $set: {
                      virtual_credit: virtual_credits
                    }
                  }
                ).then(async user => {
                  let profile = await Profile.findOne({
                    user: sellerDetails._id
                  });

                  if (profile.customer_id) {
                    await stripe.charges.create({
                      amount: amount * 100,
                      currency: "nzd",
                      customer: profile.customer_id
                    });
                  }
                });
                // else the seller virtual credit bigger then 1
              } else {
                let totalAmount = amount * 1.15;

                virtual_credits = sellerVirtualCredits - totalAmount;
                const updateUser = await User.findOneAndUpdate(
                  { _id: sellerDetails._id },
                  {
                    $set: {
                      virtual_credit: virtual_credits
                    }
                  }
                );
              }
              // seller doesn't have the virtual credit, deduct from stripe
            } else {
              let profile = await Profile.findOne({ user: sellerDetails._id });

              if (profile.customer_id) {
                await stripe.charges.create({
                  amount: amount * 115,
                  currency: "nzd",
                  customer: profile.customer_id
                });
              }
            }

            // Credit to buyer and update
            const updateBuyer = await User.findOneAndUpdate(
              { _id: buyer },
              {
                $inc: {
                  virtual_credit: amount * 1.15
                }
              }
            );

            let vehicle_name =
              vehicle.year + " " + vehicle.make + " " + vehicle.model;

            // if bidder user has opted for notifications
            if (get_bid_user_token) {
              await request({
                url: "https://fcm.googleapis.com/fcm/send",
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Authorization:
                    "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
                },
                body: JSON.stringify({
                  notification: {
                    title: "Ripa - " + vehicle_name + " Bid Rejected!",
                    body: "Your bid has been rejected by seller.",
                    icon: "./logo_192x192.png",
                    sound: "sound",
                    "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
                  },
                  data: {
                    body: "expired",
                    vehicle: vehicle._id,
                    expiry: vehicle.expiry_date
                  },
                  registration_ids: get_bid_user_token
                })
              });
            }

            // send rejected email notification to buyer and seller
            const msg = {
              to: buyer_user.email,
              from: "Ripa <info@ripa.app>",
              subject: "Sorry, " + vehicle_name + " Bid Rejected!",
              templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
              substitutions: {
                user: buyer_user.name,
                seller_name: seller_user.name,
                seller_email: seller_user.email,
                vehicle: vehicle_name,
                SITE_URL: "https://www.ripa.co.nz/contact/",
                message:
                  "Sorry, The seller of " +
                  vehicle_name +
                  " has rejected your bid."
              }
            };
            sgMail.send(msg);
          }

          if (seller_feedback > 100) {
            seller_feedback = 100;
          }

          if (seller_feedback < 0) {
            seller_feedback = 0;
          }

          var update_seller = await User.findOneAndUpdate(
            { _id: seller },
            {
              $set: { seller_feedback: seller_feedback }
            }
          );
        });
      }
    },
    {
      scheduled: true,
      timezone: "Pacific/Auckland"
    }
  );
};

module.exports = cronRejectBid;
