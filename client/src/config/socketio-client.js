import Keys from './keys'
import openSocket from "socket.io-client";

const socketUrl = Keys.socketUrl; 

export const socket = openSocket(socketUrl);

socket.on("connect", () => {
  console.log("socket connected")
});