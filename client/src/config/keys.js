const Keys = {
    socketUrl : 'http://localhost:5000',
    stripeKey : 'pk_test_lYMahl4rdI60VzDADprscYir'
};
 
if(process.env.REACT_APP_ENVIRONMENT === 'production'){

    Keys.socketUrl = 'https://www.ripa.app'
    Keys.stripeKey = 'pk_live_undapLCCqZNeaM41IQKKRVcq'
    
}else if(process.env.REACT_APP_ENVIRONMENT === 'staging'){
	
    Keys.socketUrl = 'https://dev.ripa.app'
    Keys.stripeKey = 'pk_test_lYMahl4rdI60VzDADprscYir'

}else{
    Keys.socketUrl = 'http://localhost:5000'
    Keys.stripeKey = 'pk_test_lYMahl4rdI60VzDADprscYir'
} 
console.log('You are on '+process.env.REACT_APP_ENVIRONMENT+' Environment');
export default Keys;
