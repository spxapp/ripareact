import firebase from "firebase/app";
import "firebase/messaging";
import createHistory from "history/createBrowserHistory";
const history = createHistory();

export const initializeFirebase = () => {
  firebase.initializeApp({
    messagingSenderId: "906600033130"
  });
  const messaging = firebase.messaging();
  
  messaging.onMessage(function(payload) {

    /* Commented out Foreground notification */ 
    /*
    
    if(typeof payload.notification !== 'undefined'){

       
      const notificationTitle = payload.notification.title;
      const notificationOptions = {
          body: payload.notification.body,
          icon: payload.notification.icon,        
      };

      if (!("Notification" in window)) {
          console.log("This browser does not support system notifications");
      }
      // Let's check whether notification permissions have already been granted
      else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
          var notification = new Notification(notificationTitle,notificationOptions);
          notification.onclick = function(event) {
              event.preventDefault(); // prevent the browser from focusing the Notification's tab
              window.open(payload.notification.click_action , '_blank');
              notification.close();
          }
      }
    } 
    */

    if(typeof payload.data === 'undefined'){
      return false;
    }

    if (payload.data.body === "outbid") {
      history.push('/vehicle-details',{
        id:payload.data.vehicle,
        active:true
      })
    }
    if (payload.data.body === "new_vehicle") {
      history.push('/vehicle-details',{
        id:payload.data.vehicle,
        active:true
      })
    }
    if (payload.data.body === "expired") {
      history.push('/vehicle-details',{
        id:payload.data.vehicle,
        active:false,
        expiry_date:payload.data.expiry
      })
    }
    if (payload.data.body === "seller_details") {
      window.location.replace(`/user-details/${payload.data.user}/Seller`);
    }
  }); 
};

export const AskForPermissionAndGenerateToken = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();

    return token;
  } catch (error) {
    console.error(error);
  }
};
