const myAccountStyles = {
  container: { 
    position: "relative",
    paddingTop: "0vh",  
    textAlign:"center",   
  },
  uploadBtn:{
    paddingTop: "20px",
    paddingBottom:"30px",
    "& label > span":{
      borderRadius: "100%",
      padding: "35px", 
      backgroundColor: "#d8d8d8", 
      "& svg":{
        height:"40px",
        width:"40px",
        color:"#fff"
      }
    }
  },
  profileImg:{
    borderRadius: "100%",
    height:"110px",
    width:"110px",
    objectFit:"cover"
  },
  profilePicTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  },
   section:{
    paddingTop:"150px",
    paddingLeft: "15px",
    backgroundColor:"#fff",
    paddingRight: "15px",
    minHeight:'100vh',
    "& *":{
      fontFamily: "Roboto"
    }
   },sectionSettings:{
    paddingTop:"130px",
    paddingLeft: "15px",
    backgroundColor:"#fff",
    // minHeight:'100vh',
    paddingRight: "15px",
    "& *":{
      fontFamily: "Roboto"
    }
   },
   sectionRate:{ 
    backgroundColor:"#424649",
    textAlign:"left",
    marginTop:"30px",
    marginLeft:"-15px",
    marginRight:"-15px",
    paddingTop:"30px",
    paddingBottom:"30px",
    paddingLeft: "20px",
    flexGrow:"1",
    paddingRight: "20px",
    "& *":{
      fontFamily: "Roboto",
      color:"#fff"
    }
   },
   loadingScreen:{
      paddingTop:"150px",
      paddingLeft: "0px",
      backgroundColor: "#fff",
      paddingRight: "0px",
      "& *":{
        fontFamily: "Roboto"
      },
   },
   form:{

   },
   textField:{
    flex:1,
    display:"flex",
    marginTop:"10px",
    marginBottom:"10px",
    backgroundColor:"#ededed",  
    borderRadius:"5px",
  "& *, & *:hover":{ 
      backgroundColor:"#ededed",  
      borderRadius:"5px",
  },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        display: "none"
      },
    "& input":{
      paddingTop:"18px",
      paddingRight:"15px",
      paddingBottom:"18px",
      paddingLeft:"15px",
      color:"#666",
      fontSize:"16px", 
      fontWeight:"normal",

      "&::-webkit-input-placeholder":{
        color:"#666",
        opacity:1
      }
    } 
   },
   forgotpassword:{
      display:"flex",
      alignItem:"left",
      fontSize:"12px", 
      paddingLeft:"15px",
      color:"#666",
      textDecoration:"none"
   },
   button:{
    backgroundColor:"#424649", 
    fontWeight: "500",
    color:"#fff",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:"0",
    marginTop:"30px",
     "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
   	paddingTop:"30px",
   	"& a":{ 
   		color:"#161617",
   		textDecoration:"none",
   		textTransform:"uppercase",
   		fontSize:"16px",
   		fontFamily:"Roboto",
   		fontWeight:"normal",
   		letterSpacing:"0.5px"
   	}
   },
   progress:{
    marginTop: "20px"
   },
   field:{
    "& label":{
      textAlign: "left",
      display: "block",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "15px",
      marginTop: "-5px",
      color: "#444"
    }
   }
  ,grow: {
    flexGrow: 1,
  }, 
  notiTitle:{
    fontFamily: "Roboto",
    fontSize: "16px",
    fontWeight: "normal",
    color:"rgba(0, 0, 0, 0.87)",
    "& span":{
      position: "absolute", 
      left: 0,
      bottom: 0,
      fontSize: '12px',
      color: '#666'
    }
  },
  billingInfoHeading:{
    fontFamily: "Roboto",
    fontSize: "14px",
    fontWeight: "500",
    textTransform:"uppercase",
    color:"rgba(0, 0, 0, 0.54)",
    textAlign:"left" 
  },
  billingAdd:{
    marginTop:"20px"
  },
  billingInfo:{ 
    color:"#424649",
    fontFamily: "Roboto",
    fontSize: "16px",
    fontWeight: "normal",
    textAlign:"left"
  },
  notiButton: {
    marginLeft: -12, 
    color:"rgba(0, 0, 0, 0.54)"
  },
  notiSwitch:{
    color:"#da091f"
  },
  notiToolbar:{
    paddingLeft:0,
    paddingRight:0 
  },listItems:{
    backgroundColor:"#fff",
    "& img":{
      maxWidth: "100%"
    }
  },
  listLink:{
    textDecoration:"none"
  },
  listCurrentBid:{ 
    color:"#ec1d25",
    fontSize:"14px",
    textDecoration:"none",  
    fontWeight:"bold",
    fontFamily:"Roboto",  
    letterSpacing: "0.3px",
    lineHeight:"20px",
    "& :visited":{ 
      color:"#eea122",
    }
  },
  listTitle:{
  color:"rgba(0, 0, 0, 0.87)",
  fontSize: "16px",
  lineHeight:"20px",
  paddingTop:"10px", 
  paddingBottom:"5px",
    fontFamily:"Roboto",
    fontWeight:"normal",
  "& :visited":{ 
      color:"rgba(0, 0, 0, 0.87)",
    }
  },
  listKm:{
    fontSize:"14px",
    fontFamily:"Roboto",
    fontWeight:"normal",
    marginBottom:"5px",
    color:"rgba(0, 0, 0, 0.6)",
    "& :visited":{ 
      color:"rgba(0, 0, 0, 0.6)",
    }
  },
  listEnd:{
    color:"#ec1d25",
    paddingTop:"10px",
    paddingBottom:"10px"
  },formControl: { 
    minWidth: 270,
  },
  gridItems:
  {
    "& img":{
      maxWidth:"100%"
    }
  },
  fieldRate:{
    "& label":{
      textAlign: "left",
      display: "block",
      fontSize:"16px",
      padding: "0",
      fontWeight:"500",
      marginBottom: "10px",
      paddingLeft: "5px", 
      paddingRight: "5px", 
      color: "#fff"
    }
   },
   fieldRateBig:{
    "& label":{
      textAlign: "left",
      display: "block",
      fontSize:"20px",
      fontWeight:"600",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "5px", 
      paddingRight: "5px", 
      color: "#fff",
      "& span":{
        color:"#ec1d25"
      }
    }
   },
   modelField:
  {
    width:"100%",
    "& label":{
      color:"rgba(0, 0, 0, 0.54)",
      fontFamily:"Roboto",
      fontWeight:"500",
      display:"flex",
      flex:1
    }
    
  },
  selectBoxModel:{
    flex:1,
    display:"flex",
    marginTop:"0",
    marginBottom:"25px", 
    borderRadius:"5px",
    paddingTop:"5px",
    paddingRight:"0",
    paddingBottom:"5px",
    textAlign:"left",
    paddingLeft:"0",
    color:"#424649", 
    backgroundColor:"transparent !important",
    fontSize:"14px", 
    fontWeight:"500",
    "& *, & *:hover, &:hover, &:active":{ 
      backgroundColor:"transparent !important", 
    }, 
    "&:before":{
      borderBottom:"1px solid rgba(0, 0, 0, 0.12)"
    },
    "&::-webkit-input-placeholder":{
      color:"#424649",
      opacity:1
    } 
  },
  textFieldModel:{
    flex:1,
    display:"flex",  
    backgroundColor:"transparent",  
    marginTop:0, 
    marginBottom:"25px", 
    "& *, & *:hover":{ 
      backgroundColor:"transparent", 
    },"& >*:before":{
      borderBottom:"1px solid rgba(0, 0, 0, 0.12)"
    },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        
    },
    "& input":{
      paddingTop:"10px",
      paddingRight:"0px",
      paddingBottom:"10px",
      paddingLeft:"0px",
      color:"#424649",
      fontSize:"14px", 
      fontWeight:"500", 
      "&::-webkit-input-placeholder":{
        color:"#424649",
        opacity:1
      }
    } 
  },
  linkButton:{
    color:"#ec1d25", 
    fontSize:"14px", 
    fontWeight:"500",
  },
  linkButtonCancel:{
    
    color:"rgba(0, 0, 0, 0.54)", 
    fontSize:"14px", 
    fontWeight:"500",
  },
  statusMessage:{
    paddingBottom:"10px",
    color:"rgba(0, 0, 0, 0.6)",
    fontSize:"14px"
  },
  filterError:{
    fontFamily:"Roboto",
    color:"red",
    paddingBottom:"10px"
  },
  noSavedSearch:{
    padding: "20px 0",
    color: "rgba(0, 0, 0,0.62)"
  }
};

export default myAccountStyles;
