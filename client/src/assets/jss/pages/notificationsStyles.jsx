const notificationsStyles = {
  AppBar:{
    backgroundColor: "#ec1d25",
    paddingTop:"20px", 
    position: 'absolute', 
    top: 0, 
    left: 0,
    width: '100%',
  zIndex:100

  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    color:"#fff"
  }, 
  closeButton: {
    marginRight: -12, 
    color:"#fff"
  }, 
  title:{
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "500",
    
  },
  grow:{
    flexGrow:1
  },
  container: { 
    position: "relative",
    paddingTop: "5vh", 
    paddingBottom: "10vh",  
  },
  section:{
    paddingTop:"60px",
    paddingLeft: "0px",
    paddingRight: "0px",
    textAlign:"center",
    backgroundColor:"#f9f9f9",
    "& *":{
      fontFamily: "Roboto"
    }
  },
  noNotifications:{
    textAlign:"center"
  },
  bidRow:{
    textAlign:"left",
    paddingLeft:"20px",
    paddingRight:"20px",
    "& > div":{
      padding:"10px",
      marginBottom:"10px",
      marginTop:"10px"
    }
  } 
};

export default notificationsStyles;
