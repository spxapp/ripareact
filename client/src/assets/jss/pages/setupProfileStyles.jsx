const setupProfileStyles = {
  container: { 
    position: "relative",
    paddingTop: "5vh", 
    paddingBottom: "10vh", 
    textAlign:"center",
  }, 
	header:{
		paddingTop:"5vh", 
    "& h3":{
      fontSize: "16px",
      fontFamily: "Roboto",
      fontWeight: "normal",
      letterSpacing: "0.5px"
    }  
  }, 
  Progress:{
    marginLeft: "30px",
    marginRight: "30px"
  },
  uploadBtn:{
    paddingTop: "20px",
    paddingBottom:"30px",
    "& label > span":{
      borderRadius: "100%",
      padding: "35px", 
      backgroundColor: "#d8d8d8", 
      "& svg":{
        height:"40px",
        width:"40px",
        color:"#fff"
      }
    }
  },
  profileImg:{
    borderRadius: "100%",
    height:"110px",
    width:"110px",
    objectFit:"cover"
  },
  profilePicTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  },
   section:{
   	paddingTop:"20px",
    paddingLeft: "15px",
    paddingRight: "15px",
    "& *":{
    	fontFamily: "Roboto"
    }
   },
   form:{

   },
   textField:{
   	flex:1,
   	display:"flex",
   	marginTop:"10px",
   	marginBottom:"10px",
   	backgroundColor:"#ededed",  
   	borderRadius:"5px",
	"& *, & *:hover":{ 
	   	backgroundColor:"#ededed",  
	   	borderRadius:"5px",
	},
   	"& *:after, & *:before, & *:hover:after, & *:hover:before":{
	   		display: "none"
	   	},
   	"& input":{
   		paddingTop:"18px",
   		paddingRight:"15px",
   		paddingBottom:"18px",
   		paddingLeft:"15px",
	   	color:"#666",
	   	fontSize:"16px", 
	   	fontWeight:"normal",

   		"&::-webkit-input-placeholder":{
   			color:"#666",
   			opacity:1
   		}
   	} 
   },
   forgotpassword:{
   		display:"flex",
   		alignItem:"left",
   		fontSize:"12px", 
   		paddingLeft:"15px",
   		color:"#666",
   		textDecoration:"none"
   },
   button:{
   	backgroundColor:"#424649", 
   	fontWeight: "500",
   	color:"#fff",
   	letterSpacing:"1.3px",
   	paddingLeft:"20px",
   	paddingRight:"20px",
   	borderRadius:"0",
   	marginTop:"30px",
   	 "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   cancelbutton:{
   	backgroundColor:"#424649", 
   	fontWeight: "500",
   	color:"#fff",
   	letterSpacing:"1.3px",
   	paddingLeft:"20px",
    marginLeft:"15px",
   	paddingRight:"20px",
   	borderRadius:"0",
   	marginTop:"30px",
   	 "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
   	paddingTop:"30px",
   	"& a":{ 
   		color:"#161617",
   		textDecoration:"none",
   		textTransform:"uppercase",
   		fontSize:"16px",
   		fontFamily:"Roboto",
   		fontWeight:"normal",
   		letterSpacing:"0.5px"
   	},"& span":{
      textTransform:"uppercase"
    }  
   },
   creditCardBox:{ 
    marginTop:"10px",
    marginBottom:"10px",
    backgroundColor:"#ededed", 
    paddingTop:"15px",
    paddingRight:"20px",
    paddingBottom:"15px",
    paddingLeft:"20px", 
    borderRadius:"5px",
    "& iframe":{
      minHeight:"30px !important"
    }
   },
   formRow:{
     marginBottom: "25px"
   }
};

export default setupProfileStyles;
