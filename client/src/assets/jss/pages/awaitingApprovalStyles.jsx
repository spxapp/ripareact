const awaitingApprovalStyles = {
  container: { 
    position: "relative",
    paddingTop: "0vh",  
    textAlign:"center",
    backgroundColor:"#f1f2f2",
    height:"100vh",
  },  
   section:{
    backgroundColor:"#f1f2f2",
    paddingTop:"200px",
    paddingLeft: "15px",
    paddingRight: "15px", 
    textAlign:"center",
    paddingBottom:"20px",
    "& *":{
      fontFamily: "Roboto"
    }
   },
   sectionRate:{
    marginTop:"20px",
    paddingTop:"20px",
    paddingLeft: "15px",
    flexGrow:"1",
    paddingRight: "15px",
    "& *":{
      fontFamily: "Roboto",
      color:"#fff"
    }
   },
   form:{

   },
   textField:{
    flex:1,
    display:"flex",
    marginTop:"10px",
    marginBottom:"10px",
    backgroundColor:"#ededed",  
    borderRadius:"5px",
  "& *, & *:hover":{ 
      backgroundColor:"#ededed",  
      borderRadius:"5px",
  },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        display: "none"
      },
    "& input":{
      paddingTop:"18px",
      paddingRight:"15px",
      paddingBottom:"18px",
      paddingLeft:"15px",
      color:"#666",
      fontSize:"16px", 
      fontWeight:"normal",

      "&::-webkit-input-placeholder":{
        color:"#666",
        opacity:1
      }
    } 
   },
   forgotpassword:{
      display:"flex",
      alignItem:"left",
      fontSize:"12px", 
      paddingLeft:"15px",
      color:"#666",
      textDecoration:"none"
   },
   button:{
    backgroundColor:"#424649", 
    fontWeight: "500",
    color:"#fff",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:"0",
    marginTop:"30px",
     "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
    paddingTop:"30px",
    "& a":{ 
      color:"#161617",
      textDecoration:"none",
      textTransform:"uppercase",
      fontSize:"16px",
      fontFamily:"Roboto",
      fontWeight:"normal",
      letterSpacing:"0.5px"
    }
   },
   progress:{
    marginTop: "20px"
   },
   field:{
    "& label":{ 
      display: "block",
      fontSize:"18px", 
    textAlign:"center",
      padding: "0",
      marginBottom: "20px",
      cursor:"pointer",
      paddingLeft: "5px", 
      paddingRight: "5px", 
      color: "rgba(0, 0, 0, 0.6)"
    }
   },
   fieldRate:{
    "& label":{ 
      display: "block",
      fontSize:"16px",
      padding: "0",
      marginBottom: "10px", 
    textAlign:"center",
      paddingLeft: "5px", 
      paddingRight: "5px", 
      color: "#fff"
    }
   },
   fieldRateBig:{
    "& label":{
      textAlign: "left",
      display: "block",
      fontSize:"20px",
      padding: "0",
      marginBottom: "10px",
      paddingLeft: "5px", 
      paddingRight: "5px", 
      color: "#fff"
    }
   },
   AppBar:{
    backgroundColor: "#ec1d25", 
    textAlign:"center",
  paddingTop:"20px",  
  }, 
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  grow: {
    flexGrow: 1,
  }, 
  title:{
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "500",
    textAlign:"center",
  },
  header:{  
    display:"flex",
    justifyContent:"center",
    flexGrow:1,
    "& h3":{
      fontSize: "20px",
      fontFamily: "Roboto",
      fontWeight: "500",  
    textAlign:"center",
      color: "#fff",
      letterSpacing: "0.5px"
    }  
  },
  fieldName:{
    color:"rgba(0, 0, 0, 0.87)",
    fontSize:"24px", 
    marginBottom:"10px",
      paddingLeft: "5px",  
    textAlign:"center",
      paddingRight: "5px",
  },
  awaitingIcon:{
    "& svg":{
      color:"green",
      fontSize:"60px"
    }
    
  },
  errorIcon:{
    "& svg":{
      color:"#f0992e",
      fontSize:"60px"
      
    }
  }
};

export default awaitingApprovalStyles;
