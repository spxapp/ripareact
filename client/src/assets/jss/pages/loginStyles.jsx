const loginStyles = {
  container: { 
    position: "relative",
    paddingTop: "5vh", 
    paddingBottom: "10vh", 
    textAlign:"center",
  }, 
	header:{
		paddingTop:"10vh"
	},
   section:{
   	paddingTop:"20px",
    paddingLeft: "15px",
    paddingRight: "15px",
    "& *":{
    	fontFamily: "Roboto"
    }
   },
   form:{

   },
   textField:{
   	flex:1,
   	display:"flex",
   	marginTop:"10px",
   	marginBottom:"10px",
   	backgroundColor:"#ededed",  
   	borderRadius:"5px",
	"& *, & *:hover":{ 
	   	backgroundColor:"#ededed",  
	   	borderRadius:"5px",
	},
   	"& *:after, & *:before, & *:hover:after, & *:hover:before":{
	   		display: "none"
	   	},
   	"& input":{
   		paddingTop:"18px",
   		paddingRight:"15px",
   		paddingBottom:"18px",
   		paddingLeft:"15px",
	   	color:"#666",
	   	fontSize:"16px", 
	   	fontWeight:"normal",

   		"&::-webkit-input-placeholder":{
   			color:"#666",
   			opacity:1
   		}
   	} 
   },
   forgotpassword:{
   		display:"flex",
   		alignItem:"left",
   		fontSize:"12px", 
   		paddingLeft:"15px",
   		color:"#666",
   		textDecoration:"none"
   },
   button:{
   	backgroundColor:"#424649", 
   	fontWeight: "500",
   	color:"#fff",
   	letterSpacing:"1.3px",
   	paddingLeft:"20px",
   	paddingRight:"20px",
   	borderRadius:"0",
   	marginTop:"30px",
   	 "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
   	paddingTop:"30px",
   	"& a":{ 
   		color:"#161617",
   		textDecoration:"none",
   		textTransform:"uppercase",
   		fontSize:"16px",
   		fontFamily:"Roboto",
   		fontWeight:"normal",
   		letterSpacing:"0.5px"
   	}
   },
   progress:{
    marginTop: "20px"
   },pageTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  }
};

export default loginStyles;
