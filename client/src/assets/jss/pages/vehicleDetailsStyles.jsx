const vehicleDetailsStyles = {
  AppBar:{
    backgroundColor: "#ec1d25",
    paddingTop:"20px", 
    position: 'fixed', 
    top: 0, 
    left: 0,
    width: '100%',
  zIndex:100

  }, 
  vehicleStatus:{
    backgroundColor:"#d0021b",
    fontSize:"16px",
    fontFamily:"Roboto",
    color:"#fff",
    fontWeight:"500",
    paddingLeft:"15px",
    textTransform:"uppercase",
    paddingTop:"77px"
  },
  vehicleOpenStatus:{
    backgroundColor:"#fab611", 
    fontSize:"16px",
    fontFamily:"Roboto",
    color:"#fff",
    fontWeight:"500",
    paddingLeft:"15px",
    textTransform:"normal",
    paddingTop:"77px"
  },
  vehicleOpenStatusSuccess:{
    backgroundColor:"#2eb133", 
    fontSize:"16px",
    fontFamily:"Roboto",
    color:"#fff",
    fontWeight:"500",
    paddingLeft:"15px",
    textTransform:"normal",
    paddingTop:"77px"
  },
  vehicleOpenStatusError:{
    backgroundColor:"#d0021b", 
    fontSize:"16px",
    fontFamily:"Roboto",
    color:"#fff",
    fontWeight:"500",
    paddingLeft:"15px",
    textTransform:"normal",
    paddingTop:"77px"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  }, 
  title:{
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "500"
  },
  container: { 
   // position: "relative",
    paddingTop: "0vh", 
    paddingBottom: "10vh", 
    position: "fixed",
    overflow: "scroll",
    width: "100%",
    height: "100%",
    textAlign: "center", 
  },
  uploadBtn:{
    paddingTop: "20px",
    paddingBottom:"30px",
    "& label > span":{
      borderRadius: "100%",
      padding: "35px", 
      backgroundColor: "#d8d8d8", 
      "& svg":{
        height:"40px",
        width:"40px",
        color:"#fff"
      }
    }
  },
  profileImg:{
    borderRadius: "100%",
    height:"110px",
    width:"110px"
  },
  mainImage:{
    objectFit:"contain",
    height:"122px",
    zIndex:"1000" ,
  },
  thumbImage:{
    objectFit:"contain",
    height:"65px",
    zIndex:"1000" ,
    marginLeft:"10px",
    cursor:'pointer',
    transition:"0.2s"
  },
  thumbFocus:{
    borderBottomColor:"#ec1c25",
    borderBottomWidth:"4px",
    borderBottomStyle:"solid"
  },
  mainImageBlur:{
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "inherit",
    position: "absolute",
    left: "0px",
    right: "0px",
    zIndex: "0",
    filter: "opacity(0.3)",
  },
  mainImageContainer:{
    position:"relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: "450px",
  },
  profilePicTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  },
   section:{
    paddingTop: "10px", 
    "& *":{
      fontFamily: "Roboto"
    }
   },
   imageSlider:{ 
    paddingLeft: "0px",
    paddingRight: "0px",
    textAlign:"center",
    "& img":{
      width:"100%", 
      minHeight:"100%"
    } 
   },
   mobileStepper:{ 
      top: "-30px",
      justifyContent: "center",
      zIndex: "999",
      position: "relative",
      backgroundColor: "transparent",
      textAlign: "center",
      display: "inline-block"
   },
   Dot:{
    backgroundColor:" rgba(255, 255, 255, 0.5)",
    height:"8px",
    width:"8px", 
    marginLeft:"2px",
    marginRight:"2px"
   },
   activeDot:{
    backgroundColor:"#fff"
   },
   form:{

   },
   textField:{
    flex:1,
    display:"flex",
    marginTop:"10px",
    marginBottom:"10px",
    backgroundColor:"#ededed",  
    borderRadius:"5px",
  "& *, & *:hover":{ 
      backgroundColor:"#ededed",  
      borderRadius:"5px",
  },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        display: "none"
      },
    "& input":{
      paddingTop:"18px",
      paddingRight:"15px",
      paddingBottom:"18px",
      paddingLeft:"15px",
      color:"#666",
      fontSize:"16px", 
      fontWeight:"normal",

      "&::-webkit-input-placeholder":{
        color:"#666",
        opacity:1
      }
    } 
   },
   forgotpassword:{
      display:"flex",
      alignItem:"left",
      fontSize:"12px", 
      paddingLeft:"15px",
      color:"#666",
      textDecoration:"none"
   },
   button:{
    backgroundColor:"#424649", 
    fontWeight: "500",
    color:"#fff",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:"0",
    marginTop:"20px",
    width:"100%",
     "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   buttonWhite:{
    backgroundColor:"#fff", 
    fontWeight: "500",
    color:"#424649",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:"0",
    marginTop:"20px",
    width:"100%",
     "&:hover":{
         backgroundColor:"#fff", 
      }
   },
   bidAccept:{
    paddingTop:"20px",
    paddingBottom:"20px"
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
   	paddingTop:"30px",
   	"& a":{ 
   		color:"#161617",
   		textDecoration:"none",
   		textTransform:"uppercase",
   		fontSize:"16px",
   		fontFamily:"Roboto",
   		fontWeight:"normal",
   		letterSpacing:"0.5px"
   	}
   },
   progress:{
    marginTop: "20px"
   },
   field:{
    "& label":{
      textAlign: "left",
      display: "block",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "15px",
      marginTop: "-5px",
      color: "#444"
    }
   }, 
   vehicleDetails:{
    marginTop:"10px",
    paddingLeft: "20px",
    paddingRight: "20px",
   },
  listTitle:{
    color:" rgba(0, 0, 0, 0.87)",
    fontSize: "24px", 
    paddingTop:"10px", 
    paddingBottom:"5px",
    fontFamily:"Roboto",
    fontWeight:"normal"
  },
  listKm:{
    fontSize:"16px",
    fontFamily:"Roboto",
    fontWeight:"normal",
    marginBottom:"5px",
    color:"rgba(0, 0, 0, 0.6)",
    "& :visited":{ 
      color:"rgba(0, 0, 0, 0.6)",
    }
  },
  winningBid:{
    fontSize:"14px",
    fontWeight:"500",
    fontFamily:"Roboto",
    color:"#ec1d25",
    paddingTop:"10px",
    paddingBottom:"10px"
  },
  BiddingCreditAvailable:{
    fontSize:"14px",
    fontWeight:"500",
    fontFamily:"Roboto",
    color:"#424649",
    paddingTop:"10px",
    paddingBottom:"10px"
  },
  bidPrice:{
    fontSize:"20px",
    color:"#424649",
    fontWeight:"500"
  },
  bidderDetails:{
    backgroundColor:"#f1f2f2",
    padding:"20px",
    marginTop:"20px",
    "& svg":{
      color:"rgba(0, 0, 0, 0.54)",
      marginTop:"15px"
    }
  },
  expandCollapse:{ 
    marginTop:"20px",
  },
  bidderSection:{
    color:"rgba(0, 0, 0, 0.54)",
    textTransform:"uppercase",
    fontSize:"14px",
    fontWeight:"500",
    flexgrow:"1",
    marginBottom:"5px"
  },
  bidderSectionBid:{
    color:"rgba(0, 0, 0, 0.54)",
    textTransform:"normal",
    fontSize:"14px",
    fontWeight:"500",
    flexgrow:"1",
    marginBottom:"5px"
  },
  bidderSectionBidRed:{
    color:"#ec1d25",
    textTransform:"normal",
    fontSize:"14px",
    fontWeight:"500",
    flexgrow:"1",
    marginBottom:"5px"
  },
  bidderName:{
    color:"#424649", 
    flexgrow:"1",
    lineHeight:"20px",
    fontSize:"16px",
    fontWeight:"normal"
  },
  panelSection:{
    flexDirection:"column"
  },
  bidderMargin:{
    marginBottom:"15px"
  },
  expanHeading:{
    color:"rgba(0, 0, 0, 0.38)",
    fontSize:"16px"
  },
  listCurrentBid:{
    color:"#eea122",
    fontSize:"14px",
    textDecoration:"none",  
    fontWeight:"bold",
    fontFamily:"Roboto",  
    letterSpacing: "0.3px",
    lineHeight:"20px",
  },
  nzNewStyle: {
    zIndex:10000,
    position: "absolute",
    height: "25px",
    bottom: "4px",
    right: "0",
    display: "flex",
    alignItems: "center",
    padding: ".3rem .5rem",
    "& > img": {
      height: "15px",
      width: "auto"
    },
    "& > span":{
      color: '#353535',
      paddingLeft: "10px",
      fontSize: "13px"
    }
  },
  loadingScreen:{
     paddingTop:"130px",
     paddingLeft: "0px",
     backgroundColor: "#fff",
     paddingRight: "0px",
     textAlign:"center",
     "& *":{
       fontFamily: "Roboto"
     },
  }
};

export default vehicleDetailsStyles;
