const sellAVehicleStyles = {
  AppBar:{
    backgroundColor: "#fff",
    paddingTop:"20px", 
    boxShadow:"none"

  },  
  menuButton: {
    color:"#767676",
    marginLeft: -12, 
  }, 
  container: { 
    position: "relative",
    paddingTop: "5vh", 
    paddingBottom: "10vh", 
    textAlign:"center",
  }, 
	header:{
		paddingTop:"5vh", 
    flexGrow: 1,
    "& h3":{
      fontSize: "16px",
      fontFamily: "Roboto",
      fontWeight: "normal", 
      color: "#000000",
      letterSpacing: "0.5px"
    }  
	},  
  pageTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  },
   section:{
   	paddingTop:"100px",
    paddingLeft: "20px",
    paddingRight: "20px",
    "& *":{
    	fontFamily: "Roboto"
    }
   }, 
   textField:{
   	flex:1,
   	display:"flex",
   	marginTop:"10px",
   	marginBottom:"10px",
   	backgroundColor:"#ededed",  
   	borderRadius:"5px",
	"& *, & *:hover":{ 
	   	backgroundColor:"#ededed",  
	   	borderRadius:"5px",
	},
   	"& *:after, & *:before, & *:hover:after, & *:hover:before":{
	   		display: "none"
	   	},
   	"& input":{
   		paddingTop:"18px",
   		paddingRight:"15px",
   		paddingBottom:"18px",
   		paddingLeft:"15px",
	   	color:"#666",
	   	fontSize:"16px", 
	   	fontWeight:"normal",

   		"&::-webkit-input-placeholder":{
   			color:"#666",
   			opacity:1
   		}
   	} 
   }, 
   button:{
   	backgroundColor:"#424649", 
   	fontWeight: "500",
   	color:"#fff",
   	letterSpacing:"1.3px",
   	paddingLeft:"20px",
   	paddingRight:"20px",
   	borderRadius:"0",
   	marginTop:"30px",
   	 "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   radioGroup:{
    flex: 1,
    justifyContent:"start",
    borderRadius: "5px",
    backgroundColor: "#ededed",
    display:"flex", 
    flexDirection: "row",
    paddingTop:"8px",
    paddingRight:"15px",
    paddingBottom:"8px",
    paddingLeft:"15px",
    marginBottom:"10px",
    color:"#666",
    fontSize:"16px", 
    fontWeight:"normal",
    "& label":{
      flex: 9,
      color:"#666",
      display: "flex !important",
      marginBottom:"0 !important",
      paddingLeft:"0 !important"
    },
    "& label:first-child":{
      flex:3
    }
   },
   selectBox:{
    flex:1,
    display:"flex",
    marginTop:"10px",
    marginBottom:"10px",
    backgroundColor:"#ededed",  
    borderRadius:"5px",
    paddingTop:"10px",
    paddingRight:"15px",
    paddingBottom:"10px",
    textAlign:"left",
    paddingLeft:"15px",
    color:"#666",
    fontSize:"16px", 
    fontWeight:"normal",
    "& *, & *:hover":{ 
      backgroundColor:"#ededed",  
      borderRadius:"5px",
    },
    "&:after, &:before, &:hover:after, &:hover:before":{
        display: "none"
      }, 
    "&::-webkit-input-placeholder":{
      color:"#666",
      opacity:1
    } 
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   uploadBtn:{
     display:"flex",
     flexDirection:"column",
    paddingTop: "20px",
    paddingBottom:"30px",
    "& label > span":{ 
      padding: "10px", 
      backgroundColor:"#424649 !important", 
      fontWeight: "500",
      color:"#fff",
      letterSpacing:"1.3px",
      paddingLeft:"20px",
      paddingRight:"20px",
      "& :hover":{ 
        backgroundColor:"#424649 !important"
      },
      "& :visited":{ 
        backgroundColor:"#424649 !important"
      },
      "& svg":{
        marginLeft:"10px"
      }
    }
  },
  maximumLabel: {
    fontSize:"14px",
    marginTop:"10px",
  },
   field:{
    "& label":{
      textAlign: "left",
      display: "block",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "15px",
      marginTop: "-5px",
      color: "#444"
    }
   },
    vehicleDetails:{
    marginTop:"20px", 
    marginBottom:"20px", 
    textAlign:"left"
   },
  listTitle:{
    color:" rgba(0, 0, 0, 0.87)",
    fontSize: "24px", 
    paddingTop:"10px", 
    paddingBottom:"5px",
    fontFamily:"Roboto",
    fontWeight:"normal"
  },
  listKm:{
    fontSize:"16px",
    fontFamily:"Roboto",
    fontWeight:"normal",
    marginBottom:"5px",
    color:"rgba(0, 0, 0, 0.6)",
    "& :visited":{ 
      color:"rgba(0, 0, 0, 0.6)",
    }
  },
  titleBar:{
    background: "rgba(66, 70, 73,0.8)",
    height:"30px",  
    "& div, & svg":{
      color:"#fff !important"
    }
  },
  mainImage:{
    objectFit:"contain",
    height:"100%",
  },
  mainImageBlur:{
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "inherit",
    position: "absolute",
    left: "0px",
    right: "0px",
    zIndex: "-1",
    filter: "opacity(0.3)",
  },
  mainImageContainer:{
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: "inherit",
  },
  uploadProgress: {
    position: "absolute",
    top:"0",
    height:"100%",
    width:"100%",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    "& svg": {color:"#fff"},
    "& > div":{
      height: "40px",
      width: "40px",
      backgroundColor:"rgba(0,0,0,0.7)",
      borderRadius:"50%",
      display:"flex",
      justifyContent:"center",
      alignItems:"center",
    }
  },
  vehicleDivider:{
    height: '2px',    
    backgroundColor: '#666',
    marginTop: '40px',
    marginBottom: '40px',
    maxWidth: '50%',
    marginLeft:'auto',
    marginRight:'auto'
  },
  MarginBottom:{
    marginBottom: '30px'
  }
};

export default sellAVehicleStyles;
