
const adminStyles = theme =>( { 

  adminWrapper:{
    flexGrow: 1, 
    zIndex: 1, 
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar:{
		backgroundColor: "#ec1d25",
	// paddingTop:"20px",  
  },
  appBartitle:{
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "500",
    color:"#ffffff"
  },
  sellerContainer:{
    display:'flex',
    justifyContent:'space-between',
    marginTop:"15px"
  },
  detailBottomContainer:{
    paddingLeft:"20%",
    paddingRight:"20%"
  },
  sectionAdmin:{
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    paddingTop: "130px", 
  }, 
  widget:{
    "& a":{
      textDecoration:"none",

    }
  },
  gridList:{
    paddingLeft:"15px"
  },
  bidContainer:{
    height:"100vh",
    width:"85%",
    overflow:'overlay',
    marginTop:"25px"
  },
  profileImg:{
    borderRadius: "100%",
    height:"110px",
    width:"110px"
  },
  bidderSectionBid:{
    color:"rgba(0, 0, 0, 0.54)",
    textTransform:"normal",
    fontSize:"14px",
    fontWeight:"500",
    flexgrow:"1",
    marginBottom:"5px"
  },
  bidderSectionBidRed:{
    color:"#ec1d25",
    textTransform:"normal",
    fontSize:"14px",
    fontWeight:"500",
    flexgrow:"1",
    marginBottom:"5px"
  },
  bidderMargin:{
    marginBottom:"15px"
  },
  card:{
    backgroundColor:"#babdbf",
    color:"#424649",
    padding:"8%",
    textAlign:"center",    
    webkitTransitionDuration: "0.3s",
    transitionDuration: "0.3s",
    webkitTransitionProperty: "color, background-color",
    transitionProperty: "color, background-color",
    "&:hover":{
      backgroundColor:"#cdd5da",
    },
    "& svg":{
      height:"100px",
      color:"#424649",
      width:"100px"
    },
    "& h2":{
      fontSize:"30px",
      color:"#424649",
      fontWeight:"500"
    }
  },
  formControl: { 
    minWidth: 270,
  },
  textField:{
    flex:1,
    display:"flex",
    marginTop:"10px",
    marginBottom:"20px",
    backgroundColor:"#ededed",  
    borderRadius:"5px",
    "& *, & *:hover":{ 
        backgroundColor:"#ededed",  
        borderRadius:"5px",
    },
      
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        display: "none"
      },
    "& input":{
      paddingTop:"18px",
      paddingRight:"15px",
      paddingBottom:"18px",
      paddingLeft:"15px",
      color:"#666",
      fontSize:"16px", 
      fontWeight:"normal",

      "&::-webkit-input-placeholder":{
        color:"#666",
        opacity:1
      }
    } 
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34", 
       marginTop: "-10px"
   },
   button:{
    backgroundColor:"#424649", 
    fontWeight: "500",
    color:"#fff",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:"0",
    marginTop:"30px",
     "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   SectionTitle:{
    color: "#424649",
    fontWeight: "normal",
    fontSize: "18px",
    fontFamily:"Roboto",
    letterSpacing: "0.3px"
  },field:{
    "& label":{
      textAlign: "left",
    fontFamily:"Roboto",
      display: "block",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "15px",
      marginTop: "-10px",
      color: "#444"
    }
   },
   listTitle:{
    color:" rgba(0, 0, 0, 0.87)",
    fontSize: "24px", 
    paddingTop:"10px", 
    paddingBottom:"5px",
    fontFamily:"Roboto",
    fontWeight:"normal"
  },
  listKm:{
    fontSize:"16px",
    fontFamily:"Roboto",
    fontWeight:"normal",
    marginBottom:"5px",
    color:"rgba(0, 0, 0, 0.6)",
    "& :visited":{ 
      color:"rgba(0, 0, 0, 0.6)",
    }
  },
  detailSection:{
    color:"rgba(0, 0, 0, 0.54)",
    textTransform:"uppercase",
    fontSize:"14px",
    fontWeight:"600",
    flexgrow:"1",
    marginBottom:"5px"
  },
  detailName:{
    color:"#424649", 
    flexgrow:"1",
    lineHeight:"20px",
    fontSize:"16px",
    fontWeight:"normal"
  },
  winningBid:{
    fontSize:"14px",
    fontWeight:"500",
    fontFamily:"Roboto",
    color:"#ec1d25",
    paddingTop:"10px",
    paddingBottom:"10px"
  },
  detailHeading:{
    color:"rgba(0, 0, 0, 0.38)",
    fontSize:"16px"
  },
  bidPrice:{
    fontSize:"20px",
    color:"#424649",
    fontWeight:"500"
  },
   notiButton:{
     "& *":{
     color:"#424649"
    }
   },
   addButton:{
    marginBottom: '30px',
    textAlign: 'right',
    "& button":{
      backgroundColor:"#f50057"
    }
   },
   clickable:{
     cursor:"pointer",
     "&:hover":{
        backgroundColor:"rgba(0, 0, 0, 0.08)"
     }
   }
});
 
export default adminStyles;