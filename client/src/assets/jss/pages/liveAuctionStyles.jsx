const liveAuctionStyles = {
  swipe:{
    position: "fixed",
    overflow: "scroll",
    width: "100%",
    height: "100%",
    textAlign: "center",
  },
  container: { 
    position: "relative",
    paddingTop: "0vh", 
    paddingBottom: "10vh", 
    textAlign:"center",
  },
  uploadBtn:{
    paddingTop: "20px",
    paddingBottom:"30px",
    "& label > span":{
      borderRadius: "100%",
      padding: "35px", 
      backgroundColor: "#d8d8d8", 
      "& svg":{
        height:"40px",
        width:"40px",
        color:"#fff"
      }
    }
  },
  profileImg:{
    borderRadius: "100%",
    height:"110px",
    width:"110px"
  },
  profilePicTitle:{
    color: "#424649",
    fontWeight: "500",
    fontSize: "20px",
    letterSpacing: "0.3px"
  },
   section:{
    paddingTop:"130px",
    paddingLeft: "0px",
    backgroundColor: "#fafafa",
    paddingRight: "0px",
    "& *":{
      fontFamily: "Roboto"
    },

   },
   loadingScreen:{
      paddingTop:"130px",
      paddingLeft: "0px",
      backgroundColor: "#fff",
      paddingRight: "0px",
      "& *":{
        fontFamily: "Roboto"
      },
   },
   filterBar:{ 
      "& svg":{
        color: "#737373"
      }
   },
   ActiveFilter:{
    "& svg":{
        color: "#ec3c34"
      } 
   },
   form:{

   },
   textField:{
    flex:1,
    display:"flex",  
    backgroundColor:"#ededed",  
    marginTop:0,
    borderRadius:"5px",
  "& *, & *:hover":{ 
      backgroundColor:"#ededed",  
      borderRadius:"5px",
  },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        display: "none"
      },
    "& input":{
      paddingTop:"18px",
      paddingRight:"15px",
      paddingBottom:"18px",
      paddingLeft:"15px",
      color:"#666",
      fontSize:"16px", 
      fontWeight:"normal",

      "&::-webkit-input-placeholder":{
        color:"#666",
        opacity:1
      }
    } 
   }, 
   button:{
    backgroundColor:"#424649", 
    fontWeight: "500",
    color:"#fff",
    letterSpacing:"1.3px",
    paddingLeft:"20px",
    paddingRight:"20px",
    marginTop:"7px",
    float:"right",
    width:"90%",
    flex:"1",
    display:"flex",
    borderRadius:"0", 
     "&:hover":{
         backgroundColor:"#424649", 
      }
   },
   textError:{
       borderWidth: "1px",
       borderColor: "#eb3b34",
       borderStyle: "solid"
   },
   errorMsg:{
       display: "flex",
       color: "#eb3b34",  
   },
   footer:{
   	paddingTop:"30px",
   	"& a":{ 
   		color:"#161617",
   		textDecoration:"none",
   		textTransform:"uppercase",
   		fontSize:"16px",
   		fontFamily:"Roboto",
   		fontWeight:"normal",
   		letterSpacing:"0.5px"
   	}
   },
   progress:{
    marginTop: "20px"
   },
   field:{
    "& label":{
      textAlign: "left",
      display: "block",
      padding: "0",
      marginBottom: "20px",
      paddingLeft: "15px",
      marginTop: "-5px",
      color: "#444"
    }
   },
   grow: {
    flexGrow: 1,
  }, 
  listItems:{
    backgroundColor:"#fff",
    "& img":{
      maxWidth: "100%",
      // maxHeight:"75px"
    }
  },
  listLink:{
    textDecoration:"none"
  },
  listCurrentBid:{ 
    color:"#eea122",
    fontSize:"14px",
    textDecoration:"none",  
    fontWeight:"bold",
    fontFamily:"Roboto",  
    letterSpacing: "0.3px",
    lineHeight:"20px",
    "& :visited":{ 
      color:"#eea122",
    }
  },
  listTitle:{
  color:"rgba(0, 0, 0, 0.87)",
  fontSize: "16px",
  lineHeight:"20px",
  paddingTop:"10px", 
  paddingBottom:"5px",
    fontFamily:"Roboto",
    fontWeight:"normal",
  "& :visited":{ 
      color:"rgba(0, 0, 0, 0.87)",
    }
  },
  mainImage:{
    objectFit:"contain",
    height:"100%",
    zIndex:"1000" ,
  },
  mainImageBlur:{
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: "inherit",
    position: "absolute",
    left: "0px",
    right: "0px",
    zIndex: "0",
    filter: "opacity(0.3)",
  },
  mainImageContainer:{
    position:"relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: 149,
  },
  listKm:{
    fontSize:"14px",
    fontFamily:"Roboto",
    fontWeight:"normal",
    marginBottom:"5px",
    color:"rgba(0, 0, 0, 0.6)",
    "& :visited":{ 
      color:"rgba(0, 0, 0, 0.6)",
    }
  },
  listEnd:{
    color:"#ec1d25",
    paddingTop:"10px",
    paddingBottom:"10px"
  },
  gridItems:
  {
    "& img":{
      maxWidth:"100%",
      maxHeight:"95px"
    }
  },
  modelField:
  {
    width:"100%",
    "& label":{
      color:"rgba(0, 0, 0, 0.54)",
      fontFamily:"Roboto",
      fontWeight:"500",
      display:"flex",
      flex:1
    }
    
  },
  selectBoxModel:{
    flex:1,
    display:"flex",
    marginTop:"0",
    marginBottom:"25px", 
    borderRadius:"5px",
    paddingTop:"5px",
    paddingRight:"0",
    paddingBottom:"5px",
    textAlign:"left",
    paddingLeft:"0",
    color:"#424649", 
    backgroundColor:"transparent !important",
    fontSize:"14px", 
    fontWeight:"500",
    "& *, & *:hover, &:hover, &:active":{ 
      backgroundColor:"transparent !important", 
    }, 
    "&:before":{
      borderBottom:"1px solid rgba(0, 0, 0, 0.12)"
    },
    "&::-webkit-input-placeholder":{
      color:"#424649",
      opacity:1
    } 
  },
  textFieldModel:{
    flex:1,
    display:"flex",  
    backgroundColor:"transparent",  
    marginTop:0, 
    marginBottom:"25px", 
    "& *, & *:hover":{ 
      backgroundColor:"transparent", 
    },"& >*:before":{
      borderBottom:"1px solid rgba(0, 0, 0, 0.12)"
    },
    "& *:after, & *:before, & *:hover:after, & *:hover:before":{
        
    },
    "& input":{
      paddingTop:"10px",
      paddingRight:"0px",
      paddingBottom:"10px",
      paddingLeft:"0px",
      color:"#424649",
      fontSize:"14px", 
      fontWeight:"500", 
      "&::-webkit-input-placeholder":{
        color:"#424649",
        opacity:1
      }
    } 
  },
  linkButton:{
    color:"#ec1d25", 
    fontSize:"14px", 
    fontWeight:"500",
  },
  linkButtonCancel:{
    
    color:"rgba(0, 0, 0, 0.54)", 
    fontSize:"14px", 
    fontWeight:"500",
  },
  statusMessage:{
    paddingBottom:"10px",
    color:"rgba(0, 0, 0, 0.6)",
    fontSize:"14px"
  }
};

export default liveAuctionStyles;
