const appBarStyles = (theme , drawerWidth=304) => ({ 
   
	AppBar:{
		backgroundColor: "#ec1d25",
  paddingTop:"20px",  
    position: 'fixed', 
        top: 0, 
        left: 0,
        width: '100%',
      zIndex:100
	}, 
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  grow: {
    flexGrow: 1,
  }, 
  title:{
    fontFamily: "Roboto",
    fontSize: "20px",
    fontWeight: "500"
  },
  badge:{
    "& span":{
    backgroundColor: "#fab611"
    }
  },
  Tabs:{
    "& button":{
      color:"#fff"
    },  
    
  }, 
  scrollButtons:{
      color:"#fff",
      fontSize:"14px",
      "& [aria-selected='true']":{
        color:"#fff"
      },
      "& [aria-selected='false']":{
        color:"rgba(255, 255, 255, 0.7)"
      },
      "& span":{ 
        fontSize:"14px",
      }
  }, 
  Indicator:{
      backgroundColor:"#fff"
   },
   AppBarAdmin:{
    backgroundColor: "#424649",
    paddingTop:"20px",  
    paddingBottom:"20px",
    position: 'fixed',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    }, 
  },  
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  }
});

export default appBarStyles;
