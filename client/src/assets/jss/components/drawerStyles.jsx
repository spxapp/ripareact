

const drawerStyles = (theme, drawerWidth=304) =>({ 

  List: {
    width: drawerWidth 
  },avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 64,
    height: 64,
  },
  drawerHeader:{
  	backgroundColor:"#ec1d25",
  	paddingTop: "40px",
  	paddingLeft:"15px", 
  	paddingRight:"15px",
  	paddingBottom:"20px"
  },
  email:{
  	color:"#fff",
  	fontSize:"16px",
  	fontFamily:"Roboto", 
  	marginTop: 20,
  	paddingBottom: 10,
  	display:"block"
  },
  drawerLink:{
    textDecoration:"none"
  },
  drawerAdmin:{
     width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
    [theme.breakpoints.down('sm')]: {
      display:"none"
    }
  },
  drawerHeaderAdmin:{
    backgroundColor:"#fff",
    paddingTop: "40px",
    paddingLeft:"15px", 
    paddingRight:"15px",
    paddingBottom:"20px"
  }, 
  DrawerWrapper:{
    width:drawerWidth,
    "& > div":
    {
      boxShadow:"1px 0 4px -2px #000"
    }

  }, 

  menuItemActive:{
    backgroundColor: "rgba(0, 0, 0, 0.08)"
  }

});

export default drawerStyles;
