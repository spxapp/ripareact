import Login from "../pages/Login";
import Register from "../pages/Register";
import ForgotPassword from "../pages/ForgotPassword";
import SetupProfile from "../pages/SetupProfile";
import SetupProfile2 from "../pages/SetupProfile2";
import SetupProfile3 from "../pages/SetupProfile3";
import ChangeCard from "../pages/ChangeCard"
import MyAccount from "../pages/MyAccount";
import LiveAuctions from "../pages/LiveAuctions";
import Settings from "../pages/Settings";
import VehicleDetails from "../pages/VehicleDetails";
import SellAVehicle from "../pages/SellAVehicle";
import SellAVehicle2 from "../pages/SellAVehicle2";
import SellAVehicle3 from "../pages/SellAVehicle3";
import SellAVehicle4 from "../pages/SellAVehicle4";
import UserDetails from "../pages/UserDetails";
import Notifications from "../pages/Notifications";
import AwaitingApproval from "../pages/AwaitingApproval"; 
import AccountSuspended from "../pages/AccountSuspended"; 
import AuctionStatus from "../pages/AuctionStatus"; 


import AdminLogin from "../pages/admin/AdminLogin";
import AdminDashboard from "../pages/admin/AdminDashboard";  
import AdminUsers from "../pages/admin/AdminUsers";   
import AdminReportUsers from "../pages/admin/AdminReportUsers";   
import AdminReports from "../pages/admin/AdminReports";    
import AdminReportsBranch from "../pages/admin/AdminReportsBranch";  
import AdminReportsUser from "../pages/admin/AdminReportsUser";  
import AdminVehicles from "../pages/admin/AdminVehicles"; 
import AdminSettings from "../pages/admin/AdminSettings";  

var indexRoutes = [

  { path: "/register", name: "Register", component: Register },
  { path: "/forgot-password", name: "Forgot Password", component: ForgotPassword },
  { path: "/setup-profile", name: "Setup Profile", component: SetupProfile },
  { path: "/setup-profile-2", name: "Setup Profile 2", component: SetupProfile2 },
  { path: "/setup-profile-3", name: "Setup Profile 3", component: SetupProfile3 },
  { path: "/change-card", name: "Change Card", component: ChangeCard },
  { path: "/login", name: "Login", component: Login },
  { path: "/my-account", name: "MyAccount", component: MyAccount }, 
  { path: "/live-auctions", name: "Live Auctions", component: LiveAuctions },
  { path: "/my-auction-entries", name: "My Auction Entries", component: LiveAuctions },
  { path: "/settings", name: "Settings", component: Settings },
  { path: "/vehicle-details", name: "Vehicle Deatails", component: VehicleDetails },
  { path: "/sell-a-vehicle", name: "Sell A Vehicle", component: SellAVehicle },
  { path: "/sell-a-vehicle-2", name: "Sell A Vehicle 2", component: SellAVehicle2 },
  { path: "/sell-a-vehicle-3", name: "Sell A Vehicle 3", component: SellAVehicle3 },
  { path: "/sell-a-vehicle-4", name: "Sell A Vehicle 4", component: SellAVehicle4 },
  { path: "/user-details/:user/:userType", name: "User Details", component: UserDetails },
  { path: "/notifications", name: "Notifications", component: Notifications },
  { path: "/awaiting-approval", name: "Awaiting Approval", component: AwaitingApproval},
  { path: "/auction-status", name: "Auction Status", component: AuctionStatus},
  { path: "/account-suspended", name: "Account Suspended", component: AccountSuspended},
  { path: "/admin/login", name: "Admin Login", component: AdminLogin }, 
  { path: "/admin/users", name: "Admin Users", component: AdminUsers }, 
  { path: "/admin/report-users", name: "Report Users", component: AdminReportUsers }, 
  { path: "/admin/reports/branch", name: "Reports Branch", component: AdminReportsBranch }, 
  { path: "/admin/reports/User", name: "Reports User", component: AdminReportsUser }, 
  { path: "/admin/reports", name: "Reports", component: AdminReports }, 
  { path: "/admin/vehicles", name: "Admin Vehicles", component: AdminVehicles }, 
  { path: "/admin/settings", name: "Admin Settings", component: AdminSettings }, 
  { path: "/admin", name: "Admin Dashboard", component: AdminDashboard },
  { path: "/", name: "Live Auctions", component: LiveAuctions }
  
];

export default indexRoutes;
