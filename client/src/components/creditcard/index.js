import React, {Component} from 'react';
import {CardNumberElement,CardExpiryElement,CardCVCElement, injectStripe} from 'react-stripe-elements';
import { withRouter } from "react-router-dom"; 
import Button from '@material-ui/core/Button';  

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  async submit(ev) {

    try{

      let {token} = await this.props.stripe.createToken({name: "Credit Card Save"}); 

      if(typeof token === 'undefined' || token === null)
      {
        this.props.onError("Invalid Card Details");

      }else{

        this.props.onSubmit(token);

      }
      
    }catch(err){
      this.props.onError("There is something wrong with the credit card, Please try another card.");
    }
    
  }

  render() {

    const { classes } = this.props;
 
    return (

      <section className={classes.section}>
        <div className={classes.uploadBtn}> 
          <p className={classes.profilePicTitle}>Add a credit card</p>
        </div> 
        <div className={classes.creditCardBox}>  
          <CardNumberElement  style={{base: {fontSize: '1.3em'}}} placeholder="Card number" /> 
        </div>  
        <div className={classes.creditCardBox}> 
          <CardExpiryElement  style={{base: {fontSize: '1.3em'}}} />
        </div>  
        <div className={classes.creditCardBox}> 
          <CardCVCElement  style={{base: {fontSize: '1.3em'}}} />
        </div> 
        <Button
          type="submit"  
          variant="contained" 
          onClick={this.submit}
          className={classes.button}>
                  Save
        </Button>
        <span></span>
        { this.props.updateCard && <Button
          type="submit"  
          variant="contained" 
          onClick={()=>{this.props.history.goBack()}}
          className={classes.cancelbutton}>
                  Cancel
        </Button>}
      </section>
    );
  }
}

export default injectStripe(withRouter(CheckoutForm)); 