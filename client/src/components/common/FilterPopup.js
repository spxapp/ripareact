import React from "react";    
   
import Button from '@material-ui/core/Button'; 
import Grid from '@material-ui/core/Grid';  
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel'; 
import MenuItem from '@material-ui/core/MenuItem';  
import Select from '@material-ui/core/Select';

let anydata = [ 
  "Alfa Romeo", 
  "Aston Martin", 
  "Audi", 
  "Austin", 
  "Bentley", 
  "BMW", 
  "Cadillac", 
  "Chery", 
  "Chevrolet", 
  "Chrysler", 
  "Citroen", 
  "Daewoo", 
  "Daihatsu", 
  "Daimler", 
  "Dodge", 
  "DS Automobiles", 
  "Ferrari", 
  "Fiat", 
  "Ford", 
  "Foton", 
  "GMC", 
  "Geely", 
  "Great Wall", 
  "HAVAL", 
  "Holden", 
  "Honda", 
  "HSV", 
  "INFINITI", 
  "Hyundai", 
  "Hummer", 
  "Isuzu", 
  "Jaguar", 
  "Iveco", 
  "Jeep", 
  "Kia", 
  "Lamborghini", 
  "Lancia", 
  "Land Rover", 
  "LDV", 
  "Lexus", 
  "Lotus", 
  "Mahindra", 
  "Maserati", 
  "MINI", 
  "MG", 
  "Mercedes-Benz", 
  "McLaren", 
  "Mazda", 
  "Morgan", 
  "Mitsubishi", 
  "Morris", 
  "Nissan", 
  "Opel", 
  "Peugeot", 
  "Pontiac", 
  "Porsche", 
  "Ram", 
  "Renault", 
  "Riley", 
  "Rolls-Royce", 
  "Rover", 
  "Saab", 
  "SEAT", 
  "Suzuki", 
  "Subaru", 
  "Ssongyong", 
  "Smart", 
  "Skoda", 
  "Tesla", 
  "Toyota", 
  "Triumph", 
  "TVR", 
  "Vauxhall", 
  "Volkswagen", 
  "Volvo", 
  "Other"
]

let vehicleTypes = ["Convertible","Coupe","Hatchback","Sedan","Station Wagon","RV/SUV","Ute","Van","Other"];

class FilterPopup extends React.Component {
    constructor(props) {
      super();
      this.state = {
        filterData: props.filterData,
        filterError: false,
      }; 
    }	  
  
    // TODO
     componentDidMount(){
      if(this.props.filterData.Make.length===76){
        this.props.filterData.Make=[]
      }
      if(this.props.filterData.Year===1950 && this.props.filterData.YearEnd===2019)
      {
        this.props.filterData.Year="";
        this.props.filterData.YearEnd="";
      }
      if(Array.isArray(this.props.filterData.vehicleType) && this.props.filterData.vehicleType.length===9)
      {
        this.props.filterData.vehicleType='';
      }
      
      if(this.props.filterData.Mileage)
      {
        this.props.filterData.Mileage=this.props.filterData.Mileage.start+'-'+this.props.filterData.Mileage.end;
      }
      if(this.props.filterData.Mileage==="0-1000000")
      {
        this.props.filterData.Mileage='';
      }
      this.setState({ filterData: this.props.filterData });
     }

    componentWillReceiveProps(nextProps) {
      if (nextProps.filterData !== this.props.filterData) {
        if(nextProps.filterData.Make.length===76){
          nextProps.filterData.Make=[]
        }
        if(nextProps.filterData.Year===1950 && nextProps.filterData.YearEnd===2019)
        {
          nextProps.filterData.Year="";
          nextProps.filterData.YearEnd="";
        }
        if(Array.isArray(nextProps.filterData.vehicleType) && nextProps.filterData.vehicleType.length===9)
        {
          nextProps.filterData.vehicleType='';
        }
        
        if(nextProps.filterData.Mileage)
        {
          nextProps.filterData.Mileage=nextProps.filterData.Mileage.start+'-'+nextProps.filterData.Mileage.end;
        }
        if(nextProps.filterData.Mileage==="0-1000000")
        {
          nextProps.filterData.Mileage='';
        }
        this.setState({ filterData: nextProps.filterData });
      }
    } 
    
    handleReset = () => {
      var resetData = {
          vehicleType: '',
          Mileage: '',
          Make: [],
          Model: '',
          Year: '',
          YearEnd:'',
          PriceRange: '',
          nznew: '',
          edit:  this.state.filterData.edit
      };
  
      this.setState({filterData: resetData}); 
 
  
      this.props.resetFilters();
    };
  
    handleCancel = () => {
      var resetData = {
        vehicleType: "",
        Mileage: "",
        Make: [],
        Model: "",
        Year: "",
        YearEnd: "",
        PriceRange: "",
        nznew: ""
      };
      this.props.onClose(resetData);
    };
  
    handleOk = () => { 

      if(this.props.filterForSearches)
      {  
        if(!this.state.filterData.filterName || this.state.filterData.filterName==='')
        {
          this.setState({filterError: 'Filter Name is Required.'});  
          return false;
        }
        if(
          (!this.state.filterData.vehicleType || this.state.filterData.vehicleType==='')
          &&
          (!this.state.filterData.Mileage || this.state.filterData.Mileage==='')
          &&
          (!this.state.filterData.Make || this.state.filterData.Make==='' || this.state.filterData.Make.length===0)
          &&
          (!this.state.filterData.Model || this.state.filterData.Model==='')
          &&
          (!this.state.filterData.Year || this.state.filterData.Year==='')
          &&
          (!this.state.filterData.PriceRange || this.state.filterData.PriceRange==='')
          &&
          (!this.state.filterData.nznew || this.state.filterData.nznew==='') 
        )  {
          this.setState({filterError: 'Please select one of the option below.'});  
          return false;
        }

        if(this.state.filterData.YearEnd < this.state.filterData.Year)
        {
          this.setState({filterError: 'Year To should be bigger then Year From.'});  
          return false;
        }
        let tempdata = this.state.filterData;
        if(this.state.filterData.Make.length===0){
          tempdata.Make=anydata;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Year==='' && this.state.filterData.YearEnd==='')
        {
          tempdata.Year=1950;
          tempdata.YearEnd=2019;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.YearEnd==='')
        {
          tempdata.YearEnd=2019;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Year==='')
        {
          tempdata.Year=1950;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Mileage==='')
        {
          tempdata.Mileage={
            start:0,
            end:1000000
          };
          this.setState({filterData:tempdata})
        }
        else{

          let mileage = tempdata.Mileage.split('-')
          tempdata.Mileage={
                  start: parseInt(mileage[0],10),
                  end:parseInt(mileage[1],10)
            }
            this.setState({filterData:tempdata})
        }

      if(this.state.filterData.vehicleType==='')
      {
        tempdata.vehicleType=vehicleTypes
        this.setState({filterData:tempdata})
      }

        this.setState({filterError: false});
        this.props.onClose(this.state.filterData);

      } else{

        let tempdata = this.state.filterData;
        if(this.state.filterData.Make.length===0){
          tempdata.Make=anydata;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Year==='' && this.state.filterData.YearEnd==='')
        {
          tempdata.Year=1950;
          tempdata.YearEnd=2019;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.YearEnd==='')
        {
          tempdata.YearEnd=2019;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Year==='')
        {
          tempdata.Year=1950;
          this.setState({filterData:tempdata})
        }
        if(this.state.filterData.Mileage==='')
        {
          tempdata.Mileage={
            start:0,
            end:1000000
          };
          this.setState({filterData:tempdata})
        }
        else{
          if(typeof tempdata.Mileage === 'string')
          {
            let mileage = tempdata.Mileage.split('-')
            tempdata.Mileage={
                    start: parseInt(mileage[0],10),
                    end:parseInt(mileage[1],10)
              }
              this.setState({filterData:tempdata})
          }
        }

      if(this.state.filterData.vehicleType==='')
      {
        tempdata.vehicleType=vehicleTypes
        this.setState({filterData:tempdata})
      }
        this.props.onClose(this.state.filterData); 
      }

      this.props.filterVehicles();  
      

    };

    handleUpdate = () => { 
 
        if(!this.state.filterData.filterName || this.state.filterData.filterName==='')
        {
          this.setState({filterError: 'Filter Name is Required.'});  
          return false;
        }
        if(
          (!this.state.filterData.vehicleType || this.state.filterData.vehicleType==='')
          &&
          (!this.state.filterData.Mileage || this.state.filterData.Mileage==='')
          &&
          (!this.state.filterData.Make || this.state.filterData.Make==='' || this.state.filterData.Make.length===0)
          &&
          (!this.state.filterData.Model || this.state.filterData.Model==='')
          &&
          (!this.state.filterData.Year || this.state.filterData.Year==='')
          &&
          (!this.state.filterData.PriceRange || this.state.filterData.PriceRange==='')
          &&
          (!this.state.filterData.nznew || this.state.filterData.nznew==='') 
        )  {
          this.setState({filterError: 'Please select one of the option below.'});  
          return false;
        }

        if(this.state.filterData.YearEnd < this.state.filterData.Year)
        {
          this.setState({filterError: 'Year To should be bigger then Year From.'});  
          return false;
        }

      let tempdata = this.state.filterData;

      if(this.state.filterData.Make.length===0)
      {
        tempdata.Make=anydata;
        this.setState({filterData:tempdata})
      }

      if(this.state.filterData.Year==='' && this.state.filterData.YearEnd==='')
      {
        tempdata.Year=1950;
        tempdata.YearEnd=2019;
        this.setState({filterData:tempdata})
      }
      

      if(this.state.filterData.Mileage==='')
      {
        tempdata.Mileage={
          start:0,
          end:1000000
        };
        this.setState({filterData:tempdata})
      }
      else{
        let mileage = tempdata.Mileage.split('-')
        tempdata.Mileage={
                start: parseInt(mileage[0],10),
                end:parseInt(mileage[1],10)
          }
          this.setState({filterData:tempdata})
      }

      if(this.state.filterData.vehicleType==='')
      {
        tempdata.vehicleType=vehicleTypes
        this.setState({filterData:tempdata})
      }

      
     

      this.setState({filterError: false});
      this.props.onClose(this.state.filterData);    

      this.props.filterUpdateVehicles();  
      

    };
  
    handleChange = (e) => {
         
        let filterData = this.state.filterData;
        if(e.target.name==="Make"){
          if((e.target.value.indexOf("")>0))
          {
            e.target.value=[]
          }
          else if(e.target.value.indexOf("")===0){
            e.target.value.splice(0,1)
          }
        }
        filterData[e.target.name] = e.target.value;
        this.setState({ filterData }); 
  
    };

    getYears = () => {
      
      var data = [];
      for(let i=1950;i<=new Date().getFullYear();i++)
      {
        data.push(i);
      }

      return data;
    }

    makeValue = (Make) =>{
      if( Make instanceof Array)
      {
        if(Make.length===0)
        {
          return [""];
        }
        return Make;
      }
      else{
        return [Make];
      }
    }
  
    render() {

     

      return (
        <Dialog 
          fullWidth={true} 
          open={this.props.open}
          style={{
            position: 'absolute', 
                width: '100%',
          }}
          aria-labelledby="auction-filter" 
        > 
          <DialogContent>

          { this.state.filterError &&

          (
              <Grid className={this.props.classes.filterError}>{this.state.filterError}</Grid>
          )

          }  


          { this.props.filterForSearches && 

            (

            <Grid className={this.props.classes.modelField}>

                <label htmlFor="filterName">Filter Name</label>
                <TextField
                  id="filterName"
                  placeholder="Enter Filter Name"
                  className={this.props.classes.textFieldModel}
                  type="text"
                  name="filterName" 
                  margin="normal"
                  variant="filled" 
                  value={this.state.filterData.filterName}
                  onChange={this.handleChange}
                />

            </Grid>

            ) 

            }
   
  
             <Grid className={this.props.classes.modelField}>
  
             <label htmlFor="transmission">Vehicle Type</label>
              <Select
                  value={this.state.filterData.vehicleType}
                  onChange={this.handleChange}
                  name="vehicleType"
                  displayEmpty
                  className={this.props.classes.selectBoxModel}
                >
                  <MenuItem value="">
                    Any
                  </MenuItem>
                  <MenuItem value="Convertible">Convertible</MenuItem>
                  <MenuItem value="Coupe">Coupe</MenuItem> 
                  <MenuItem value="Hatchback">Hatchback</MenuItem> 
                  <MenuItem value="Sedan">Sedan</MenuItem> 
                  <MenuItem value="Station Wagon">Station Wagon</MenuItem> 
                  <MenuItem value="RV/SUV">RV/SUV</MenuItem> 
                  <MenuItem value="Ute">Ute</MenuItem> 
                  <MenuItem value="Van">Van</MenuItem> 
                  <MenuItem value="Other">Other</MenuItem>   
              </Select> 
  
              </Grid> 
  
              <Grid className={this.props.classes.modelField}>
  
                     <label htmlFor="Mileage">Mileage(km)</label>
  
                     <Select
                      value={this.state.filterData.Mileage}
                      onChange={this.handleChange}
                      name="Mileage"
                      displayEmpty
                      className={this.props.classes.selectBoxModel}
                    >
                      <MenuItem value="">
                        Any
                      </MenuItem> 
                      <MenuItem value="0-10000">0 - 10k</MenuItem>
                      <MenuItem value="10000-20000">10 - 20k</MenuItem> 
                      <MenuItem value="20000-30000">20 - 30k</MenuItem> 
                      <MenuItem value="30000-50000">30 - 50k</MenuItem> 
                      <MenuItem value="50000-100000">50 - 100k</MenuItem> 
                      <MenuItem value="100000-1000000">100k+</MenuItem> 
                  </Select>
               
  
             </Grid>
  
              <Grid className={this.props.classes.modelField}>
  
             <label htmlFor="Make">Make</label>
              <Select
                  multiple
                  value={ this.makeValue(this.state.filterData.Make)}
                  onChange={this.handleChange} 
                  name="Make"
                  displayEmpty
                  className={this.props.classes.selectBoxModel} 
                >
                  <MenuItem value="" >
                    Any
                  </MenuItem> 
                  <MenuItem value="Alfa Romeo">Alfa Romeo</MenuItem>
                  <MenuItem value="Aston Martin">Aston Martin</MenuItem> 
                  <MenuItem value="Audi">Audi</MenuItem> 
                  <MenuItem value="Austin">Austin</MenuItem> 
                  <MenuItem value="Bentley">Bentley</MenuItem> 
                  <MenuItem value="BMW">BMW</MenuItem> 
                  <MenuItem value="Cadillac">Cadillac</MenuItem> 
                  <MenuItem value="Chery">Chery</MenuItem> 
                  <MenuItem value="Chevrolet">Chevrolet</MenuItem> 
                  <MenuItem value="Chrysler">Chrysler</MenuItem> 
                  <MenuItem value="Citroen">Citroen</MenuItem> 
                  <MenuItem value="Daewoo">Daewoo</MenuItem> 
                  <MenuItem value="Daihatsu">Daihatsu</MenuItem> 
                  <MenuItem value="Daimler">Daimler</MenuItem> 
                  <MenuItem value="Dodge">Dodge</MenuItem> 
                  <MenuItem value="DS Automobiles">DS Automobiles</MenuItem> 
                  <MenuItem value="Ferrari">Ferrari</MenuItem> 
                  <MenuItem value="Fiat">Fiat</MenuItem> 
                  <MenuItem value="Ford">Ford</MenuItem> 
                  <MenuItem value="Foton">Foton</MenuItem> 
                  <MenuItem value="Geely">Geely</MenuItem> 
                  <MenuItem value="GMC">GMC</MenuItem> 
                  <MenuItem value="Great Wall">Great Wall</MenuItem> 
                  <MenuItem value="HAVAL">HAVAL</MenuItem> 
                  <MenuItem value="Holden">Holden</MenuItem> 
                  <MenuItem value="Honda">Honda</MenuItem> 
                  <MenuItem value="HSV">HSV</MenuItem> 
                  <MenuItem value="Hummer">Hummer</MenuItem> 
                  <MenuItem value="Hyundai">Hyundai</MenuItem> 
                  <MenuItem value="INFINITI">INFINITI</MenuItem> 
                  <MenuItem value="Isuzu">Isuzu</MenuItem> 
                  <MenuItem value="Iveco">Iveco</MenuItem> 
                  <MenuItem value="Jaguar">Jaguar</MenuItem> 
                  <MenuItem value="Jeep">Jeep</MenuItem> 
                  <MenuItem value="Kia">Kia</MenuItem> 
                  <MenuItem value="Lamborghini">Lamborghini</MenuItem> 
                  <MenuItem value="Lancia">Lancia</MenuItem> 
                  <MenuItem value="Land Rover">Land Rover</MenuItem> 
                  <MenuItem value="LDV">LDV</MenuItem> 
                  <MenuItem value="Lexus">Lexus</MenuItem> 
                  <MenuItem value="Lotus">Lotus</MenuItem> 
                  <MenuItem value="Mahindra">Mahindra</MenuItem> 
                  <MenuItem value="Maserati">Maserati</MenuItem> 
                  <MenuItem value="Mazda">Mazda</MenuItem> 
                  <MenuItem value="McLaren">McLaren</MenuItem> 
                  <MenuItem value="Mercedes-Benz">Mercedes-Benz</MenuItem> 
                  <MenuItem value="MG">MG</MenuItem> 
                  <MenuItem value="MINI">MINI</MenuItem> 
                  <MenuItem value="Mitsubishi">Mitsubishi</MenuItem> 
                  <MenuItem value="Morgan">Morgan</MenuItem> 
                  <MenuItem value="Morris">Morris</MenuItem> 
                  <MenuItem value="Nissan">Nissan</MenuItem> 
                  <MenuItem value="Opel">Opel</MenuItem>  
                  <MenuItem value="Peugeot">Peugeot</MenuItem> 
                  <MenuItem value="Pontiac">Pontiac</MenuItem> 
                  <MenuItem value="Porsche">Porsche</MenuItem> 
                  <MenuItem value="Ram">Ram</MenuItem> 
                  <MenuItem value="Renault">Renault</MenuItem> 
                  <MenuItem value="Riley">Riley</MenuItem> 
                  <MenuItem value="Rolls-Royce">Rolls-Royce</MenuItem> 
                  <MenuItem value="Rover">Rover</MenuItem> 
                  <MenuItem value="Saab">Saab</MenuItem> 
                  <MenuItem value="SEAT">SEAT</MenuItem> 
                  <MenuItem value="Skoda">Skoda</MenuItem> 
                  <MenuItem value="Smart">Smart</MenuItem> 
                  <MenuItem value="Ssongyong">Ssongyong</MenuItem> 
                  <MenuItem value="Subaru">Subaru</MenuItem> 
                  <MenuItem value="Suzuki">Suzuki</MenuItem> 
                  <MenuItem value="Tesla">Tesla</MenuItem> 
                  <MenuItem value="Toyota">Toyota</MenuItem> 
                  <MenuItem value="Triumph">Triumph</MenuItem> 
                  <MenuItem value="TVR">TVR</MenuItem> 
                  <MenuItem value="Vauxhall">Vauxhall</MenuItem> 
                  <MenuItem value="Volkswagen">Volkswagen</MenuItem> 
                  <MenuItem value="Volvo">Volvo</MenuItem> 
                  <MenuItem value="Other">Other</MenuItem> 
              </Select> 
  
              </Grid>

               
              <Grid container className={this.props.classes.modelField}>         

                  <Grid item lg={6} xs={6} md={6} className={this.props.classes.modelField}>
      
                    <label htmlFor="year">Year</label>
                      
                      <Select
                        value={this.state.filterData.Year}
                        onChange={this.handleChange}
                        name="Year"
                        displayEmpty
                        className={this.props.classes.selectBoxModel}
                        >
                        <MenuItem value="">
                        From
                        </MenuItem> 
                        {
                          this.getYears().map( (year, key)=> { 
                             return <MenuItem key={key} value={year}>{year}</MenuItem>
                           })
                        } 
                      </Select>
      
                  </Grid>

                  <Grid item lg={6} xs={6} md={6} className={this.props.classes.modelField}>
      
                    <label htmlFor="YearEnd">&nbsp;</label>
                      <Select
                        value={this.state.filterData.YearEnd}
                        onChange={this.handleChange}
                        name="YearEnd"
                        displayEmpty
                        className={this.props.classes.selectBoxModel}
                        >
                        <MenuItem value="">
                        To
                        </MenuItem> 
                        {
                          this.getYears().map( (year, key)=> { 
                             return <MenuItem key={key} value={year}>{year}</MenuItem>
                          })
                        } 
                      </Select> 
                  </Grid>

              </Grid> 
  
              <Grid className={this.props.classes.modelField}>
                  <label htmlFor="nznew">NZ New</label>
                  <RadioGroup
                      aria-label="NZ New"
                      name="nznew"
                      className={this.props.classes.radioGroup}
                      value={this.state.filterData.nznew}
                      onChange={this.handleChange}
                    >
                      <FormControlLabel value="1" control={<Radio />} label="Yes" />
                      <FormControlLabel value="0" control={<Radio />} label="No" /> 
                  </RadioGroup>
              </Grid>     

              { this.state.filterError &&

                (
                <Grid className={this.props.classes.filterError}>{this.state.filterError}</Grid>
                )

              }  
              
          </DialogContent>
          <DialogActions> 
             
            <Button onClick={this.handleReset} className={this.props.classes.linkButtonCancel} color="primary">
              Reset
            </Button>	
  
            <Button onClick={this.handleCancel} className={this.props.classes.linkButtonCancel} color="primary">
              Cancel
            </Button>

            { this.state.filterData.edit ? 

            <Button onClick={this.handleUpdate} className={this.props.classes.linkButton} color="primary">
              Update
            </Button>

              :

            <Button onClick={this.handleOk} className={this.props.classes.linkButton} color="primary">
              { this.props.filterForSearches ?
                "Save"
                :
                "Apply"
              }
            </Button>

            }


          </DialogActions>
        </Dialog>
      );
    }
  }
  
 export default FilterPopup; 