import React from "react";     
import { compose } from 'redux'; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import isEmpty from '../../validation/isEmpty';
import withStyles from "@material-ui/core/styles/withStyles";     
import { getNotifications } from '../../actions/notificationsActions';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography'; 
import MenuIcon from '@material-ui/icons/Menu'; 
import NotificationsIcon from '@material-ui/icons/Notifications'; 
import ArrowBackIcon from '@material-ui/icons/ArrowBack'; 
import Badge from '@material-ui/core/Badge'; 

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
   
import appBarStyles from "../../assets/jss/components/appBarStyles.jsx"; 

class RipaAppBar extends React.Component {
	 
	constructor(){
		super();
		this.state = {  
			value:0, 
			notifications:false,
			countNotifications:0
		}
 
	} 

	componentDidMount()
	{	
		this.props.getNotifications();
	}

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.notifications.all)
		{
			this.setState({notifications:nextProps.notifications.all});
		}
	}

	openNotifications = () =>{
		this.props.history.push('/notifications');
	}
 
	render() { 

		const { classes, tabs, isAdmin } = this.props; 

	    return ( 
	    	 
	    	 <AppBar className={ isAdmin ? (classes.AppBar,classes.AppBarAdmin) : classes.AppBar } position={ isAdmin ? "absolute" : "fixed" }>
		          <Toolbar>
		            <IconButton onClick={this.props.menuClick} className={ isAdmin ? (classes.menuButton, classes.navIconHide) : classes.menuButton} color="inherit" aria-label="Open drawer">
		              <MenuIcon />
		            </IconButton>


								{ isAdmin && this.props.backIcon && 

								<IconButton onClick={this.props.backClick} className={ classes.menuButton} color="inherit" aria-label="Back to All Reports">
		              <ArrowBackIcon />
		            </IconButton>

								}

		            <Typography className={classes.title} color="inherit" noWrap>
		              {this.props.title}
		            </Typography>
		             
		            <div className={classes.grow} />
		            <div className={classes.sectionDesktop}>
		              
		              

		              	{
		              		this.state.notifications && !isEmpty(this.state.notifications)

		              		?




		              		(
												<IconButton color="inherit" onClick={this.openNotifications}>
		              			<Badge className={classes.badge} badgeContent={this.state.notifications.length} color="secondary">
				                  <NotificationsIcon />
				                </Badge>
												</IconButton>
		              		)

		              		:
		              		<IconButton color="inherit" onClick={this.openNotifications} >
		              		<NotificationsIcon  />
											</IconButton>
		              	}

		                
		              {/* </IconButton> */}
		              
		            </div>
		            
		          </Toolbar>

		          { tabs && (
		          
		          <Tabs
		          	className={classes.Tabs}
		            value={this.props.tabIndex}
		            onChange={this.props.onChange}
		            indicatorColor="primary"
		            textColor="primary"  
		            variant={this.props.fullWidth? "fullWidth" : "scrollable"} 
		            scrollButtons="auto"
		            classes={{
		            	flexContainer: classes.scrollButtons,
					    		indicator: classes.Indicator
					  }}
		          >
		            { 
		            	tabs.map(
			            	(val,key) => <Tab key={key} label={val} />
			          	)
			        }

		          </Tabs> 

		          )
		      }
		        </AppBar>
	    ); 
	}
}  
 
RipaAppBar.propTypes = {
	notifications: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	notifications: state.notifications
});

export default compose( 
  withStyles(appBarStyles, {withTheme: true}),
  connect(mapStateToProps, { getNotifications })
)(withRouter(RipaAppBar));