import React from "react";    
import { Link } from "react-router-dom"; 
 
import withStyles from "@material-ui/core/styles/withStyles";  
import Avatar from '@material-ui/core/Avatar'; 
import Grid from '@material-ui/core/Grid';    
 
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon'; 
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'; 
import List from '@material-ui/core/List'; 
import Divider from '@material-ui/core/Divider';
import Hidden from '@material-ui/core/Hidden';
 
import DirectionsCar from '@material-ui/icons/DirectionsCar'; 
import Gavel from '@material-ui/icons/Gavel';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';  
import Settings from '@material-ui/icons/Settings';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';
import StoreMallDirectoryIcon from '@material-ui/icons/StoreMallDirectory';
import GroupIcon from '@material-ui/icons/Group';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
  
import drawerStyles from "../../assets/jss/components/drawerStyles.jsx"; 

import { logoutUser } from "../../actions/authActions";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { saveNotificationsToken } from '../../actions/profileActions';
import {
	isMobile,
	isAndroid,
	isIOS 
  } from "react-device-detect";


import logo from "../../assets/img/logo.svg";

class RipaDrawer extends React.Component {
	
	constructor(props){
		super();
		this.state = { 
			imageURL:'', 
			value:0,
			drawer:false, 
		}
 
	}

	onLogoutClick=(e)=>
	{	
		e.preventDefault();
		let tokenData = {}
		let token = null;
		if(isIOS){
			tokenData = {
			token: token,
			type:'ios'
		  }; 
	  }
	  else if(isAndroid){
		tokenData = {
			token: token,
			type:'android'
		  }; 
	  }
	  else{
		tokenData = {
			token: token,
			type:'web'
		  }; 
	  }
	  this.props.saveNotificationsToken(tokenData);
		this.props.logoutUser();
		this.props.history.push('/login');
	} 
 
	render() { 
		const { classes, isAdmin, active } = this.props;  

		const DrawerData = ( 
					<div
		            tabIndex={0}
		            role="button"
		            onClick={this.props.onClose}
		            onKeyDown={this.props.onClose}
		          >
		            <div className={classes.list}>

		              <div className={classes.drawerHeader}>	
		            	<Grid item xs={12}>
						<Avatar
							alt="Adelle Charles"
							src={this.props.userProfile}
							className={classes.bigAvatar}
						/>
						</Grid>

						<Grid item xs={12}>
							<span className={classes.email}>{this.props.userEmail}</span>
						</Grid>
				      </div>		

				        <List component="nav"> 
				        	<Link className={classes.drawerLink} to="/sell-a-vehicle">
				        	<ListItem button> 
					          <ListItemIcon>
					            <DirectionsCar />
					          </ListItemIcon>
					          <ListItemText primary="Sell a vehicle" />
					        </ListItem>
					        </Link> 

					        <Link className={classes.drawerLink} to="/live-auctions">
					        <ListItem button> 
					          <ListItemIcon>
					            <Gavel />
					          </ListItemIcon>
					          <ListItemText primary="Live auctions" /> 
					        </ListItem>
					        </Link>  

					        <Divider /> 

					        <Link className={classes.drawerLink} to="/settings">
					        <ListItem button>
					          <ListItemIcon>
					            <Settings />
					          </ListItemIcon>
					          <ListItemText primary="Settings" />
					        </ListItem>
					        </Link> 

					        <Link className={classes.drawerLink} to="/my-account">
					        <ListItem button>
					          <ListItemIcon>
					            <AccountCircle />
					          </ListItemIcon>
					          <ListItemText primary="Account" />
					        </ListItem>
					        </Link> 
 
					        <ListItem button onClick={(e)=>this.onLogoutClick(e)}>
					          <ListItemIcon>
					            <PowerSettingsNew />
					          </ListItemIcon>
					          <ListItemText primary="Logout" />
					        </ListItem> 
				        </List> 
				      </div>
		          	</div>
		);

		const DrawerDataAdmin = (

		 
		            <div className={classes.List}>

		              <div className={classes.drawerHeaderAdmin}>	
		            	<Grid item xs={12}>
							<Link to="/admin">
								<img src={logo} alt="Ripa" width="94" /> 
							</Link>
						</Grid> 

				      </div>		

				        <List component="nav"> 
				        	<Link className={classes.drawerLink} to="/admin">
				        	<ListItem button className={ (active==='dashboard') ? classes.menuItemActive : null }> 
					          <ListItemIcon>
					            <StoreMallDirectoryIcon />
					          </ListItemIcon>
					          <ListItemText primary="Dashboard" />
					        </ListItem>
					        </Link> 
					        <Divider /> 

									{ this.props.role && this.props.role===1 && 
									<div>
					        <Link className={classes.drawerLink} to="/admin/users">
					        <ListItem button className={ (active==='users') ? classes.menuItemActive  : null }> 
					          <ListItemIcon>
					            <GroupIcon />
					          </ListItemIcon>
					          <ListItemText primary="Users" /> 
					        </ListItem>
					        </Link> 
									<Divider /> 
									
									

									<Link className={classes.drawerLink} to="/admin/report-users">
					        <ListItem button className={ (active==='admin-users') ? classes.menuItemActive  : null }> 
					          <ListItemIcon>
					            <GroupAddIcon />
					          </ListItemIcon>
					          <ListItemText primary="Report Users" /> 
					        </ListItem>
					        </Link> 
					        <Divider /> 

					        <Link className={classes.drawerLink} to="/admin/vehicles">
					        <ListItem button className={ (active==='vehicles') ? classes.menuItemActive  : null }>
					          <ListItemIcon>
					            <DirectionsCar />
					          </ListItemIcon>
					          <ListItemText primary="Vehicles" />
					        </ListItem>
					        </Link> 

									<Divider /> 

									<Link className={classes.drawerLink} to="/admin/settings">
					        <ListItem button className={ (active==='settings') ? classes.menuItemActive  : null }>
					          <ListItemIcon>
					            <Settings />
					          </ListItemIcon>
					          <ListItemText primary="Settings" />
					        </ListItem>
					        </Link> 
 							<Divider /> 
									</div>
									}
									{ this.props.role && this.props.role!==1 && 
									<div>
					        <Link className={classes.drawerLink} to="/admin/reports">
					        <ListItem button className={ (active==='reports') ? classes.menuItemActive  : null }>
					          <ListItemIcon>
					            <InsertDriveFileIcon />
					          </ListItemIcon>
					          <ListItemText primary="Reports" />
					        </ListItem>
					        </Link> 
 							<Divider /> 
							 </div>
									}
 
					        <ListItem button onClick={this.props.onLogout}>
					          <ListItemIcon>
					            <PowerSettingsNew />
					          </ListItemIcon>
					          <ListItemText primary="Logout" />
					        </ListItem> 
				        </List> 
				      </div> 
		);

		const DrawerFront = (

			<SwipeableDrawer
			          open={this.props.status}
			          onClose={this.props.onClose}
			          onOpen={this.props.onOpen} 
			        >
			          {DrawerData}
	      </SwipeableDrawer> 

		);

		const DrawerAdmin = (

			<div>
	    	 <Hidden mdUp>
		    	  <SwipeableDrawer
			          open={this.props.status}
			          onClose={this.props.onClose}
			          onOpen={this.props.onOpen} 
			        >
			          {DrawerDataAdmin}
			      </SwipeableDrawer> 
		     </Hidden>

		     <Hidden smDown implementation="css">
		         <Drawer
		            variant="permanent"
		            open={true}
		            className={classes.DrawerWrapper}
		          >
		        	{DrawerDataAdmin}

		        </Drawer>	
		     </Hidden>
		    </div>  
		);

	    return ( 	
	    	<div className={classes.drawerAdmin}>
	    	  {isAdmin ? DrawerAdmin : DrawerFront }
	    	</div>  
	    );
	}
}  

const mapStateToProps = state => ({
	auth: state.auth,
  });
  

// export default withStyles(drawerStyles, {withTheme:true})(RipaDrawer);

export default compose(
	withStyles(drawerStyles, {withTheme:true}),
	connect(
	  mapStateToProps,
	  {
		logoutUser,
		saveNotificationsToken
	  }
	)
  )(withRouter(RipaDrawer));