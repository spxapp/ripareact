import React, {Component} from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './creditcard/index';
import Keys from '../config/keys';

class CreditCard extends Component {
  render() {
    return (
      <StripeProvider apiKey={Keys.stripeKey}> 
          <Elements>
            <CheckoutForm updateCard={this.props.updateCard} classes={this.props.classes} onSubmit={this.props.onSubmit} onError={this.props.onError} />
          </Elements> 
      </StripeProvider>
    );
  }
}

export default CreditCard;