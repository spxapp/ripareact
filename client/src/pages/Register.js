import React from "react"; 
import PropTypes from 'prop-types';
import {Link, withRouter} from "react-router-dom"; 
import classnames from 'classnames';
import { connect } from 'react-redux';
import { registerUser } from '../actions/authActions';
import { compose } from 'redux';

import withStyles from "@material-ui/core/styles/withStyles";  
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'; 
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';

import SnackBar from '../components/common/SnackBar';
import logo from "../assets/img/logo.svg";
 
import registerStyles from "../assets/jss/pages/registerStyles.jsx";

class Register extends React.Component {

	constructor(){
		super();
		this.state = {
			name: '',
			email: '',
			password: '',
			password2: '', 
			dealer_number: '',
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			errors: {}
		}
 
	}

	componentWillMount(){
	    document.title = "Register | Ripa - Car App";
	}

	componentWillReceiveProps(nextProps){

		if(nextProps.auth.isRegistred)
		{	
			this.props.history.push('/');
		}

		if(nextProps.errors){
			this.setState({errors: nextProps.errors, loading:false, Msg:true, MsgVariant: 'error', MsgText: 'Please fix all the errors above.'});
    	}
	}

	onChange = (e) => {
		if(e.target.name==="name"){
			e.target.value = e.target.value.toString().slice(0,30)
		}
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (e) => {
		e.preventDefault();

		const newUser = {
			name: this.state.name,
			email: this.state.email,
			password: this.state.password,
			password2: this.state.password2,
			dealer_number: this.state.dealer_number
		}	 	


		this.props.registerUser(newUser, this.props.history);


	}

	handleClose = () => {
    	 
	   this.setState({ Msg: false });

	};

	render() { 
		const { classes } = this.props;
		const { errors } = this.state;
  

	    return (
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div> 
				<header className={classes.header}>
					<Link to="/">
						<img src={logo} alt="Ripa" width="94" /> 
					</Link>	
				</header>
				<section className={classes.section}>

					<form onSubmit={this.onSubmit} className={classes.form}>		

						<TextField
				          id="name"
				          placeholder="Name"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.name}
				          )}
				          type="text"
				          name="name" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.name}
									onChange={this.onChange}
				        />

				        {errors.name && (
				        	<span className={classes.errorMsg}>{errors.name}</span>
				        )}

						<TextField
				          id="email"
				          placeholder="Email address"
				          className={classnames(classes.textField, {
				          	[classes.textError] : errors.email
				          })}
				          type="text"
				          name="email" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.email}
				          onChange={this.onChange}
				        />

				        {errors.email && (
				        	<span className={classes.errorMsg}>{errors.email}</span>
				        )}

				        <TextField
				          id="password"
				          placeholder="Password"
				          className={classnames(classes.textField, {
				          	[classes.textError] : errors.password
				          })}
				          name="password" 
				          type="password" 
				          margin="normal"
				          variant="filled"
				          value={this.state.password}
				          onChange={this.onChange}
				        />

				        {errors.password && (
				        	<span className={classes.errorMsg}>{errors.password}</span>
				        )}

				        <TextField
				          id="password2"
				          placeholder="Confirm Password"
				          className={classnames(classes.textField, {
				          	[classes.textError] : errors.password2
				          })}
				          type="password" 
				          name="password2" 
				          margin="normal"
				          variant="filled"
				          value={this.state.password2}
				          onChange={this.onChange}
				        />

				        {errors.password2 && (
				        	<span className={classes.errorMsg}>{errors.password2}</span>
						)}
						
						<TextField
				          id="dealer_number"
				          placeholder="Dealer Number"
				          className={classnames(classes.textField, {
				          	[classes.textError] : errors.dealer_number
				          })}
				          type="text" 
				          name="dealer_number" 
				          margin="normal"
				          variant="filled"
				          value={this.state.dealer_number}
				          onChange={this.onChange}
				        />

				        {
							errors.dealer_number && (
								<span className={classes.errorMsg}>{errors.dealer_number}</span>
							)
						}


				        <Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>
 						
 						<Grid item xs={12}>
				        <Button type="submit" variant="contained" className={classes.button}>
					        Register
					    </Button>
					    </Grid>
			        </form>
				</section>
				<footer className={classes.footer}>
					<Link to="/">
						Already Registered? Login here
					</Link>
				</footer>

				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}
	    	</div>
	    	</Fade>
	    	</Grid>
	    	</Grid>
	    );
	}
} 

Register.propTypes = {
	registerUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth: state.auth,
	errors: state.errors
});

export default compose(
  withStyles(registerStyles),
  connect(mapStateToProps, { registerUser })
)(withRouter(Register));