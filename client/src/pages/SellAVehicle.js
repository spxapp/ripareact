import React from "react"; 
import PropTypes from 'prop-types';
import { Link, withRouter } from "react-router-dom";  
import { connect } from 'react-redux';  
import { sellAVehicle } from '../actions/vehicleActions'; 
import { compose } from 'redux'; 
import classnames from 'classnames'; 
import CircularProgress from '@material-ui/core/CircularProgress'; 
import SnackBar from '../components/common/SnackBar';

import withStyles from "@material-ui/core/styles/withStyles";   
import ArrowBack from '@material-ui/icons/ArrowBack';    
import CloseIcon from '@material-ui/icons/Close'; 

import Grid from '@material-ui/core/Grid';  
import LinearProgress from '@material-ui/core/LinearProgress'; 

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton'; 
import Button from '@material-ui/core/Button'; 
import TextField from '@material-ui/core/TextField'; 

import sellAVehicleStyles from "../assets/jss/pages/sellAVehicleStyles.jsx";
 

class SellAVehicle extends React.Component {
	
	constructor(){
		super();
		this.state = { 
			registration_number:'', 
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			errors: {},  
		}
 
	}

	componentWillMount(){
	    document.title = "Sell A Vehicle | Ripa - Car App"; 
	}

	componentDidMount(){
	  	
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}   
		
	}  

	componentWillReceiveProps(nextProps){
		if(nextProps.vehicle.sell.registration_number || nextProps.vehicle.sell.registration_number_empty) {						
			this.props.history.push('/sell-a-vehicle-2');
		} else if(nextProps.errors && Object.keys(nextProps.errors).length > 0) {			
			this.setState({ errors: nextProps.errors});
						
			if(Object.keys(nextProps.errors).length) {				
				this.setState({loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors['error']});				
			}

			if(nextProps.errors.error) {
				this.setState({loading:false});
			}
		}
	}

 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (e) => {
		e.preventDefault();

		this.setState({loading: true});
 		 
 		const vehicleData = {
 			step: 1,
 			registration_number : this.state.registration_number
 		}
		sessionStorage.clear();
 		this.props.sellAVehicle(vehicleData);
 		 
	}

	handleClose = () => {
    	 
		this.setState({ Msg: false });

	};
 
	render() { 
		const { classes } = this.props;
		const { errors } = this.state;
		const _err = this.props.errors;
 		
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>      
	          	

	          	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			            <IconButton onClick={this.props.history.goBack} className={classes.menuButton} color="inherit" aria-label="Go back">
			              <ArrowBack />
			            </IconButton>
			   			
			            <header className={classes.header}>
							<h3>Sell a vehicle</h3>
							<div className={classes.Progress}>
								<LinearProgress 
									color="secondary" 
									variant="determinate" 
									value={20}
								/>
							</div>
						</header> 

			            <Link to="/">
				            <IconButton className={classes.menuButton} color="inherit" aria-label="Close">
				              <CloseIcon />
				            </IconButton>
			            </Link>

			          </Toolbar> 

	        	</AppBar> 

        		<section className={classes.section}>

					<form onSubmit={this.onSubmit} className={classes.form}>					
				 		 
						<h2 className={classes.pageTitle}>Car registration</h2>
						 
 							
 						<div className={classes.textBoxes}>
 							<TextField
				          id="registration_number"
				          placeholder="Registration number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors[Object.keys(errors)[0]]}
				          )}
				          type="text"
				          name="registration_number" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.registration_number}
				          onChange={this.onChange}
				        />

				        {errors.registration_number&& (
				        	<span className={classes.errorMsg}>{errors.registration_number}</span>
						)}
						
						{_err.error && (
				        	<span className={classes.errorMsg}>{_err.error}</span>
				        )}
 
  
 						</div>	

 						<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>
						{!this.state.loading && (
				        <Button
				        type="submit"  
				        variant="contained" 
				        className={classes.button}>
					        Next
						</Button>
						)
						}
			        </form>
				</section>

				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}

	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

SellAVehicle.propTypes = { 

	auth: PropTypes.object.isRequired,
	vehicle: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired 
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,
	vehicle: state.vehicle,
	errors: state.errors
});

export default compose(
  withStyles(sellAVehicleStyles),
  connect(mapStateToProps, { sellAVehicle })
)(withRouter(SellAVehicle));