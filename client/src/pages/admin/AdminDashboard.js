import React from "react"; 
import PropTypes from 'prop-types';
import { Link, withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { compose } from 'redux'; 

import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';  
import Card from '@material-ui/core/Card'; 
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar';

import GroupIcon from '@material-ui/icons/Group';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';  
import Settings from '@material-ui/icons/Settings';
import DirectionsCar from '@material-ui/icons/DirectionsCar';

import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';
import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx"; 

class AdminDashboard extends React.Component {
	
	constructor(){
		super();
		this.state = { 
			drawer:false,
			errors: {}
		}
 
	}

	componentWillMount(){
	    document.title = "Dashboard | Ripa - Car App";  
 
	}

	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   
		
	}  

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open,
		});
	}; 
 
	render() { 
		const { classes } = this.props; 

	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title="Dashboard"   
	    			isAdmin={true}
		    	/>  

		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
						active="dashboard"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 
		           
	            <section className={classes.sectionAdmin}> 
          		 	

								 {this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.role===1 ?

								 <Grid container spacing={24}>
          		 		<Grid item xs={6} className={classes.widget}>
	          		 		<Link to='/admin/users'>
	          		 		 <Card className={classes.card}>
							      <CardContent>  
							        <GroupIcon /> 
							        <Typography component="h2">
							         	Users
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		 		</Link>	
          		 		</Grid>
									 <Grid item xs={6} className={classes.widget}>
	          		 		<Link to='/admin/report-users'>
	          		 		 <Card className={classes.card}>
							      <CardContent>  
							        <GroupAddIcon /> 
							        <Typography component="h2">
							         	Report Users
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		 		</Link>	
          		 		</Grid>
          		 		<Grid item xs={6} className={classes.widget}>
	          		 		<Link to='/admin/vehicles'>
	          		 		 <Card className={classes.card}>
							      <CardContent>  
							        <DirectionsCar /> 
							        <Typography component="h2">
							         	Vehicles
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		 		</Link>	
          		 		</Grid> 
          		 		<Grid item xs={6} className={classes.widget}>
	          		 		<Link to='/admin/settings'>
	          		 		 <Card className={classes.card}>
							      <CardContent>  
							        <Settings /> 
							        <Typography component="h2">
							         	Settings
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		 		</Link>	
          		 		</Grid>
									 
          		 	</Grid>
								 :

								 <Grid container spacing={24}>
          		 		 
								 
          		 		<Grid item xs={6} className={classes.widget}>
	          		 		<Link to='/admin/reports'>
	          		 		 <Card className={classes.card}>
							      <CardContent>  
							        <InsertDriveFileIcon /> 
							        <Typography component="h2">
							         	Reports
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		 		</Link>	
          		 		</Grid>
          		 		<Grid item xs={6} className={classes.widget}>
	          		 		 
	          		 		 <Card className={classes.card} onClick={this.onLogoutClick.bind(this)}>
							      <CardContent>  
							        <PowerSettingsNew /> 
							        <Typography component="h2">
							         	Logout
							        </Typography>  
							      </CardContent> 
							    </Card>
	          		  
          		 		</Grid>
									 
          		 	</Grid>
}
			    </section> 
	    			 
 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminDashboard.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired 
}

// converting state to props
const mapStateToProps = (state) => ({
	authAdmin: state.authAdmin
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { logoutUserAdmin })
)(withRouter(AdminDashboard));