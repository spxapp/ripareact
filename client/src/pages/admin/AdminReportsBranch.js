import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { getReportBranchData } from '../../actions/admin/reportsActionsAdmin';
import { compose } from 'redux'; 
import isEmpty from '../../validation/isEmpty'; 
import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';     
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead'; 
import TableRow from '@material-ui/core/TableRow'; 
import Paper from '@material-ui/core/Paper';   

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar'; 

import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx";  

class AdminReportsBranch extends React.Component {
	
	constructor(props){
		super(props);
		this.state = { 
			drawer:false,
			loader:true,
			users:{},
			userData:[],
			userDetails:{
				name:'',
				username:'',
				password:'',  
				dealer_number:''
			}, 
			errors: {},
			open:false,
			deleteopen:false,
			removeUser:'',
			updateUser:false,
		}
 		
	}
 
	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   

		if(this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.name){
			document.title = this.props.authAdmin.user.name+" | Ripa - Car App";  
		}
	
		const filter = {
			branch: this.props.location.state.branch
		}

		this.props.getReportBranchData(filter);
		
	}  

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.reports)
		{
			this.setState({users: nextProps.reports.all,userData:nextProps.reports.all, loader:false})
		}
	}

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open
		});
	}; 

	goToUserList = (e) => {

        const userData = JSON.parse(e.currentTarget.getAttribute('data-user'));
        
        const branch = this.props.location.state.branch;

		this.props.history.push('/admin/reports/user', {userData, branch})
		
    }
    
    goBack = () => {

        this.props.history.push('/admin/reports')
		
    }
 
	render() { 
		const { classes } = this.props; 
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title={this.props.authAdmin.isAuthenticated && this.props.location.state.branch + " "+ this.props.authAdmin.user.name}   
                    isAdmin={true}
                    backIcon={true}
                    backClick={this.goBack}
		    	/>  
 
		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
	    			active="reports"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 
		           
	            <section className={classes.sectionAdmin}> 
          		 	 
 
	            	 <Paper className={classes.root}>

								

							  <Table className={classes.table}>
					        <TableHead>
					          <TableRow>
					            <TableCell>User</TableCell>
					            <TableCell>Car Offered On</TableCell>
					            <TableCell>Car Purchased</TableCell>
					            <TableCell>Total Cost</TableCell>
					            <TableCell>Incomplete Purchases</TableCell>
					            <TableCell>Fee Paid</TableCell>
					          </TableRow>
					        </TableHead>
					        <TableBody>
					          { 
											this.state.loader ?

<TableRow>
					                <TableCell scope="row">
					                	Loading...
					                </TableCell>
					           </TableRow>    

											:

					          	!isEmpty(this.state.userData) ?

					          	this.state.userData.map((row,i) => {
					            return ( 
					              <TableRow key={row._id} data-user={JSON.stringify(row)} className={classes.clickable} onClick={this.goToUserList.bind(this)}>
					                <TableCell>
					                  {row.name}
					                </TableCell>
					                <TableCell>
														{row.carOffered}
					                </TableCell>
					                <TableCell>
					                  {row.carPurchased}
					                </TableCell>
					                <TableCell>
					                   {row.total}
					                </TableCell>
					                <TableCell>  
					                   {row.carIncompletePurchased}
					                </TableCell>
													<TableCell>  
					                   0
					                </TableCell>
													 
					              </TableRow>
					            );
					          })
					          :

					           <TableRow>
					                <TableCell scope="row" colSpan="6">
					                	No Reports Available Yet.
					                </TableCell>
					           </TableRow>     
					      }
					        </TableBody>
					      </Table>
					    </Paper>

			    </section>  
				 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminReportsBranch.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired, 
	reports: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	reports: state.reports,
	authAdmin: state.authAdmin 
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { getReportBranchData, logoutUserAdmin })
)(withRouter(AdminReportsBranch));