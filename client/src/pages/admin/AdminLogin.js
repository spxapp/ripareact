import React from "react"; 
import PropTypes from 'prop-types';
import { Link,withRouter } from "react-router-dom"; 
import classnames from 'classnames';
import { connect } from 'react-redux';
import { loginUserAdmin } from '../../actions/admin/authActionsAdmin';
import { compose } from 'redux';



import withStyles from "@material-ui/core/styles/withStyles";  
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress'; 
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';


import SnackBar from '../../components/common/SnackBar';

import logo from "../../assets/img/logo.svg";

import loginStyles from "../../assets/jss/pages/loginStyles.jsx";

class AdminLogin extends React.Component {

	constructor(){
		super();
		this.state = { 
			username: '',
			password: '', 
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			errors: {}
		}
 
	}

	componentWillMount(){
	    document.title = "Admin Login | Ripa - Car App";

	    
	}

	componentDidMount(){
	  	
	    if(this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin');
		} 


		/*if(typeof this.props.location.state.successMsg !== 'undefined')
	    {
	    	this.setState({ Msg:true, MsgVariant: 'success', MsgText: this.props.location.state.successMsg});
	    }*/
	} 

	componentWillReceiveProps(nextProps){

		if(nextProps.authAdmin.isAuthenticated){
			this.setState({loading:false, Msg:true, MsgVariant: 'success', MsgText: 'Logged In successfully!'});
			this.props.history.push('/admin');
		}

		if(nextProps.errors){
			this.setState({ errors: nextProps.errors});

			if(nextProps.errors.username)
			{
				this.setState({loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.username});
			}

			if(nextProps.errors.password)
			{
				this.setState({loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.password});
			}
		}
	}

	onChange = (e) =>{
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (e) => {
		e.preventDefault();

		this.setState({loading: true});

		const User = { 
			username: this.state.username,
			password: this.state.password, 
		}	 	

		this.props.loginUserAdmin(User);

	}

	handleClose = () => {
    	 
	   this.setState({ Msg: false });

	};

	render() { 
		const { classes } = this.props;
		const { errors } = this.state;

	    return (
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div> 
	    		
				<header className={classes.header}>
					<Link to="/">
						<img src={logo} alt="Ripa" width="94" /> 
					</Link>	
				</header>
				<section className={classes.section}>

					<form onSubmit={this.onSubmit} className={classes.form}>					

					<h2 className={classes.pageTitle}>Admin Login</h2>
						

						<TextField
				          id="username"
				          placeholder="Username"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.username}
				          )}
				          type="text"
				          name="username" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.username}
				          onChange={this.onChange}
				        />

				        {errors.username && (
				        	<span className={classes.errorMsg}>{errors.username}</span>
				        )}

				        <TextField
				          id="password"
				          placeholder="Password"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.password}
				          )}
				          type="password" 
				          name="password"
				          margin="normal"
				          variant="filled"
				          value={this.state.password}
				          onChange={this.onChange}
				        />

				        {errors.password && (
				        	<span className={classes.errorMsg}>{errors.password}</span>
				        )}

				        
				        <Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>
				        <Grid item xs={12}>
				        <Button type="submit" variant="contained" className={classes.button}>
					        Sign In
					    </Button>
					    </Grid>
			        </form>
				</section>
				<footer className={classes.footer}>
					 
				</footer>
				 
				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}

				 
	    	</div> 
	    	</Fade>
	    	</Grid>
	    	</Grid>
	    );
	}
} 

AdminLogin.propTypes = {
	loginUserAdmin: PropTypes.func.isRequired,
	authAdmin: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({
	authAdmin: state.authAdmin,
	errors: state.errors
});

export default compose(
  withStyles(loginStyles),
  connect(mapStateToProps, { loginUserAdmin })
)(withRouter(AdminLogin));