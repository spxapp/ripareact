import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { getAdminSettings, updateAdminSettings } from '../../actions/admin/settingsActionsAdmin';
import { compose } from 'redux';  
import classnames from 'classnames';

import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';    
 
 
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'; 
import CircularProgress from '@material-ui/core/CircularProgress';
 
import SnackBar from '../../components/common/SnackBar';

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar';
 

import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx"; 

class AdminSettings extends React.Component {
	
	constructor(props){
		super(props);
		this.state = { 
			drawer:false, 
			settings:{
				_id:'',
				credit_limit:'',
				admin_email:'', 
			},
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			errors: {}
		}
 		
	}

	componentWillMount(){
	    document.title = "Users | Ripa - Car App";  

	    
 
	}

	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   

		if(this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.role!==1){
			this.props.history.push('/admin/reports');
		}

		this.props.getAdminSettings();
		
	}  

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.settings.settings)
		{
			this.setState({settings: nextProps.settings.settings});
		}

		if(nextProps.settings.updated)
		{
			this.setState({errors:{}, settings: nextProps.settings.settings, loading:false, Msg:true, MsgVariant: 'success', MsgText: 'Settings updated.'});
		}

		else if(nextProps.errors){
			
			if(nextProps.errors.credit_limit)
			{
				this.setState({errors: nextProps.errors, loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.credit_limit});
			}else if(nextProps.errors.admin_email)
			{
				this.setState({errors: nextProps.errors, loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.admin_email});
			}

    	}
	}

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 

	handleClose = () => {
    	 
	   this.setState({ Msg: false });

	};

	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open
		});
	};  

	onChange = (e) => {

		 
		const newSettings = this.state.settings;
 
		newSettings[e.target.name] = e.target.value; 

		this.setState({settings: newSettings});
	}

	onSubmit = (e) => {
		e.preventDefault();

		const adminSettings = {
			settings_id: this.state.settings._id,
			credit_limit: this.state.settings.credit_limit,
			admin_email: this.state.settings.admin_email 
		}	 	


		this.props.updateAdminSettings(adminSettings);


	}
 
	render() { 
		const { classes } = this.props; 
 		const { errors } = this.state;
  
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title="Settings"   
	    			isAdmin={true}
		    	/>  

		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
	    			active="settings"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 	

		           
	            <section className={classes.sectionAdmin}> 

		            <header className={classes.header}>
						<h3 className={classes.SectionTitle}>Change Settings</h3>
					</header>
          		 	 
          		 	 <form onSubmit={this.onSubmit} className={classes.form}>		
          		 	 	<Grid className={classes.field}>
						<TextField
				          id="credit_limit"
				          placeholder="Enter Credit Limit"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.credit_limit}
				          )}
				          type="number"
				          name="credit_limit" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.settings.credit_limit}
				          onChange={this.onChange}
				        />
				        <label htmlFor="credit_limit">Credit Limit</label>


				        {errors.credit_limit && (
				        	<span className={classes.errorMsg}>{errors.credit_limit}</span>
				        )}
				         </Grid>

				         <Grid className={classes.field}>
				        <TextField
				          id="admin_email"
				          placeholder="Enter Admin Email"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.admin_email}
				          )}
				          type="text"
				          name="admin_email" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.settings.admin_email}
				          onChange={this.onChange}
				        />
				        <label htmlFor="admin_email">Admin Email</label>

				        {errors.admin_email && (
				        	<span className={classes.errorMsg}>{errors.admin_email}</span>
				        )}
				        </Grid>
 
				        <Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>
 						
 						<Grid item xs={12}>
				        <Button type="submit" variant="contained" className={classes.button}>
					        Update
					    </Button>
					    </Grid>
			        </form>
	            	  
			    </section> 
	    			 
	    			{
						this.state.Msg ?
						(
							<SnackBar
							  open={this.state.Msg}
							  duration={5000}
					          variant={this.state.MsgVariant}
					          className={classes.margin}
					          message={this.state.MsgText}
					          onClose={this.handleClose}
					        />
						)
						:
						null

					}
 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminSettings.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired, 
	settings: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	settings: state.settings,
	authAdmin: state.authAdmin,
	errors: state.errors
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { getAdminSettings, updateAdminSettings, logoutUserAdmin })
)(withRouter(AdminSettings));