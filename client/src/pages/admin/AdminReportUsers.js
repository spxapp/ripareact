import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { getAllReportUsers, activeReportUserAction, addReportUser ,updateReportUser,deleteReportUser } from '../../actions/admin/reportUsersActionsAdmin';
import { compose } from 'redux'; 
import isEmpty from '../../validation/isEmpty'; 
import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';    
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl'; 
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead'; 
import TableRow from '@material-ui/core/TableRow'; 
import Paper from '@material-ui/core/Paper'; 
import IconButton from '@material-ui/core/IconButton'; 
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete'; 
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import classnames from 'classnames';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar';
import moment from 'moment-timezone';

import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx";  

class AdminReportUsers extends React.Component {
	
	constructor(props){
		super(props);
		this.state = { 
			drawer:false,
			loader:true,
			users:{},
			userData:[],
			userDetails:{
				name:'',
				username:'',
				password:'',  
				dealer_number:''
			}, 
			errors: {},
			open:false,
			deleteopen:false,
			removeUser:'',
			updateUser:false,
		}
 		
	}

	componentWillMount(){
	    document.title = "Report Users | Ripa - Car App";  

	    this.props.getAllReportUsers();
 
	}

	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   
		if(this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.role!==1){
			this.props.history.push('/admin/reports');
		}
	}  

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.reportUsers)
		{
			this.setState({users: nextProps.reportUsers.all,userData:nextProps.reportUsers.all, loader:false})
		}
	}

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open
		});
	}; 

	update=()=>{
		const { userDetails } = this.state
		this.props.updateReportUser(userDetails)
		this.setState({open:false}) 
	}

	add=()=>{
		const { userDetails } = this.state
		this.props.addReportUser(userDetails)
		this.setState({open:false})  
	}

	activeReportUser = (data,index) => {

		let userDetails=this.state.userData;  
		let status = userDetails[index].active 
		status = status ? false : true 
		
		console.log(status);

		const user = {
			user_id : data._id,
			status: status
		}
		this.props.activeReportUserAction(user);  
	}

	addUser = () => { 
			 this.setState({open:true, updateUser:false })
	}
 
	editUser = (row) => {
		  
		this.setState({open:true, updateUser:true, userDetails: row })

	}

	deleteUser = () => {
		this.setState({deleteopen:false})
		this.props.deleteReportUser(this.state.removeUser)
		this.setState({removeReportUser:''})
	//  this.props.getAllUsers();
	}

	onChange=(e)=>{
		const newUserData = this.state.userDetails;
		newUserData[e.target.name] = e.target.value; 
		this.setState({userDetails: newUserData});
	}
	

	render() { 
		const { classes } = this.props; 
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title="Report Users"   
	    			isAdmin={true}
		    	/>  

		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
	    			active="admin-users"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 
		           
	            <section className={classes.sectionAdmin}> 
          		 	 

							<Grid container className={classes.root} spacing={24}>
									<Grid item xs={12} className={classes.addButton}>
									<Fab onClick={this.addUser} color="primary" aria-label="Add" className={classes.fab}>
										<AddIcon />
									</Fab>
									</Grid>
								</Grid>

	            	 <Paper className={classes.root}>

								

							  <Table className={classes.table}>
					        <TableHead>
					          <TableRow>
					            <TableCell>Name</TableCell>
					            <TableCell>Username</TableCell>
					            <TableCell>Role</TableCell>
					            <TableCell>Dealer Number</TableCell>
					            <TableCell>Active</TableCell>
					            <TableCell>Date</TableCell>
					            <TableCell>Action</TableCell>
					          </TableRow>
					        </TableHead>
					        <TableBody>
					          { 
											this.state.loader ?

<TableRow>
					                <TableCell scope="row">
					                	Loading...
					                </TableCell>
					           </TableRow>    

											:

					          	!isEmpty(this.state.userData) ?

					          	this.state.userData.map((row,i) => {
					            return (
					              <TableRow key={row._id}>
					                <TableCell>
					                  {row.name}
					                </TableCell>
					                <TableCell>
					                  {row.username}
					                </TableCell>
					                <TableCell>
					                  {row.role && row.role===1?'Ripa Admin':'Report User'}
					                </TableCell>
					                <TableCell>
					                  {row.dealer_number?row.dealer_number:'N/A'}
					                </TableCell>
					                <TableCell>  
					                  <Switch
								          checked={row.active} 
								          onChange={()=>{this.activeReportUser(row,i)}}
								          value={row._id}
								          name={row.active?'yes':'no'}
								        />
					                </TableCell>
													
					                <TableCell style={{width:80}}>
					                  {moment(row.date).format('YYYY-MM-DD')}
					                </TableCell>
													<TableCell style={{width:114}}>  
													{row.role === 2 && 

														<div>
														<IconButton onClick={()=>{this.editUser(row)}} className={classes.notiButton} color="inherit" aria-label="Edit User">
															<EditIcon />
														</IconButton>
														<IconButton onClick={()=>{this.setState({deleteopen:true,removeUser:row._id})}} className={classes.notiButton} color="inherit" aria-label="Delete User">
															<DeleteIcon />
														</IconButton> 
														</div>
													} 
					                </TableCell>
					              </TableRow>
					            );
					          })
					          :

					           <TableRow>
					                <TableCell scope="row" colSpan="7">
					                	No Report Users Available. You can add one by click on the + icon at top right.
					                </TableCell>
					           </TableRow>     
					      }
					        </TableBody>
					      </Table>
					    </Paper>

			    </section> 
	    			 
					<Dialog
          fullWidth="fullWidth"
          maxWidth="md"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <DialogTitle id="max-width-dialog-title">{this.state.updateUser ? "Edit User" : "Add New User" }</DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              You can set my maximum width and whether to adapt or not.
            </DialogContentText> */}
            
						
						<form className={classes.form}  noValidate>
              <FormControl className={classes.formControl}>
							<Table>
							<TableRow>
							<TableCell>
							<Grid className={classes.field}>
							<TextField
				          id="name"
				          placeholder="Company Name"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="name" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.name}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Company Name</label>
              </Grid> 

							<Grid className={classes.field}>
							<TextField
				          id="dealer_number"
				          placeholder="Dealer Number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="dealer_number" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.dealer_number}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Dealer Number<br /><br /><small>Note: For multiple dealer number, please use comma separated <br />eg. m123456,m566645,m879765</small></label>
              </Grid>  
							</TableCell>
 
							<TableCell>
							<Grid className={classes.field}>
							<TextField
				          id="username"
				          placeholder="Username"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="username" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.username}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Username</label>
              </Grid> 

							<Grid className={classes.field}>
							{this.state.updateUser ? 
								<div>
								<TextField
											id="role"
											placeholder="Role"
											className={classnames(classes.textField, 
												{ [classes.textError] : false}
											)}
											disabled
											type="text"
											name="role" 
											margin="normal"
										 style={{width:400}}
											variant="filled" 
											value="Report User" 
										/>
										<label htmlFor="name">Role <small>(not editable)</small></label>
									</div>
							:
						<div>
						<TextField
				          id="password"
				          placeholder="Password"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="password"
				          name="password" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.password}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Password</label>
								</div>
							}	
              </Grid> 
					  
							</TableCell>
							 
							</TableRow>

							</Table>
							</FormControl>
            </form>
          </DialogContent>
          <DialogActions style={{marginRight:50}}>
            <Button onClick={()=>{this.setState({open:false})}} color="primary">
              Close
            </Button>

						{this.state.updateUser ?   
 
						<Button onClick={this.update} variant="outlined" color="primary">
              Update
            </Button>

						:

						<Button onClick={this.add} variant="outlined" color="primary">
							Save
						</Button>
						}
          </DialogActions>
        </Dialog>
				{/* dialog for delete */}
				<Dialog
          open={this.state.deleteopen}
          onClose={()=>{this.setState({deleteopen:false})}}
        >
          <DialogTitle>{"Are You Sure?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
             Do you really want to remove this user?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>{this.setState({deleteopen:false})}} color="primary">
              No
            </Button>
            <Button onClick={this.deleteUser.bind(this)} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminReportUsers.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired, 
	reportUsers: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	reportUsers: state.reportUsers,
	authAdmin: state.authAdmin 
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { getAllReportUsers, addReportUser, updateReportUser, activeReportUserAction,deleteReportUser, logoutUserAdmin })
)(withRouter(AdminReportUsers));