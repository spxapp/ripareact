import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { getAllUsers, activeUserAction,updateUser,deleteUser } from '../../actions/admin/usersActionsAdmin';
import { compose } from 'redux'; 
import isEmpty from '../../validation/isEmpty';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';    
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import Avatar from '@material-ui/core/Avatar';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
//import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//import TableSortLabel from '@material-ui/core/TableSortLabel';
//import Toolbar from '@material-ui/core/Toolbar';
//import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
//import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
//import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
//import FilterListIcon from '@material-ui/icons/FilterList';
//import { lighten } from '@material-ui/core/styles/colorManipulator';
import classnames from 'classnames';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar';
import moment from 'moment-timezone';

import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx"; 
import placeholder from "../../assets/img/user_placeholder.jpg";

class AdminUsers extends React.Component {
	
	constructor(props){
		super(props);
		this.state = { 
			drawer:false,
			users:{},
			userData:[],
			userDetails:{
				name:'',
				email:'',
				address:'',
				phone:'',
				company:'',
				creditlimit:'',
				virtualcredit:'',
				status:false,
				notification:false,
				steps:0,
				profilepic:'',
				dealer:'',
				branch:'',
				suspended:false,
			},

			errors: {},
			open:false,
			deleteopen:false,
			removeUser:''
		}
 		
	}

	componentWillMount(){
	    document.title = "Users | Ripa - Car App";  

	    this.props.getAllUsers();
 
	}

	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   

		if(this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.role!==1){
			this.props.history.push('/admin/reports');
		}
		
	}  

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.users)
		{
			this.setState({users: nextProps.users.all,userData:nextProps.users.users})
		}
	}

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open
		});
	}; 

	update=()=>{
		const { userDetails } = this.state
		this.props.updateUser(userDetails)
		this.setState({open:false})
		this.props.getAllUsers();
	}

	activeUser = (data,index) => {
		let userDetails=this.state.userData;
		userDetails[index].status=!data.status
		this.setState({userData:userDetails})
 
		const user = {
			user_id : data._id,
			status: data.status
		}
		this.props.activeUserAction(user);  
	}
 
	editUser = (row) => {
		
		let details=[]
		details = this.state.users.find( data => data.user._id === row._id )


 

		let userData=this.state.userDetails;
		userData['name']= details ? details.user.name : row.name;
		userData['email']= details ? details.user.email : row.email;
		userData['address']= details && details.address;
		userData['company']= details && details.company;
		userData['phone']= details && details.phone;
		userData['status']= details ? details.user.status: row.status;
		userData['notification']= details ? details.user.notifications : row.notifications;
		userData['creditlimit']= details ? details.user.credit_limit : row.credit_limit;
		userData['steps']= details && details.step;
		userData['virtualcredit']= details ? details.user.virtual_credit.toFixed(2) : row.virtual_credit;
		userData['user_id']= details ? details.user._id : row._id;
		userData['profile_id']=  details && details._id;
		userData['profilepic']= details && details.profile_pic;
		userData['dealer']=  details ? details.user.dealer_number : row.dealer_number;
		userData['branch']=  details ? details.user.branch : row.branch;
		userData['suspended'] = details ? details.user.suspended: row.suspended;
			 this.setState({open:true,userDetails: userData })
	}

	deleteUser = () => {
		this.setState({deleteopen:false})
		this.props.deleteUser(this.state.removeUser)
		this.setState({removeUser:''})
	//  this.props.getAllUsers();
	}

	onChange=(e)=>{
		const newUserData = this.state.userDetails;
		newUserData[e.target.name] = e.target.value; 
		this.setState({userDetails: newUserData});
	}
	

	render() { 
		const { classes } = this.props; 
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title="Users"   
	    			isAdmin={true}
		    	/>  

		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
	    			active="users"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 
		           
	            <section className={classes.sectionAdmin}> 
          		 	 
	            	 <Paper className={classes.root}>
					      <Table className={classes.table}>
					        <TableHead>
					          <TableRow>
					            <TableCell>Name</TableCell>
					            <TableCell>Email</TableCell>
					            <TableCell>Date</TableCell>
					            <TableCell>Credit Limit</TableCell>
					            <TableCell>Active</TableCell>
					            <TableCell>Branch</TableCell>
					            <TableCell>Action</TableCell>
					          </TableRow>
					        </TableHead>
					        <TableBody>
					          { 
					          	!isEmpty(this.state.userData) ?

					          	this.state.userData.map((row,i) => {
					            return (
					              <TableRow key={row._id}>
					                <TableCell>
					                  {row.name}
					                </TableCell>
					                <TableCell>
					                  {row.email}
					                </TableCell>
					                <TableCell style={{width:80}}>
					                  {moment(row.date).format('YYYY-MM-DD')}
					                </TableCell>
					                <TableCell>
					                  {row.credit_limit?row.credit_limit:0}
					                </TableCell>
					                <TableCell>  
					                  <Switch
								          checked={row.status} 
								          onChange={()=>{this.activeUser(row,i)}}
								          value={row._id}
								          name={row.status?'yes':'no'}
								        />
					                </TableCell>

													<TableCell>
					                  {row.branch}
					                </TableCell>
													
													<TableCell style={{width:114}}>  
														<IconButton onClick={()=>{this.editUser(row)}} className={classes.notiButton} color="inherit" aria-label="Edit User">
															<EditIcon />
														</IconButton>
														<IconButton onClick={()=>{this.setState({deleteopen:true,removeUser:row._id})}} className={classes.notiButton} color="inherit" aria-label="Delete User">
															<DeleteIcon />
														</IconButton>  
					                </TableCell>
					              </TableRow>
					            );
					          })
					          :

					           <TableRow>
					                <TableCell scope="row">
					                	No Data.
					                </TableCell>
					           </TableRow>     
					      }
					        </TableBody>
					      </Table>
					    </Paper>

			    </section> 
	    			 
					<Dialog
          fullWidth="fullWidth"
          maxWidth="md"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <DialogTitle id="max-width-dialog-title">User Details</DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              You can set my maximum width and whether to adapt or not.
            </DialogContentText> */}
            
						
						<form className={classes.form}  noValidate>
              <FormControl className={classes.formControl}>
							<Table>
							<TableRow>
							<TableCell>
							<Grid className={classes.field}>
							<TextField
				          id="name"
				          placeholder="Enter The Name"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="name" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.name}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Name</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="email"
				          placeholder="Enter The Email"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="email" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.email}
				          onChange={this.onChange}
				        />
				        <label htmlFor="email">Email</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="name"
				          placeholder="Enter The Address"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="address" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.address}
				          onChange={this.onChange}
				        />
				        <label htmlFor="address">Address</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="phone"
				          placeholder="Enter The Phone Number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="number"
				          name="phone" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.phone}
				          onChange={this.onChange}
				        />
				        <label htmlFor="phone">Phone</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="company"
				          placeholder="Enter The Company"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="company" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.company}
				          onChange={this.onChange}
				        />
				        <label htmlFor="company">Company</label>
              </Grid>

							<Grid className={classes.field}>			
							<TextField
				          id="branch"
				          placeholder="Enter The Branch"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="branch" 
									margin="normal"
								 style={{width:400}}
				          variant="filled" 
				          value={this.state.userDetails.branch}
				          onChange={this.onChange}
				        />
				        <label htmlFor="branch">Branch</label>
              </Grid>
							</TableCell>
							<TableCell>
							{this.state.userDetails.profilepic ? <Avatar
							src={this.state.userDetails.profilepic}
							className={classes.profileImg}
						/>
						:
						<Avatar
						src={placeholder}
						className={classes.profileImg}
						/>}
						<TableRow>
							<label htmlFor="Status">Status</label>
						 					<Switch
								          checked={this.state.userDetails.status} 
								          onChange={()=>{
														let Userstatus=this.state.userDetails;
														Userstatus['status']=!this.state.userDetails.status
														this.setState({userDetails:Userstatus})
													
													}}
								          value={this.state.userDetails.status}
								          name={this.state.userDetails.status?'yes':'no'}
								        />

							<label htmlFor="Notification">Notification</label>
						 <Switch
								          checked={this.state.userDetails.notification} 
													onChange={()=>{
														let Userstatus=this.state.userDetails;
														Userstatus['notification']=!this.state.userDetails.notification
														this.setState({userDetails:Userstatus})
													
													}}
													value={this.state.userDetails.notification}
								          name={this.state.userDetails.notification?'yes':'no'}
								        />

												</TableRow>

											<label htmlFor="suspended">Suspended</label>
						 					<Switch
								          checked={this.state.userDetails.suspended} 
													onChange={()=>{
														let Userstatus=this.state.userDetails;
														Userstatus['suspended']=!this.state.userDetails.suspended
														this.setState({userDetails:Userstatus})
													
													}}
													value={this.state.userDetails.suspended}
								          name={this.state.userDetails.suspended?'yes':'no'}
								        />

							<Grid className={classes.field}>
							<TextField
				          id="creditlimit"
				          placeholder="Enter The Credit limit"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="creditlimit" 
									margin="normal"
				          variant="filled" 
				          value={this.state.userDetails.creditlimit}
				          onChange={this.onChange}
				        />
				        <label htmlFor="creditlimit">Credit Limit</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="virtualcredit"
				          placeholder="Enter The Virtual Credit"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="virtualcredit" 
									margin="normal"
				          variant="filled" 
				          value={this.state.userDetails.virtualcredit}
				          onChange={this.onChange}
				        />
				        <label htmlFor="virtualcredit">Virtual Credit</label>
              </Grid>
							<Grid className={classes.field}>
							<TextField
				          id="dealer"
				          placeholder="Enter The Dealer Number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : false}
				          )}
				          type="text"
				          name="dealer" 
									margin="normal"
				          variant="filled" 
				          value={this.state.userDetails.dealer}
				          onChange={this.onChange}
				        />
				        <label htmlFor="dealer">Dealer Number</label>
              </Grid>
							<Grid className={classes.field}>
							<Stepper activeStep={this.state.userDetails.steps} alternativeLabel>
            <Step key={"label1"}>
              <StepLabel></StepLabel>
            </Step>
						<Step key={"label1"}>
              <StepLabel></StepLabel>
            </Step>
						<Step key={"label1"}>
              <StepLabel></StepLabel>
            </Step>
        </Stepper>
				<label htmlFor="virtualcredit">Steps completed</label>
				</Grid>
					</TableCell>
							</TableRow>

							</Table>
							</FormControl>
            </form>
          </DialogContent>
          <DialogActions style={{marginRight:50}}>
            <Button onClick={()=>{this.setState({open:false})}} color="primary">
              Close
            </Button>
						<Button onClick={this.update} variant="outlined" color="primary">
              Update
            </Button>
          </DialogActions>
        </Dialog>
				{/* dialog for delete */}
				<Dialog
          open={this.state.deleteopen}
          onClose={()=>{this.setState({deleteopen:false})}}
        >
          <DialogTitle>{"Are You Sure?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
             Do you really want to archive this user?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={()=>{this.setState({deleteopen:false})}} color="primary">
              No
            </Button>
            <Button onClick={this.deleteUser.bind(this)} color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminUsers.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired, 
	users: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	users: state.users,
	authAdmin: state.authAdmin 
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { getAllUsers,updateUser, activeUserAction,deleteUser, logoutUserAdmin })
)(withRouter(AdminUsers));