import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUserAdmin } from '../../actions/admin/authActionsAdmin'; 
import { getAllVehicles,getVehicleBid } from '../../actions/admin/vehiclesActionsAdmin';
import { compose } from 'redux'; 
import isEmpty from '../../validation/isEmpty';

import withStyles from "@material-ui/core/styles/withStyles";  
import Grid from '@material-ui/core/Grid';    
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
//import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//import TableSortLabel from '@material-ui/core/TableSortLabel';
//import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
//import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
//import Tooltip from '@material-ui/core/Tooltip';
//import DeleteIcon from '@material-ui/icons/Delete';
//import FilterListIcon from '@material-ui/icons/FilterList';
//import { lighten } from '@material-ui/core/styles/colorManipulator';
import moment from 'moment-timezone';

import RipaDrawer from '../../components/common/RipaDrawer'; 
import RipaAppBar from '../../components/common/RipaAppBar';
 

import myAccountStyles from "../../assets/jss/pages/myAccountStyles.jsx";
import adminStyles from "../../assets/jss/pages/adminStyles.jsx"; 

import Dialog from '@material-ui/core/Dialog';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
  });

class AdminVehicles extends React.Component {
	
	constructor(props){
		super(props);
		this.state = { 
			drawer:false,
			vehicles:{},
			errors: {},
			detailDrawer:false,
			vehicleDetails:{},
			loading:false,
			bids:[],
			itemListed:{},
			winningBidder:{},
			openSuspend:false
		}
 		
	}

	componentWillMount(){
	    document.title = "Vehicles | Ripa - Car App";  

	    this.props.getAllVehicles();
 
	}

	componentDidMount(){
	  	
	    if(!this.props.authAdmin.isAuthenticated){
			this.props.history.push('/admin/login');
		}   
		if(this.props.authAdmin.isAuthenticated && this.props.authAdmin.user.role!==1){
			this.props.history.push('/admin/reports');
		}
	}  

	componentWillReceiveProps(nextProps)
	{
		if(nextProps.vehicles)
		{
			this.setState({vehicles: nextProps.vehicles.vehicles.all})
		if(nextProps.vehicles.bidDetails.bids){
			this.setState({bids:nextProps.vehicles.bidDetails.bids})
		}
		if(nextProps.vehicles.bidDetails.itemListed){
			this.setState({itemListed:nextProps.vehicles.bidDetails.itemListed})
		}
		if(nextProps.vehicles.bidDetails.winningBidder){
			this.setState({winningBidder:nextProps.vehicles.bidDetails.winningBidder})
		}
		}
	}

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUserAdmin();
		this.props.history.push('/admin/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open
		});
	}; 

	openDetails =(data)=>{
										this.props.getVehicleBid(data._id)
		this.setState({detailDrawer:true,vehicleDetails:data})
	}

	closeDetails = () =>{
		this.setState({detailDrawer:false,
			vehicleDetails:{},
			bids:[],
			itemListed:{},
			winningBidder:{}
		})
	}

	onSuspendUser=()=>{

	}
 
	render() { 
		const { classes,vehicles } = this.props; 
		const { detailDrawer,vehicleDetails, bids,winningBidder,itemListed, openSuspend } = this.state;
		const bidTrails =
      !isEmpty(bids) ?
      bids.map((bid, index) => {
        return (
          <div key={index} className={classes.bidderMargin}>
            <div className={classes.bidderSectionBid}>
              {bid.date && "@ " +
                moment(bid.date)
                  .tz("Antarctica/McMurdo")
                  .format("hh:mm A")}
            </div>
            <div
              className={
                index === 0
                  ? classes.bidderSectionBidRed
                  : classes.bidderSectionBid
              }
            >
              {bid.user.name} placed $
              {bid.bid_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              bid
            </div>
          </div>
        );
	  }):<div className={classes.bidderSectionBid}>No bids</div>;
	  
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid className={classes.adminWrapper} item xs={12} sm={12} md={12}>
	    	 	
	    	 	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)}   
		    		title="Vehicles"   
	    			isAdmin={true}
		    	/>  

		    	<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			isAdmin={true} 	
	    			onLogout={this.onLogoutClick.bind(this)}
	    			active="vehicles"
						role={ this.props.authAdmin.isAuthenticated ? this.props.authAdmin.user.role : null}
	    		/>
			 
		           
	            <section className={classes.sectionAdmin}> 
          		 	 
	            	 <Paper className={classes.root}>
					      <Table className={classes.table}>
					        <TableHead>
					          <TableRow>
					            <TableCell>Image</TableCell>
					            <TableCell>Registration Number</TableCell>
					            <TableCell>Year</TableCell>
					            <TableCell>Make</TableCell>
					            <TableCell>Model</TableCell>
					            <TableCell>Current Bid</TableCell>
					            <TableCell>In Auction</TableCell>
					            <TableCell>Date Added</TableCell>
					          </TableRow>
					        </TableHead>
					        <TableBody>
					          { 
					          	!isEmpty(this.state.vehicles) ?

					          	this.state.vehicles.map(row => {
					            return (
					              <TableRow key={row._id} onClick={()=>this.openDetails(row)}>
					                <TableCell>
					                 <img src={row.images[0]} alt='...' width='50' />
					                </TableCell>
					                <TableCell>
					                  {row.registration_number}
					                </TableCell>
					                <TableCell>
					                  {row.year}
					                </TableCell>
					                <TableCell>
					                  {row.make}
					                </TableCell>
					                <TableCell>
					                  {row.model}
					                </TableCell>
					                <TableCell>
					                  {row.current_bid}
					                </TableCell>
					                <TableCell>
					                  {row.active ? "Yes" : "No"}
					                </TableCell>
					                <TableCell>
					                  {moment(row.date).format('YYYY-MM-DD HH:mm')}
					                </TableCell>
					              </TableRow>
					            );
					          })
					          :

					           <TableRow>
					                <TableCell scope="row">
					                	Loading...
					                </TableCell>
					           </TableRow>     
					      }
					        </TableBody>
					      </Table>
					    </Paper>

			    </section> 
				<Dialog fullScreen open={detailDrawer} onClose={()=>this.setState({detailDrawer:false})}  TransitionComponent={Transition}>
				<AppBar className={classes.appBar}>
				<Toolbar>
					<IconButton edge="start" color="inherit" onClick={this.closeDetails} aria-label="Close">
					<CloseIcon />
					</IconButton>
					<Typography variant="h6" className={classes.appBartitle}>
					Vehicle details
					</Typography>
				</Toolbar>
				</AppBar>
				<section>
					<div  style={{padding:40,marginTop:70}}>
				<Grid container spacing={2}>
				<Grid item xs={4} >
					<div style={{display:'flex',justifyContent: 'space-between'}}>
					<div>
					<div className={classes.listTitle}>
					{vehicleDetails.year && (vehicleDetails.year +
                    " " +
                    vehicleDetails.make +
                    " " +
                    vehicleDetails.model)}
					</div>
					<div  className={classes.listKm}>
						{vehicleDetails.power_train && vehicleDetails.power_train}
					</div>
					<div  className={classes.listKm}>
					{vehicleDetails.engine && (vehicleDetails.engine + " CC / ")}
                  {vehicleDetails.km &&
                    Math.round(vehicleDetails.km).toString().replace(
                      /(\d)(?=(\d{3})+(?!\d))/g,
                      "$1,"
                    ) + " KM"}
					</div>
					<div  className={classes.listKm}>
						{vehicleDetails.location ? vehicleDetails.location : "-"}
					</div>
					<div>
                    <div className={classes.winningBid}>Winning bid</div>
                    <div className={classes.bidPrice}>$
					{vehicleDetails.current_bid
                        ? vehicleDetails.current_bid
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        : 1}
                    </div>
                  </div>
				  <div className={classes.detailHeading} style={{marginTop:15}}>
					  Details
				  </div>
				  <div style={{marginTop:15}}>
                      <div className={classes.detailSection}>VIN</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.vin ? vehicleDetails.vin : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>YEAR</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.year ? vehicleDetails.year : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>MAKE</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.make ? vehicleDetails.make : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>MODEL</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.model ? vehicleDetails.model : "-"}
                      </div>
                    </div>
					</div>
					<div>
					<div>
                      <div className={classes.detailSection}>VEHICLE TYPE</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.vehicle_type ? vehicleDetails.vehicle_type : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>COLOUR</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.colour ? vehicleDetails.colour : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>TRANSMISSION</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.transmission ? vehicleDetails.transmission : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>ENGINE</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.engine ? vehicleDetails.engine : "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>REGISTRATION EXPIRES</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.registration_expire ? moment(vehicleDetails.registration_expire).format(
                          "YYYY-MM-DD"
                        ): "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>WOF EXPIRES</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.wof_expire ? moment(vehicleDetails.wof_expire).format(
                          "YYYY-MM-DD"
                        ): "-"}
                      </div>
                    </div>
					<div style={{marginTop:15}}>
                      <div className={classes.detailSection}>ADDITIONAL DETAILS</div>
                      <div className={classes.detailName}>
					  {vehicleDetails.add_details ? vehicleDetails.add_details : "-"}
                      </div>
                    </div>
					</div>
					</div>
				</Grid>
				<Grid item xs={5}>
				<GridList cellHeight={160} className={classes.gridList} cols={3}>
					{vehicleDetails.images && vehicleDetails.images.map(image=>(
						<GridListTile >
						<img src={image} alt={"..."} />
					</GridListTile>
					))}
			</GridList>
				</Grid>
				<Grid item xs={3} style={{display: 'flex',alignItems: 'center',flexDirection:'column'}}>
				<div className={classes.detailHeading}>
					  Bid Trails
				  </div>
				  <div className={classes.bidContainer}>
				  {bidTrails}
				  </div>
				</Grid>
				</Grid>
				<div className={classes.detailBottomContainer}>
				<div className={classes.sellerContainer}>
					<div>
                      <div className={classes.detailSection}>ITEM LISTED</div>
                      <div className={classes.detailName}>
                       {itemListed.name + ", " + itemListed.location + " " + itemListed.make}
                      </div>
					  <div className={classes.detailName}>
					  {"$100 charge - " + moment(itemListed.listed_date).format("YYYY-MM-DD HH:mm A")}
					  </div>
					  </div>
					  <Button onClick={()=>this.setState({openSuspend:true})} color="secondary">
						SUSPEND USER
					</Button>
                    </div>
					<div  className={classes.sellerContainer}>
						<div>
                      <div className={classes.detailSection}>WINNING BIDDER</div>
                      <div className={classes.detailName}>
					  {winningBidder.name ? (winningBidder.name + ", " + winningBidder.location + " " + winningBidder.make):"-"}
                      </div>
					  <div className={classes.detailName}>
					 $100 charge
					  </div>
					  </div>
					  <Button onClick={()=>this.setState({openSuspend:true})} color="secondary">
						SUSPEND USER
					</Button>
                    </div>
					<div  className={classes.sellerContainer}>
						<div>
                      <div className={classes.detailSection}>SELLER ACCEPTED</div>
                      <div className={classes.detailName}>
					  {itemListed.name + ", " + itemListed.location + " " + itemListed.make}
                      </div>
					  <div className={classes.detailName}>
					  {moment(itemListed.listed_date).format("YYYY-MM-DD HH:mm A")}
					  </div>
					  </div>
					  <div></div>
                    </div>
					</div>
					</div>
				</section>
				<Dialog
					open={openSuspend}
					onClose={()=>this.setState({openSuspend:false})}
				>
					<DialogTitle id="alert-dialog-title">Suspend the user?</DialogTitle>
					<DialogContent>
					<DialogContentText id="alert-dialog-description">
						Do you really want to suspend the user
					</DialogContentText>
					</DialogContent>
					<DialogActions>
					<Button onClick={()=>this.setState({openSuspend:false})} color="primary">
						No
					</Button>
					<Button onClick={this.onSuspendUser} color="primary" autoFocus>
						Yes
					</Button>
					</DialogActions>
				</Dialog>
			</Dialog>	 
				
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AdminVehicles.propTypes = {
	logoutUserAdmin: PropTypes.func.isRequired, 
	authAdmin: PropTypes.object.isRequired, 
	vehicles: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({ 
	vehicles: state.vehicles,
	authAdmin: state.authAdmin 
});

export default compose(
  (withStyles(myAccountStyles),
  withStyles(adminStyles, { withTheme: true })),
  connect(mapStateToProps, { getAllVehicles, logoutUserAdmin, getVehicleBid })
)(withRouter(AdminVehicles));