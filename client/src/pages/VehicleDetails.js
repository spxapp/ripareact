import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { getUser } from "../actions/authActions";
import {
  getVehicleDetails,
  vehicleAcceptedRejected
} from "../actions/vehicleDetailsActions";
import {
  expireVehicle,
  vehicleBid,
  outBidStatus,
  clearVehicle,
  getCurrentTimer
} from "../actions/vehicleActions";
import { getVehicleBids } from "../actions/vehicleBidsActions";
import { compose } from "redux";
import isEmpty from "../validation/isEmpty";
import SnackBar from "../components/common/SnackBar";
import moment from "moment-timezone";

import Carousel from "nuka-carousel";

import withStyles from "@material-ui/core/styles/withStyles";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Grid from "@material-ui/core/Grid";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ArrowRight";
import classnames from "classnames";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import { Detector } from "react-detect-offline";

import vehicleDetailsStyles from "../assets/jss/pages/vehicleDetailsStyles.jsx";

import CircularProgress from "@material-ui/core/CircularProgress";

import { socket } from "./../config/socketio-client";
import nz_flag from "../assets/img/nz_flag.png";

import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";

import Keys from "../config/keys";

const socketUrl = Keys.socketUrl;

//import carImage from "../assets/img/vehicle-details.png";

class VehicleDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      activeImage: 0,
      vehicle: [],
      timer: false,
      bid_price: "",
      Msg: false,
      current_bid: "",
      MsgVariant: false,
      MsgText: false,
      time: false,
      openLightbox: false,
      photoIndex: 0,
      timerAcceptReject: false,
      vehicleBids: {},
      screenLoading: true,
      Msg: false,
      MsgVariant: false,
      MsgText: false
    };

    this.interval = false;
    if (this.props.location.state) {
      let room = "vehicle:" + this.props.location.state.id;
      socket.emit("room", room);
    }
    socket.on("bidUpdate", bid => {
      if (bid.vehicle_id === this.props.location.state.id) {
        var prevVehicle = this.state.vehicle;

        prevVehicle.current_bid = bid.bid_price;

        this.setState({ errors: {}, time: new Date(), vehicle: prevVehicle });
        if (bid.user === this.props.auth.user.id) {
          prevVehicle.statusType = "success";
          prevVehicle.statusMessage = "You are leading";
          return false;
        } else if (
          this.state.vehicle.current_bid > 0 &&
          this.props.auth.user.id !== prevVehicle.user._id
        ) {
          prevVehicle.statusType = "orange";
          prevVehicle.statusMessage = "You've been outbid";
        }
        this.setState({ errors: {}, time: new Date(), vehicle: prevVehicle });

        // const vehicleData = {
        //   vehicle_id: this.state.vehicle._id
        // };
        // this.props.getVehicleDetails(vehicleData);
        // this.props.getVehicleBids(vehicleData);
      }
    });
    socket.on("timer", data => {
      if (this.props.location.state.id === data.vehicle_id) {
        this.setState({ timer: data.time });

        if (data.time <= 0) {
          let expiry_date = new Date();
          var UpdateVehicle = {
            vehicle_id: this.props.location.state.id,
            expiry_date: expiry_date
          };
          this.setState({ timer: 0 });
          // this.props.expireVehicle(UpdateVehicle);
        }
      }
    });

    socket.on("accept-timer", data => {
      if (this.props.location.state.id === data.vehicle_id) {
        this.setState({ timerAcceptReject: data.time });
        if (data.time <= 0) {
          this.setState({ timerAcceptReject: 0 });
        }
      }
    });
    window.onfocus = () => {
      if (!isEmpty(this.props.match.params)) {
        const vehicleData = {
          vehicle_id: this.props.location.state.id
        };
        this.props.getVehicleDetails(vehicleData);
        this.props.getVehicleBids(vehicleData);
      } else {
        this.props.history.push("/live-auctions");
      }
      if (this.props.vehicle.vehicleDetails) {
        this.setState({ vehicle: this.props.vehicle.vehicleDetails }, () => {
          // this.bidEndsIn()
          // this.acceptRejectEndIn()
        });
      }
      this.props.getUser();
    };
  }

  componentDidMount() {
    this.element = document.getElementById("body");

    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
    if (!this.props.location.state.active) {
      let eventTime =
        new Date(this.props.location.state.expiry_date).getTime() +
        1000 * 60 * 15;
      let currentTime = new Date().getTime();
      let diffTime = (eventTime - currentTime) / 1000;
      if (diffTime >= 0) {
        this.setState({ timerAcceptReject: "loading" });
      }
      // this.props.getCurrentTimer(this.props.location.state.id);
    }

    if (!isEmpty(this.props.location.state)) {
      const vehicleData = {
        vehicle_id: this.props.location.state.id
      };
      this.props.getVehicleDetails(vehicleData);
      this.props.getVehicleBids(vehicleData);
    } else {
      this.props.history.push("/live-auctions");
    }
    // if(this.props.vehicle.vehicleDetails){

    //   this.setState({vehicle:this.props.vehicle.vehicleDetails},()=>{
    //     // this.bidEndsIn()

    //     // this.acceptRejectEndIn()
    //   })
    // }
    this.props.getUser();

    document.title = "Vehicle Details | Ripa - Car App";
    document.addEventListener(
      "resume",
      () => {
        setTimeout(() => {
          if (this.props.location.state) {
            let room = "vehicle:" + this.props.location.state.id;
            socket.emit("room", room);
          }
          this.setState({ timer: false });

          if (!isEmpty(this.props.location.state)) {
            const vehicleData = {
              vehicle_id: this.props.location.state.id
            };
            this.props.getVehicleDetails(vehicleData);
            this.props.getVehicleBids(vehicleData);
          }
          if (!this.props.location.state.active) {
            let eventTime =
              new Date(this.props.location.state.expiry_date).getTime() +
              1000 * 60 * 15;
            let currentTime = new Date().getTime();
            let diffTime = (eventTime - currentTime) / 1000;
            if (diffTime >= 0) {
              this.setState({ timerAcceptReject: "loading" });
            }
          }
        }, 1000);
      },
      false
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vehicle.bidPlaced) {
      socket.emit("bidUpdate", {
        [this.props.location.state.id]: this.state.bid_price
      });

      this.props.getUser();
    } else if (nextProps.errors.bid_price) {
      this.setState(
        { errors: nextProps.errors },
        this.handleItemError(nextProps.errors.bid_price)
      );
    } else if (nextProps.vehicleDetails) {
      this.setState({ vehicle: nextProps.vehicleDetails }, () => {
        // this.bidEndsIn();
        // check if there is acept/reject pending
        //  this.acceptRejectEndIn();
      });
      if (
        nextProps.vehicleDetails.current_bid === 1 ||
        nextProps.vehicleDetails.accepted
      ) {
        this.setState({ timerAcceptReject: false });
      }
    }
    if (nextProps.vehicleBids.bids) {
      this.setState({ vehicleBids: nextProps.vehicleBids.bids });
    }

    this.setState({ screenLoading: false });
  }

  componentDidUpdate = prevProps => {
    const { state } = this.props.location;
    if (prevProps.location.state != state && state.type === "notification") {
      const vehicleData = {
        vehicle_id: state.id
      };
      this.setState({ screenLoading: true, timer: false });
      this.props.getVehicleDetails(vehicleData);
      this.props.getVehicleBids(vehicleData);
    }
  };

  handleStepChange = () => {
    let activeImage = this.state.activeImage;

    activeImage++;

    const imageLength = !isEmpty(this.state.vehicle)
      ? this.state.vehicle.images.length
      : 0;

    if (activeImage >= imageLength) {
      this.setState({ activeImage: 0 });
    } else {
      this.setState({ activeImage });
    }
  };

  handleItemError = error => {
    this.setState({ Msg: true, MsgVariant: "error", MsgText: error });
  };

  handleitemSuccess = () => {
    this.setState({ Msg: true, MsgVariant: "success", MsgText: "Bid added!" });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onBid = e => {
    e.preventDefault();

    var UpdateVehicle = {
      vehicle_id: this.state.vehicle._id,
      bid_price: this.state.bid_price,
      userTo: this.state.vehicle.user
    };
    if (this.state.bid_price > this.state.vehicle.current_bid) {
      this.setState({ bid_price: "" });
      this.handleitemSuccess();
    }

    this.props.vehicleBid(UpdateVehicle);
  };

  userDetails = () => {
    this.state.vehicle.user &&
      this.props.history.push(
        `/user-details/${this.state.vehicle.user._id}/Seller`
      );
  };

  buyerDetails = () => {
    this.state.vehicle.accepted &&
      this.state.vehicle.accepted_bid &&
      this.props.history.push(
        `/user-details/${this.state.vehicle.accepted_bid}/Buyer`
      );
  };

  bidEndsIn = () => {
    if (typeof this.props.vehicleDetails.date === "undefined") {
      return;
    }
    clearInterval(this.interval);

    var dtstr = this.props.vehicleDetails.date;
    // replace anything but numbers by spaces
    dtstr = dtstr.replace(/\D/g, " ");

    // trim any hanging white space
    dtstr = dtstr.replace(/\s+$/, "");

    // split on space
    var dtcomps = dtstr.split(" ");

    // not all ISO 8601 dates can convert, as is
    // unless month and date specified, invalid
    if (dtcomps.length < 3) return "invalid date";
    // if time not provided, set to zero
    if (dtcomps.length < 4) {
      dtcomps[3] = 0;
      dtcomps[4] = 0;
      dtcomps[5] = 0;
    }

    // modify month between 1 based ISO 8601 and zero based Date
    dtcomps[1]--;

    var convdt = new Date(
      Date.UTC(
        dtcomps[0],
        dtcomps[1],
        dtcomps[2],
        dtcomps[3],
        dtcomps[4],
        dtcomps[5]
      )
    );

    var curDate = new Date();

    var seconds = (curDate.getTime() - convdt.getTime()) / 1000;

    //console.log(seconds);

    var one_hour = 3600;

    if (seconds >= one_hour || seconds <= 0) {
      seconds = 0;
    } else {
      seconds = one_hour - seconds;
    }

    var totalSeconds = parseInt(seconds, 10);

    //totalSeconds = 10;

    if (totalSeconds <= 0) {
      //var UpdateVehicle = {
      //	vehicle_id : this.props.item._id
      //}

      //this.props.expireVehicle(UpdateVehicle);

      totalSeconds = 0;
    }

    this.setState({ timer: totalSeconds });

    this.interval = setInterval(() => {
      if (totalSeconds <= 0) {
        clearInterval(this.interval);

        var UpdateVehicle = {
          vehicle_id: this.props.vehicleDetails._id
        };

        this.setState({ timer: 0 });

        this.props.expireVehicle(UpdateVehicle);

        //this.props.activeVehicles();
      } else {
        this.setState({ timer: totalSeconds });
      }

      totalSeconds--;
    }, 1000);

    if (totalSeconds <= 0) {
      clearInterval(this.interval);
    }
  };

  acceptRejectEndIn = () => {
    if (
      typeof this.props.vehicleDetails.expiry_date === "undefined" ||
      this.props.vehicleDetails.expiry_date === null
    ) {
      return;
    }

    var dtstr = this.props.vehicleDetails.expiry_date;
    // replace anything but numbers by spaces
    dtstr = dtstr.replace(/\D/g, " ");

    // trim any hanging white space
    dtstr = dtstr.replace(/\s+$/, "");

    // split on space
    var dtcomps = dtstr.split(" ");

    // not all ISO 8601 dates can convert, as is unless month and date specified,
    // invalid
    if (dtcomps.length < 3) return "invalid date";

    // if time not provided, set to zero
    if (dtcomps.length < 4) {
      dtcomps[3] = 0;
      dtcomps[4] = 0;
      dtcomps[5] = 0;
    }

    // modify month between 1 based ISO 8601 and zero based Date
    dtcomps[1]--;

    var convdt = new Date(
      Date.UTC(
        dtcomps[0],
        dtcomps[1],
        dtcomps[2],
        dtcomps[3],
        dtcomps[4],
        dtcomps[5]
      )
    );

    var curDate = new Date();

    var seconds = (curDate.getTime() - convdt.getTime()) / 1000;

    // check if seconsd note more then 5 minutes

    var fifteen_minutes = 900;

    if (seconds >= fifteen_minutes || seconds <= 0) {
      seconds = 0;
    } else {
      seconds = fifteen_minutes - seconds;
    }

    var totalSeconds = parseInt(seconds, 10);

    if (typeof this.props.vehicleDetails.accepted !== "undefined") {
      totalSeconds = 0;
    }

    //console.log(totalSeconds);

    if (totalSeconds <= 0) {
      totalSeconds = 0;
    }

    this.setState({ timerAcceptReject: totalSeconds });

    var TimerInterval = setInterval(() => {
      if (totalSeconds <= 0) {
        clearInterval(TimerInterval);

        this.setState({ timerAcceptReject: 0 });
      } else {
        this.setState({ timerAcceptReject: totalSeconds });
      }

      totalSeconds--;
    }, 1000);

    if (totalSeconds <= 0) {
      clearInterval(this.interval);
      // if (typeof this.props.vehicleDetails.accepted === "undefined") {
      //   const vehicleData = {
      //     vehicle_id: this.state.vehicle._id,
      //     accepted_rejected: false
      //   };
      //   console.log(vehicleData)
      //   //this.props.vehicleAcceptedRejected(vehicleData);
      // }
    }
  };

  vehicleAcceptedRejected = e => {
    let accepted_rejected = e.currentTarget.getAttribute("data-accepted");

    if (accepted_rejected === "true") {
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText: "Congratulations! You have accepted the bid."
      });
    } else {
      this.setState({
        Msg: true,
        MsgVariant: "error",
        MsgText: "You have rejected the bid."
      });
    }

    //this.setState({ timerAcceptReject: 0 });
    const vehicleData = {
      vehicle_id: this.state.vehicle._id,
      accepted_rejected: accepted_rejected
    };
    this.props.vehicleAcceptedRejected(vehicleData);
  };

  convertDateToTime = date => {
    var dtstr = date;
    // replace anything but numbers by spaces
    dtstr = dtstr.replace(/\D/g, " ");

    // trim any hanging white space
    dtstr = dtstr.replace(/\s+$/, "");

    // split on space
    var dtcomps = dtstr.split(" ");

    // not all ISO 8601 dates can convert, as is unless month and date specified,
    // invalid
    if (dtcomps.length < 3) return "invalid date";

    // if time not provided, set to zero
    if (dtcomps.length < 4) {
      dtcomps[3] = 0;
      dtcomps[4] = 0;
      dtcomps[5] = 0;
    }

    // modify month between 1 based ISO 8601 and zero based Date
    dtcomps[1]--;

    var convdt = new Date(
      Date.UTC(
        dtcomps[0],
        dtcomps[1],
        dtcomps[2],
        dtcomps[3],
        dtcomps[4],
        dtcomps[5]
      )
    );

    return convdt.toLocaleString("en-US", {
      hour: "numeric",
      minute: "numeric",
      hour12: true
    });
  };

  secondsToHms = d => {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);

    var hDisplay = h > 0 ? ("0" + h).slice(-2) : "00";
    var mDisplay = m > 0 ? ("0" + m).slice(-2) : "00";
    var sDisplay = s > 0 ? ("0" + s).slice(-2) : "00";

    return hDisplay + ":" + mDisplay + ":" + sDisplay;
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };

  goBack = () => {
    // if(!isEmpty(this.props.location.state)){
    //   this.props.history.goBack()
    //   return false
    // }
    this.props.clearVehicle();
    this.props.history.push("/");
  };

  openLightBoxViewer = index => {
    this.element.style.overflow = "hidden";
    this.element.style.height = "100vh";
    this.setState({ openLightbox: true, photoIndex: index });
  };

  getThumbnail = () => {
    const { classes } = this.props;
    const { photoIndex } = this.state;
    return (
      <div style={{ whiteSpace: "nowrap", overflowX: "auto" }}>
        {this.state.vehicle.images.map((image, index) => (
          <img
            key={index}
            className={
              classes.thumbImage +
              " " +
              (photoIndex === index ? classes.thumbFocus : "")
            }
            src={image}
            alt="Loading..."
            onClick={() => this.setState({ photoIndex: index })}
          />
        ))}
      </div>
    );
  };

  connectionChange = connect => {
    if (connect) {
      if (this.props.location.state) {
        let room = "vehicle:" + this.props.location.state.id;
        socket.emit("room", room);
      }
      if (!isEmpty(this.props.location.state)) {
        const vehicleData = {
          vehicle_id: this.props.location.state.id
        };
        this.props.getVehicleDetails(vehicleData);
        this.props.getVehicleBids(vehicleData);
      }
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText: "Back to online"
      });
    } else {
      this.setState({
        Msg: true,
        MsgVariant: "error",
        MsgText: "No internet connection"
      });
    }
  };

  render() {
    const { classes } = this.props;
    const { errors, photoIndex, openLightbox } = this.state;

    const bidTrails =
      !isEmpty(this.state.vehicleBids) &&
      this.state.vehicleBids.map((bid, index) => {
        // console.log("======>", bid.date);
        return (
          <div key={index} className={classes.bidderMargin}>
            <div className={classes.bidderSectionBid}>
              {bid.date &&
                "@ " +
                  moment(bid.date)
                    .tz("Antarctica/McMurdo")
                    .format("hh:mm A")}
            </div>
            <div
              className={
                index === 0
                  ? classes.bidderSectionBidRed
                  : classes.bidderSectionBid
              }
            >
              Someone placed $
              {bid.bid_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              bid
            </div>
          </div>
        );
      });

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          // style={{ overflow: "hidden", height: "100%" }}
        >
          <Detector
            render={({ online }) => <span></span>}
            onChange={connect => this.connectionChange(connect)}
          />
          <div>
            <AppBar className={classes.AppBar} position="fixed">
              <Toolbar>
                <IconButton
                  onClick={this.goBack}
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="Go back"
                >
                  <ArrowBack />
                </IconButton>
                <Typography className={classes.title} color="inherit" noWrap>
                  Vehicle details
                </Typography>

                <div className={classes.grow} />
              </Toolbar>
            </AppBar>

            {isEmpty(this.state.vehicle) ? (
              <section className={classes.loadingScreen}>
                <CircularProgress className={classes.progress} />
                <br />
                <br />
                Loading...
              </section>
            ) : (
              <section>
                {this.state.vehicle.active ? (
                  this.state.vehicle.statusMessage ? (
                    <Toolbar
                      className={
                        this.state.vehicle.statusType &&
                        this.state.vehicle.statusType === "success"
                          ? classes.vehicleOpenStatusSuccess
                          : classes.vehicleOpenStatusError
                      }
                    >
                      <span>{this.state.vehicle.statusMessage}</span>
                    </Toolbar>
                  ) : (
                    <Toolbar className={classes.vehicleOpenStatus}>
                      <span>
                        {this.state.time
                          ? "@ " +
                            this.state.time.toLocaleString("en-US", {
                              hour: "numeric",
                              minute: "numeric",
                              hour12: true
                            }) +
                            " someone "
                          : "Current "}
                        bid ${this.state.vehicle.current_bid}
                      </span>
                    </Toolbar>
                  )
                ) : this.state.timerAcceptReject === "loading" ? (
                  <Toolbar className={classes.vehicleStatus}>
                    <span>
                      <CircularProgress
                        className={classes.progress}
                        size={20}
                        color="#ffffff"
                      />
                    </span>
                  </Toolbar>
                ) : this.state.timerAcceptReject &&
                  this.state.vehicle.current_bid > 1 &&
                  typeof this.state.vehicle.accepted === "undefined" &&
                  ((this.props.auth.user &&
                    (this.state.vehicleBids &&
                      this.state.vehicleBids[0].user ===
                        this.props.auth.user.id)) ||
                    (this.state.vehicle &&
                      this.state.vehicle.user._id ===
                        this.props.auth.user.id)) ? (
                  <Toolbar className={classes.vehicleStatus}>
                    <span>
                      APPROVAL WINDOW LEFT:{" "}
                      {this.secondsToHms(this.state.timerAcceptReject)}
                    </span>
                  </Toolbar>
                ) : this.state.vehicle.statusMessage ? (
                  this.state.vehicle.accepted === false &&
                  this.state.vehicleBids[0].user === this.props.auth.user.id ? (
                    <Toolbar
                      className={
                        this.state.vehicle.statusType &&
                        this.state.vehicle.statusType === "error"
                          ? classes.vehicleOpenStatusSuccess
                          : classes.vehicleOpenStatusError
                      }
                    >
                      <span>Sorry, The seller has rejected your bid.</span>
                    </Toolbar>
                  ) : (
                    <Toolbar
                      className={
                        this.state.vehicle.statusType &&
                        this.state.vehicle.statusType === "success"
                          ? classes.vehicleOpenStatusSuccess
                          : classes.vehicleOpenStatusError
                      }
                    >
                      <span>{this.state.vehicle.statusMessage}</span>
                    </Toolbar>
                  )
                ) : (
                  <Toolbar className={classes.vehicleStatus}>
                    <span>Closed</span>
                  </Toolbar>
                )}

                {openLightbox && (
                  <Lightbox
                    mainSrc={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[photoIndex]
                    }
                    nextSrc={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[
                        (photoIndex + 1) % this.state.vehicle.images.length
                      ]
                    }
                    prevSrc={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[
                        (photoIndex + this.state.vehicle.images.length - 1) %
                          this.state.vehicle.images.length
                      ]
                    }
                    nextSrcThumbnail={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[
                        (photoIndex + 1) % this.state.vehicle.images.length
                      ]
                    }
                    prevSrcThumbnail={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[
                        (photoIndex + this.state.vehicle.images.length - 1) %
                          this.state.vehicle.images.length
                      ]
                    }
                    mainSrcThumbnail={
                      this.state.vehicle.images &&
                      this.state.vehicle.images[photoIndex]
                    }
                    imageCaption={this.getThumbnail()}
                    imageTitle={
                      this.state.vehicle.year &&
                      this.state.vehicle.year +
                        " " +
                        this.state.vehicle.make +
                        " " +
                        this.state.vehicle.model +
                        " " +
                        this.state.vehicle.transmission
                    }
                    onCloseRequest={() => {
                      this.setState({ openLightbox: false });
                      this.element.style.overflow = "scroll";
                    }}
                    onMovePrevRequest={() => {
                      if (this.state.vehicle.images) {
                      }
                      this.setState({
                        photoIndex:
                          (photoIndex + this.state.vehicle.images.length - 1) %
                          this.state.vehicle.images.length
                      });
                    }}
                    onMoveNextRequest={() => {
                      if (this.state.vehicle.images) {
                        this.setState({
                          photoIndex:
                            (photoIndex + 1) % this.state.vehicle.images.length
                        });
                      }
                    }}
                  />
                )}
                <section className={classes.section}>
                  <Grid item xs={12} className={classes.imageSlider}>
                    <Carousel
                      heightMode="current"
                      ref={c => (this.carousel = c)}
                    >
                      {!isEmpty(this.state.vehicle) ? (
                        this.state.vehicle.images.map((image, index) => (
                          <div
                            key={index}
                            className={classes.mainImageContainer}
                          >
                            <img
                              className={classes.mainImage}
                              src={image}
                              alt="Loading..."
                              onClick={() => this.openLightBoxViewer(index)}
                              onLoad={() => {
                                this.carousel.setDimensions();
                              }}
                            />
                            {this.state.vehicle.nz_new ? (
                              <div className={classes.nzNewStyle}>
                                <img
                                  src={
                                    "https://ripa.app/static/media/nz_flag.42b50f93.png"
                                  }
                                  alt="nz flag"
                                />
                                <span>NZ New </span>
                              </div>
                            ) : null}
                            <div
                              style={{ backgroundImage: `url(${image})` }}
                              className={classes.mainImageBlur}
                            ></div>
                          </div>
                        ))
                      ) : (
                        <div key={0}>Loading images...</div>
                      )}
                    </Carousel>
                  </Grid>
                  <Grid item xs={12} className={classes.vehicleDetails}>
                    <div className={classes.listCurrentBid}>
                      Current bid $
                      {this.state.vehicle.current_bid &&
                        this.state.vehicle.current_bid
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    </div>

                    <div className={classes.listTitle}>
                      {this.state.vehicle.year &&
                        this.state.vehicle.year +
                          " " +
                          this.state.vehicle.make +
                          " " +
                          this.state.vehicle.model +
                          " " +
                          this.state.vehicle.transmission}
                    </div>
                    {this.state.vehicle.registration_number && (
                      <div className={classes.listKm}>
                        {"Registration Number : " +
                          this.state.vehicle.registration_number}
                      </div>
                    )}
                    <div className={classes.listKm}>
                      {this.state.vehicle.power_train}
                    </div>
                    <div className={classes.listKm}>
                      {this.state.vehicle.engine &&
                        this.state.vehicle.engine + " CC / "}
                      {this.state.vehicle.km &&
                        Math.round(this.state.vehicle.km)
                          .toString()
                          .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " KM"}
                    </div>
                    <div className={classes.listKm}>
                      {this.state.vehicle.location}
                    </div>

                    {this.state.timer > 0 && (
                      <div className={classes.listEnd}>
                        {"Ends in " + this.secondsToHms(this.state.timer)}
                      </div>
                    )}

                    {this.props.auth.user &&
                    this.state.vehicle.user &&
                    this.state.timer &&
                    this.state.timer > 0 ? (
                      this.props.auth.user.id !==
                        this.state.vehicle.user._id && (
                        <form onSubmit={this.onBid}>
                          <Grid container spacing={16}>
                            <Grid item xs={this.props.type === "grid" ? 12 : 8}>
                              <TextField
                                id="bid_price"
                                className={classnames(classes.textField, {
                                  [classes.textError]: errors.bid_price
                                })}
                                placeholder="Enter Bid"
                                type="number"
                                name="bid_price"
                                margin="normal"
                                variant="filled"
                                onChange={this.onChange}
                                value={this.state.bid_price}
                              />
                            </Grid>
                            <Grid item xs={this.props.type === "grid" ? 12 : 4}>
                              <Button
                                type="submit"
                                variant="contained"
                                className={classes.button}
                              >
                                Bid
                              </Button>
                            </Grid>
                          </Grid>

                          <div className={classes.BiddingCreditAvailable}>
                            Bidding credit available
                          </div>
                          <div className={classes.bidPrice}>
                            $
                            {this.props.auth.user.credit_limit
                              ? this.props.auth.user.credit_limit
                                  .toString()
                                  .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                              : 0}
                          </div>
                        </form>
                      )
                    ) : (
                      <div>
                        <div className={classes.winningBid}>Winning bid</div>
                        <div className={classes.bidPrice}>
                          $
                          {this.state.vehicle.current_bid &&
                            this.state.vehicle.current_bid
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </div>
                      </div>
                    )}

                    {!this.state.timer > 0 &&
                    (this.props.auth.user && this.state.vehicle.user) &&
                    typeof this.state.vehicle.accepted === "undefined"
                      ? this.state.timerAcceptReject > 0 &&
                        this.state.vehicle.current_bid > 1 &&
                        this.props.auth.user.id ===
                          this.state.vehicle.user._id && (
                          <Grid
                            container
                            spacing={40}
                            className={classes.bidAccept}
                          >
                            <Grid item xs={6}>
                              <Button
                                type="button"
                                onClick={this.vehicleAcceptedRejected.bind(
                                  this
                                )}
                                data-accepted={false}
                                variant="contained"
                                className={classes.buttonWhite}
                              >
                                Reject
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                type="button"
                                onClick={this.vehicleAcceptedRejected.bind(
                                  this
                                )}
                                data-accepted={true}
                                variant="contained"
                                className={classes.button}
                              >
                                Accept
                              </Button>
                            </Grid>
                          </Grid>
                        )
                      : null}
                  </Grid>

                  {this.state.vehicle &&
                  this.props.auth.user &&
                  typeof this.state.vehicle.accepted_bid !== "undefined" &&
                  this.state.vehicle.accepted === true ? (
                    this.state.vehicle.user._id === this.props.auth.user.id ? (
                      <Grid
                        container
                        onClick={this.buyerDetails}
                        className={classes.bidderDetails}
                      >
                        <Grid item xs={11}>
                          <div className={classes.bidderSection}>Buyer</div>
                          <div className={classes.bidderName}>
                            Click here to see buyer details
                          </div>
                        </Grid>
                        <Grid item xs={1}>
                          <KeyboardArrowRightIcon />
                        </Grid>
                      </Grid>
                    ) : (
                      this.state.vehicle.statusMessage ===
                        "You've won the auction." && (
                        <Grid
                          container
                          onClick={this.userDetails}
                          className={classes.bidderDetails}
                        >
                          <Grid item xs={11}>
                            <div className={classes.bidderSection}>Seller</div>
                            <div className={classes.bidderName}>
                              {this.state.vehicle.user &&
                                this.state.vehicle.user.name}
                            </div>
                            <div className={classes.bidderName}>
                              Sell through rate
                              <strong>
                                {" "}
                                {this.state.vehicle.user &&
                                  this.state.vehicle.user.sell_rating}
                              </strong>
                            </div>
                          </Grid>
                          <Grid item xs={1}>
                            <KeyboardArrowRightIcon />
                          </Grid>
                        </Grid>
                      )
                    )
                  ) : null}

                  <Grid item xs={12} className={classes.expandCollapse}>
                    <ExpansionPanel>
                      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.expanHeading}>
                          Details
                        </Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails className={classes.panelSection}>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Vin</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.vin}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Year</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.year}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Make</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.make}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Model</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.model}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>
                            Vehicle Type
                          </div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.vehicle_type}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Colour</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.colour}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>
                            Transmission
                          </div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.transmission}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>Engine</div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.engine}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>
                            Registration Expires
                          </div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.registration_expire &&
                              moment(
                                this.state.vehicle.registration_expire
                              ).format("YYYY-MM-DD")}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>
                            WOF Expires
                          </div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.wof_expire &&
                              moment(this.state.vehicle.wof_expire).format(
                                "YYYY-MM-DD"
                              )}
                          </div>
                        </div>
                        <div className={classes.bidderMargin}>
                          <div className={classes.bidderSection}>
                            Additional Details
                          </div>
                          <div className={classes.bidderName}>
                            {this.state.vehicle.add_details}
                          </div>
                        </div>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>

                    {!isEmpty(this.state.vehicleBids) ? (
                      <ExpansionPanel>
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                          <Typography className={classes.expanHeading}>
                            Bid trail
                          </Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails className={classes.panelSection}>
                          {bidTrails}

                          <div className={classes.bidderMargin}>
                            <div className={classes.bidderSectionBid}>
                              Starting Bid $1
                            </div>
                          </div>
                        </ExpansionPanelDetails>
                      </ExpansionPanel>
                    ) : null}
                  </Grid>
                </section>
              </section>
            )}

            {this.state.Msg ? (
              <SnackBar
                open={this.state.Msg}
                duration={5000}
                variant={this.state.MsgVariant}
                className={classes.margin}
                message={this.state.MsgText}
                onClose={this.handleClose}
              />
            ) : null}
          </div>
        </Grid>
      </Grid>
    );
  }
}

VehicleDetails.propTypes = {
  auth: PropTypes.object.isRequired,
  vehicleDetails: PropTypes.object.isRequired,
  vehicleBids: PropTypes.object.isRequired,
  vehicle: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  vehicleDetails: state.vehicleDetails,
  vehicleBids: state.vehicleBids,
  vehicle: state.vehicle,
  errors: state.errors
});

export default compose(
  withStyles(vehicleDetailsStyles),
  connect(
    mapStateToProps,
    {
      getUser,
      getVehicleDetails,
      outBidStatus,
      vehicleAcceptedRejected,
      getVehicleBids,
      expireVehicle,
      vehicleBid,
      getCurrentTimer,
      clearVehicle
    }
  )
)(withRouter(VehicleDetails));
