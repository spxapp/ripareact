import React from "react";    
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { loadProfile, setupProfile3 } from '../actions/profileActions';
import { compose } from 'redux';
import PropTypes from 'prop-types'; 
import CreditCard from '../components/CreditCard';
import isEmpty from '../validation/isEmpty'; 

import withStyles from "@material-ui/core/styles/withStyles";   
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';
import LinearProgress from '@material-ui/core/LinearProgress';
import SnackBar from '../components/common/SnackBar';
 
import CircularProgress from '@material-ui/core/CircularProgress';

import setupProfileStyles from "../assets/jss/pages/setupProfileStyles.jsx";

class SetupProfile3 extends React.Component {

	constructor(){
		super();
		this.state = { 
			loading:false,
			errors: {}
		}
 
	}
 
	componentWillMount(){
	    document.title = "Setup Profile 3 | Ripa - Car App"; 
	}

	componentDidMount(){ 

	    this.props.loadProfile();
	}

	componentWillReceiveProps(nextProps){
		if(nextProps.location.state && !nextProps.location.state.myaccount){
			if(!isEmpty(nextProps.profile))
			{	
				if(nextProps.profile.step === 3){

					if(nextProps.profile.addedCard)
					{ 
						this.props.history.push('/', {addedCard: 1});
					}else{
						this.props.history.push('/');
					}

				}else if(nextProps.profile.step === 1){
					this.props.history.push('/setup-profile-2');
				}else if(nextProps.profile.step === 0){
					this.props.history.push('/setup-profile');
				} else if(nextProps.profile.status === false) {
					this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.profile.error});
				}
			} 
		}
		this.setState({loading:false});
	}
 	
 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (token) => { 

			this.setState({loading:true});
			 const UpdateProfile = {   
				token: token  
			}	 	 

			this.props.setupProfile3(UpdateProfile, this.props.history);
	}

	handleItemError = (error) => {

		this.setState({ Msg:true, MsgVariant: 'error', MsgText: error});
	}

	handleClose = () => {
		 
	   this.setState({ Msg: false });

	};

	render() { 
		const { classes } = this.props;
	  
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div> 
				<header className={classes.header}>
					<h3>Set up your profile</h3>
					<div className={classes.Progress}>
						<LinearProgress 
							color="secondary" 
							variant="determinate" 
							value={90}
						/>
					</div>
				</header>


				 	 
	        	<CreditCard classes={classes} onSubmit={this.onSubmit} onError={this.handleItemError} /> 
				
				<Grid item xs={12}>
		        {
		        this.state.loading ? (
					<div>
				<div>&nbsp;</div>
		        <CircularProgress className={classes.progress} />
				</div>
		        ):
		        null	
		        }
		        </Grid>
		        
				<footer className={classes.footer}>
				 
				</footer>
	    	</div> 
 
	    	</Fade>
	    	{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}
	    	</Grid>
	    	</Grid>

	    );
	}
} 
 

SetupProfile3.propTypes = { 
	profile: PropTypes.object.isRequired, 
	errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	profile: state.profile, 
	errors: state.errors
});

export default compose(
  withStyles(setupProfileStyles),
  connect(mapStateToProps, { loadProfile, setupProfile3 })
)(withRouter(SetupProfile3));