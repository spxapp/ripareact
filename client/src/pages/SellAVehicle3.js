import React from "react";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  sellAVehicle,
  mainImage,
  clearselected,
  clearselectedMob
} from "../actions/vehicleActions";
import { compose } from "redux";
//import classnames from 'classnames';
import CircularProgress from "@material-ui/core/CircularProgress";
import SnackBar from "../components/common/SnackBar";
import { isMobile, isAndroid, isIOS } from "react-device-detect";

import withStyles from "@material-ui/core/styles/withStyles";
import ArrowBack from "@material-ui/icons/ArrowBack";
import CloseIcon from "@material-ui/icons/Close";
import GalleryIcon from "@material-ui/icons/Collections";
import CameraIcon from "@material-ui/icons/CameraAlt";

import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";

import sellAVehicleStyles from "../assets/jss/pages/sellAVehicleStyles.jsx";

class SellAVehicle3 extends React.Component {
  constructor() {
    super();
    this.state = {
      images: "",
      imageUploaded: false,
      imageSize: 0,
      loading: false,
      Msg: false,
      MsgVariant: false,
      MsgText: false,
      vehicle_id: "",
      vehicle:'',
      vehicleImages: [],
      uploadImages: false,
      mainImage: "",
      files: "",
      errors: {}
    };
  }

  componentWillMount() {
    document.title = "Sell A Vehicle | Ripa - Car App";
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }

    if (!this.props.vehicle.sell.step) {
      this.props.history.push("/sell-a-vehicle");
    }

    if (this.props.vehicle.sell.vehicle) {
      this.setState({ vehicle_id: this.props.vehicle.sell.vehicle._id,vehicle:this.props.vehicle.sell.vehicle});

      if (this.props.vehicle.sell.vehicle.images.length) {
        this.setState({
          vehicleImages: this.props.vehicle.sell.vehicle.images,
          uploadImages: true,
          mainImage: this.props.vehicle.sell.vehicle.main_photo
        });
      }
    }

    document.addEventListener(
      "resume",
      () => {
        setTimeout(() => {
          console.log("resuming")
          if (this.props.vehicle.sell.vehicle) {
            this.setState({ vehicle_id: this.props.vehicle.sell.vehicle._id,vehicle:this.props.vehicle.sell.vehicle});
      
            if (this.props.vehicle.sell.vehicle.images.length) {
              this.setState({
                vehicleImages: this.props.vehicle.sell.vehicle.images,
                uploadImages: true,
                mainImage: this.props.vehicle.sell.vehicle.main_photo
              });
            }
          }
        }, 0);
      
      },false)
  }

  getProgress(id) {
      let idx = this.props.vehicle.selectedImages.findIndex(img => img.id === id);
     if (idx >= 0){
       return this.props.vehicle.selectedImages[idx].progress;
     }
  }

   isCompleted() {
        if(this.props.vehicle.selectedImages.length===0){
          return false;
        }
        return this.props.vehicle.selectedImages.filter(img => img.progress === 100)
          .length === this.props.vehicle.selectedImages.length
         ? true
         : false;
    }
  

  componentWillReceiveProps(nextProps) {
    if (nextProps.vehicle.sell.imagesUploaded && this.isCompleted()) {
      this.setState({
        loading: false,
        Msg: false,
        uploadImages: true,
        mainImage: nextProps.vehicle.sell.vehicle.main_photo,
        vehicle:nextProps.vehicle.sell.vehicle,
        vehicleImages: nextProps.vehicle.sell.vehicle.images
      });
    }
  }

  onChange = async e => {
    let files = e.target.files;
    // if(isMobile && isIOS && files.length>8){
    //   this.setState({
    //     Msg: true,
    //     MsgVariant: "error",
    //     MsgText: "Only 8 files can be selected at a time"
    //   });
    //   return false;
    // }
    this.props.clearselected(); 
    let size = 0;
    await Array.from(files).forEach(file => (size += file.size));
    size += this.state.imageSize;
    this.setState({ imageSize: size });
    if (this.state.vehicleImages.length + files.length > 20) {
      this.setState({
        Msg: true,
        MsgVariant: "error",
        MsgText: "Maximum file limit is 20"
      });
      return false;
    } else if (this.state.imageSize >= 200000000) {
      this.setState({
        imageSize: 0,
        Msg: true,
        MsgVariant: "error",
        MsgText: "Total file size should not exceed 200mb. Please try again"
      });
      return false;
    }
    let imageUrls=[]
    Array.from(files).map((file)=>{
      imageUrls.push(URL.createObjectURL(file))
    })
    this.setState({
      loading: true,
      Msg: true,
      MsgVariant: "info",
      MsgText: "Uploading...",
      files:imageUrls
    });
    const vehicleData = {
      step: 3,
      vehicle_id: this.state.vehicle_id,
      images: files
    };
    this.props.sellAVehicle(vehicleData);
  };

  cameraOpenMobile = e => {
    try {
      navigator.camera.cleanup();
      navigator.camera.getPicture( 
         
        imgData => { 
          
        this.props.clearselected();
        this.props.clearselectedMob();
  
          window.resolveLocalFileSystemURL(
            imgData,
            fileEntry => {
  

              fileEntry.file(
                file => {
                  var reader = new FileReader();
                  reader.onloadend = e => {
                    var fd = new FormData();
                    var imgBlob = new Blob([e.target.result], {
                      type: "image/jpeg"
                    });

                    fd.append("images", imgBlob);
                    fd.append("vehicle_id", this.state.vehicle_id);
 
                    let imageurl = URL.createObjectURL(imgBlob)
   
                    this.setState({
                      loading: true,
                      Msg: true,
                      MsgVariant: "info",
                      MsgText: "Uploading...",
                      files: [imageurl]
                    })

                    const vehicleData = {
                      type: "mobile",
                      step: 3,
                      vehicle_id: this.state.vehicle_id,
                      fd: fd,
                    };

                    let size = file.size;
                    size += this.state.imageSize;
                    this.setState({ imageSize: size });
                    if (this.state.vehicleImages.length + 1 > 20) {
                      this.setState({
                        loading: false,
                        Msg: true,
                        MsgVariant: "error",
                        MsgText: "Maximum file limit is 20"
                      });
                      return false;
                    } else if (this.state.imageSize >= 200000000) {
                      this.setState({
                        imageSize: 0,
                        Msg: true,
                        MsgVariant: "error",
                        MsgText:
                          "Total file size should not exceed 200mb. Please try again"
                      });
                      return false;
                    }
                    this.props.sellAVehicle(vehicleData);
                  };
                  reader.readAsArrayBuffer(file);
                },
                function(e) {
                  console.log("error with photo file");
                }
              );
            },
            function(e) {
              console.log("error with photo file 2");
            }
          );
        },
        function() {
          console.log("Error taking picture", "Error");
        }
      );
    } catch (err) {
      this.setState({
        loading: true,
        Msg: true,
        MsgVariant: "error",
        MsgText: err
      });
    }
  };

  onSubmit = e => {
    e.preventDefault();
    const totalImages = this.state.vehicleImages
      ? this.state.vehicleImages.length
      : 0;
    console.log(totalImages);
    if (totalImages < 8) {
      this.setState({
        Msg: true,
        MsgVariant: "warning",
        MsgText: "Minimum 8 photos, include any damage"
      });
    } else {
      this.setState({
        Msg: false
      });
      this.props.history.push("/sell-a-vehicle-4",{
        sellVehicle:this.state.vehicle
      });
    }
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };

  onChangeMainImage = e => {
    let image = e.currentTarget.getAttribute("data-image");

    this.setState({
      loading: true,
      Msg: true,
      MsgVariant: "info",
      MsgText: "Changing Main Image"
    });

    const vehicleData = {
      step: 3,
      vehicle_id: this.state.vehicle_id,
      mainImage: image
    };

    this.props.mainImage(vehicleData);
  };

  render() {
    const { classes } = this.props;
    //const { errors } = this.state;

    const uploadImages = (
      <div>
        <h2 className={classes.pageTitle}>Upload Images</h2>
        <div className={classes.uploadBtn}>
          <input
            accept="image/*"
            className={classes.input}
            style={{ display: "none" }}
            id="upload-pics"
            name="upload_pics"
            type="file"
            multiple
            onChange={this.onChange}
          />
          <label htmlFor="upload-pics">
            <Button
              variant="contained"
              component="span"
              className={classes.upload}
            >
              Upload from device
              <GalleryIcon className={classes.rightIcon} />
            </Button>
          </label>
          <label className={classes.maximumLabel}>
            Maximum of 20 photos can be uploaded
          </label>
        </div>

        <div className={classes.orText}>
          <p>or</p>
        </div>

        {isMobile && (isAndroid || isIOS) && !!window.cordova ? (
          <div className={classes.uploadBtn}>
            <input
              accept="image/*"
              className={classes.input}
              style={{ display: "none" }}
              id="take-a-photoE"
              name="take_a_photo"
              type="file"
              multiple
            />
            <label>
              <Button
                onClick={this.cameraOpenMobile}
                variant="contained"
                component="span"
                className={classes.upload}
              >
                Take a photo
                <CameraIcon className={classes.rightIcon} />
              </Button>
            </label>
          </div>
        ) : (
          <div className={classes.uploadBtn}>
            <input
              accept="image/*"
              className={classes.input}
              style={{ display: "none" }}
              id="take-a-photo"
              name="take_a_photo"
              type="file"
              multiple
              capture="camera"
              onChange={this.onChange}
            />
            <label htmlFor="take-a-photo">
              <Button
                variant="contained"
                component="span"
                className={classes.upload}
              >
                Take a photo
                <CameraIcon className={classes.rightIcon} />
              </Button>
            </label>
          </div>
        )}
      </div>
    );
    

    var uploadingProgress = (
      <div>
        <GridList
          cellHeight={100}
          spacing={5}
          className={classes.gridList}
          style={{justifyContent:'center'}}
          cols={4}
        > 
       {this.state.files &&
         Array.from(this.state.files).map((tile, index) => (
           <GridListTile key={index} cols={1} rows={1}>
             <img src={tile} alt="..." /> 
             <div className={classes.uploadProgress}>
               <div>
                 <CircularProgress
                   className={classes.progress}
                   variant="static"
                   value={this.getProgress(index)}
                   size={30}
                   thickness={6}
                 />
               </div>
             </div>
           </GridListTile>
           ))}
        </GridList>
      </div>
    );

    var reviewImages = (
      <div>
        <h2 className={classes.pageTitle}>Review Images</h2>

        <div className={classes.uploadBtn}>
          <input
            accept="image/*"
            className={classes.input}
            style={{ display: "none" }}
            id="upload-pics"
            name="upload_pics"
            type="file"
            multiple
            onChange={this.onChange}
          />
          <label htmlFor="upload-pics">
            <Button
              variant="contained"
              component="span"
              className={classes.upload}
            >
              Upload more
              <GalleryIcon className={classes.rightIcon} />
            </Button>
          </label>
          <label className={classes.maximumLabel}>
            Maximum of 20 photos can be uploaded
          </label>
        </div>

        <div className={classes.orText}>
          <p>or</p>
        </div>

        {isMobile && (isAndroid || isIOS) && !!window.cordova ? (
          <div className={classes.uploadBtn}>
            <input
              accept="image/*"
              className={classes.input}
              style={{ display: "none" }}
              id="take-a-photoE"
              name="take_a_photo"
              type="file"
              multiple
            />
            <label>
              <Button
                onClick={this.cameraOpenMobile}
                variant="contained"
                component="span"
                className={classes.upload}
              >
                Take a photo
                <CameraIcon className={classes.rightIcon} />
              </Button>
            </label>
          </div>
        ) : (
          <div className={classes.uploadBtn}>
            <input
              accept="image/*"
              className={classes.input}
              style={{ display: "none" }}
              id="take-a-photo"
              name="take_a_photo"
              type="file"
              multiple
              capture="camera"
              onChange={this.onChange}
            />
            <label htmlFor="take-a-photo">
              <Button
                variant="contained"
                component="span"
                className={classes.upload}
              >
                Take a photo
                <CameraIcon className={classes.rightIcon} />
              </Button>
            </label>
          </div>
        )}

        <GridList
          cellHeight={100}
          spacing={5}
          className={classes.gridList}
          cols={4}
        >
          {this.state.vehicleImages.length >= 2 &&
            this.state.vehicleImages.map((tile, index) => (
              <GridListTile key={tile} cols={1} rows={1}>
                <img src={tile} alt="..." />
                <GridListTileBar
                  title=""
                  titlePosition="top"
                  actionIcon={
                    tile === this.state.mainImage ? (
                      <IconButton className={classes.icon}>
                        <CheckBoxIcon />
                      </IconButton>
                    ) : (
                      <IconButton
                        data-image={tile}
                        onClick={this.onChangeMainImage}
                        className={classes.icon}
                      >
                        <CheckBoxOutlineBlankIcon />
                      </IconButton>
                    )
                  }
                  actionPosition="left"
                  className={classes.titleBar}
                />
              </GridListTile>
            ))}

          <GridListTile key="FeaturedImage" cols={4} rows={3}>
            <div className={classes.mainImageContainer}>
              <img
                src={this.state.mainImage}
                alt="..."
                className={classes.mainImage}
              />
              <div
                style={{ backgroundImage: `url(${this.state.mainImage})` }}
                className={classes.mainImageBlur}
              />
            </div>
            <GridListTileBar
              title="Main Image"
              titlePosition="top"
              className={classes.titleBar}
            />
          </GridListTile>
        </GridList>

        <Button type="submit" variant="contained" className={classes.button}>
          Next
        </Button>
      </div>
    );

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={12} md={4}>
          <div>
            <AppBar className={classes.AppBar} position="fixed">
              <Toolbar>
                <IconButton
                  onClick={this.props.history.goBack}
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="Go back"
                >
                  <ArrowBack />
                </IconButton>

                <header className={classes.header}>
                  <h3>Sell a vehicle</h3>
                  <div className={classes.Progress}>
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={60}
                    />
                  </div>
                </header>

                <Link to="/">
                  <IconButton
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="Close"
                  >
                    <CloseIcon />
                  </IconButton>
                </Link>
              </Toolbar>
            </AppBar>

            <section className={classes.section}>
              {this.state.loading ? (
                <div>
                  <Grid item xs={12}>
                    <CircularProgress className={classes.progress} />
                  </Grid>
                  <Grid item xs={12} className={classes.MarginBottom}></Grid>

                  <Grid item xs={12}>
                    {uploadingProgress}
                  </Grid>
                </div>
              ) : (
                <form
                  encType="multipart/form-data"
                  onSubmit={this.onSubmit}
                  className={classes.form}
                >
                  {this.state.uploadImages ? reviewImages : uploadImages}
                </form>
              )}
            </section>

            {this.state.Msg ? (
              <SnackBar
                open={this.state.Msg}
                duration={5000}
                variant={this.state.MsgVariant}
                className={classes.margin}
                message={this.state.MsgText}
                onClose={this.handleClose}
              />
            ) : null}
          </div>
        </Grid>
      </Grid>
    );
  }
}

SellAVehicle3.propTypes = {
  auth: PropTypes.object.isRequired,
  vehicle: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  vehicle: state.vehicle
});

export default compose(
  withStyles(sellAVehicleStyles),
  connect(
    mapStateToProps,
    { sellAVehicle, mainImage, clearselected, clearselectedMob }
  )
)(withRouter(SellAVehicle3));
