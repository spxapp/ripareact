import React from "react"; 
import PropTypes from 'prop-types';
import { Link, withRouter } from "react-router-dom";  
import { connect } from 'react-redux'; 
import { sellAVehicle, getCurrentTimer } from '../actions/vehicleActions'; 
import { loadProfile } from '../actions/profileActions';
import { compose } from 'redux'; 
import CircularProgress from '@material-ui/core/CircularProgress';  
import SnackBar from '../components/common/SnackBar';

import withStyles from "@material-ui/core/styles/withStyles";   
import ArrowBack from '@material-ui/icons/ArrowBack';    
import CloseIcon from '@material-ui/icons/Close';      

import Grid from '@material-ui/core/Grid';  
import LinearProgress from '@material-ui/core/LinearProgress'; 

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton'; 
import Button from '@material-ui/core/Button'; 
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import TextField from '@material-ui/core/TextField'; 
 
import sellAVehicleStyles from "../assets/jss/pages/sellAVehicleStyles.jsx";
 
class SellAVehicle4 extends React.Component {
	
	constructor(){
		super();
		this.state = { 
			images:'',
			vehicle:{},
			add_details:'', 
			errors: {},  
		}
 
	}

	componentWillMount(){
	    document.title = "Sell A Vehicle | Ripa - Car App"; 
	}

	componentDidMount(){
	  	
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}   

		if(this.props.location.state.sellVehicle)
		{	
			this.setState({vehicle: this.props.location.state.sellVehicle});
		}
		
		this.props.loadProfile();	 
	
		
	}  

	componentWillReceiveProps(nextProps){
 			
 		if(nextProps.vehicle.sell.step===4)
 		{
			this.props.history.replace('/', null);
		
			 this.props.getCurrentTimer(nextProps.vehicle.sell.vehicle._id);
		}	
		 
		if(nextProps.errors && Object.keys(nextProps.errors).length !== 0){
			this.setState({ errors: nextProps.errors});

			if(nextProps.errors && Object.keys(nextProps.errors).length !== 0)
			{
				this.setState({loading:false, Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.error});
			}
 
		}
 
	}

 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}
	
	handleClose = () => {
    	 
		this.setState({ Msg: false });

	};

	onSubmit = (e) => {
		e.preventDefault(); 

		this.setState({loading: true});
 		 
 		const vehicleData = {
 			step: 4,
 			add_details: this.state.add_details,
 			vehicle_id : this.state.vehicle._id,
 			customer_id : this.props.profile.customer_id
 		}

 		this.props.sellAVehicle(vehicleData);
	}
 
	render() { 
		const { classes } = this.props;
	 	//const { errors } = this.state;
 

	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>      
	          	

	          	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			            <IconButton onClick={this.props.history.goBack} className={classes.menuButton} color="inherit" aria-label="Go back">
			              <ArrowBack />
			            </IconButton>
			   			
			            <header className={classes.header}>
							<h3>Sell a vehicle</h3>
							<div className={classes.Progress}>
								<LinearProgress 
									color="secondary" 
									variant="determinate" 
									value={80}
								/>
							</div>
						</header> 

			            <Link to="/">
				            <IconButton className={classes.menuButton} color="inherit" aria-label="Close">
				              <CloseIcon />
				            </IconButton>
			            </Link>

			          </Toolbar> 

	        	</AppBar> 

        		<section className={classes.section} style={{overflowX:'hidden'}}>

					<form onSubmit={this.onSubmit} className={classes.form}>					
				 		
				 		<h2 className={classes.pageTitle}>Review detail</h2>
						   
						<GridList cellHeight={100} spacing={5} className={classes.gridList} cols={4}>
				        {
				        	this.state.vehicle.images &&

				        this.state.vehicle.images.map( (tile, index) => (
 
				          <GridListTile key={tile} cols={1} rows={1}>
				            <img src={tile} alt="..." />
				            
				          </GridListTile>
						))}

						<GridListTile key="FeaturedImage" cols={4} rows={2}>
						<div className={classes.mainImageContainer}>
            <img src={this.state.vehicle.main_photo} alt="..." className={classes.mainImage}/>
            <div style={{backgroundImage: `url(${this.state.vehicle.main_photo})`}} className={classes.mainImageBlur}></div>
            </div>
				             
				        </GridListTile>
      					</GridList>

      					<Grid item xs={12} className={classes.vehicleDetails}>
								 
							<div className={classes.listTitle}>
							{ this.state.vehicle.year ? this.state.vehicle.year : ''} {this.state.vehicle.make ? this.state.vehicle.make : '' } {this.state.vehicle.model ? this.state.vehicle.model : ''}
							</div> 
							<div className={classes.listKm}> 
								{ this.state.vehicle.power_train ? this.state.vehicle.power_train : ''} { this.state.vehicle.engine ? this.state.vehicle.engine : ''}
							</div> 
							<div className={classes.listKm}> 
								{ this.state.vehicle.km ? this.state.vehicle.km : ''}
							</div> 
							<div className={classes.listKm}> 
								{ this.state.vehicle.location ? this.state.vehicle.location : ''}
							</div> 
						</Grid> 


 						<Grid className={classes.field}> 
						<TextField
				          id="add-details"
									placeholder="Make sure to include any potential issues that are not shown in the photos, such as abnormal noises or mechanical problems"
									label="Add details"
				          className={classes.textField}
				          type="text"
				          name="add_details" 
				          margin="normal"
				          variant="filled"
				          multiline
         				  rows="6"
         				  rowsMax="6"
				          value={this.state.add_details}
				          onChange={this.onChange}
				        /> 
				        </Grid>


        				<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>


      					<Button
				        type="submit"  
				        variant="contained" 
				        className={classes.button}>
					        List Vehicle
					    </Button>
						

			        </form>
				</section>
								
				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

SellAVehicle4.propTypes = {   
	vehicle: PropTypes.object.isRequired, 
	auth: PropTypes.object.isRequired 
}

// converting state to props
const mapStateToProps = (state) => ({
	profile:state.profile,
	auth: state.auth,
	vehicle: state.vehicle,  
	errors: state.errors   
});

export default compose(
  withStyles(sellAVehicleStyles),
  connect(mapStateToProps, { sellAVehicle, loadProfile, getCurrentTimer })
)(withRouter(SellAVehicle4));