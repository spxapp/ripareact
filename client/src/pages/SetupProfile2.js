import React from "react";   
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { loadProfile, setupProfile1, setupProfile2 } from '../actions/profileActions';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import isEmpty from '../validation/isEmpty'; 

import classnames from 'classnames'; 
import withStyles from "@material-ui/core/styles/withStyles";   
import Button from '@material-ui/core/Button'; 
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';
import LinearProgress from '@material-ui/core/LinearProgress'; 
import CircularProgress from '@material-ui/core/CircularProgress';

import setupProfileStyles from "../assets/jss/pages/setupProfileStyles.jsx";

class SetupProfile2 extends React.Component {

	constructor(){
		super();
		this.state = {
			name: '',
			address: '',
			phone: '',
			company: '', 
			image: '',
			imageURL:'',
			loading:false,
			imageChanged: false,
			errors: {}
		}
 
	}
 
	componentWillMount(){
	    document.title = "Setup Profile 2 | Ripa - Car App";

	    this.props.loadProfile();

	    this.setState({imageURL: this.props.profile.profile_pic});
	}


	componentWillReceiveProps(nextProps){

		if(!isEmpty(nextProps.profile))
		{
			if(nextProps.profile.step === 3){
				this.props.history.push('/');
			}else if(nextProps.profile.step === 2){
				this.props.history.push('/setup-profile-3',{myaccount:false});
			}else if(nextProps.profile.step === 0){
				 this.props.history.push('/setup-profile');
			}
		}
 

		if(nextProps.profile.profile_pic){
			  
			this.setState({imageURL: nextProps.profile.profile_pic})  

		}


		if(nextProps.errors){
			this.setState({errors: nextProps.errors});
		}


		this.setState({loading:false});
	}
 
 	
 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onUploadChange = (e) => {

		e.preventDefault();  

		let reader = new FileReader();
	    let file = e.target.files[0];

	    reader.onloadend = () => {
	      this.setState({
	        image: file,
	        imageURL: reader.result,
	        imageChanged: true
	      });
	    }  

	    reader.readAsDataURL(file);
 
	}

	onSubmit = (e) => {
		e.preventDefault(); 

		this.setState({loading:true});

		if(this.state.imageChanged)
		{
			this.props.setupProfile1(this.state.image);
		}


		const UpdateProfile = {  

			address: this.state.address,
			phone: this.state.phone,
			company: this.state.company

		}	 	


		this.props.setupProfile2(UpdateProfile, this.props.history);
 
	}

	render() { 
		const { classes } = this.props;
	 	const { errors } = this.state;

	 	const profileNoImg = (

			  <Button variant="raised" component="span" className={classes.upload}>
			    <AddIcon />
			  </Button>

		);

		const profileImg = (
 
			  <img 
				  className={classes.profileImg} 
				  alt="..." 
				  src={

				  	this.state.imageURL ? 

				  	this.state.imageURL

				 	:

				 	null

				  }
			   />
			 

		);


	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div>  
				<header className={classes.header}>
					<h3>Set up your profile</h3>
					<div className={classes.Progress}>
						<LinearProgress 
							color="secondary" 
							variant="determinate" 
							value={70}
						/>
					</div>
				</header>
				<section className={classes.section}>

					<form 
						onSubmit={this.onSubmit} 
						className={classes.form}
						method='post' 
	      				encType="multipart/form-data"
      				>	

				 		<div className={classes.uploadBtn}>
							<input
							  accept="image/*"
							  className={classes.input}
							  style={{ display: 'none' }}
							  id="profile-pic"
							  name="profile_pic"
							  type="file" 
				          	  onChange={this.onUploadChange}
							/>
							<label htmlFor="profile-pic">
							 	{this.state.imageURL ? profileImg : profileNoImg}
							</label> 
						</div>
				
						<Grid item xs={12} className={classes.formRow}>
 						 
						<TextField
				          id="address"
				          placeholder="Address"
				          className={classnames(classes.textField, {
								[classes.textError] : errors.address
							})}
				          type="text"
				          name="address" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.address}
				          onChange={this.onChange}
				        />

						{
							errors.address && (
							<span className={classes.errorMsg}>{errors.address}</span>
							)
						}

						</Grid>	


						<Grid item xs={12} className={classes.formRow}> 
				        <TextField
				          id="phone"
				          placeholder="Phone"
				          className={classnames(classes.textField, {
								[classes.textError] : errors.phone
							})}
				          type="text"
				          name="phone" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.phone}
				          onChange={this.onChange}
				        />

						{
							errors.phone && (
							<span className={classes.errorMsg}>{errors.phone}</span>
							)
						}

						</Grid>

						<Grid item xs={12} className={classes.formRow}>

				        <TextField
				          id="company"
				          placeholder="Company"
				          className={classnames(classes.textField, {
							[classes.textError] : errors.company
						  })}
				          type="text"
				          name="company" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.company}
				          onChange={this.onChange}
				        />
	
						{
							errors.company && (
							<span className={classes.errorMsg}>{errors.company}</span>
							)
						} 

						</Grid>

 
 						<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>	

				        <Button 
				        type="submit"   
				        variant="contained" 
				        className={classes.button}>
					        Next
					    </Button>
			        </form>
				</section>
				<footer className={classes.footer}>
				 
				</footer>
	    	</div> 
	    	</Fade>
	    	</Grid>
	    	</Grid>
	    );
	}
} 
 
SetupProfile2.propTypes = { 
	profile: PropTypes.object.isRequired, 
	errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	profile: state.profile, 
	errors: state.errors
});

export default compose(
  withStyles(setupProfileStyles),
  connect(mapStateToProps, { loadProfile, setupProfile1, setupProfile2 })
)(withRouter(SetupProfile2));