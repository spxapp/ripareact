import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';  
import { compose } from 'redux'; 
import { logoutUser } from '../actions/authActions';
import { loadProfile } from '../actions/profileActions';

import withStyles from "@material-ui/core/styles/withStyles";  
import CheckCirlceIcon from '@material-ui/icons/CheckCircle';  
import Grid from '@material-ui/core/Grid';   
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';  

import awaitingApprovalStyles from "../assets/jss/pages/awaitingApprovalStyles.jsx";
 
class AwaitingApproval extends React.Component {
	   
	componentWillMount(){
	    document.title = "Awaiting Approval | Ripa - Car App"; 
	     
	    this.props.loadProfile();
	}

	componentDidMount(){
	  	
	    if (!this.props.auth.isAuthenticated) {
				this.props.history.push("/login");
			}else{

				if(this.props.auth.user.status)
				{ 
					this.props.history.push("/");
					return false;
				} 
	
			}
		
	} 
 	
 	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUser();
		this.props.history.push('/login');
	} 

	componentWillReceiveProps(nextProps){

		if(nextProps.errors){
			if(nextProps.errors==='Unauthorized')
			{
				//this.props.logoutUser();
				//this.props.history.push('/login');
			}
		} 
	}	
 
	render() { 
		const { classes } = this.props;
 
		  	
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>   
 

		    	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			         
			            <header className={classes.header}>
							<h3>Awaiting Approval</h3>
							 
						</header>  

			          </Toolbar> 

	        	</AppBar>   
			  	 
				<section className={classes.section}>
	          		  
					<Grid className={classes.field}>

						{ 
							this.props.history.location.state && this.props.history.location.state.addedCard 
							
						? 

						(	
							<div>
								<label className={classes.awaitingIcon}>
									<CheckCirlceIcon /> 
								</label>

								<label htmlFor="phone">You have successfully added your credit card and your account has been created but awaiting for approval.</label>
							</div>
						)
					 
						:
						
						(
							<label htmlFor="phone">Your account is awaiting authorisation.<br/>We will be in touch very soon.</label>
						)
						
						}

			        </Grid>	

					<Grid className={classes.field}>
					 
		        		<label onClick={this.onLogoutClick.bind(this)} htmlFor="email">Logout</label>

			        </Grid>
 						
 					 
				   </section>
  
 
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AwaitingApproval.propTypes = {  
	logoutUser: PropTypes.func.isRequired,
	profile: PropTypes.object.isRequired, 
	auth: PropTypes.object.isRequired, 
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,  
	profile: state.profile,  
});
 
export default compose(
  withStyles(awaitingApprovalStyles),
  connect(mapStateToProps, { logoutUser, loadProfile })
)(withRouter(AwaitingApproval));