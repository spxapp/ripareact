import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";   
import classnames from 'classnames';
import { connect } from 'react-redux';
import { logoutUser, changePassword } from '../actions/authActions';
import { loadProfile, updateProfile, getBillingInfo,setupProfile1 } from '../actions/profileActions';
import { getTransactionHistory } from '../actions/vehicleActions';
import { compose } from 'redux'; 
import isEmpty from '../validation/isEmpty'; 

import withStyles from "@material-ui/core/styles/withStyles";   
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button'; 
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid'; 
import SwipeableViews from 'react-swipeable-views';
import CircularProgress from '@material-ui/core/CircularProgress';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

import List from '@material-ui/core/List'; 
import ListItem from '@material-ui/core/ListItem';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';  
import Divider from '@material-ui/core/Divider';  
import SnackBar from '../components/common/SnackBar';

import RipaDrawer from '../components/common/RipaDrawer'; 
import RipaAppBar from '../components/common/RipaAppBar';

import myAccountStyles from "../assets/jss/pages/myAccountStyles.jsx";
import carImage from "../assets/img/car_image.png";
import Visa from '../assets/img/visa-card.png';
import Master from '../assets/img/mastercard.png';
import AE from '../assets/img/ae.png';
import Discover from '../assets/img/discover.png';
import UnknownCard from '../assets/img/unknown-card.png';

class MyAccount extends React.Component {
	
	constructor(){
		super();
		this.state = {
			screenLoading:true,
			name: '',
			email:'',
			address: '',
			phone: '',
			company: '', 
			dealer_number: '',
			image: '',
			imageURL:'',
			imageChanged: false,
			tabIndex:0,
			drawer:false,
			billingInfo:false,
			billing_address:true,
			transactionHistoryList:[],
			transactionHistoryLoading:true, 
			buy_rating:false, 
			buy_rating_red:false, 
			sell_rating:false,
			credit_limit:false,
			virtual_credit:false,
			current_password: '',
			new_password: '',
			confirm_password: '',
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			billingCard:UnknownCard,
			errors: {}
		}
 
	}

	componentWillMount(){
	    document.title = "My Account | Ripa - Car App";
	}

	componentDidMount(){
	  	
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}   
 
		this.props.loadProfile();	 
		
		this.props.getTransactionHistory();
		this.props.getBillingInfo({customer_id:this.props.profile.customer_id});
		
	} 

	componentWillReceiveProps(nextProps){
   
		if(!isEmpty(nextProps.profile))
		{	

			if(nextProps.profile.step === 2){
				this.props.history.push('/setup-profile-3');
			}else if(nextProps.profile.step === 1){
				 this.props.history.push('/setup-profile-2');
			}else if(nextProps.profile.step === 0){
				 this.props.history.push('/setup-profile');
			}else if(nextProps.profile.user.status===false)
			{
				 this.props.history.push('/awaiting-approval');
			}

			// if(isEmpty(this.props.billingInfo) && nextProps.profile.customer_id)
			// { 	console.log("=========Billing info")
	    // 		this.props.getBillingInfo({customer_id:nextProps.profile.customer_id});
	    		 
			// } 

		} 

		if(!isEmpty(nextProps.billingInfo))
		{
			this.setState({billingInfo:nextProps.billingInfo[0]});

			if(nextProps.billingInfo[0].brand==='Visa')
			{
				this.setState({billingCard: Visa});
			}
			else if(nextProps.billingInfo[0].brand==='MasterCard')
			{
				this.setState({billingCard: Master});
			}
			else if(nextProps.billingInfo[0].brand==='American Express')
			{
				this.setState({billingCard: AE});
			} 
			else if(nextProps.billingInfo[0].brand==='Discover')
			{
				this.setState({billingCard: Discover});
			} 
		} 

		if(!isEmpty(nextProps.transactionHistory))
		{
			this.setState({transactionHistoryList:nextProps.transactionHistory, transactionHistoryLoading:false});
		} 


		if(nextProps.profile.profile_pic){
			  
			this.setState({
				imageURL: nextProps.profile.profile_pic,
				address: nextProps.profile.address,
				phone: nextProps.profile.phone,
				company: nextProps.profile.company,
				dealer_number: !isEmpty(nextProps.profile.user.dealer_number) ?  nextProps.profile.user.dealer_number : ''
			})  

		}else{
			this.setState({ 
				address: nextProps.profile.address,
				phone: nextProps.profile.phone,
				company: nextProps.profile.company,
				dealer_number: nextProps.profile.user && !isEmpty(nextProps.profile.user.dealer_number) ?  nextProps.profile.user.dealer_number : ''
			})  
		}

		if(nextProps.auth.isAuthenticated){
			this.setState({
				screenLoading:false,
				name: nextProps.auth.user.name,
				email: nextProps.auth.user.email
			})
		}

		if(nextProps.auth.isPasswordChanged)
		{
			this.setState({
				screenLoading:false,
				errors: false,
				Msg:true,
				MsgVariant: 'success', 
				MsgText: 'Password Changed successfully.',
				loading:false
			})
		}

		if(nextProps.profile.user){
			this.setState({ 
				credit_limit:nextProps.profile.user.credit_limit,
				buy_rating: nextProps.profile.user.buy_rating,
				buy_rating_red: nextProps.profile.user.buy_rating_red,
				sell_rating: nextProps.profile.user.sell_rating,
				virtual_credit: nextProps.profile.user.virtual_credit && nextProps.profile.user.virtual_credit,
				email:nextProps.profile.user.email,
				name:nextProps.profile.user.name,
			})  

			if(nextProps.profile.user.account_updated)
			{
				this.setState({
					screenLoading:false,
					errors: false,
					Msg:true,
					MsgVariant: 'success', 
					MsgText: 'Account updated successfully.',
					loading:false
				})
			}
		}
		
		if(nextProps.errors){
			this.setState({errors: nextProps.errors, loading:false});

			if(nextProps.errors.current_password || nextProps.errors.new_password || nextProps.errors.confirm_password)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: 'Please fix the errors above.'});
			}else if(nextProps.errors.name && nextProps.errors.name!=='CastError')
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.name});
			}else if(nextProps.errors.address)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.address});
			}else if(nextProps.errors.phone)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.phone});
			}else if(nextProps.errors.email)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.email});
			}else if(nextProps.errors.company)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.company});
			}else if(nextProps.errors.dealer_number)
			{
				this.setState({ Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.dealer_number});
			}   
		}

	}

	componentWillUnmount = () => {
    this.props.loadProfile();
  };

	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUser();
		this.props.history.push('/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open,
		});
	};

	handleChange = (event, value) => {
		this.setState({ tabIndex: value });
	};

	handleClose = () => {
    	 
		this.setState({ Msg: false });

	};

	handleSwitch = (e) => {

		var check = true; 

		if(this.state[e.target.name])
		{
			check = false;
		}

		this.setState({ [e.target.name] : check });
	};

	handleChangeIndex = index => {
		this.setState({ tabIndex: index });
	}; 

	itemDetails = (e) => {
 		
 		var itemId = e.currentTarget.getAttribute('data-item-id');
		this.props.history.push('/vehicle-details', {
			id:itemId,
			screen: 'my-account'
		})

	}; 

	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (e) => {
		e.preventDefault();

		const updateUser = {
			name: this.state.name,
			address: this.state.address,
			phone: this.state.phone,
			email: this.state.email, 
			company: this.state.company, 
			dealer_number: this.state.dealer_number
		}	 	


		this.props.updateProfile(updateUser);


	}
 

	onUploadChange = (e) => {

		e.preventDefault();  

		let reader = new FileReader();
	    let file = e.target.files[0];

	    reader.onloadend = () => {
	      this.setState({
	        image: file,
	        imageURL: reader.result,
	        imageChanged: true
	      },()=>{
			  if(this.state.imageChanged){
				  this.props.setupProfile1(this.state.image)
			  }
		  });
	    }  

	    reader.readAsDataURL(file);
 
	}

	onSubmitChangePassword = (e) => {

		e.preventDefault();

		this.setState({loading:true});

		const changePass = {
			current_password: this.state.current_password,
			new_password: this.state.new_password,
			confirm_password: this.state.confirm_password
		}

		this.props.changePassword(changePass);

	};
	componentDidUpdate() {
    this.swipeableActions.updateHeight();
}

 
	render() { 
		const { classes } = this.props;
		const { errors } = this.state;  

		const profileNoImg = (

			  <Button variant="raised" component="span" className={classes.upload}>
			    <AddIcon />
			  </Button>

		);

		const profileImg = (
 
			  <img 
				  className={classes.profileImg} 
				  alt="..." 
				  src={

				  	this.state.imageURL ? 

				  	this.state.imageURL

				 	:

				 	null

				  }
			   />
			 
 
		);
		  	
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>   

	    		<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			onLogout={this.onLogoutClick.bind(this)}
	    				
	    		/>

		    	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)} 
		    		onChange={this.handleChange}
		    		tabIndex={this.state.tabIndex}
		    		title="Account" 
		    		tabs={['Profile', 'Change Password', 'Billing Info', 'Transaction History']}

		    	/>  


		    	{ this.state.screenLoading ? 

		    		(
		    			<section className={classes.loadingScreen}>
		    				 
		    				 <CircularProgress className={classes.progress} />	
		    				 <br/><br/>Loading...
		    				  
		    		 	</section>
		    		 )

		    		:

		    		(
			  	 
				<SwipeableViews
		          axis={'x'}
		          index={this.state.tabIndex}
							onChangeIndex={this.handleChangeIndex}
							action={actions => {
								this.swipeableActions = actions;
							}}
							animateHeight
				>
		           
		          <section dir={'x'} className={classes.section}>
	          			<form 
						onSubmit={this.onSubmit} 
						className={classes.form}
						method='post' 
	      				encType="multipart/form-data"
      				>	

				 		<div className={classes.uploadBtn}>
							<input
							  accept="image/*"
							  className={classes.input}
							  style={{ display: 'none' }}
							  id="profile-pic"
							  name="profile_pic"
							  type="file" 
				          	  onChange={this.onUploadChange}
							/>
							<label htmlFor="profile-pic">
							 	{this.state.imageURL ? profileImg : profileNoImg}
							</label> 
						</div>
  						
  						<Grid className={classes.field}>
  						<TextField
				          id="name"
				          placeholder="Name"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.name}
				          )}
				          type="text"
				          name="name" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.name}
				          onChange={this.onChange}
				        />
				        <label htmlFor="name">Name</label> 
								{errors.name && (
				        	<span className={classes.errorMsg}>{errors.name}</span>
				        )}
				        </Grid>
 						
 						<Grid className={classes.field}> 
						<TextField
				          id="address"
				          placeholder="Address"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.address}
				          )}
				          type="text"
				          name="address" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.address}
				          onChange={this.onChange}
				        />
				        <label htmlFor="address">Address</label>
								{errors.address && (
				        	<span className={classes.errorMsg}>{errors.address}</span>
				        )}
				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="phone"
				          placeholder="Phone"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.phone}
				          )}
				          type="text"
				          name="phone" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.phone}
				          onChange={this.onChange}
				        />
				        <label htmlFor="phone">Phone</label>
								{errors.phone && (
				        	<span className={classes.errorMsg}>{errors.phone}</span>
				        )}
				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="email"
				          placeholder="Email"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.email}
				          )}
				          type="text"
				          name="email" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.email}
				          onChange={this.onChange}
				        />
				        <label htmlFor="email">Email</label>
								{errors.email && (
				        	<span className={classes.errorMsg}>{errors.email}</span>
				        )}
				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="company"
				          placeholder="Company"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.company}
				          )}
				          type="text"
				          name="company" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.company}
				          onChange={this.onChange}
				        />
				        <label htmlFor="company">Company</label>
								{errors.company && (
				        	<span className={classes.errorMsg}>{errors.company}</span>
				        )}
				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="dealer_number"
				          placeholder="Dealer number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.dealer_number}
				          )}
				          type="text"
				          name="dealer_number" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.dealer_number}
				          onChange={this.onChange}
				        />
				        <label 	htmlFor="dealer_number">Dealer Number</label>
								{errors.dealer_number && (
				        	<span className={classes.errorMsg}>{errors.dealer_number}</span>
				        )}
				        </Grid>

								<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>
				        <Grid item xs={12}>
				        <Button type="submit" variant="contained" className={classes.button}>
					        Update Profile
					    </Button>
					    </Grid>
    
			        </form>
						
						<section className={classes.sectionRate}>
							<Grid className={classes.fieldRate}>

								<label htmlFor="rate">Sell through rate</label>

							</Grid>

							<Grid className={classes.fieldRateBig}>

								<label htmlFor="rate">{this.state.sell_rating && this.state.sell_rating}</label>

							</Grid>

							<Grid className={classes.fieldRate}>

								<label htmlFor="rate">Buyer rating</label>

							</Grid>

							<Grid className={classes.fieldRateBig}>

								<label htmlFor="rate">{this.state.buy_rating && this.state.buy_rating} {this.state.buy_rating_red && 
								( <span>{this.state.buy_rating_red}</span> )}</label>

							</Grid>

							<Grid className={classes.fieldRate}>

								<label htmlFor="rate">Bidding credit available</label>

							</Grid>

							<Grid className={classes.fieldRateBig}>

								<label htmlFor="rate">$ {this.state.credit_limit && this.state.credit_limit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}</label>

							</Grid>

							<Grid className={classes.fieldRate}>

								<label htmlFor="rate">Virtual Credit</label>

							</Grid>

							<Grid className={classes.fieldRateBig}>

								<label htmlFor="rate">$ {this.state.virtual_credit && (this.state.virtual_credit > 0) ? parseFloat(this.state.virtual_credit).toFixed(2): 0}</label>

							</Grid>
						</section>
				   
				   </section>

				   <section dir={'x'} className={classes.section}>
	          			<form 
						onSubmit={this.onSubmitChangePassword} 
						className={classes.form}
						method='post' 
	      				encType="multipart/form-data"
      				>	  
  
  						<Grid className={classes.field}>
  						<TextField
				          id="current_password"
				          placeholder="Current Password"
				          className={classnames(classes.textField, 
								{ [classes.textError] : errors.current_password}
							)}
				          type="password"
				          name="current_password" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.current_password}
				          onChange={this.onChange}
				        /> 
						<label htmlFor="current_password">Current Password</label>
						{errors.current_password && (
				        	<span className={classes.errorMsg}>{errors.current_password}</span>
				        )}
				        </Grid>
 						
 						<Grid className={classes.field}> 
						<TextField
				          id="new_password"
				          placeholder="New Password"
				          className={classnames(classes.textField, 
							{ [classes.textError] : errors.new_password}
						)}
				          type="password"
				          name="new_password" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.new_password}
				          onChange={this.onChange}
				        />
						
				        <label htmlFor="new_password">New Password</label>
						{errors.new_password && (
				        	<span className={classes.errorMsg}>{errors.new_password}</span>
				        )}
				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="confirm_password"
				          placeholder="Confirm Password"
				          className={classnames(classes.textField, 
							{ [classes.textError] : errors.confirm_password}
						)}
				          type="password"
				          name="confirm_password" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.confirm_password}
				          onChange={this.onChange}
				        />
						
				        <label htmlFor="confirm_password">Confirm Password</label>
						{errors.confirm_password && (
				        	<span className={classes.errorMsg}>{errors.confirm_password}</span>
				        )}
				        </Grid>  

						<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>	

						<Grid item xs={12}>
				        <Button type="submit" variant="contained" className={classes.button}>
					        Change Password
					    </Button>
					    </Grid>
    
			        </form> 
						 
				   </section>


				   <section dir={'x'} className={classes.section}>
		          		

				   	


		          		{
		          			this.state.billingInfo && this.state.billingCard ? 

		          			(
		          			<Grid container>
		          			<Grid item xs={12} className={classes.field}>
								<Typography className={classes.billingInfoHeading} color="inherit" noWrap>
									Payment Info
								</Typography>
		          			</Grid>
		          			<Grid item xs={12} className={classes.field}>
	          				<Toolbar className={classes.notiToolbar} onClick={()=>{	this.props.history.push('/change-card',{myaccount:true});}}>
				            <IconButton onClick={this.props.menuClick} className={classes.notiButton} color="inherit" aria-label="Open drawer">
				               <img src={this.state.billingCard} alt={this.state.billingInfo.brand} />
				            </IconButton>
				            <Typography className={classes.billingInfo} color="inherit" noWrap>
				              **** **** **** {this.state.billingInfo.last4}
				            </Typography>
				             
				            <div className={classes.grow} />
				            <div className={classes.sectionDesktop}>
				             	
				             	<KeyboardArrowRightIcon />
				              
				            </div> 
					        </Toolbar>

					        <Divider />  
					        </Grid> 

					        </Grid>
		          			)

		          			:

		          			(<Grid className={classes.field}>
		          				<Grid item xs={12} className={classes.field}>
									<Typography className={classes.billingInfoHeading} color="inherit" noWrap>
										Payment Info
									</Typography>
			          			</Grid>
			          			<Grid item xs={12} className={classes.field}>
			          			<Toolbar className={classes.notiToolbar}>
					           
					            <Typography className={classes.billingInfo} color="inherit" noWrap>
									No credit card added
								</Typography>
								
						        </Toolbar>
		          				
								</Grid>
								 <Divider />  
		          			 </Grid>	
		          			)
		          		}


		          		<Grid container className={classes.billingAdd}>
		          			<Grid item xs={12} className={classes.field}>
								<Typography className={classes.billingInfoHeading} color="inherit" noWrap>
									Billing Details
								</Typography>
		          			</Grid>
		          			<Grid item xs={12} className={classes.field}>
	          				<Toolbar className={classes.notiToolbar}>
					             
					            <Typography className={classes.billingInfo} color="inherit">
					              Billing address and current address are same
					            </Typography>
					             
					            <div className={classes.grow} />
					            <div className={classes.sectionDesktop}>
					              
					              <Switch
							          checked={this.state.billing_address} 
							          onChange={this.handleSwitch.bind(this)}
							          value="1"
							          name='billing_address'
							          className={classes.notiSwitch}
							        />
					              
					            </div> 
					        </Toolbar>

					        <Divider />  
					        </Grid> 
					        </Grid>


				   </section>

				   <section dir={'x'} className={classes.section}>

					<List className={classes.listItems}>

						{ 

					 this.state.transactionHistoryLoading && !isEmpty(this.state.transactionHistoryList) ? 

		    		(
		    			<section className={classes.loadingScreen}>
		    				 
		    				 <CircularProgress className={classes.progress} />	
		    				 <br/><br/>Loading...
		    				  
		    		 	</section>
		    		 )

		    		:



							!isEmpty(this.state.transactionHistoryList)

							?
							this.state.transactionHistoryList.map( (item, index)=>
							(<ListItem key={index}>   
									
										<Grid container spacing={24}>	
			<Grid item xs={12}>
			<div className={classes.listCurrentBid}>

				{ item.vehicleStatus}

			</div> 
			</Grid>							


			<Grid onClick={this.itemDetails} item xs={4} md={3} lg={4}>
			 
				<img src={item.main_photo ? item.main_photo : "https://ripa.app/static/media/car_image.e29aaa4d.png" } alt="..." />
				 
			</Grid>
			<Grid item xs={8} md={9} lg={8}>
				 
 
			
			<div style={{"font-weight":"500"}}>
					{item.accepted_bid ? `$${item.accepted_bid.bid_price}` : null}
			</div>
			<div className={classes.listTitle} data-item-id={item._id} onClick={this.itemDetails.bind(this)}>
				{item.year} {item.make} {item.model}
			</div>
			<Grid container data-item-id={item._id} onClick={this.itemDetails.bind(this)}> 
				<Grid item xs={11}>
					{item.km && <div className={classes.listKm}>
						{item.km.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} KM
					</div>}
					<div className={classes.listKm}>
						{item.location}
					</div>
				</Grid>
				<Grid item xs={	1}>
					 <KeyboardArrowRightIcon />
				</Grid>  
			</Grid> 
  

					<Divider />
			</Grid>
 
							</Grid> 
								</ListItem> ))
						
						:

						<p>No Transaction History</p>

						}	


					</List>	
				 
		          		 
				   </section>


	             </SwipeableViews> 

	             )
	         }

				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}
				 
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

MyAccount.propTypes = {
	logoutUser: PropTypes.func.isRequired,
	profile: PropTypes.object.isRequired,  
	auth: PropTypes.object.isRequired,   
	errors: PropTypes.object.isRequired,
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,
	profile: state.profile,  
	billingInfo: state.billingInfo,
	transactionHistory: state.transactionHistory,
	errors: state.errors
});

export default compose(
  withStyles(myAccountStyles),
  connect(mapStateToProps, { logoutUser, loadProfile, updateProfile, changePassword,setupProfile1, getBillingInfo, getTransactionHistory })
)(withRouter(MyAccount));