import React from "react";
import {
  expireVehicle,
  activeVehicles,
  vehicleBid,
  outBidStatus,
  getCurrentTimer
} from "../actions/vehicleActions";
import { loginUser } from "../actions/authActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classnames from "classnames";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import isEmpty from "../validation/isEmpty";
import Divider from "@material-ui/core/Divider";
import { socket } from "./../config/socketio-client";
import { Detector } from "react-detect-offline";
import carImage from "../assets/img/car_image.png";
import Keys from "../config/keys";

const socketUrl = Keys.socketUrl;

class AuctionItem extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      timer: 0,
      current_bid: {},
      current_date: "",
      bid_price: "",
      last_bid_prize: "",
      Msg: false,
      MsgVariant: false,
      MsgText: false,
      errors: {},
      statusMessage: false
    };

    this.interval = false;
    let room = "vehicle:" + this.props.item._id;
    let eventTime =
      new Date(this.props.item.date).getTime() + 1000 * 60 * (15 + 60 + 5);
    let currentTime = new Date().getTime();
    let diffTime = (eventTime - currentTime) / 1000;
    if (diffTime >= 0) {
      socket.emit("room", room);
      socket.on("bidUpdate", bid => {
        if (bid.vehicle_id === this.props.item._id) {
          this.setState({
            current_bid: { [this.props.item._id]: bid.bid_price }
          });
          this.props.item.current_bid = bid.bid_price;

          if (bid.user === this.props.auth.user.id) {
            this.state.statusMessage = "You are leading";
          } else if (
            this.state.last_bid_prize > 0 &&
            this.props.auth.user.id !== this.props.item.user
          ) {
            this.state.statusMessage = `You've been outbid`;
          }
        }
        // if (
        //   bid.hasOwnProperty(this.props.item._id) &&
        //   bid[this.props.item._id] !== "" &&
        //   this.props.user.id !== this.state.listing_user
        // ) {
        //   if ((this.props.auth.user.id !== this.props.item.user) && this.props.vehicle.statusMessage) {
        //     if (this.state.last_bid_prize >= bid[this.props.item._id]) {
        //       this.state.statusMessage = "You are leading";
        //     }else {
        //       this.state.statusMessage = `You've been outbid`;
        //     }
        //   }
        // }

        // if (!isEmpty(bid[this.props.item._id]) && isEmpty(this.state.errors)) {
        //   this.setState({
        //     current_bid: bid
        //   });
        // }
      });

      socket.on("timer", data => {
        if (data.vehicle_id === this.props.item._id) {
          this.setState({ timer: data.time });
          if (data.time <= 0) {
            let expiry_date = new Date();
            var UpdateVehicle = {
              vehicle_id: this.props.item._id,
              expiry_date: expiry_date
            };
            this.setState({ timer: 0 });
            this.props.expireVehicle(this.props.item);
          }
        }
      });

      socket.on("bidAccept", data => {
        if (data.vehicle_id === this.props.item._id) {
          if (data.user === this.props.auth.user.id) {
            this.setState({ statusMessage: data.message });
          }
        }
      });
    }

    window.onfocus = () => {
      // var vehicleData = {
      //   vehicle_id: this.props.item._id,
      //   current_bid: this.props.item.current_bid,
      //   listing_user: this.props.item.user
      // };
      // this.bidEndsIn();
      // this.props.outBidStatus(vehicleData);
    };
  }

  componentDidMount = () => {
    this.setState({
      current_bid: { [this.props.item._id]: this.props.item.current_bid }
    });
    // let room = "vehicle:" + this.props.item._id;
    // console.log("rooom ecist -----",socket.adapter.rooms[room])
    var vehicleData = {
      vehicle_id: this.props.item._id,
      current_bid: this.props.item.current_bid,
      listing_user: this.props.item.user
    };

    if (this.props.item.active) {
      this.setState({ timer: "loading" });

      // this.props.getCurrentTimer(this.props.item._id);
    }
    this.props.outBidStatus(vehicleData);
    // this.bidEndsIn();

    document.addEventListener(
      "resume",
      () => {
        setTimeout(() => {
          this.setState({
            current_bid: { [this.props.item._id]: this.props.item.current_bid }
          });

          var vehicleData = {
            vehicle_id: this.props.item._id,
            current_bid: this.props.item.current_bid,
            listing_user: this.props.item.user
          };
          if (this.props.item) {
            let eventTime =
              new Date(this.props.item.date).getTime() +
              1000 * 60 * (15 + 60 + 5);
            let currentTime = new Date().getTime();
            let diffTime = (eventTime - currentTime) / 1000;
            if (diffTime >= 0 && this.props.item.active) {
              // this.bidEndsIn();
              this.setState({ timer: "loading" });
              let room = "vehicle:" + this.props.item._id;
              socket.emit("room", room);
            }
          }
          this.props.outBidStatus(vehicleData);
        }, 1000);
      },
      false
    );
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.vehicle.allActive.length != this.props.vehicle.allActive.length
    ) {
      var vehicleData = {
        vehicle_id: this.props.item._id,
        current_bid: this.props.item.current_bid,
        listing_user: this.props.item.user
      };
      this.setState({
        current_bid: { [this.props.item._id]: this.props.item.current_bid }
      });
      this.props.outBidStatus(vehicleData);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vehicle.bidPlaced) {
      socket.emit("bidUpdate", {
        [this.props.item._id]: this.state.bid_price
      });

      this.setState({ errors: {} });

      this.setState({ bid_price: "" });
    } else if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });

      if (nextProps.errors.bid_price) {
        //this.setState({Msg:true, MsgVariant: 'error', MsgText: nextProps.errors.bid_price});
        this.props.itemError(nextProps.errors.bid_price);
      }
    }

    if (
      nextProps.vehicle.statusMessage &&
      nextProps.auth.user.id !== this.props.item.user
    ) {
      if (nextProps.vehicle.statusMessage._id === this.props.item._id) {
        this.setState({
          statusMessage: nextProps.vehicle.statusMessage.message
        });
      }
    }
  }

  itemDetails = () => {
    this.props.history.push("/vehicle-details", {
      id: this.props.item._id,
      active: this.props.item.active,
      expiry_date: this.props.item.expiry_date
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onBid = e => {
    e.preventDefault();

    var UpdateVehicle = {
      vehicle_id: this.props.item._id,
      bid_price: this.state.bid_price,
      userTo: this.props.item.user
    };
    if (this.state.bid_price > this.props.item.current_bid) {
      this.props.itemSuccess();
    }

    this.setState({ last_bid_prize: this.state.bid_price });

    this.props.vehicleBid(UpdateVehicle);

    // this.props.socket.emit("liveAuction");
  };

  bidEndsIn = () => {
    clearInterval(this.interval);

    var dtstr = this.props.item.date;

    // replace anything but numbers by spaces
    dtstr = dtstr.replace(/\D/g, " ");

    // trim any hanging white space
    dtstr = dtstr.replace(/\s+$/, "");

    // split on space
    var dtcomps = dtstr.split(" ");

    // not all ISO 8601 dates can convert, as is
    // unless month and date specified, invalid
    if (dtcomps.length < 3) return "invalid date";
    // if time not provided, set to zero
    if (dtcomps.length < 4) {
      dtcomps[3] = 0;
      dtcomps[4] = 0;
      dtcomps[5] = 0;
    }

    // modify month between 1 based ISO 8601 and zero based Date
    dtcomps[1]--;

    var convdt = new Date(
      Date.UTC(
        dtcomps[0],
        dtcomps[1],
        dtcomps[2],
        dtcomps[3],
        dtcomps[4],
        dtcomps[5]
      )
    );
    var curDate = new Date();

    var seconds = (curDate.getTime() - convdt.getTime()) / 1000;

    var one_hour = 3600;

    if (seconds >= one_hour || seconds <= 0) {
      seconds = 0;
    } else {
      seconds = one_hour - seconds;
    }

    var totalSeconds = parseInt(seconds, 10);

    //totalSeconds = 10;

    if (totalSeconds <= 0) {
      if (this.props.item.active === true) {
        // add one hour to the vehicle dates
        var expiry_date = new Date(
          convdt.getTime() + 1000 * (0 + 60 * (0 + 60 * (1 + 24 * 0)))
        );

        var UpdateVehicle = {
          vehicle_id: this.props.item._id,
          expiry_date: expiry_date
        };
        this.props.expireVehicle(UpdateVehicle);

        totalSeconds = 0;
      }
    } else {
      if (this._isMounted) {
        this.setState({ timer: totalSeconds });
      }
      this.interval = setInterval(() => {
        if (totalSeconds <= 0) {
          clearInterval(this.interval);

          // add one hour to the vehicle dates
          var expiry_date = new Date(
            convdt.getTime() + 1000 * (0 + 60 * (0 + 60 * (1 + 24 * 0)))
          );

          var UpdateVehicle = {
            vehicle_id: this.props.item._id,
            expiry_date: expiry_date
          };
          if (this._isMounted) {
            this.setState({ timer: 0 });
          }
          //this.props.onAuctionend(this.props.index);
          this.props.expireVehicle(UpdateVehicle);
          //this.props.activeVehicles();
        }
        // else {
        if (this._isMounted) {
          this.setState({ timer: totalSeconds });
        }
        // send notification for 5 minutes left
        //console.log(totalSeconds);
        if (totalSeconds === 300) {
          console.log("5 mins left");

          var vehicleId = {
            vehicle_id: this.props.item._id
          };

          this.props.sendFiveMinsNotification(vehicleId);
        }

        // }

        totalSeconds--;
      }, 1000);

      if (totalSeconds <= 0) {
        clearInterval(this.interval);
      }
    }
  };

  secondsToHms = d => {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);

    var hDisplay = h > 0 ? ("0" + h).slice(-2) : "00";
    var mDisplay = m > 0 ? ("0" + m).slice(-2) : "00";
    var sDisplay = s > 0 ? ("0" + s).slice(-2) : "00";

    return hDisplay + ":" + mDisplay + ":" + sDisplay;
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };

  connectionChange = connect => {
    console.log("connection changed");
    if (connect) {
      this.setState({
        current_bid: { [this.props.item._id]: this.props.item.current_bid }
      });

      var vehicleData = {
        vehicle_id: this.props.item._id,
        current_bid: this.props.item.current_bid,
        listing_user: this.props.item.user
      };
      if (this.props.item) {
        if (this.props.item.expired !== true) {
          this.setState({ timer: "loading" });
          let room = "vehicle:" + this.props.item._id;
          socket.emit("room", room);
        }
      }
      this.props.outBidStatus(vehicleData);
    } else {
      this.setState({ timer: "loading" });
    }
  };

  render() {
    const { item, classes } = this.props;
    const { errors } = this.state;

    return (
      <Grid container spacing={24}>
        <Grid
          itemID={item._id}
          onClick={this.itemDetails}
          item
          xs={this.props.type === "grid" ? 12 : 4}
          md={this.props.type === "grid" ? 12 : 3}
          lg={this.props.type === "grid" ? 12 : 4}
        >
          <div className={classes.mainImageContainer}>
            <div style={{ zIndex: 100, height: "100%", display: "flex" }}>
              <img
                src={
                  item.main_photo
                    ? item.main_photo
                    : "https://ripa.app/static/media/car_image.e29aaa4d.png"
                }
                alt="..."
                className={classes.mainImage}
              />
            </div>
            <div
              style={{
                backgroundImage: `url(${
                  item.main_photo
                    ? item.main_photo
                    : "https://ripa.app/static/media/car_image.e29aaa4d.png"
                })`
              }}
              className={classes.mainImageBlur}
            />
          </div>
          {/* <img src={item.main_photo ? item.main_photo : "https://ripa.app/static/media/car_image.e29aaa4d.png"} alt="..." /> */}
        </Grid>
        <Detector
          render={({ online }) => <span></span>}
          onChange={connect => this.connectionChange(connect)}
        />
        <Grid
          item
          xs={this.props.type === "grid" ? 12 : 8}
          md={9}
          lg={8}
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-end"
          }}
        >
          {this.state.timer ? (
            <div
              className={classes.listCurrentBid}
              itemID={item._id}
              onClick={this.itemDetails}
            >
              Current bid $
              {!isEmpty(this.state.current_bid[item._id])
                ? this.state.current_bid[item._id]
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                : 1}
            </div>
          ) : (
            <div
              className={classes.listCurrentBid}
              itemID={item._id}
              onClick={this.itemDetails}
            >
              Winning bid $
              {!isEmpty(this.state.current_bid[item._id])
                ? this.state.current_bid[item._id]
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                : 1}
            </div>
          )}

          <div
            className={classes.listTitle}
            itemID={item._id}
            onClick={this.itemDetails}
          >
            {item.year} {item.make} {item.model}
          </div>

          <Grid container itemID={item._id} onClick={this.itemDetails}>
            <Grid item xs={11}>
              {item.km && (
                <div className={classes.listKm}>
                  {Math.round(item.km)
                    .toString()
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}{" "}
                  KM
                </div>
              )}
              <div className={classes.listKm}>{item.location}</div>
            </Grid>
            <Grid item xs={1}>
              <KeyboardArrowRightIcon />
            </Grid>
          </Grid>

          <div className={classes.listEnd}>
            {this.state.timer === "loading" ? (
              <CircularProgress
                className={classes.progress}
                size={15}
                color="secondary"
              />
            ) : this.state.timer > 0 ? (
              "Ends in " + this.secondsToHms(this.state.timer)
            ) : (
              "Ended"
            )}
          </div>

          {this.state.timer
            ? this.props.auth.user.id !== item.user && (
                <form onSubmit={this.onBid.bind(this)}>
                  <Grid container>
                    <Grid item xs={this.props.type === "grid" ? 12 : 8}>
                      <TextField
                        id="bid_price"
                        className={classnames(classes.textField, {
                          [classes.textError]:
                            item._id === errors.vehicle_id && errors.bid_price
                        })}
                        placeholder="Enter Bid"
                        disabled={
                          this.state.timer > 0 || this.state.timer !== "loading"
                            ? false
                            : true
                        }
                        type="number"
                        name="bid_price"
                        margin="normal"
                        variant="filled"
                        onChange={this.onChange}
                        value={this.state.bid_price}
                      />
                    </Grid>
                    <Grid item xs={this.props.type === "grid" ? 12 : 4}>
                      <Button
                        type="submit"
                        variant="contained"
                        className={classes.button}
                      >
                        Bid
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              )
            : null}

          {this.state.statusMessage && (
            <div className={classes.statusMessage}>
              {this.state.statusMessage}
            </div>
          )}

          <Divider />
        </Grid>
      </Grid>
    );
  }
}

AuctionItem.propTypes = {
  vehicle: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  vehicle: state.vehicle,
  errors: state.errors,
  user: state.auth.user
});

export default connect(
  mapStateToProps,
  {
    expireVehicle,
    activeVehicles,
    vehicleBid,
    outBidStatus,
    loginUser,
    getCurrentTimer
  }
)(AuctionItem);
