import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";
import { loadProfile, saveNotificationsToken } from "../actions/profileActions";
import {
  sellAVehicle,
  activeVehicles,
  myListingVehicles,
  myBidsVehicles,
  VehiclesFilter,
  sendFiveMinsNotification
} from "../actions/vehicleActions";
import { compose } from "redux";
import isEmpty from "../validation/isEmpty";
import { isMobile, isAndroid, isIOS } from "react-device-detect";
import AuctionItem from "./AuctionItem";
import { AskForPermissionAndGenerateToken } from "../push_notifications";
import { socket } from "./../config/socketio-client";

import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import SwipeableViews from "react-swipeable-views";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ViewListIcon from "@material-ui/icons/ViewList";
import RefreshIcon from "@material-ui/icons/Refresh";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import TuneIcon from "@material-ui/icons/Tune";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import SnackBar from "../components/common/SnackBar";
import CircularProgress from "@material-ui/core/CircularProgress";

import RipaDrawer from "../components/common/RipaDrawer";
import RipaAppBar from "../components/common/RipaAppBar";

import liveAuctionStyles from "../assets/jss/pages/liveAuctionStyles.jsx";

import FilterPopup from "../components/common/FilterPopup";

import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import { allowUserNotifications } from "../actions/authActions";
import { Detector } from "react-detect-offline";

import Keys from "../config/keys";
import zIndex from "@material-ui/core/styles/zIndex";

const socketUrl = Keys.socketUrl;

window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
FilterPopup.propTypes = {
  onClose: PropTypes.func,
  filterData: PropTypes.object
};

class LiveAuctions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      screenLoading: true,
      name: "",
      email: "",
      address: "",
      phone: "",
      company: "",
      dealernumber: "",
      image: "",
      imageURL: "",
      imageChanged: false,
      tabIndex: 0,
      drawer: false,
      viewTypeAll: "list",
      viewTypeMy: "grid",
      openPopup: false,
      activeVehicles: [],
      filterData: {
        vehicleType: "",
        Mileage: "",
        Make: [],
        Model: "",
        Year: "",
        YearEnd: "",
        PriceRange: "",
        nznew: ""
      },
      Msg: false,
      items: {},
      timer: false,
      MsgVariant: false,
      MsgText: false,
      timestamp: "no timestamp yet",
      liveAuctions: false,
      myListing: false,
      myBids: false,
      errors: {},
      openNotificationsPopup: false,
      isOnline: true
    };

    // socket.on("liveAuction", () => {
    //   this.props.activeVehicles();
    // });
  }

  componentWillMount() {
    document.title = "Auctions | Ripa - Car App";
  }

  componentWillUnmount() {
    this.props.activeVehicles();
  }

  updateOnlineStatus = event => {
    let connection = navigator.onLine ? "online" : "offline";
    if (connection === "offline") {
      this.setState({ isOnline: false });
    } else {
      this.setState({ isOnline: true }, () => {
        this.handleRefreshLiveAuctions();
      });
    }
  };

  componentDidMount() {
    this.updateOnlineStatus();
    window.addEventListener("online", this.updateOnlineStatus);
    window.addEventListener("offline", this.updateOnlineStatus);

    socket.on("liveAuction", () => {
      this.props.activeVehicles();
    });
    if (isMobile && !isEmpty(window.FirebasePlugin) && (isAndroid || isIOS)) {
      window.FirebasePlugin.onNotificationOpen(
        notification => {
          let status = notification;
          if (status.tap) {
            if (status.body === "outbid") {
              this.props.history.push("/vehicle-details", {
                id: status.vehicle,
                active: true,
                type: "notification"
              });
            }
            if (status.body === "new_vehicle") {
              this.props.history.push("/vehicle-details", {
                id: status.vehicle,
                active: true,
                type: "notification"
              });
            }
            if (status.body === "seller_details") {
              this.props.history.push(`/user-details/${status.user}/Seller`);
            }
            if (status.body === "expired") {
              this.props.history.push("/vehicle-details", {
                id: status.vehicle,
                active: false,
                type: "notification",
                expiry_date: status.expiry
              });
            }
          }
        },
        function(error) {
          console.error(error);
        }
      );
    }
    let notification = window.localStorage.getItem("notification");
    if (this.props.auth && !isEmpty(this.props.auth.user)) {
      if (
        !notification &&
        (isEmpty(this.props.auth.user.notifications) ||
          this.props.auth.user.notifications === false)
      ) {
        this.setState({ openNotificationsPopup: true });
      }
    }

    document.addEventListener(
      "resume",
      () => {
        // if (!this.props.auth.isAuthenticated) {
        // 	this.props.history.push("/login");
        // }else{

        // 	if(!this.props.auth.user.status)
        // 	{
        // 		this.props.history.push("/awaiting-approval");
        // 		return false;
        // 	}

        // }
        setTimeout(() => {
          this.props.loadProfile();
          this.props.activeVehicles();
          this.props.myListingVehicles();
          this.props.myBidsVehicles();
          if (
            isMobile &&
            !isEmpty(window.FirebasePlugin) &&
            (isAndroid || isIOS)
          ) {
            if (isIOS) {
              window.FirebasePlugin.grantPermission();
            }
            window.FirebasePlugin.onTokenRefresh(token => {
              // save this server-side and use it to push notifications to this
              if (token) {
                let tokenData = {};
                if (isIOS) {
                  tokenData = {
                    token: token,
                    type: "ios"
                  };
                } else if (isAndroid) {
                  tokenData = {
                    token: token,
                    type: "android"
                  };
                }
                this.props.saveNotificationsToken(tokenData);
              }
            });
          }
        }, 1000);
      },
      false
    );

    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
      return false;
    } else {
      if (!this.props.auth.user.status) {
        this.props.history.push("/awaiting-approval");
        return false;
      }
    }

    this.props.loadProfile();
    this.props.activeVehicles();
    this.props.myListingVehicles();
    this.props.myBidsVehicles();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.vehicle.allActive) {
      this.setState({
        activeVehicles: nextProps.vehicle.allActive,
        screenLoading: false
      });
    }
    if (!isEmpty(nextProps.profile)) {
      if (
        !isEmpty(nextProps.profile.user) &&
        nextProps.profile.user.status === false
      ) {
        if (
          !isEmpty(this.props.history.location.state) &&
          !isEmpty(this.props.history.location.state.addedCard)
        ) {
          this.props.history.push("/awaiting-approval", { addedCard: 1 });
        } else {
          this.props.history.push("/awaiting-approval");
        }
      } else if (
        !isEmpty(nextProps.profile.user) &&
        nextProps.profile.user.suspended === true
      ) {
        this.props.history.push("/account-suspended");
      } else if (nextProps.profile.step === 2) {
        this.props.history.push("/setup-profile-3");
      } else if (nextProps.profile.step === 1) {
        this.props.history.push("/setup-profile-2");
      } else if (nextProps.profile.step === 0) {
        this.props.history.push("/setup-profile");
      }
      if (this.props.location.search !== "") {
        let redirectUrl = this.props.location.search.replace("?redirect=", "");
        this.props.history.push("/auction-status?" + redirectUrl);
      }

      if (
        nextProps.profile.user &&
        (isEmpty(nextProps.profile.user.isSavedSearch) ||
          nextProps.profile.user.isSavedSearch === false) &&
        !nextProps.savedSearches.saved_search
      ) {
        this.props.history.push("/settings", {
          tabIndex: 1,
          msg: "Please add search filters for notifications."
        });
      }
    }

    if (nextProps.profile.profile_pic) {
      this.setState({
        imageURL: nextProps.profile.profile_pic,
        address: nextProps.profile.address,
        phone: nextProps.profile.phone,
        company: nextProps.profile.company,
        dealernumber: nextProps.profile.dealer_number
      });
    }

    if (nextProps.auth.isAuthenticated) {
      this.setState({
        name: nextProps.auth.user.name,
        email: nextProps.auth.user.email
      });
    }

    if (nextProps.vehicle && nextProps.vehicle.sell.vehicleListedMsg) {
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText: "Your auctioned vehicle is now live!"
      });

      socket.emit("liveAuction");
    }

    if (nextProps.vehicle.allActive) {
      this.setState({ screenLoading: false, liveAuctions: true });
    }

    if (nextProps.vehicle.myListing) {
      this.setState({ myListing: true });
    }

    if (nextProps.vehicle.myBids) {
      this.setState({ myBids: true });
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  filterVehicles = () => {
    this.setState({ screenLoading: true });
    this.props.VehiclesFilter(this.state.filterData, this.state.tabIndex);
  };

  resetFilters = () => {
    var resetData = {
      vehicleType: "",
      Mileage: "",
      Make: [],
      Model: "",
      Year: "",
      YearEnd: "",
      PriceRange: "",
      nznew: ""
    };

    this.setState({ filterData: resetData });
  };

  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser();
    this.props.history.push("/login");
  }

  onAuctionEntries(e) {
    e.preventDefault();
    this.props.history.push("/my-auction-entires", { tabIndex: 1 });
  }

  toggleDrawer = open => () => {
    this.setState({
      drawer: open
    });
  };

  handleChange = (event, value) => {
    this.setState({ tabIndex: value });

    if (value === 2) {
      this.props.myBidsVehicles();
    }
  };

  handleChangeIndex = index => {
    this.setState({ tabIndex: index });
  };

  hangeChangeViewTypeAll = e => {
    e.preventDefault();

    if (e.currentTarget.getAttribute("viewtype") !== null) {
      this.setState({ viewTypeAll: e.currentTarget.getAttribute("viewtype") });
    }
  };

  hangeChangeViewTypeMy = e => {
    e.preventDefault();

    if (e.currentTarget.getAttribute("viewtype") !== null) {
      this.setState({ viewTypeMy: e.currentTarget.getAttribute("viewtype") });
    }
  };

  handlePopupOpen = () => {
    this.setState({ openPopup: true });
  };

  handlePopupClose = filterData => {
    this.setState({ filterData, openPopup: false });
  };

  handleItemError = error => {
    this.setState({ Msg: true, MsgVariant: "error", MsgText: error });
  };

  handleitemSuccess = () => {
    this.setState({ Msg: true, MsgVariant: "success", MsgText: "Bid added!" });
  };

  handleRefreshLiveAuctions = () => {
    this.setState({ screenLoading: true });
    this.props.activeVehicles();
    // this.props.myListingVehicles();
    // this.props.myBidsVehicles();
  };

  handleRefreshMyListing = () => {
    this.setState({ screenLoading: true });
    this.props.myListingVehicles();
    // this.props.myBidsVehicles();
  };

  handleRefreshMyBids = () => {
    this.setState({ screenLoading: true });
    this.props.myBidsVehicles();
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };
  onAuctionend = index => {
    let vehicles = this.state.activeVehicles.splice(index, 1);
    this.setState({ activeVehicles: vehicles });
  };
  itemDetails = e => {
    e.preventDefault();
    let itemId = e.currentTarget.getAttribute("itemID");
    //let itemTimerVar = e.currentTarget.getAttribute('itemtimer');
    //console.log(itemTimerVar)
    if (itemId) {
      this.props.history.push("/vehicle-details", {
        id: itemId
      });
    }
  };

  handleNotificationPopup = async e => {
    const value = e.currentTarget.getAttribute("data-value");
    await window.localStorage.setItem("notification", true);
    this.setState({ openNotificationsPopup: false });

    const notifications = {
      notifications: value
    };

    this.props.allowUserNotifications(notifications);

    if (value === "true") {
      if (isMobile && !isEmpty(window.FirebasePlugin) && (isAndroid || isIOS)) {
        if (isIOS) {
          window.FirebasePlugin.grantPermission();
        }
        window.FirebasePlugin.onTokenRefresh(token => {
          // save this server-side and use it to push notifications to this

          if (token) {
            let tokenData = {};
            if (isIOS) {
              tokenData = {
                token: token,
                type: "ios"
              };
            } else if (isAndroid) {
              tokenData = {
                token: token,
                type: "android"
              };
            }
            this.props.saveNotificationsToken(tokenData);
          }
        });
      } else {
        //subscribe user to the push notifications

        AskForPermissionAndGenerateToken().then(token => {
          if (token) {
            const tokenData = {
              token: token,
              type: "web"
            };

            this.props.saveNotificationsToken(tokenData);
          }
        });
      }
    }
  };

  connectionChange = connect => {
    if (connect) {
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText: "Back to online"
      });
      return false;
    }
    this.setState({
      Msg: true,
      MsgVariant: "error",
      MsgText: "No internet connection"
    });
  };

  render() {
    const { classes } = this.props;
    const { openNotificationsPopup } = this.state;

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={12} md={12}>
          <Detector
            render={({ online }) => <span></span>}
            onChange={connect => this.connectionChange(connect)}
          />
          <div>
            <RipaDrawer
              status={this.state.drawer}
              onOpen={this.toggleDrawer(true)}
              onClose={this.toggleDrawer(false)}
              userProfile={this.state.imageURL}
              userEmail={this.state.email}
              onLogout={this.onLogoutClick.bind(this)}
              onAuctionEntries={this.onAuctionEntries.bind(this)}
            />

            <RipaAppBar
              menuClick={this.toggleDrawer(true)}
              onChange={this.handleChange}
              tabIndex={this.state.tabIndex}
              title="Auctions"
              fullWidth
              tabs={["Live", "My Listings", "My Bids"]}
            />
            <Dialog
              fullWidth={true}
              open={openNotificationsPopup}
              aria-labelledby="auction-filter"
            >
              <DialogContent>
                <DialogContentText>
                  Would you like to receive notifications from Ripa?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={this.handleNotificationPopup}
                  data-value={true}
                  className={this.props.classes.linkButtonCancel}
                  color="primary"
                >
                  Yes
                </Button>
                <Button
                  onClick={this.handleNotificationPopup}
                  data-value={false}
                  className={this.props.classes.linkButton}
                  color="primary"
                >
                  No
                </Button>
              </DialogActions>
            </Dialog>

            {this.state.screenLoading ? (
              <section className={classes.loadingScreen}>
                <CircularProgress className={classes.progress} />
                <br />
                <br />
                Loading...
              </section>
            ) : (
              <SwipeableViews
                axis={"x"}
                index={this.state.tabIndex}
                onChangeIndex={this.handleChangeIndex}
                className={classes.swipe}
              >
                {/* LIVE AUCTIONS */}

                <section
                  dir={"x"}
                  style={{
                    filter: this.state.isOnline ? "none" : "grayscale(1)",
                    position: "relative"
                  }}
                  className={classes.section}
                >
                  {this.state.isOnline ? null : (
                    <div
                      style={{
                        position: "absolute",
                        zIndex: "999",
                        left: "0",
                        right: "0",
                        top: "0",
                        bottom: "0"
                      }}
                    >
                    </div>
                  )}
                  <Toolbar className={classes.filterBar}>
                    <IconButton
                      className={
                        this.state.viewTypeAll === "list"
                          ? classes.ActiveFilter
                          : null
                      }
                      onClick={this.hangeChangeViewTypeAll}
                      viewtype="list"
                      color="inherit"
                    >
                      <ViewListIcon />
                    </IconButton>

                    <IconButton
                      className={
                        this.state.viewTypeAll === "grid"
                          ? classes.ActiveFilter
                          : null
                      }
                      onClick={this.hangeChangeViewTypeAll}
                      viewtype="grid"
                      color="inherit"
                    >
                      <ViewModuleIcon />
                    </IconButton>

                    <div className={classes.grow} />

                    <Typography
                      className={classes.title}
                      color="inherit"
                      noWrap
                    >
                      Refresh
                    </Typography>

                    <IconButton
                      onClick={this.handleRefreshLiveAuctions}
                      color="inherit"
                    >
                      <RefreshIcon />
                    </IconButton>

                    <Typography
                      className={classes.title}
                      color="inherit"
                      noWrap
                    >
                      Filter
                    </Typography>

                    <IconButton onClick={this.handlePopupOpen} color="inherit">
                      <TuneIcon />
                    </IconButton>
                  </Toolbar>

                  {this.state.viewTypeAll === "grid" ? (
                    <Grid container spacing={24} className={classes.gridItems}>
                      {this.state.liveAuctions &&
                      !isEmpty(this.state.activeVehicles) ? (
                        this.props.vehicle.allActive.map((item, index) => (
                          <Grid key={item._id} item xs={6}>
                            <ListItem key={item._id}>
                              <AuctionItem
                                item={item}
                                index={index}
                                auth={this.props.auth}
                                classes={classes}
                                type="grid"
                                history={this.props.history}
                                itemError={this.handleItemError}
                                itemSuccess={this.handleitemSuccess}
                                socket={socket}
                                tabIndex={this.state.tabIndex}
                                updateLiveAuction={this.props.activeVehicles}
                                onAuctionend={this.onAuctionend}
                                sendFiveMinsNotification={
                                  this.props.sendFiveMinsNotification
                                }
                              />
                            </ListItem>
                          </Grid>
                        ))
                      ) : (
                        <Grid item xs={12}>
                          <p>No Live Auction Available</p>
                        </Grid>
                      )}
                    </Grid>
                  ) : (
                    <List className={classes.listItems}>
                      {this.state.liveAuctions &&
                      !isEmpty(this.state.activeVehicles) ? (
                        this.state.activeVehicles.map((item, index) => (
                          <ListItem key={item._id}>
                            <AuctionItem
                              item={item}
                              index={index}
                              auth={this.props.auth}
                              classes={classes}
                              type="list"
                              history={this.props.history}
                              itemError={this.handleItemError}
                              itemSuccess={this.handleitemSuccess}
                              onAuctionend={this.onAuctionend}
                              socket={socket}
                              tabIndex={this.state.tabIndex}
                              updateLiveAuction={this.props.activeVehicles}
                              sendFiveMinsNotification={
                                this.props.sendFiveMinsNotification
                              }
                            />
                          </ListItem>
                        ))
                      ) : (
                        <p>No Live Auction Available</p>
                      )}
                    </List>
                  )}
                </section>

                {/* MY LISTING */}
                {
                  <section dir={"x"} className={classes.section}>
                    <Toolbar className={classes.filterBar}>
                      <IconButton
                        className={
                          this.state.viewTypeAll === "list"
                            ? classes.ActiveFilter
                            : null
                        }
                        onClick={this.hangeChangeViewTypeAll}
                        viewtype="list"
                        color="inherit"
                      >
                        <ViewListIcon />
                      </IconButton>

                      <IconButton
                        className={
                          this.state.viewTypeAll === "grid"
                            ? classes.ActiveFilter
                            : null
                        }
                        onClick={this.hangeChangeViewTypeAll}
                        viewtype="grid"
                        color="inherit"
                      >
                        <ViewModuleIcon />
                      </IconButton>

                      <div className={classes.grow} />

                      <Typography
                        className={classes.title}
                        color="inherit"
                        noWrap
                      >
                        Refresh
                      </Typography>

                      <IconButton
                        onClick={this.handleRefreshMyListing}
                        color="inherit"
                      >
                        <RefreshIcon />
                      </IconButton>

                      <Typography
                        className={classes.title}
                        color="inherit"
                        noWrap
                      >
                        Filter
                      </Typography>

                      <IconButton
                        onClick={this.handlePopupOpen}
                        color="inherit"
                      >
                        <TuneIcon />
                      </IconButton>
                    </Toolbar>

                    {this.state.viewTypeAll === "grid" ? (
                      <Grid
                        container
                        spacing={24}
                        className={classes.gridItems}
                      >
                        {this.state.myListing &&
                        !isEmpty(this.props.vehicle.myListing) ? (
                          this.props.vehicle.myListing.map((item, index) => (
                            <Grid key={item._id} item xs={6}>
                              <ListItem key={item._id}>
                                <AuctionItem
                                  item={item}
                                  index={index}
                                  auth={this.props.auth}
                                  classes={classes}
                                  type="grid"
                                  history={this.props.history}
                                  itemError={this.handleItemError}
                                  itemSuccess={this.handleitemSuccess}
                                  socket={socket}
                                  tabIndex={this.state.tabIndex}
                                  updateLiveAuction={this.props.activeVehicles}
                                  sendFiveMinsNotification={
                                    this.props.sendFiveMinsNotification
                                  }
                                />
                              </ListItem>
                            </Grid>
                          ))
                        ) : (
                          <Grid item xs={12}>
                            <p>You have not added any vehicle.</p>
                          </Grid>
                        )}
                      </Grid>
                    ) : (
                      <List className={classes.listItems}>
                        {this.state.myListing &&
                        !isEmpty(this.props.vehicle.myListing) ? (
                          this.props.vehicle.myListing.map((item, index) => (
                            <ListItem key={item._id}>
                              <AuctionItem
                                item={item}
                                index={index}
                                auth={this.props.auth}
                                classes={classes}
                                type="list"
                                history={this.props.history}
                                itemError={this.handleItemError}
                                itemSuccess={this.handleitemSuccess}
                                socket={socket}
                                tabIndex={this.state.tabIndex}
                                updateLiveAuction={this.props.activeVehicles}
                                sendFiveMinsNotification={
                                  this.props.sendFiveMinsNotification
                                }
                              />
                            </ListItem>
                          ))
                        ) : (
                          <p>You have not added any vehicle.</p>
                        )}
                      </List>
                    )}
                  </section>
                }

                {/* MY BIDS */}
                {
                  <section dir={"x"} className={classes.section}>
                    <Toolbar className={classes.filterBar}>
                      <IconButton
                        className={
                          this.state.viewTypeAll === "list"
                            ? classes.ActiveFilter
                            : null
                        }
                        onClick={this.hangeChangeViewTypeAll}
                        viewtype="list"
                        color="inherit"
                      >
                        <ViewListIcon />
                      </IconButton>

                      <IconButton
                        className={
                          this.state.viewTypeAll === "grid"
                            ? classes.ActiveFilter
                            : null
                        }
                        onClick={this.hangeChangeViewTypeAll}
                        viewtype="grid"
                        color="inherit"
                      >
                        <ViewModuleIcon />
                      </IconButton>

                      <div className={classes.grow} />

                      <Typography
                        className={classes.title}
                        color="inherit"
                        noWrap
                      >
                        Refresh
                      </Typography>

                      <IconButton
                        onClick={this.handleRefreshMyBids}
                        color="inherit"
                      >
                        <RefreshIcon />
                      </IconButton>

                      <Typography
                        className={classes.title}
                        color="inherit"
                        noWrap
                      >
                        Filter
                      </Typography>

                      <IconButton
                        onClick={this.handlePopupOpen}
                        color="inherit"
                      >
                        <TuneIcon />
                      </IconButton>
                    </Toolbar>

                    {this.state.viewTypeAll === "grid" ? (
                      <Grid
                        container
                        spacing={24}
                        className={classes.gridItems}
                      >
                        {this.state.myBids &&
                        !isEmpty(this.props.vehicle.myBids) ? (
                          this.props.vehicle.myBids.map((item, index) => (
                            <Grid key={item._id} item xs={6}>
                              <ListItem key={item._id}>
                                <AuctionItem
                                  item={item}
                                  index={index}
                                  auth={this.props.auth}
                                  classes={classes}
                                  type="grid"
                                  history={this.props.history}
                                  itemError={this.handleItemError}
                                  itemSuccess={this.handleitemSuccess}
                                  socket={socket}
                                  tabIndex={this.state.tabIndex}
                                  updateLiveAuction={this.props.activeVehicles}
                                  sendFiveMinsNotification={
                                    this.props.sendFiveMinsNotification
                                  }
                                />
                              </ListItem>
                            </Grid>
                          ))
                        ) : (
                          <Grid item xs={12}>
                            <p>You have not added any bid yet.</p>
                          </Grid>
                        )}
                      </Grid>
                    ) : (
                      <List className={classes.listItems}>
                        {this.state.myBids &&
                        !isEmpty(this.props.vehicle.myBids) ? (
                          this.props.vehicle.myBids.map((item, index) => (
                            <ListItem key={item._id}>
                              <AuctionItem
                                item={item}
                                index={index}
                                auth={this.props.auth}
                                classes={classes}
                                type="list"
                                history={this.props.history}
                                itemError={this.handleItemError}
                                itemSuccess={this.handleitemSuccess}
                                socket={socket}
                                tabIndex={this.state.tabIndex}
                                updateLiveAuction={this.props.activeVehicles}
                                sendFiveMinsNotification={
                                  this.props.sendFiveMinsNotification
                                }
                              />
                            </ListItem>
                          ))
                        ) : (
                          <p>You have not added any bid yet</p>
                        )}
                      </List>
                    )}
                  </section>
                }
              </SwipeableViews>
            )}

            <footer className={classes.footer} />

            {this.state.openPopup && (
              <FilterPopup
                classes={classes}
                open={this.state.openPopup}
                onClose={this.handlePopupClose}
                screen="live-auction"
                filterData={this.state.filterData}
                filterVehicles={this.filterVehicles}
                resetFilters={this.resetFilters}
              />
            )}

            {this.state.Msg ? (
              <SnackBar
                open={this.state.Msg}
                duration={5000}
                variant={this.state.MsgVariant}
                className={classes.margin}
                message={this.state.MsgText}
                onClose={this.handleClose}
              />
            ) : null}
          </div>
        </Grid>
      </Grid>
    );
  }
}

LiveAuctions.propTypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  vehicle: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile,
  vehicle: state.vehicle,
  savedSearches: state.savedSearches
});

export default compose(
  withStyles(liveAuctionStyles),
  connect(
    mapStateToProps,
    {
      logoutUser,
      loadProfile,
      saveNotificationsToken,
      sellAVehicle,
      activeVehicles,
      myListingVehicles,
      myBidsVehicles,
      sendFiveMinsNotification,
      VehiclesFilter,
      allowUserNotifications
    }
  )
)(withRouter(LiveAuctions));
