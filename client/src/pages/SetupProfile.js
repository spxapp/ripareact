import React from "react";  
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { allowUserNotifications } from '../actions/authActions';
import { loadProfile, setupProfile1, skipProfileStep1 } from '../actions/profileActions';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import {
  isMobile,
  isAndroid,
  isIOS 
} from "react-device-detect";

import isEmpty from '../validation/isEmpty'; 
import withStyles from "@material-ui/core/styles/withStyles";   
import Button from '@material-ui/core/Button'; 
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade';
import LinearProgress from '@material-ui/core/LinearProgress';  
import CircularProgress from '@material-ui/core/CircularProgress'; 

import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import DialogContentText from '@material-ui/core/DialogContentText';

import SnackBar from '../components/common/SnackBar';


import { AskForPermissionAndGenerateToken } from '../push_notifications';
 

import setupProfileStyles from "../assets/jss/pages/setupProfileStyles.jsx";

class SetupProfile extends React.Component {

	constructor(){
		super();
		this.state = {
			imageURL: '',  
			loading:false,
			Msg:false,
			MsgVariant: 'info',
			MsgText: 'File Uploading...',
			openNotificationsPopup:false,
			errors: {}
		}
 
	}

	componentDidMount(){

		if(this.props.auth && !isEmpty(this.props.auth.user))
		{	   
			if( isEmpty(this.props.auth.user.notifications) || this.props.auth.user.notifications===false)
			{
				this.setState({openNotificationsPopup: true});
			}
		}	
	
	}
  
	componentWillMount(){
	    document.title = "Setup Profile | Ripa - Car App";

	    this.props.loadProfile();
	}

	componentWillReceiveProps(nextProps){ 

		if(!isEmpty(nextProps.profile))
		{
			if(nextProps.profile.step === 3){  
				this.props.history.push('/');
			}else if(nextProps.profile.step === 2){
				this.props.history.push('/setup-profile-3');
			}else if(nextProps.profile.step === 1){
				 this.props.history.push('/setup-profile-2');
			}
		}
		

		
		if(nextProps.errors){
			this.setState({errors: nextProps.errors});
		}

		this.setState({loading:false, Msg: false});
	}

	onChange = (e) => {

		e.preventDefault();  

		let reader = new FileReader();
	    let file = e.target.files[0];
 
	    reader.onloadend = () => {
	      this.setState({
	        image: file, 
	        imageURL: reader.result
	      });
	    }  

	    reader.readAsDataURL(file);
 
	}
 

	onSubmit = (e) => {
		e.preventDefault();   

		this.setState({loading:true, Msg: true});

 		this.props.setupProfile1(this.state.image);
	}

	onSkip = (e) => {
		e.preventDefault();     
 		this.props.skipProfileStep1();
	}
	
	handleNotificationPopup = (e) => {
		const value = e.currentTarget.getAttribute('data-value'); 
		if(value==='true')
		{	
			window.localStorage.setItem('notification',true);

			if (
				isMobile && !isEmpty(window.FirebasePlugin) &&
				(isAndroid || isIOS)
			) {
				if (isIOS) {
					window.FirebasePlugin.grantPermission();
				  }
				window.FirebasePlugin.onTokenRefresh((token) => {
					// save this server-side and use it to push notifications to this 
					 
					if(token){
						let notificationData={}
						if(isIOS){
							notificationData = {
								notifications: true,
								token: token,
								type:'ios'
							};
						}
						else if(isAndroid){
							notificationData = {
								notifications: true,
								token: token,
								type:'android'
							};
						}
						this.props.allowUserNotifications(notificationData);
					}

 
				});

				}else{


				//subscribe user to the push notifications

				AskForPermissionAndGenerateToken().then( token => {

					if(token){

						const notifications = {
							notifications: true,
							token: token,
							type:'web'
						};

						this.props.allowUserNotifications(notifications);
					}

				});
			}
			

		}else{
			window.localStorage.setItem('notification',false);
			const notifications = {
				notifications: false,
				token: null
			};

			this.props.allowUserNotifications(notifications);
		}
 
		this.setState({openNotificationsPopup: false});

	}

	render() { 
		const { classes } = this.props;

		const profileNoImg = (

			  <Button variant="raised" component="span" className={classes.upload}>
			    <AddIcon />
			  </Button>

		);

		const profileImg = (
 
			  <img 
				  className={classes.profileImg} 
				  alt="..." 
				  src={

				  	this.state.imageURL
				  	?
			  		this.state.imageURL
				  	:
				  	null
					
				  }
			   />
			 

		);


		const btnDisabled = (
			<Button
				        type="submit"  
				        variant="contained" 
				        disabled
				        className={classes.button}>
					        Next
					    </Button>
		);

		const btnEnabled = (
			<Button
				        type="submit"  
				        variant="contained"  
				        className={classes.button}>
					        Next
					    </Button>
		);
	 
	    return (
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div>  
				<header className={classes.header}>
					<h3>Set up your profile</h3>
					<div className={classes.Progress}>
						<LinearProgress 
							color="secondary" 
							variant="determinate" 
							value={50}
						/>
					</div>
				</header>
				<section className={classes.section}>

					<form 
						onSubmit={this.onSubmit} 
						className={classes.form}
						method='post' 
	      				encType="multipart/form-data"
      				>					
				 		
				 		

				 		<div className={classes.uploadBtn}>
						<input
						  accept="image/*"
						  className={classes.input}
						  style={{ display: 'none' }}
						  id="profile-pic"
						  name="profile_pic"
						  type="file" 
			          	  onChange={this.onChange}
						/>
						<label htmlFor="profile-pic">
						 	{this.state.imageURL ? profileImg : profileNoImg}
						</label> 
					</div>

						<p className={classes.profilePicTitle}>Add a profile/company picture</p>

						<Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>


						{this.state.imageURL ? btnEnabled : btnDisabled}

				        
			        </form>
				</section>
				<footer className={classes.footer}>
					<span onClick={this.onSkip}>
						Skip
					</span>
				</footer>


				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}


		<Dialog 
          fullWidth={true} 
          open={this.state.openNotificationsPopup}
          aria-labelledby="auction-filter" 
        > 
          <DialogContent> 
  
			<DialogContentText>
				Would you like to receive notifications from Ripa?
			</DialogContentText>
				
          </DialogContent>
          <DialogActions>  
  
            <Button onClick={this.handleNotificationPopup} data-value={true} className={this.props.classes.linkButtonCancel} color="primary">
              Yes
            </Button>
            <Button onClick={this.handleNotificationPopup} data-value={false} className={this.props.classes.linkButton} color="primary">
               No
            </Button>
          </DialogActions>
        </Dialog>
      

	    	</div> 
	    	</Fade>
	    	</Grid>
	    	</Grid>

	    );
	}
} 
 

SetupProfile.propTypes = { 
	profile: PropTypes.object.isRequired, 
	errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth: state.auth,
	profile: state.profile, 
	errors: state.errors
});

export default compose(
  withStyles(setupProfileStyles),
  connect(mapStateToProps, { loadProfile, setupProfile1, skipProfileStep1, allowUserNotifications })
)(withRouter(SetupProfile));