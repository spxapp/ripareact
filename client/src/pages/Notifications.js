import React from "react"; 
import PropTypes from 'prop-types';
import { Link, withRouter } from "react-router-dom";  
import { connect } from 'react-redux';  
import { compose } from 'redux';  
import { updateNotifications } from '../actions/notificationsActions';
import isEmpty from '../validation/isEmpty';
import Typography from '@material-ui/core/Typography';  
import CircularProgress from '@material-ui/core/CircularProgress';  

import withStyles from "@material-ui/core/styles/withStyles";   
import ArrowBack from '@material-ui/icons/ArrowBack';    
import CloseIcon from '@material-ui/icons/Close'; 

import Grid from '@material-ui/core/Grid';   
import Divider from '@material-ui/core/Divider';  

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton';  

import notificationsStyles from "../assets/jss/pages/notificationsStyles.jsx";
 

class Notifications extends React.Component {
	
	constructor(){
		super();
		this.state = { 
			screenLoading:true,
			notifications: false,  
		}
 
	}

	componentWillMount(){
	    document.title = "Notifications | Ripa - Car App"; 
	}

	componentDidMount(){
	  	
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}   

		this.props.updateNotifications();
		
	}     

	componentWillReceiveProps(nextProps){
	  	 

		if(nextProps.notifications.all)
 		{console.log("/======",nextProps.notifications.all)
 			this.setState({screenLoading:false, notifications: nextProps.notifications.all});
 		}
		
	}     

	vehilceDetails = (vehicle) =>{
		
		this.props.history.push('/vehicle-details',
		{
			id:vehicle._id,
			date: vehicle._id,
			expiry_date: vehicle.expiry_date,
			active: vehicle.active
		}) 
	};
 
	render() { 
		const { classes } = this.props; 

		const notifications = (this.state.notifications && !isEmpty(this.state.notifications)) &&

							this.state.notifications.map((notification, key) => (
								  
									(

								(	notification.type==='new_bid' && notification.vehicle[0])
 
									  && 

									<Grid key={key} className={classes.bidRow} space={24} vehicle={notification.vehicle[0]._id} onClick={()=>this.vehilceDetails(notification.vehicle[0])}>

										
										<div>Someone placed a new bid of ${notification.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} to your {notification.vehicle[0].year} {notification.vehicle[0].make} {notification.vehicle[0].model} listing</div>
										
										<Divider />

									</Grid>

									)

									||

									(

									(notification.type==='auction_ended_buyer' && notification.vehicle[0])

									&&


									<Grid key={key} className={classes.bidRow} space={24} vehicle={notification.vehicle[0]._id} onClick={()=>this.vehilceDetails(notification.vehicle[0])}>

										
										<div>The vehicle: {notification.vehicle[0].year} {notification.vehicle[0].make} {notification.vehicle[0].model} auction has ended and you are the highest bidder! Congratulations!</div>
										
										<Divider />

									</Grid>  

									)

									||

									(

									(notification.type==='auction_ended_seller' && notification.vehicle[0])

									&&


									<Grid key={key} className={classes.bidRow} space={24} vehicle={notification.vehicle[0]._id} onClick={()=>this.vehilceDetails(notification.vehicle[0])}>

										
										<div>Your vehicle: {notification.vehicle[0].year} {notification.vehicle[0].make} {notification.vehicle[0].model} auction has ended. You have 15 mins to approve or reject the offer. Tap to see details.</div>
										
										<Divider />

									</Grid> 

									)

									||

									(

									(notification.type==='auction_ended_seller_no_bids' && notification.vehicle[0])

									&&


									<Grid key={key} className={classes.bidRow} space={24} vehicle={notification.vehicle[0]._id} onClick={()=>this.vehilceDetails(notification.vehicle[0])}>

										
										<div>Your vehicle: {notification.vehicle[0].year} {notification.vehicle[0].make} {notification.vehicle[0].model} auction has ended. Unfortunately, there are no bids. Please try listing the vehicle again.</div>
										
										<Divider />

									</Grid> 

									)

									||

									(

									notification.type==='auction_accepted'

									&&


									<Grid key={key} className={classes.bidRow} space={24} vehicle={notification.vehicle[0] && notification.vehicle[0]._id} onClick={()=>this.vehilceDetails(notification.vehicle[0])}>

										
										<div>Your bid has been accepted by seller. Tap to view seller contact details.</div>
										
										<Divider />

									</Grid>

									)
									 
							)	
							);	
			
					

	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>      
	          	

	          	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			            <IconButton onClick={this.props.history.goBack} className={classes.menuButton} color="inherit" aria-label="Go back">
			              <ArrowBack />
			            </IconButton>
			   			
			            <Typography className={classes.title} color="inherit" noWrap>
			              Notifications
			            </Typography>

			            <div className={classes.grow} />

			            <Link to="/">
				            <IconButton className={classes.closeButton} color="inherit" aria-label="Close">
				              <CloseIcon />
				            </IconButton>
			            </Link>

			          </Toolbar> 

	        	</AppBar> 

        		<section className={classes.section}>


        		{ this.state.screenLoading ? 

		    		(
		    			<section className={classes.loadingScreen}>
		    				 
		    				 <CircularProgress className={classes.progress} />	
		    				 <br/><br/>Loading...
		    				  
		    		 	</section>
		    		 )

		    		 :

        		

        			 this.state.notifications && !isEmpty(this.state.notifications)

        			 ?	

					 notifications

					 :

					 (<div className={classes.noNotifications}>No Notifications.</div>)

        			

        		}	
				</section>
 

	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

Notifications.propTypes = { 

	auth: PropTypes.object.isRequired,
	notifications: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,
	notifications: state.notifications 
});

export default compose(
  withStyles(notificationsStyles),
  connect(mapStateToProps, {updateNotifications})
)(withRouter(Notifications));