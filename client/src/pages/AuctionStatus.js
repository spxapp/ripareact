import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';  
import { compose } from 'redux'; 
import { logoutUser } from '../actions/authActions';
import { loadProfile } from '../actions/profileActions'; 
import { auctionStatus } from '../actions/vehicleActions';
import * as qs from 'query-string';

import withStyles from "@material-ui/core/styles/withStyles";  
import CheckCirlceIcon from '@material-ui/icons/CheckCircle';  
import ErrorIcon from '@material-ui/icons/Error';
import Grid from '@material-ui/core/Grid';   
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';  

import awaitingApprovalStyles from "../assets/jss/pages/awaitingApprovalStyles.jsx";
 
class AuctionStatus extends React.Component {

    constructor()
    {
        super();
        this.state = {
            status: ''
        }
    }
	   
	componentWillMount(){
	    document.title = "Auction Status | Ripa - Car App"; 
	     
	    this.props.loadProfile();
	}

	componentDidMount(){
        
	    if(!this.props.auth.isAuthenticated){  
            let redirectUrl = this.props.location.search.substring(1);
			this.props.history.push('/login?redirect='+redirectUrl);
        }    
        
        const queryData = qs.parse(atob(this.props.location.search.substring(1)));

        if(queryData!=='')
        { 
            const {user, vehicle, status} = queryData;

            this.setState({status:status});

            const auction = {
                user,
                vehicle,
                status
            };

            this.props.auctionStatus(auction);
        }

        
       
		
	} 
 	 

	componentWillReceiveProps(nextProps){

		if(nextProps.errors){
			if(nextProps.errors==='Unauthorized')
			{
				//this.props.logoutUser();
				//this.props.history.push('/login');
			}
		}
 
    }	
    
    
 	onHomeClick(e)
     {
         e.preventDefault(); 
         this.props.history.push('/');
     } 
 
	render() { 
		const { classes } = this.props;
 
		  	
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>   
 

		    	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			         
			            <header className={classes.header}>
							<h3>Auction Status</h3>
							 
						</header>  

			          </Toolbar> 

	        	</AppBar>   
			  	 
				<section className={classes.section}>
	          		  
					<Grid className={classes.field}>

                            {
                                this.state.status && this.state.status==='yes' ?
                                (
                                    <div>
                                    <label className={classes.awaitingIcon}>
                                        <CheckCirlceIcon /> 
                                    </label>

                                    <label htmlFor="phone">Congratulations for a successful deal!</label>
                                    </div>
                                )

                                :

                                (
                                    <div>
                                    <label className={classes.errorIcon}>
                                        <ErrorIcon /> 
                                    </label>

                                    <label htmlFor="phone">We're sorry the deal didn't happen.</label>
                                    </div>
                                )
                            }
				  

			        </Grid>	

					<Grid className={classes.field}>
					 
		        		<label onClick={this.onHomeClick.bind(this)} htmlFor="email">Go back to home</label>

			        </Grid>
 						
 					 
				   </section>
  
 
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AuctionStatus.propTypes = {  
	logoutUser: PropTypes.func.isRequired,
	profile: PropTypes.object.isRequired, 
	auth: PropTypes.object.isRequired, 
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,  
	profile: state.profile,  
});
 
export default compose(
  withStyles(awaitingApprovalStyles),
  connect(mapStateToProps, { logoutUser, loadProfile, auctionStatus })
)(withRouter(AuctionStatus));