import React from "react";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import classnames from "classnames";
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";
import { compose } from "redux";

import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Fade from "@material-ui/core/Fade";

import { saveNotificationsToken } from "../actions/profileActions";
import { AskForPermissionAndGenerateToken } from "../push_notifications";
import { isMobile, isAndroid, isIOS } from "react-device-detect";
import isEmpty from "../validation/isEmpty";

import SnackBar from "../components/common/SnackBar";

import logo from "../assets/img/logoRipa.png";

import loginStyles from "../assets/jss/pages/loginStyles.jsx";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      loading: false,
      Msg: false,
      MsgVariant: false,
      MsgText: false,
      errors: {}
    };
  }

  componentWillMount() {
    document.title = "Login | Ripa - Car App";
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }

    if (this.props.auth.isRegistred) {
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText:
          "Registration pending approval, you will recieve an email once approved"
      });
    }

    if (this.props.location.state && this.props.location.state.successMsg) {
      this.setState({
        Msg: true,
        MsgVariant: "success",
        MsgText: this.props.location.state.successMsg
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.setState({
        loading: false,
        Msg: true,
        MsgVariant: "success",
        MsgText: "Logged In successfully!"
      });

      if (this.props.location.search !== "") {
        let redirectUrl = this.props.location.search;
        this.props.history.push("/" + redirectUrl);
      } else {
        if (
          isMobile &&
          !isEmpty(window.FirebasePlugin) &&
          (isAndroid || isIOS)
        ) {
          if (isIOS) {
            window.FirebasePlugin.grantPermission();
          }
          window.FirebasePlugin.onTokenRefresh(token => {
            // save this server-side and use it to push notifications to this
            if (token) {
              let tokenData = {};
              if (isIOS) {
                tokenData = {
                  token: token,
                  type: "ios"
                };
              } else if (isAndroid) {
                tokenData = {
                  token: token,
                  type: "android"
                };
              }
              this.props.saveNotificationsToken(tokenData);
            }
          });
        } else {
          //subscribe user to the push notifications
          AskForPermissionAndGenerateToken().then(token => {
            if (token) {
              const tokenData = {
                token: token,
                type: "web"
              };
              this.props.saveNotificationsToken(tokenData);
            }
          });
        }
        this.props.history.push("/");
      }
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });

      if (nextProps.errors.email) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: nextProps.errors.email
        });
      }

      if (nextProps.errors.password) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: nextProps.errors.password
        });
      }
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    this.setState({ loading: true });

    const User = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(User);
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };

  render() {
    const { classes } = this.props;
    const { errors } = this.state;

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={12} md={4}>
          <Fade in={true}>
            <div>
              <header className={classes.header}>
                <Link to="/">
                  <img src={logo} alt="Ripa" width="94" />
                </Link>
              </header>
              <section className={classes.section}>
                <form
                  onSubmit={this.onSubmit}
                  className={classes.form}
                  novalidate
                >
                  <TextField
                    id="email"
                    placeholder="Email address"
                    className={classnames(classes.textField, {
                      [classes.textError]: errors.email
                    })}
                    type="text"
                    name="email"
                    margin="normal"
                    variant="filled"
                    value={this.state.email}
                    onChange={this.onChange}
                  />

                  {errors.email && (
                    <span className={classes.errorMsg}>{errors.email}</span>
                  )}

                  <TextField
                    id="password"
                    placeholder="Password"
                    className={classnames(classes.textField, {
                      [classes.textError]: errors.password
                    })}
                    type="password"
                    name="password"
                    margin="normal"
                    variant="filled"
                    value={this.state.password}
                    onChange={this.onChange}
                  />

                  {errors.password && (
                    <span className={classes.errorMsg}>{errors.password}</span>
                  )}

                  <Link
                    to="/forgot-password"
                    className={classes.forgotpassword}
                  >
                    Forgot password?
                  </Link>
                  <Grid item xs={12}>
                    {this.state.loading ? (
                      <CircularProgress className={classes.progress} />
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      variant="contained"
                      className={classes.button}
                    >
                      Sign In
                    </Button>
                  </Grid>
                </form>
              </section>
              <footer className={classes.footer}>
                <Link to="/register">Not Registered?</Link>
              </footer>

              {this.state.Msg ? (
                <SnackBar
                  open={this.state.Msg}
                  duration={5000}
                  variant={this.state.MsgVariant}
                  className={classes.margin}
                  message={this.state.MsgText}
                  onClose={this.handleClose}
                />
              ) : null}
            </div>
          </Fade>
        </Grid>
      </Grid>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default compose(
  withStyles(loginStyles),
  connect(
    mapStateToProps,
    { loginUser, saveNotificationsToken }
  )
)(withRouter(Login));
