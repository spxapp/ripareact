import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getUserDetails } from "../actions/userDetailsActions";
import { loadProfile } from "../actions/profileActions";
import { compose } from "redux";

import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import ArrowBack from "@material-ui/icons/ArrowBack";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";

import userDetailsStyles from "../assets/jss/pages/userDetailsStyles.jsx";

import UserPlaceholder from "../assets/img/user_placeholder.jpg";

class UserDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      type: this.props.match.params.userType
    };
  }

  componentWillMount() {
    document.title = "User Details | Ripa - Car App";
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }

    const UserData = {
	  user_id: this.props.match.params.user,
	  type: this.props.match.params.userType
    };

    this.props.getUserDetails(UserData);
    this.props.loadProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userDetails) {
      this.setState({ user: nextProps.userDetails });
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={12} md={4}>
          <div>
            <AppBar className={classes.AppBar} position="fixed">
              <Toolbar>
                <IconButton
                  onClick={this.props.history.goBack}
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="Go back"
                >
                  <ArrowBack />
                </IconButton>

                <header className={classes.header}>
                  <h3>{this.state.type}&nbsp;details</h3>
                </header>
              </Toolbar>
            </AppBar>

            <section className={classes.section}>
              <div className={classes.uploadBtn}>
                <label htmlFor="profile-pic">
                  {this.state.user.profile ? (
                    this.state.user.profile.profile_pic ? (
                      <img
                        className={classes.profileImg}
                        alt="..."
                        src={this.state.user.profile.profile_pic}
                      />
                    ) : (
                      <img
                        className={classes.profileImg}
                        alt="..."
                        src={UserPlaceholder}
                      />
                    )
                  ) : null}
                </label>
              </div>

              <Grid className={classes.fieldName}>
                <label htmlFor="name">
                  {this.state.user.user && this.state.user.user.name}
                </label>
              </Grid>

              <Grid className={classes.field}>
                <label htmlFor="phone">
                  {this.state.user.user && this.state.user.profile.phone}
                </label>
              </Grid>

              <Grid className={classes.field}>
                <label htmlFor="email">
                  {this.state.user.user && this.state.user.user.email}
                </label>
              </Grid>
            </section>

            <section className={classes.sectionRate}>
              <Grid className={classes.fieldRate}>
                <label htmlFor="rate">Sell through rate</label>
              </Grid>

              <Grid className={classes.fieldRateBig}>
                <label htmlFor="rate">
                  {this.state.user.ratings &&
                    this.state.user.ratings.sell_rating}
                </label>
              </Grid>
            </section>
          </div>
        </Grid>
      </Grid>
    );
  }
}

UserDetails.propTypes = {
  auth: PropTypes.object.isRequired,
  userDetails: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  userDetails: state.userDetails,
  profile: state.profile
});

export default compose(
  withStyles(userDetailsStyles),
  connect(
    mapStateToProps,
    { getUserDetails, loadProfile }
  )
)(withRouter(UserDetails));
