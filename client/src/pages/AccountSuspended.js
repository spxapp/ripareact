import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';  
import { compose } from 'redux'; 
import { logoutUser } from '../actions/authActions';
import { loadProfile } from '../actions/profileActions';  

import withStyles from "@material-ui/core/styles/withStyles";    
import ErrorIcon from '@material-ui/icons/Error';
import Grid from '@material-ui/core/Grid';   
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';  

import awaitingApprovalStyles from "../assets/jss/pages/awaitingApprovalStyles.jsx";
 
class AccountSuspended extends React.Component {

     
	   
	componentWillMount(){
	    document.title = "Account Suspended | Ripa - Car App"; 
	     
	    this.props.loadProfile();
	}

	componentDidMount(){
        
	    if(!this.props.auth.isAuthenticated){   
			this.props.history.push('/login');
        }     
		
	}  
    
    
    onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUser();
		this.props.history.push('/login');
	} 
 
	render() { 
		const { classes } = this.props;
 
		  	
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>   
 

		    	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			         
			            <header className={classes.header}>
							<h3>Account Suspended</h3>
							 
						</header>  

			          </Toolbar> 

	        	</AppBar>   
			  	 
				<section className={classes.section}>
	          		  
					<Grid className={classes.field}>

                            
                            <div>
                            <label className={classes.errorIcon}>
                                <ErrorIcon /> 
                            </label>

                            <label htmlFor="phone">Your account has been suspended. You can contact <a style={{color:"#ec1d25"}} href="mailto:info@ripa.app">info@ripa.app</a> for any inquiries.</label>
                            </div>
                        

			        </Grid>	

					<Grid className={classes.field}>
					 
		        		<label onClick={this.onLogoutClick.bind(this)} htmlFor="email">Logout</label>

			        </Grid>
 						
 					 
				   </section>
  
 
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

AccountSuspended.propTypes = {  
	logoutUser: PropTypes.func.isRequired,
	profile: PropTypes.object.isRequired, 
	auth: PropTypes.object.isRequired, 
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,  
	profile: state.profile,  
});
 
export default compose(
  withStyles(awaitingApprovalStyles),
  connect(mapStateToProps, { logoutUser, loadProfile })
)(withRouter(AccountSuspended));