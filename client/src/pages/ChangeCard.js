import React from "react";    
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { loadProfile, changeCard } from '../actions/profileActions';
import { compose } from 'redux';
import PropTypes from 'prop-types'; 
import CreditCard from '../components/CreditCard'; 

import withStyles from "@material-ui/core/styles/withStyles";   
import Grid from '@material-ui/core/Grid';
import Fade from '@material-ui/core/Fade'; 
import SnackBar from '../components/common/SnackBar';
 
import CircularProgress from '@material-ui/core/CircularProgress';

import setupProfileStyles from "../assets/jss/pages/setupProfileStyles.jsx";

class ChangeCard extends React.Component {

	constructor(){
		super();
		this.state = { 
			loading:false,
			errors: {}
		}
 
	}
 
	componentWillMount(){
	    document.title = "Setup Profile 3 | Ripa - Car App"; 
	}

	componentDidMount(){ 

	    this.props.loadProfile();
	}

	componentWillReceiveProps(nextProps){
		console.log("=======",nextProps)
		if(nextProps.profile.cardUpdated){
			this.props.history.push('/my-account',{cardUpdated:true})
		}
		this.setState({loading:false});
	}
 	
 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = (token) => { 
		
		this.setState({loading:true});
 		const UpdateProfile = {   
			token: token  
		}	 	 

		this.props.changeCard(UpdateProfile, this.props.history);
	}

	handleItemError = () => {

		this.setState({ Msg:true, MsgVariant: 'error', MsgText: 'Invalid card details.'});
	}

	handleClose = () => {
		 
	   this.setState({ Msg: false });

	};

	render() { 
		const { classes } = this.props;
	  
	    return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<Fade in={true}>
	    	<div> 
				 	 
	        	<CreditCard classes={classes} updateCard={true} onSubmit={this.onSubmit} onError={this.handleItemError} /> 
				
				<Grid item xs={12}>
		        {
		        this.state.loading ? (
					<div>
				<div>&nbsp;</div>
		        <CircularProgress className={classes.progress} />
				</div>
		        ):
		        null	
		        }
		        </Grid>
		        
				<footer className={classes.footer}>
				 
				</footer>
	    	</div> 
 
	    	</Fade>
	    	{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}
	    	</Grid>
	    	</Grid>

	    );
	}
} 
 

ChangeCard.propTypes = { 
	profile: PropTypes.object.isRequired, 
	errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	profile: state.profile, 
	errors: state.errors
});

export default compose(
  withStyles(setupProfileStyles),
  connect(mapStateToProps, { loadProfile, changeCard })
)(withRouter(ChangeCard));