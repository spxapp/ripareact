import React from "react"; 
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";  
import { connect } from 'react-redux';
import { logoutUser, allowUserNotifications } from '../actions/authActions';
import { loadProfile, saveNotificationsToken } from '../actions/profileActions';
import { saveUserSearchFilters, getUserSearchFilters, updateUserSearchFilters, deleteUserSearchFilters } from '../actions/userSavedSearchesAction';
import { compose } from 'redux'; 
import isEmpty from '../validation/isEmpty'; 
import {
  isMobile,
  isAndroid,
  isIOS 
} from "react-device-detect";

import withStyles from "@material-ui/core/styles/withStyles";  
import SwipeableViews from 'react-swipeable-views';  
import Grid from '@material-ui/core/Grid'; 
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider'; 
import { AskForPermissionAndGenerateToken } from '../push_notifications';
 
import CloseIcon from '@material-ui/icons/Close';
import EditIcon from '@material-ui/icons/Edit';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';  
import Switch from '@material-ui/core/Switch';
import NotificationsIcon from '@material-ui/icons/Notifications'; 
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'; 

import RipaDrawer from '../components/common/RipaDrawer'; 
import RipaAppBar from '../components/common/RipaAppBar';

import myAccountStyles from "../assets/jss/pages/myAccountStyles.jsx";

import FilterPopup from "../components/common/FilterPopup";  
import SnackBar from "../components/common/SnackBar";

FilterPopup.propTypes = {
  onClose: PropTypes.func,
  filterData: PropTypes.object
};



class Settings extends React.Component {
	
	constructor(props){
		super(props);

	  const tabIndex = (typeof props.location.state !=='undefined') ? props.location.state.tabIndex : 0;

		this.state = {
			screenLoading:true, 
			tabIndex:tabIndex,
			notifications:false,
			drawer:false,
			imageURL:'',
			name:'',
			email:'',
			openPopup:false, 
			filterData:{
				vehicleType: '',
				Mileage: '',
				Make: [],
				Model: '',
				Year: '',
				YearEnd:'',
				PriceRange: '',
				nznew: '',
				edit: false 
			},
			filterId:'',
			searchedFilters:[],
			errors: {}, 
			Msg:false,
      MsgVariant: false,
      MsgText: false,
		}
 
 
	}

	componentWillMount(){
		document.title = "Settings | Ripa - Car App"; 
	}

	componentDidMount(){
	  	
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}

		this.setState({screenLoading:false});   

		this.props.loadProfile();

		this.props.getUserSearchFilters();
		if(!isEmpty(this.props.profile.user))
		{
			this.setState({notifications: this.props.profile.user.notifications});

		}
		if(typeof this.props.location.state !=='undefined')
		{	
			this.setState({ Msg: true, MsgVariant: "warning", MsgText: this.props.location.state.msg });
		}
	
	} 


	componentWillReceiveProps(nextProps){
		if(nextProps.errors){
			if(nextProps.errors==='Unauthorized')
			{
				//this.props.logoutUser();
				//this.props.history.push('/login');
			}
		}

		if(nextProps.profile)
		{

			if(nextProps.profile.step === 2){
				this.props.history.push('/setup-profile-3');
			}else if(nextProps.profile.step === 1){
				this.props.history.push('/setup-profile-2');
			}else if(nextProps.profile.step === 0){
				this.props.history.push('/setup-profile');
			}else if(nextProps.profile.user && nextProps.profile.user.status===false)
			{
				this.props.history.push('/awaiting-approval');
			}
 

		}
		 
		if(nextProps.savedSearches && nextProps.savedSearches.savedContent)
		{	
			
			this.setState({searchedFilters: nextProps.savedSearches.savedContent});
		}


		if(nextProps.profile.profile_pic){
			  
			this.setState({
				imageURL: nextProps.profile.profile_pic
			})  

		}


		if(nextProps.auth.isAuthenticated){
			this.setState({ 
				name: nextProps.auth.user.name,
				email: nextProps.auth.user.email
			})
		}

	}

	componentWillUnmount()
	{
		this.props.loadProfile();
	}

	handleClose = () => {
    this.setState({ Msg: false });
	};
	
	onLogoutClick(e)
	{
		e.preventDefault();
		this.props.logoutUser();
		this.props.history.push('/login');
	} 


	toggleDrawer = (open) => () => {
		this.setState({
		  drawer: open,
		});
	};

	handleChange = (event, value) => {
		this.setState({ tabIndex: value });
	};
 

	handleChangeSwitch = (e) => {
		
		e.preventDefault();
		
		var check = true; 

		if(this.state[e.target.name])
		{
			check = false;
		}

		this.setState({ notifications : check });

		const notifications = {
			notifications: check
		};

		this.props.allowUserNotifications(notifications); 

		if(check===true)
		{	window.localStorage.setItem('notification',true);
			if (
				isMobile && !isEmpty(window.FirebasePlugin) &&
				(isAndroid || isIOS)
			) {
				if (isIOS) {
					window.FirebasePlugin.grantPermission();
				  }
				window.FirebasePlugin.onTokenRefresh((token) => {
					// save this server-side and use it to push notifications to this 
					 
					if(token){
						let tokenData={}
						if(isIOS){
							  tokenData = {
							  token: token,
							  type:'ios'
							}; 
						}
						else if(isAndroid){
						  tokenData = {
							  token: token,
							  type:'android'
							}; 
						}
						this.props.saveNotificationsToken(tokenData);
					}
 
				});

				}else{
 
					//subscribe user to the push notifications

					AskForPermissionAndGenerateToken().then( token => {

						if(token){
							
							const tokenData = {
								token: token,
								type:'web'
							  }; 

							this.props.saveNotificationsToken(tokenData);
						}

					});

				}

			
		}else{
			window.localStorage.setItem('notification',false);
			//unsubscribe user to the push notifications
			 
			// const tokenData = {
			// 	token: null
			// };

			// this.props.saveNotificationsToken(tokenData);


		}

		
		
	};

	handleChangeIndex = index => {
		this.setState({ tabIndex: index });
	}; 

	handlePopupOpen = () => {
		this.setState({ openPopup: true });
	};

	handlePopupClose = (filterData) => {
		this.setState({ filterData, openPopup: false }); 
	};

	filterVehicles = () => {	 

		const filterName = this.state.filterData.filterName;
		const filterData = this.state.filterData;	 

		this.props.saveUserSearchFilters(
			{
				filterName:filterName,
				filterData:filterData
			}
		); 
		
		this.setState({ Msg: true, MsgVariant: "success", MsgText: 'Filter Added' });
	}

	filterUpdateVehicles = () => {	 

		const filterName = this.state.filterData.filterName;
		const filterData = this.state.filterData;	 

		this.props.updateUserSearchFilters(
			{
				filterName:filterName,
				filterData:filterData,
				filterId: this.state.filterId
			}
		); 
	
		this.setState({ Msg: true, MsgVariant: "success", MsgText: 'Filter Updated' });
	}

	resetFilters = () => {

		var resetData = {
			filterName:'',
			vehicleType: '',
			Mileage: '',
			Make: [],
			Model: '',
			Year: '',
			YearEnd:'',
			PriceRange: '',
			nznew: '' 
		};
		
		this.setState({filterData: resetData});
	}

	addSearchFilter = () => {
		this.resetFilters();
		this.handlePopupOpen();
	}
	
	editSearchFilter = (e) =>{
		e.preventDefault();

		try{

			const filterId = e.currentTarget.getAttribute('id');
			const filterDataLocal = JSON.parse(e.currentTarget.getAttribute('filterData'));
			const filterName = e.currentTarget.getAttribute('filterName'); 

			const filterData = {
				filterName,
				vehicleType: filterDataLocal.vehicleType,
				Mileage: filterDataLocal.Mileage,
				Make: filterDataLocal.Make,
				Model: filterDataLocal.Model,
				Year: filterDataLocal.Year,
				YearEnd: filterDataLocal.YearEnd ? filterDataLocal.YearEnd : '',
				PriceRange: filterDataLocal.PriceRange,
				nznew: filterDataLocal.nznew,
				edit:true
			};

			this.setState({filterId:filterId, filterData: filterData});

		}catch(err){
			console.log(err);
		} 
		this.handlePopupOpen();
	}


	removeSearchFilter = (e) =>{
		e.preventDefault();
		const filterId = e.currentTarget.getAttribute('id');
		const filterData = {
			filterId: filterId
		};

		this.props.deleteUserSearchFilters(filterData);

		this.setState({ Msg: true, MsgVariant: "success", MsgText: 'Filter Removed' });
	}

	render() { 
		const { classes } = this.props; 

		return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>   

				<RipaDrawer 
	    			status={this.state.drawer} 
	    			onOpen={this.toggleDrawer(true)} 
	    			onClose={this.toggleDrawer(false)} 
	    			userProfile={this.state.imageURL}
	    			userEmail={this.state.email}
	    			onLogout={this.onLogoutClick.bind(this)}
	    				
	    		/>

		    	<RipaAppBar 
		    		menuClick={this.toggleDrawer(true)} 
		    		onChange={this.handleChange}
		    		tabIndex={this.state.tabIndex}
		    		title="Settings" 
		    		tabs={['General', 'Configure']}

		    	/>  


		    	{ this.state.screenLoading ? 

		    		(
		    			<section className={classes.loadingScreen}>
		    				 
		    				 <CircularProgress className={classes.progress} />	
		    				 <br/><br/>Loading...
		    				  
		    		 	</section>
		    		 )

		    		:

		    		(
			  	 
				<SwipeableViews
		          axis={'x'}
		          index={this.state.tabIndex}
		          onChangeIndex={this.handleChangeIndex}
				>
		           
		          <section dir={'x'} className={classes.sectionSettings}>
	          		
	          		<Toolbar className={classes.notiToolbar}>
			            <IconButton onClick={this.props.menuClick} className={classes.notiButton} color="inherit" aria-label="Open drawer">
			              <NotificationsIcon />
			            </IconButton>
			            <Typography className={classes.notiTitle} color="inherit" noWrap>
			              Notifications
			            </Typography>
			             
			            <div className={classes.grow} />
			            <div className={classes.sectionDesktop}>
			              
			              <Switch
					          checked={this.state.notifications} 
					          onChange={this.handleChangeSwitch.bind(this)}
					          value={this.state.notifications} 
					          name='notifications'
					          className={classes.notiSwitch}
					        />
			              
			            </div> 
			        </Toolbar>

			        <Divider /> 

				   </section>

				   <section dir={'x'} className={classes.sectionSettings}>
				   	<Toolbar className={classes.notiToolbar}> 
			            <Typography className={classes.notiTitle} color="inherit" noWrap>
			              Saved Searches <span>(maximum 3)</span>
			            </Typography>
			             
			            <div className={classes.grow} />
			            <div className={classes.sectionDesktop}>

						{
						this.state.searchedFilters && !isEmpty(this.state.searchedFilters) 
						?

						(this.state.searchedFilters.length<=2)  &&

						<IconButton onClick={this.addSearchFilter} className={classes.notiButton} color="inherit" aria-label="Open drawer">
			              <AddCircleOutlineIcon />
			            </IconButton>
						
						:
						<IconButton onClick={this.addSearchFilter} className={classes.notiButton} color="inherit" aria-label="Open drawer">
							<AddCircleOutlineIcon />
						</IconButton>
						
						} 
			            </div> 
			        </Toolbar>

			        <Divider /> 	 


					{
						this.state.searchedFilters && !isEmpty(this.state.searchedFilters) 
						
						?

						this.state.searchedFilters.map((filter, key) => (
							   
								  <Grid key={key} className={classes.bidRow} space={24}>
 
									   <Toolbar className={classes.notiToolbar}>
											 
											<Typography className={classes.notiTitle} color="inherit" noWrap>
											{filter.filterData.filterName}
											</Typography>
											
											<div className={classes.grow} />
											<div className={classes.sectionDesktop}>

											<IconButton onClick={this.editSearchFilter.bind(this)} id={filter._id} filterData={JSON.stringify(filter.filterData)} filterName={filter.name} className={classes.notiButton} color="inherit" aria-label="Open drawer">
											<EditIcon />
											</IconButton>
											
											<IconButton onClick={this.removeSearchFilter.bind(this)} id={filter._id} className={classes.notiButton} color="inherit" aria-label="Open drawer">
											<CloseIcon />
											</IconButton>
											
											</div> 
										</Toolbar>
									  <Divider />

								  </Grid>
								 
						)	
						)
						:
						(
							<Grid space={24}>

									   <div className={classes.noSavedSearch}>No saved search yet.</div>
									  <Divider />

								  </Grid>
						)
					}	


				   </section>
 

	             </SwipeableViews> 

	             )
	         }

					
				<footer className={classes.footer}>
					 
				</footer>

				<FilterPopup
					classes={classes}
					open={this.state.openPopup}
					onClose={this.handlePopupClose} 
					filterData={this.state.filterData}
					filterVehicles={this.filterVehicles}
					filterUpdateVehicles={this.filterUpdateVehicles}
					filterForSearches={true}
					resetFilters={this.resetFilters}
				/>

					{this.state.Msg ? (
              <SnackBar
                open={this.state.Msg}
                duration={5000}
                variant={this.state.MsgVariant}
                className={classes.margin}
                message={this.state.MsgText}
                onClose={this.handleClose}
              />
            ) : null}
	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

Settings.propTypes = {
	logoutUser: PropTypes.func.isRequired,
	profile: PropTypes.object.isRequired, 
	auth: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,
	profile: state.profile,  
	errors: state.errors,
	savedSearches: state.savedSearches
});

export default compose(
  withStyles(myAccountStyles),
  connect(mapStateToProps, { logoutUser, allowUserNotifications, loadProfile, saveNotificationsToken, saveUserSearchFilters, getUserSearchFilters, deleteUserSearchFilters, updateUserSearchFilters })
)(withRouter(Settings));