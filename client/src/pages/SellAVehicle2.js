import React from "react"; 
import PropTypes from 'prop-types';
import { Link, withRouter } from "react-router-dom";  
import { connect } from 'react-redux'; 
import { sellAVehicle } from '../actions/vehicleActions'; 
import { compose } from 'redux'; 
import classnames from 'classnames'; 
import CircularProgress from '@material-ui/core/CircularProgress'; 
import SnackBar from '../components/common/SnackBar';

import Divider from '@material-ui/core/Divider';

import withStyles from "@material-ui/core/styles/withStyles";   
import ArrowBack from '@material-ui/icons/ArrowBack';    
import CloseIcon from '@material-ui/icons/Close'; 

import Grid from '@material-ui/core/Grid';  
import LinearProgress from '@material-ui/core/LinearProgress'; 

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'; 
import IconButton from '@material-ui/core/IconButton'; 
import Button from '@material-ui/core/Button'; 
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem'; 
import Select from '@material-ui/core/Select';
import { DatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import sellAVehicleStyles from "../assets/jss/pages/sellAVehicleStyles.jsx";
let vehicle_make = [ 
	"Alfa Romeo", 
	"Aston Martin", 
	"Audi", 
	"Austin", 
	"Bentley", 
	"BMW", 
	"Cadillac", 
	"Chery", 
	"Chevrolet", 
	"Chrysler", 
	"Citroen", 
	"Daewoo", 
	"Daihatsu", 
	"Daimler", 
	"Dodge", 
	"DS Automobiles", 
	"Ferrari", 
	"Fiat", 
	"Ford", 
	"Foton", 
	"GMC", 
	"Geely", 
	"Great Wall", 
	"HAVAL", 
	"Holden", 
	"Honda", 
	"HSV", 
	"INFINITI", 
	"Hyundai", 
	"Hummer", 
	"Isuzu", 
	"Jaguar", 
	"Iveco", 
	"Jeep", 
	"Kia", 
	"Lamborghini", 
	"Lancia", 
	"Land Rover", 
	"LDV", 
	"Lexus", 
	"Lotus", 
	"Mahindra", 
	"Maserati", 
	"MINI", 
	"MG", 
	"Mercedes-Benz", 
	"McLaren", 
	"Mazda", 
	"Morgan", 
	"Mitsubishi", 
	"Morris", 
	"Nissan", 
	"Opel", 
	"Peugeot", 
	"Pontiac", 
	"Porsche", 
	"Ram", 
	"Renault", 
	"Riley", 
	"Rolls-Royce", 
	"Rover", 
	"Saab", 
	"SEAT", 
	"Suzuki", 
	"Subaru", 
	"Ssongyong", 
	"Smart", 
	"Skoda", 
	"Tesla", 
	"Toyota", 
	"Triumph", 
	"TVR", 
	"Vauxhall", 
	"Volkswagen", 
	"Volvo", 
	"Other"
  ]
  
const materialTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: '#ec1d25',
      },
    },
    MuiPickersDay: {
      "&$selected": {
        backgroundColor: '#ec1d25',
      },
    }
  },
});
 

class SellAVehicle2 extends React.Component {
	
	constructor(){
		super();
		this.state = { 
			registration_number:'',
			initial:"Select Option",
			vin:'',
			year:'',
			make:'',
			model:'', 
			km:'', 
			transmission:'', 
			nz_new:'',
			location:'',
			power_train:'',
			vehicle_type:'',
			colour:'',
			engine:'',
			fuel_type:'', 
			registration_expire:'', 
			wof_expire:'',
			loading:false,
			Msg:false,
			MsgVariant:false,
			MsgText:false,
			errors: {}, 
			regnError : {}
		}
 
	}

	componentWillMount(){
	    document.title = "Sell A Vehicle | Ripa - Car App"; 
	}

	patchVehicleForm(vehicle){
		this.setState({
			registration_number: vehicle.registration_number,
			vin: vehicle.vin,
			year: vehicle.year,
			make:vehicle.make,
			model:vehicle.model, 
			km:vehicle.km, 
			transmission:vehicle.transmission, 
			nz_new:vehicle.nz_new,
			location:vehicle.location,
			power_train:vehicle.power_train,
			vehicle_type:vehicle.vehicle_type,
			colour:vehicle.colour,
			engine:vehicle.engine,
			fuel_type:vehicle.fuel_type,
			registration_expire: new Date(vehicle.registration_expire),
			wof_expire: new Date(vehicle.wof_expire)
		})
	}

	componentDidMount(){
			let cachedVehicleData = JSON.parse(sessionStorage.getItem('vehicleData'));
			if(cachedVehicleData){
				this.patchVehicleForm(cachedVehicleData);
			}
	    if(!this.props.auth.isAuthenticated){
			this.props.history.push('/login');
		}   

		if(!this.props.vehicle.sell.step)
		{
			this.props.history.push('/sell-a-vehicle');
		}		
		if(this.props.vehicle.sell.registration_number)
		{
			this.setState({registration_number: this.props.vehicle.sell.registration_number});
		}
		if(this.props.vehicle.sell.vin)
		{
			this.setState({vin: this.props.vehicle.sell.vin});
		}

		if(this.props.vehicle.sell.year)
		{
			this.setState({year: this.props.vehicle.sell.year});
		}

		if(this.props.vehicle.sell.make)
		{	
			let make = vehicle_make.find(make=>{
				let regx = new RegExp(make,"ig");
				if(this.props.vehicle.sell.make.match(regx))
				{
					return make
				}
			})
			if(make)
			{
				this.setState({make})
			}
			else{
				this.setState({make:"Other"})
			}
		}

		if(this.props.vehicle.sell.model)
		{
			this.setState({model: this.props.vehicle.sell.model});
		}

		if(this.props.vehicle.sell.vehicle_type)
		{
			this.setState({vehicle_type: this.props.vehicle.sell.vehicle_type});
		}

		if(this.props.vehicle.sell.colour)
		{
			this.setState({colour: this.props.vehicle.sell.colour});
		}

		if(this.props.vehicle.sell.engine)
		{
			this.setState({engine: this.props.vehicle.sell.engine});
		}

		if(this.props.vehicle.sell.fuel_type)
		{
			this.setState({fuel_type: this.props.vehicle.sell.fuel_type});
		}
		if(this.props.vehicle.sell.km)
		{
			this.setState({km: this.props.vehicle.sell.km });
		}

		if(this.props.vehicle.sell.wof_expire)
		{
			this.setState({wof_expire: new Date(this.props.vehicle.sell.wof_expire)});
		}

		if(this.props.vehicle.sell.registration_expire)
		{
			this.setState({registration_expire: new Date(this.props.vehicle.sell.registration_expire)});
		}

		if(this.props.vehicle.sell.regnError)
		{			 
			this.setState({regnError : this.props.vehicle.sell.regnError});
		}
	}  

	componentWillReceiveProps(nextProps){
 			
 		if(nextProps.vehicle.sell.step===2)
 		{
 			this.props.history.push('/sell-a-vehicle-3');
 		}	

		if(nextProps.errors && Object.keys(nextProps.errors).length !== 0){
			this.setState({ errors: nextProps.errors});

			if(nextProps.errors && Object.keys(nextProps.errors).length !== 0)
			{
				this.setState({loading:false, Msg:true, MsgVariant: 'error', MsgText: 'There are errors above.'});
			}
 
		}
	}

 	onChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	}

	onSubmit = async (e) => {
		e.preventDefault();
		
 		this.setState({loading: true});
 		 
 		const vehicleData = {
 			step: 2,
 			registration_number : this.state.registration_number,
 			vin:this.state.vin,
			year:this.state.year,
			cvg:this.state.cvg,
			make:this.state.make,
			model:this.state.model,
			km:this.state.km, 
			transmission:this.state.transmission, 
			nz_new:this.state.nz_new,
			location:this.state.location,
			power_train:this.state.power_train,
			vehicle_type:this.state.vehicle_type,
			colour:this.state.colour,
			engine:this.state.engine,
			fuel_type:this.state.fuel_type,
			registration_expire:this.state.registration_expire,
			wof_expire:this.state.wof_expire
		 }

		sessionStorage.setItem('vehicleData', JSON.stringify(vehicleData));

 		this.props.sellAVehicle(vehicleData);
	}
 
	render() { 
		const { classes } = this.props;
	 	const { errors } = this.state;
		
		 return (
	    	
	    	<Grid container justify="center" className={classes.container}>
			 <Grid item xs={12} sm={12} md={4}>
	    	<div>      
	          	

	          	<AppBar className={classes.AppBar} position="fixed">
			          <Toolbar>
			            <IconButton onClick={this.props.history.goBack} className={classes.menuButton} color="inherit" aria-label="Go back">
			              <ArrowBack />
			            </IconButton>
			   			
			            <header className={classes.header}>
							<h3>Sell a vehicle</h3>
							<div className={classes.Progress}>
								<LinearProgress 
									color="secondary" 
									variant="determinate" 
									value={40}
								/>
							</div>
						</header> 

			            <Link to="/">
				            <IconButton className={classes.menuButton} color="inherit" aria-label="Close">
				              <CloseIcon />
				            </IconButton>
			            </Link>

			          </Toolbar> 

	        	</AppBar> 

        		<section className={classes.section}>

					<form onSubmit={this.onSubmit} className={classes.form}>					
				 		 
						<h2 className={classes.pageTitle}>Car registration</h2>
						 
						{(this.state.regnError.errorCode==='NO_REGN') && (
							<span className={classes.errorMsg}>{this.state.regnError.error}</span>
						)}
 						<Grid className={classes.field}>
  						<TextField
				          id="registration_number"
				          placeholder="Registration number"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.registration_number}
				          )}
				          type="text"
				          name="registration_number" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.registration_number}
				          onChange={this.onChange}
				        />
				        <label htmlFor="registration_number">Registration number</label>
					
				        {errors.registration_number && (
				        	<span className={classes.errorMsg}>{errors.registration_number}</span>
				        )}

				        </Grid>
 						
 						<Grid className={classes.field}> 
						<TextField
				          id="vin"
				          placeholder="Vin"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.vin}
				          )}
				          type="text"
				          name="vin" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.vin}
				          onChange={this.onChange}
				        />
				        <label htmlFor="vin">Vin</label>

				        {errors.vin && (
				        	<span className={classes.errorMsg}>{errors.vin}</span>
				        )}

				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="year"
				          placeholder="Year"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.year}
				          )}
				          type="number"
				          name="year" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.year}
				          onChange={this.onChange}
				        />
				        <label htmlFor="year">Year</label>

				        {errors.year && (
				        	<span className={classes.errorMsg}>{errors.year}</span>
				        )}

				        </Grid>

				        <Grid className={classes.field}>
				        <Select
							value={this.state.make}
				            onChange={this.onChange}
							name="make"
							displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.make}
				          )}
				          >
				            <MenuItem value="" disabled>
				             {this.state.initial}
				            </MenuItem>
				            <MenuItem value="Alfa Romeo">Alfa Romeo</MenuItem>
				            <MenuItem value="Aston Martin">Aston Martin</MenuItem> 
				            <MenuItem value="Audi">Audi</MenuItem> 
				            <MenuItem value="Austin">Austin</MenuItem> 
				            <MenuItem value="Bentley">Bentley</MenuItem> 
				            <MenuItem value="BMW">BMW</MenuItem> 
				            <MenuItem value="Cadillac">Cadillac</MenuItem> 
				            <MenuItem value="Chery">Chery</MenuItem> 
				            <MenuItem value="Chevrolet">Chevrolet</MenuItem> 
				            <MenuItem value="Chrysler">Chrysler</MenuItem> 
				            <MenuItem value="Citroen">Citroen</MenuItem> 
				            <MenuItem value="Daewoo">Daewoo</MenuItem> 
				            <MenuItem value="Daihatsu">Daihatsu</MenuItem> 
				            <MenuItem value="Daimler">Daimler</MenuItem> 
				            <MenuItem value="Dodge">Dodge</MenuItem> 
				            <MenuItem value="DS Automobiles">DS Automobiles</MenuItem> 
				            <MenuItem value="Ferrari">Ferrari</MenuItem> 
				            <MenuItem value="Fiat">Fiat</MenuItem> 
				            <MenuItem value="Ford">Ford</MenuItem> 
				            <MenuItem value="Foton">Foton</MenuItem> 
				            <MenuItem value="Geely">Geely</MenuItem> 
				            <MenuItem value="GMC">GMC</MenuItem> 
				            <MenuItem value="Great Wall">Great Wall</MenuItem> 
				            <MenuItem value="HAVAL">HAVAL</MenuItem> 
				            <MenuItem value="Holden">Holden</MenuItem> 
				            <MenuItem value="Honda">Honda</MenuItem> 
				            <MenuItem value="HSV">HSV</MenuItem> 
				            <MenuItem value="Hummer">Hummer</MenuItem> 
				            <MenuItem value="Hyundai">Hyundai</MenuItem> 
				            <MenuItem value="INFINITI">INFINITI</MenuItem> 
				            <MenuItem value="Isuzu">Isuzu</MenuItem> 
				            <MenuItem value="Iveco">Iveco</MenuItem> 
				            <MenuItem value="Jaguar">Jaguar</MenuItem> 
				            <MenuItem value="Jeep">Jeep</MenuItem> 
				            <MenuItem value="Kia">Kia</MenuItem> 
				            <MenuItem value="Lamborghini">Lamborghini</MenuItem> 
				            <MenuItem value="Lancia">Lancia</MenuItem> 
				            <MenuItem value="Land Rover">Land Rover</MenuItem> 
				            <MenuItem value="LDV">LDV</MenuItem> 
				            <MenuItem value="Lexus">Lexus</MenuItem> 
				            <MenuItem value="Lotus">Lotus</MenuItem> 
				            <MenuItem value="Mahindra">Mahindra</MenuItem> 
				            <MenuItem value="Maserati">Maserati</MenuItem> 
				            <MenuItem value="Mazda">Mazda</MenuItem> 
				            <MenuItem value="McLaren">McLaren</MenuItem> 
				            <MenuItem value="Mercedes-Benz">Mercedes-Benz</MenuItem> 
				            <MenuItem value="MG">MG</MenuItem> 
				            <MenuItem value="MINI">MINI</MenuItem> 
				            <MenuItem value="Mitsubishi">Mitsubishi</MenuItem> 
				            <MenuItem value="Morgan">Morgan</MenuItem> 
				            <MenuItem value="Morris">Morris</MenuItem> 
				            <MenuItem value="Nissan">Nissan</MenuItem> 
				            <MenuItem value="Opel">Opel</MenuItem>  
				            <MenuItem value="Peugeot">Peugeot</MenuItem> 
				            <MenuItem value="Pontiac">Pontiac</MenuItem> 
				            <MenuItem value="Porsche">Porsche</MenuItem> 
				            <MenuItem value="Ram">Ram</MenuItem> 
				            <MenuItem value="Renault">Renault</MenuItem> 
				            <MenuItem value="Riley">Riley</MenuItem> 
				            <MenuItem value="Rolls-Royce">Rolls-Royce</MenuItem> 
				            <MenuItem value="Rover">Rover</MenuItem> 
				            <MenuItem value="Saab">Saab</MenuItem> 
				            <MenuItem value="SEAT">SEAT</MenuItem> 
				            <MenuItem value="Skoda">Skoda</MenuItem> 
				            <MenuItem value="Smart">Smart</MenuItem> 
				            <MenuItem value="Ssongyong">Ssongyong</MenuItem> 
				            <MenuItem value="Subaru">Subaru</MenuItem> 
				            <MenuItem value="Suzuki">Suzuki</MenuItem> 
				            <MenuItem value="Tesla">Tesla</MenuItem> 
				            <MenuItem value="Toyota">Toyota</MenuItem> 
				            <MenuItem value="Triumph">Triumph</MenuItem> 
				            <MenuItem value="TVR">TVR</MenuItem> 
				            <MenuItem value="Vauxhall">Vauxhall</MenuItem> 
				            <MenuItem value="Volkswagen">Volkswagen</MenuItem> 
				            <MenuItem value="Volvo">Volvo</MenuItem> 
				            <MenuItem value="Other">Other</MenuItem>  
				        </Select>
				        <label htmlFor="make">Make</label>

				        {errors.make && (
				        	<span className={classes.errorMsg}>{errors.make}</span>
				        )}

				        </Grid>

				        <Grid className={classes.field}>
				        <TextField
				          id="model"
				          placeholder="Model"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.model}
				          )}
				          type="text"
				          name="model" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.model}
				          onChange={this.onChange}
				        />
				        <label htmlFor="model">Model</label>

				        {errors.model && (
				        	<span className={classes.errorMsg}>{errors.model}</span>
				        )}

				        </Grid>  


								<Divider class={classes.vehicleDivider} />

								<Grid className={classes.field}>
				        <TextField
				          id="engine"
				          placeholder="Engine Size"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.engine}
				          )}
				          type="text"
				          name="engine" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.engine}
				          onChange={this.onChange}
				        />
				        <label htmlFor="engine">Engine Size</label>

				        {errors.engine && (
				        	<span className={classes.errorMsg}>{errors.engine}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
								<MuiPickersUtilsProvider utils={MomentUtils}>
									<MuiThemeProvider theme={materialTheme}>
										<div className={classnames(classes.textField, 
												{ [classes.textError] : errors.registration_expire}
											)}>
											<DatePicker
												name="registration_expire"
												value={this.state.registration_expire}
												onChange={(e)=>this.setState({ registration_expire: e.format('YYYY-MM-DD') })}
												format="YYYY-MM-DD"
											/>
										</div>
									</MuiThemeProvider>
								</MuiPickersUtilsProvider>
				        <label htmlFor="registration_expire">Registration Expires</label>

				        {errors.registration_expire && (
				        	<span className={classes.errorMsg}>{errors.registration_expire}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
								<MuiPickersUtilsProvider utils={MomentUtils}>
									<MuiThemeProvider theme={materialTheme}>
										<div className={classnames(classes.textField, 
												{ [classes.textError] : errors.wof_expire}
											)}>
											<DatePicker
												name="wof_expire"
												value={this.state.wof_expire}
												onChange={(e)=>this.setState({ wof_expire: e.format('YYYY-MM-DD') })}
												format="YYYY-MM-DD"
											/>
										</div>
									</MuiThemeProvider>
								</MuiPickersUtilsProvider>
								<label htmlFor="wof_expire">WOF Expires</label>
				        {errors.wof_expire && (
				        	<span className={classes.errorMsg}>{errors.wof_expire}</span>
				        )}

				        </Grid> 
				        
								<Divider class={classes.vehicleDivider} />

				        <Grid className={classes.field}>
				        <TextField
				          id="km"
				          placeholder="Kilometers"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.km}
				          )}
				          type="text"
				          name="km" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.km}
						  onChange={this.onChange}
						  InputProps={{
							endAdornment: <InputAdornment position="end">KM</InputAdornment>,
						  }}
				        />
				        <label htmlFor="km">Kilometers</label>

				        {errors.km && (
				        	<span className={classes.errorMsg}>{errors.km}</span>
				        )}

				        </Grid>  

				        <Grid className={classes.field}>
				        <Select
				            value={this.state.transmission}
				            onChange={this.onChange}
				            name="transmission"
				            displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.transmission}
				          )}
				          >
				            <MenuItem value="" disabled>
				              Select Option
				            </MenuItem>
				            <MenuItem value="Don't Know">Don't Know</MenuItem>
				            <MenuItem value="Manual">Manual</MenuItem> 
				            <MenuItem value="Automatic">Automatic</MenuItem> 
				            <MenuItem value="Tiptronic">Tiptronic</MenuItem> 
				        </Select>
				        <label htmlFor="transmission">Transmission</label>

				        {errors.transmission && (
				        	<span className={classes.errorMsg}>{errors.transmission}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
				        <RadioGroup
				            aria-label="NZ New"
				            name="nz_new"
				            className={classnames( classes.radioGroup, 
				          	{ [classes.textError] : errors.nz_new}
				            )}
				            value={this.state.nz_new}
				            onChange={this.onChange}
				          >
				            <FormControlLabel value="1" control={<Radio />} label="Yes" />
				            <FormControlLabel value="0" control={<Radio />} label="No" /> 
				        </RadioGroup>
				        <label htmlFor="nz_new">NZ New</label>

				        {errors.nz_new && (
				        	<span className={classes.errorMsg}>{errors.nz_new}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}> 
						<Select
				            value={this.state.location}
				            onChange={this.onChange}
				            name="location"
				            displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.location}
				          )}
				          >
				            <MenuItem value="" disabled>
				              Select Option
				            </MenuItem>
				            <MenuItem value="Northland - Dargaville">Northland - Dargaville</MenuItem>
							<MenuItem value="Northland - Kaikohe">Northland - Kaikohe</MenuItem>
							<MenuItem value="Northland - Kaitaia">Northland - Kaitaia</MenuItem>
							<MenuItem value="Northland - Kawakawa">Northland - Kawakawa</MenuItem>
							<MenuItem value="Northland - Kerikeri">Northland - Kerikeri</MenuItem>
							<MenuItem value="Northland - Maungaturoto">Northland - Maungaturoto</MenuItem>
							<MenuItem value="Northland - Paihia">Northland - Paihia</MenuItem>
							<MenuItem value="Northland - Whangarei">Northland - Whangarei</MenuItem>
							<MenuItem value="Auckland - Auckland City">Auckland - Auckland City</MenuItem>
							<MenuItem value="Auckland - Franklin">Auckland - Franklin</MenuItem>
							<MenuItem value="Auckland - Great Barrier Island">Auckland - Great Barrier Island</MenuItem>
							<MenuItem value="Auckland - Helensville">Auckland - Helensville</MenuItem>
							<MenuItem value="Auckland - Hibiscus Coast">Auckland - Hibiscus Coast</MenuItem>
							<MenuItem value="Auckland - Manukau City">Auckland - Manukau City</MenuItem>
							<MenuItem value="Auckland - North Shore">Auckland - North Shore</MenuItem>
							<MenuItem value="Auckland - Papakura City">Auckland - Papakura City</MenuItem>
							<MenuItem value="Auckland - Waiheke Island">Auckland - Waiheke Island</MenuItem>
							<MenuItem value="Auckland - Waitakere City">Auckland - Waitakere City</MenuItem>
							<MenuItem value="Auckland - Warkworth">Auckland - Warkworth</MenuItem>
							<MenuItem value="Auckland - Wellsford">Auckland - Wellsford</MenuItem>
							<MenuItem value="Waikato - Cambridge">Waikato - Cambridge</MenuItem>
							<MenuItem value="Waikato - Coromandel">Waikato - Coromandel</MenuItem>
							<MenuItem value="Waikato - Hamilton">Waikato - Hamilton</MenuItem>
							<MenuItem value="Waikato - Huntly">Waikato - Huntly</MenuItem>
							<MenuItem value="Waikato - Matamata">Waikato - Matamata</MenuItem>
							<MenuItem value="Waikato - Morrinsville">Waikato - Morrinsville</MenuItem>
							<MenuItem value="Waikato - Otorohanga">Waikato - Otorohanga</MenuItem>
							<MenuItem value="Waikato - Paeroa">Waikato - Paeroa</MenuItem>
							<MenuItem value="Waikato - Raglan">Waikato - Raglan</MenuItem>
							<MenuItem value="Waikato - Taumarunui">Waikato - Taumarunui</MenuItem>
							<MenuItem value="Waikato - Taupo">Waikato - Taupo</MenuItem>
							<MenuItem value="Waikato - Te Awamutu">Waikato - Te Awamutu</MenuItem>
							<MenuItem value="Waikato - Te Kuiti">Waikato - Te Kuiti</MenuItem>
							<MenuItem value="Waikato - Thames">Waikato - Thames</MenuItem>
							<MenuItem value="Waikato - Tokoroa/Putaruru">Waikato - Tokoroa/Putaruru</MenuItem>
							<MenuItem value="Waikato - Turangi">Waikato - Turangi</MenuItem>
							<MenuItem value="Waikato - Waihi">Waikato - Waihi</MenuItem>
							<MenuItem value="Waikato - Whangamata">Waikato - Whangamata</MenuItem>
							<MenuItem value="Waikato - Whitianga">Waikato - Whitianga</MenuItem>
							<MenuItem value="Bay of Plenty - Katikati">Bay of Plenty - Katikati</MenuItem>
							<MenuItem value="Bay of Plenty - Mt. Maunganui">Bay of Plenty - Mt. Maunganui</MenuItem>
							<MenuItem value="Bay of Plenty - Opotiki">Bay of Plenty - Opotiki</MenuItem>
							<MenuItem value="Bay of Plenty - Rotorua">Bay of Plenty - Rotorua</MenuItem>
							<MenuItem value="Bay of Plenty - Tauranga">Bay of Plenty - Tauranga</MenuItem>
							<MenuItem value="Bay of Plenty - Te Puke">Bay of Plenty - Te Puke</MenuItem>
							<MenuItem value="Bay of Plenty - Waihi Beach">Bay of Plenty - Waihi Beach</MenuItem>
							<MenuItem value="Bay of Plenty - Whakatane">Bay of Plenty - Whakatane</MenuItem>
							<MenuItem value="Gisborne - Gisborne">Gisborne - Gisborne</MenuItem>
							<MenuItem value="Gisborne - Ruatoria">Gisborne - Ruatoria</MenuItem>
							<MenuItem value="Hawke's Bay - Hastings">Hawke's Bay - Hastings</MenuItem>
							<MenuItem value="Hawke's Bay - Napier">Hawke's Bay - Napier</MenuItem>
							<MenuItem value="Hawke's Bay - Waipukurau">Hawke's Bay - Waipukurau</MenuItem>
							<MenuItem value="Hawke's Bay - Wairoa">Hawke's Bay - Wairoa</MenuItem>
							<MenuItem value="Taranaki - Hawera">Taranaki - Hawera</MenuItem>
							<MenuItem value="Taranaki - Mokau">Taranaki - Mokau</MenuItem>
							<MenuItem value="Taranaki - New Plymouth">Taranaki - New Plymouth</MenuItem>
							<MenuItem value="Taranaki - Opunake">Taranaki - Opunake</MenuItem>
							<MenuItem value="Taranaki - Stratford">Taranaki - Stratford</MenuItem>
							<MenuItem value="Whanganui - Ohakune">Whanganui - Ohakune</MenuItem>
							<MenuItem value="Whanganui - Taihape">Whanganui - Taihape</MenuItem>
							<MenuItem value="Whanganui - Waiouru">Whanganui - Waiouru</MenuItem>
							<MenuItem value="Whanganui - Whanganui">Whanganui - Whanganui</MenuItem>
							<MenuItem value="Manawatu - Bulls">Manawatu - Bulls</MenuItem>
							<MenuItem value="Manawatu - Dannevirke">Manawatu - Dannevirke</MenuItem>
							<MenuItem value="Manawatu - Feilding">Manawatu - Feilding</MenuItem>
							<MenuItem value="Manawatu - Levin">Manawatu - Levin</MenuItem>
							<MenuItem value="Manawatu - Manawatu">Manawatu - Manawatu</MenuItem>
							<MenuItem value="Manawatu - Marton">Manawatu - Marton</MenuItem>
							<MenuItem value="Manawatu - Pahiatua">Manawatu - Pahiatua</MenuItem>
							<MenuItem value="Manawatu - Palmerston North">Manawatu - Palmerston North</MenuItem>
							<MenuItem value="Manawatu - Woodville">Manawatu - Woodville</MenuItem>
							<MenuItem value="Wairarapa - Carterton">Wairarapa - Carterton</MenuItem>
							<MenuItem value="Wairarapa - Featherston">Wairarapa - Featherston</MenuItem>
							<MenuItem value="Wairarapa - Greytown">Wairarapa - Greytown</MenuItem>
							<MenuItem value="Wairarapa - Martinborough">Wairarapa - Martinborough</MenuItem>
							<MenuItem value="Wairarapa - Masterton">Wairarapa - Masterton</MenuItem>
							<MenuItem value="Wellington - Kapiti">Wellington - Kapiti</MenuItem>
							<MenuItem value="Wellington - Lower Hutt City">Wellington - Lower Hutt City</MenuItem>
							<MenuItem value="Wellington - Porirua">Wellington - Porirua</MenuItem>
							<MenuItem value="Wellington - Upper Hutt City">Wellington - Upper Hutt City</MenuItem>
							<MenuItem value="Wellington - Wellington City">Wellington - Wellington City</MenuItem>
							<MenuItem value="Nelson Bays - Golden Bay">Nelson Bays - Golden Bay</MenuItem>
							<MenuItem value="Nelson Bays - Motueka">Nelson Bays - Motueka</MenuItem>
							<MenuItem value="Nelson Bays - Murchison">Nelson Bays - Murchison</MenuItem>
							<MenuItem value="Nelson Bays - Nelson">Nelson Bays - Nelson</MenuItem>
							<MenuItem value="Marlborough - Blenheim">Marlborough - Blenheim</MenuItem>
							<MenuItem value="Marlborough - Marlborough Sounds">Marlborough - Marlborough Sounds</MenuItem>
							<MenuItem value="Marlborough - Picton">Marlborough - Picton</MenuItem>
							<MenuItem value="West Coast - Greymouth">West Coast - Greymouth</MenuItem>
							<MenuItem value="West Coast - Hokitika">West Coast - Hokitika</MenuItem>
							<MenuItem value="West Coast - Westport">West Coast - Westport</MenuItem>
							<MenuItem value="Canterbury - Akaroa">Canterbury - Akaroa</MenuItem>
							<MenuItem value="Canterbury - Amberley">Canterbury - Amberley</MenuItem>
							<MenuItem value="Canterbury - Ashburton">Canterbury - Ashburton</MenuItem>
							<MenuItem value="Canterbury - Cheviot">Canterbury - Cheviot</MenuItem>
							<MenuItem value="Canterbury - Christchurch City">Canterbury - Christchurch City</MenuItem>
							<MenuItem value="Canterbury - Darfield">Canterbury - Darfield</MenuItem>
							<MenuItem value="Canterbury - Fairlie">Canterbury - Fairlie</MenuItem>
							<MenuItem value="Canterbury - Geraldine">Canterbury - Geraldine</MenuItem>
							<MenuItem value="Canterbury - Hanmer Springs">Canterbury - Hanmer Springs</MenuItem>
							<MenuItem value="Canterbury - Kaiapoi">Canterbury - Kaiapoi</MenuItem>
							<MenuItem value="Canterbury - Kaikoura">Canterbury - Kaikoura</MenuItem>
							<MenuItem value="Canterbury - Mt Cook">Canterbury - Mt Cook</MenuItem>
							<MenuItem value="Canterbury - Rangiora">Canterbury - Rangiora</MenuItem>
							<MenuItem value="Timaru - Oamaru - Kurow">Timaru - Oamaru - Kurow</MenuItem>
							<MenuItem value="Timaru - Oamaru - Oamaru">Timaru - Oamaru - Oamaru</MenuItem>
							<MenuItem value="Timaru - Oamaru - Timaru">Timaru - Oamaru - Timaru</MenuItem>
							<MenuItem value="Timaru - Oamaru - Twizel">Timaru - Oamaru - Twizel</MenuItem>
							<MenuItem value="Timaru - Oamaru - Waimate">Timaru - Oamaru - Waimate</MenuItem>
							<MenuItem value="Otago - Alexandra">Otago - Alexandra</MenuItem>
							<MenuItem value="Otago - Balclutha">Otago - Balclutha</MenuItem>
							<MenuItem value="Otago - Cromwell">Otago - Cromwell</MenuItem>
							<MenuItem value="Otago - Dunedin">Otago - Dunedin</MenuItem>
							<MenuItem value="Otago - Lawrence">Otago - Lawrence</MenuItem>
							<MenuItem value="Otago - Milton">Otago - Milton</MenuItem>
							<MenuItem value="Otago - Palmerston">Otago - Palmerston</MenuItem>
							<MenuItem value="Otago - Queenstown">Otago - Queenstown</MenuItem>
							<MenuItem value="Otago - Ranfurly">Otago - Ranfurly</MenuItem>
							<MenuItem value="Otago - Roxburgh">Otago - Roxburgh</MenuItem>
							<MenuItem value="Otago - Wanaka">Otago - Wanaka</MenuItem>
							<MenuItem value="Southland - Bluff">Southland - Bluff</MenuItem>
							<MenuItem value="Southland - Edendale">Southland - Edendale</MenuItem>
							<MenuItem value="Southland - Gore">Southland - Gore</MenuItem>
							<MenuItem value="Southland - Invercargil">Southland - Invercargill</MenuItem>
							<MenuItem value="Southland - Lumsden">Southland - Lumsden</MenuItem>
							<MenuItem value="Southland - Otautau">Southland - Otautau</MenuItem>
							<MenuItem value="Southland - Riverton">Southland - Riverton</MenuItem>
							<MenuItem value="Southland - Stewart Island">Southland - Stewart Island</MenuItem>
							<MenuItem value="Southland - Te Anau">Southland - Te Anau</MenuItem>
							<MenuItem value="Southland - Tokanui">Southland - Tokanui</MenuItem>
							<MenuItem value="Southland - Winton">Southland - Winton</MenuItem>
							<MenuItem value="Chatham Islands">Chatham Islands</MenuItem>
				        </Select>
				        <label htmlFor="location">Location</label>

				        {errors.location && (
				        	<span className={classes.errorMsg}>{errors.location}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
				        <Select
				            value={this.state.power_train}
				            onChange={this.onChange}
				            name="power_train"
				            displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.power_train}
				          )}
				          >
				            <MenuItem value="" disabled>
				              Select Option
				            </MenuItem>
				            <MenuItem value="Front Wheel Drive">Front Wheel Drive</MenuItem>
				            <MenuItem value="Rear Wheel Drive">Rear Wheel Drive</MenuItem>
				            <MenuItem value="4WD / AWD">4WD / AWD</MenuItem>
				          </Select>
				        <label htmlFor="power_train">Power Train</label>

				        {errors.power_train && (
				        	<span className={classes.errorMsg}>{errors.power_train}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
				        <Select
				            value={this.state.fuel_type}
				            onChange={this.onChange}
				            name="fuel_type"
				            displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.fuel_type}
				          )}
				        >
				            <MenuItem value="" disabled>
				              Select Option
				            </MenuItem>
				            <MenuItem value="Don't Know">Don't Know</MenuItem>
				            <MenuItem value="Petrol">Petrol</MenuItem>
				            <MenuItem value="Diesel">Diesel</MenuItem> 
				            <MenuItem value="Hybrid">Hybrid</MenuItem> 
				            <MenuItem value="Plug-in hybrid">Plug-in hybrid</MenuItem> 
				            <MenuItem value="Electric">Electric</MenuItem> 
				            <MenuItem value="LPG">LPG</MenuItem>
							<MenuItem value="Alternative">Alternative</MenuItem>  
							{this.state.fuel_type && (<MenuItem value={this.state.fuel_type}>{this.state.fuel_type}</MenuItem>)}  
				        </Select>
				        <label htmlFor="fuel_type">Fuel Type</label>

				        {errors.fuel_type && (
				        	<span className={classes.errorMsg}>{errors.fuel_type}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
				        <Select
				            value={this.state.vehicle_type}
				            onChange={this.onChange}
				            name="vehicle_type"
				            displayEmpty
				            className={classnames(classes.selectBox, 
				          	{ [classes.textError] : errors.vehicle_type}
				          )}
				        >
				            <MenuItem value="" disabled>
				              Select Option
				            </MenuItem>
				            <MenuItem value="Convertible">Convertible</MenuItem>
				            <MenuItem value="Coupe">Coupe</MenuItem> 
				            <MenuItem value="Hatchback">Hatchback</MenuItem> 
				            <MenuItem value="Sedan">Sedan</MenuItem> 
				            <MenuItem value="Station Wagon">Station Wagon</MenuItem> 
				            <MenuItem value="RV/SUV">RV/SUV</MenuItem> 
				            <MenuItem value="Ute">Ute</MenuItem> 
				            <MenuItem value="Van">Van</MenuItem> 
				            <MenuItem value="Other">Other</MenuItem>   
				        </Select>
				        <label htmlFor="vehicle_type">Body Style</label>

				        {errors.vehicle_type && (
				        	<span className={classes.errorMsg}>{errors.vehicle_type}</span>
				        )}

				        </Grid> 

				        <Grid className={classes.field}>
				        <TextField
				          id="colour"
				          placeholder="Colour"
				          className={classnames(classes.textField, 
				          	{ [classes.textError] : errors.colour}
				          )}
				          type="text"
				          name="colour" 
				          margin="normal"
				          variant="filled" 
				          value={this.state.colour}
				          onChange={this.onChange}
				        />
				        <label htmlFor="colour">Colour</label>

				        {errors.colour && (
				        	<span className={classes.errorMsg}>{errors.colour}</span>
				        )}

				        </Grid> 

				        

				        <Grid item xs={12}>
				        {
				        this.state.loading ? (
				        <CircularProgress className={classes.progress} />
				        ):
				        null	
				        }
				        </Grid>

				        <Button
				        type="submit"  
				        variant="contained" 
				        className={classes.button}>
					        Next
					    </Button>


							<Divider class={classes.vehicleDivider} />
			        </form>
				</section>


				{
					this.state.Msg ?
					(
						<SnackBar
						  open={this.state.Msg}
						  duration={5000}
				          variant={this.state.MsgVariant}
				          className={classes.margin}
				          message={this.state.MsgText}
				          onClose={this.handleClose}
				        />
					)
					:
					null

				}

	    	</div> 
	    	</Grid>
	    	</Grid>

	    );
	}
} 

SellAVehicle2.propTypes = { 
  
	auth: PropTypes.object.isRequired,
	vehicle: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
}

// converting state to props
const mapStateToProps = (state) => ({
	auth: state.auth,
	vehicle: state.vehicle,
	errors: state.errors   
});

export default compose(
  withStyles(sellAVehicleStyles),
  connect(mapStateToProps, { sellAVehicle })
)(withRouter(SellAVehicle2));