import React from "react";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import { forgetPassword } from "../actions/authActions";
import * as qs from "query-string";

import classnames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Fade from "@material-ui/core/Fade";

import logo from "../assets/img/logo.svg";

import loginStyles from "../assets/jss/pages/loginStyles.jsx";
import CircularProgress from "@material-ui/core/CircularProgress";
import SnackBar from "../components/common/SnackBar";

class ForgotPassword extends React.Component {
  state = {
    email: "",
    password: "",
    password2: "",
    resetPassword: false,
    loading: false,
    Msg: false,
    MsgVariant: false,
    MsgText: false,
    errors: {}
  };

  componentWillMount() {
    document.title = "Forgot password | Ripa - Car App";
  }

  componentDidMount() {
    // if (this.props.auth.isAuthenticated) {
    //   this.props.history.push("/");
    // }

    if (this.props.location.search !== "") {
      try {
        const queryData = qs.parse(this.props.location.search);

        const { token } = queryData;

        const { email, timestamp } = qs.parse(atob(token));

        if (email !== "" && timestamp !== "") {
          this.setState({ email, timestamp });

          let currentTime = new Date().getTime();

          let numberOfHours = parseFloat(
            (currentTime - timestamp) / 1000 / 60 / 60,
            2
          ).toFixed(2);

          console.log(numberOfHours);

          if (numberOfHours <= 1) {
            this.setState({ resetPassword: true });
          } else {
            this.setState({
              loading: false,
              Msg: true,
              MsgVariant: "error",
              MsgText: "Sorry, your reset password token is expired."
            });
          }
        }
      } catch (err) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: "Sorry, Invalid token."
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.successType) {
 
      this.setState({
        screenLoading: false,
        errors: false,
        Msg: true,
        MsgVariant: "success",
        MsgText: nextProps.auth.successMsg,
        loading: false
			});
			
			if(nextProps.auth.successType==='change-password')
			{
					this.props.history.push('/login', {successMsg: nextProps.auth.successMsg});
			}

    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });

      if (nextProps.errors.email) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: nextProps.errors.email
        });
      }else if (nextProps.errors.password) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: nextProps.errors.password
        });
      }else if (nextProps.errors.password2) {
        this.setState({
          loading: false,
          Msg: true,
          MsgVariant: "error",
          MsgText: nextProps.errors.password2
        });
      }
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

		this.setState({ loading: true });
		 
		var User;
			 
		if(this.state.resetPassword)
		{
			User = {
				email: this.state.email,
				timestamp: new Date().getTime(), 
				resetPassword: this.state.resetPassword,
				password: this.state.password,
				password2: this.state.password2
			}
		}else{
			User = {
				email: this.state.email,
				timestamp: new Date().getTime(), 
				resetPassword: false, 
			}
		} 

    this.props.forgetPassword(User);
  };

  handleClose = () => {
    this.setState({ Msg: false });
  };

  render() {
    const { classes } = this.props;
    const { errors } = this.state;

    return (
      <Grid container justify="center" className={classes.container}>
        <Grid item xs={12} sm={12} md={4}>
          <Fade in={true}>
            <div>
              <header className={classes.header}>
                <Link to="/">
                <img src={logo} alt="Ripa" width="94" />
                </Link>
              </header>
              <section className={classes.section}>
                <form
                  className={classes.form}
                  onSubmit={this.onSubmit.bind(this)}
                >
                  {this.state.resetPassword ? (
                    <div>
                      <TextField
                        id="password"
                        placeholder="New Password"
                        className={classnames(classes.textField, {
                          [classes.textError]: errors.password
                        })}
                        type="password"
                        name="password"
                        margin="normal"
                        variant="filled"
                        value={this.state.password}
                        onChange={this.onChange}
                      />

                      {errors.password && (
                        <span className={classes.errorMsg}>
                          {errors.password}
                        </span>
                      )}

                      <TextField
                        id="password2"
                        placeholder="Confirm Password"
                        className={classnames(classes.textField, {
                          [classes.textError]: errors.password2
                        })}
                        type="password"
                        name="password2"
                        margin="normal"
                        variant="filled"
                        value={this.state.password2}
                        onChange={this.onChange}
                      />

                      {errors.password2 && (
                        <span className={classes.errorMsg}>
                          {errors.password2}
                        </span>
                      )}
                    </div>
                  ) : (
                    <div>
                      <TextField
                        id="email"
                        placeholder="Email address"
                        className={classnames(classes.textField, {
                          [classes.textError]: errors.email
                        })}
                        type="text"
                        name="email"
                        autoComplete="email"
                        margin="normal"
                        variant="filled"
                        value={this.state.email}
                        onChange={this.onChange}
                      />

                      {errors.email && (
                        <span className={classes.errorMsg}>{errors.email}</span>
                      )}

                      <span className={classes.forgotpassword}>
                        {" "}
                        Enter your registered Email and click reset password
                      </span>
                    </div>
                  )}

                  <Grid item xs={12}>
                    {this.state.loading ? (
                      <CircularProgress className={classes.progress} />
                    ) : null}
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      variant="contained"
                      className={classes.button}
                    >
                      Reset Password
                    </Button>
                  </Grid>
                </form>
              </section>
              <footer className={classes.footer}>
                <Link to="/">Back to Login</Link>
              </footer>

              {this.state.Msg ? (
                <SnackBar
                  open={this.state.Msg}
                  duration={5000}
                  variant={this.state.MsgVariant}
                  className={classes.margin}
                  message={this.state.MsgText}
                  onClose={this.handleClose}
                />
              ) : null}
            </div>
          </Fade>
        </Grid>
      </Grid>
    );
  }
}

ForgotPassword.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

// converting state to props
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default compose(
  withStyles(loginStyles),
  connect(
    mapStateToProps,
    { forgetPassword }
  )
)(withRouter(ForgotPassword));
