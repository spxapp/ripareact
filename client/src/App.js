import React from "react"; 
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"; 
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser } from './actions/authActions';

import setAuthTokenAdmin from './utils/admin/setAuthTokenAdmin';
import { setCurrentUserAdmin } from './actions/admin/authActionsAdmin';

import { Provider } from "react-redux";
import store from './store';


import './config/socketio-client'
 

import indexRoutes from "./routes/index.jsx";
 

var current_path = window.location.pathname;
if(current_path.includes("/admin"))
{

	// check for token

	if(window.localStorage.jwtTokenAdmin){
		// Set the Auth Token header auth
		setAuthTokenAdmin(window.localStorage.jwtTokenAdmin);
		// Decode token and get user info and exp
		const decoded = jwt_decode(window.localStorage.jwtTokenAdmin);
		// set user and is authenticated
		store.dispatch(setCurrentUserAdmin(decoded));
	}
  

}else{ 

	// check for token

	if(window.localStorage.jwtTokenFront){
		// Set the Auth Token header auth
		setAuthToken(window.localStorage.jwtTokenFront);
		// Decode token and get user info and exp
		const decoded = jwt_decode(window.localStorage.jwtTokenFront);
		// set user and is authenticated
		store.dispatch(setCurrentUser(decoded));
	}

}




class App extends React.Component{
 
	 
	render(){ 
		return( 
			 	<Provider store={ store }>
				  <Router>
					 <Switch>
				      {indexRoutes.map((prop, key) => {
				        return <Route path={prop.path} key={key} component={prop.component} />;
				      })}
				    </Switch> 
				  </Router>
			 	 </Provider> 
		);
	}
}

export default App;