import { SET_CURRENT_USER,REGISTRATION_SUCCESSFULL,CHANGE_PASSWORD, USER_NOTIFICATIONS, FORGOT_PASSWORD } from '../actions/types';
import isEmpty from '../validation/isEmpty';

const initialState = {
	isAuthenticated: false,
	user: {}
}

export default function(state = initialState, action){
	switch(action.type){
		case REGISTRATION_SUCCESSFULL:
			return {
				...state,
				isRegistred:true
			};
		case SET_CURRENT_USER:
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				user: action.payload
			}; 
		case CHANGE_PASSWORD:
			return{ 
				...state,
				isPasswordChanged: !isEmpty(action.payload)
			}	
		case FORGOT_PASSWORD:
			return{ 
				...state,
				successType: action.payload.type,
				successMsg: action.payload.successMsg
			}	
		case USER_NOTIFICATIONS:
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				user: action.payload
			}; 
		default:
			return {...state, 
				isPasswordChanged: false,
				isRegistred: false
			};
	}
}