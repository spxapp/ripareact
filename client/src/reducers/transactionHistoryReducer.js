import { GET_TRANSACTION_HISTORY
		} from '../actions/types'; 

const initialState = {}

export default function(state = initialState, action){
	switch(action.type){
		case GET_TRANSACTION_HISTORY:
			return action.payload 
		default:
			return {}
	}
}