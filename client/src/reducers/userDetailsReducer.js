import { GET_USER_DETAILS } from '../actions/types'; 

const initialState = {}

export default function(state = initialState, action){
	switch(action.type){
		case GET_USER_DETAILS:
			return action.payload; 
		default:
			return state;
	}
}