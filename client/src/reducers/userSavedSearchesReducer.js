import { SEARCH_FILTERS,SET_SEARCH_FILTER } from '../actions/types'; 

const initialState = {
	saved_search:false,
	savedContent:[]
}

export default function(state = initialState, action){


	switch(action.type){ 
		case SEARCH_FILTERS:
			return {...state,savedContent:action.payload,saved_search:true}
		case SET_SEARCH_FILTER:
			return { ...state,saved_search:action.payload}
		
		default:
			return state;
	}
}