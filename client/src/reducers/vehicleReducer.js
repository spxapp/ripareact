import { SELL_A_VEHICLE, 
	GET_ALL_ACTIVE_VEHICLES, 
	GET_MY_LISTING_VEHICLES,
	GET_MY_BIDS_VEHICLES,
	GET_VEHICLE_DETAILS,
	EXPIRE_VEHICLE_AUCTION,
	BID_VEHICLE_AUCTION,
	CHANGE_MAIN_IMAGE,
	OUT_BID_STATUS, 
	SET_SELECTED_IMAGES,
	UPLOADED_IMAGE,
	UPDATE_VEHICLES
   } from '../actions/types'; 
   
import { isMobile, isIOS } from 'react-device-detect';

const initialState = { 
sell: {bidPlaced:false},
allActive: {},
vehicleDetails:{},
statusMessage:false, 
selectedImages:[],
progress:0,
}

export default function(state = initialState, action){
switch(action.type){
   case SELL_A_VEHICLE:
	   return {
		   ...state, 
		   sell: action.payload
	   };
   case CHANGE_MAIN_IMAGE:
	   return {
		   ...state, 
		   sell: action.payload
	   };    
   case GET_ALL_ACTIVE_VEHICLES:
	   return {
		   ...state, 
		   sell: {bidPlaced: false},
		   allActive: action.payload,
	   };
   case GET_MY_LISTING_VEHICLES:
	   return {
		   ...state, 
		   myListing: action.payload
	   };
   case GET_MY_BIDS_VEHICLES:
	   return {
		   ...state, 
		   myBids: action.payload
	   };                
   case GET_VEHICLE_DETAILS:
	   return{
		   ...state,
		   vehicleDetails: action.payload
	   }    
   case EXPIRE_VEHICLE_AUCTION:    
	   return{
		   ...state, 
		   auctionExpire: true, 
		   allActive: action.payload
	   };
	case UPDATE_VEHICLES:    
	return{
		...state, 
		myListing: action.payload.myListing, 
		myBids: action.payload.myBids
	};   
   case OUT_BID_STATUS:    
	   return{
		   ...state, 
		   statusMessage: action.payload
	   };    
   case BID_VEHICLE_AUCTION:    
	   return{
		   ...state, 
		   bidPlaced: action.bidPlaced,
		   allActive: action.payload,
		   statusMessage:true
	   };    
   case UPLOADED_IMAGE:
   
   return {
		   ...state, 
		   selectedImages: action.payload
	   };
   case SET_SELECTED_IMAGES: 
   return {
	   ...state,
	   selectedImages: action.payload
   }    
   default:
	   return {
		   ...state,
		   bidPlaced: false 
	   };
}
}