import { GET_ALL_REPORT_USERS } from '../../actions/types'; 

const initialState = { 
	 users: {}
}

export default function(state = initialState, action){
	switch(action.type){
		case GET_ALL_REPORT_USERS:
			return action.payload;
		default:
			return state;
	}
}