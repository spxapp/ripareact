import { GET_REPORT_DATA } from '../../actions/types'; 

const initialState = { 
	 users: {}
}

export default function(state = initialState, action){
	switch(action.type){
		case GET_REPORT_DATA:
			return action.payload;
		default:
			return state;
	}
}