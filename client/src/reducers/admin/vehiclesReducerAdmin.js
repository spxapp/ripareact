import { GET_ALL_VEHICLES, GET_VEHICLE_BIDS } from '../../actions/types'; 

const initialState = { 
	 vehicles: {},
	 bidDetails:{}
}

export default function(state = initialState, action){
	switch(action.type){
		case GET_ALL_VEHICLES:
			return {...state,
				vehicles:action.payload }
			case GET_VEHICLE_BIDS:
				return {...state,
					bidDetails:action.payload }
		default:
			return state;
	}
}