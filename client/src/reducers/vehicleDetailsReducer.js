import { GET_VEHICLE_DETAILS,CLEAR_VEHICLE_DETAILS 
		} from '../actions/types'; 

const initialState = {}

export default function(state = initialState, action){
	switch(action.type){ 
		case GET_VEHICLE_DETAILS:
			return action.payload
		case CLEAR_VEHICLE_DETAILS:
			return initialState	 
		default:
			return {
				...state 
			};
	}
}