import { LOAD_PROFILE, SETUP_PROFILE_STEPS, USER_NOTIFICATIONS } from '../actions/types'; 

const initialState = {}

export default function(state = initialState, action){


	switch(action.type){
		case LOAD_PROFILE:
			return action.payload;
		case SETUP_PROFILE_STEPS:
			return action.payload;
			case USER_NOTIFICATIONS:
			return {
				...state,
				profile:{...state.profile,user:{...state.profile.user,notifications:action.payload.notifications}}
			}; 	
		default:
			return {
				...state 
			};
	}
}