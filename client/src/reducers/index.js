import { combineReducers } from 'redux';
import authReducer from './authReducer';
import profileReducer from './profileReducer';
import billingInfoReducer from './billingInfoReducer';
import vehicleReducer from './vehicleReducer';
import vehicleDetailsReducer from './vehicleDetailsReducer'; 
import vehicleBidsReducer from './vehicleBidsReducer'; 
import userDetailsReducer from './userDetailsReducer';
import notificationsReducer from './notificationsReducer';
import transactionHistoryReducer from './transactionHistoryReducer';
import userSavedSearchesReducer from './userSavedSearchesReducer';
import errorReducer from './errorReducer';


import authReducerAdmin from './admin/authReducerAdmin'; 
import usersReducerAdmin from './admin/usersReducerAdmin';
import reportUsersReducerAdmin from './admin/reportUsersReducerAdmin';
import reportsReducerAdmin from './admin/reportsReducerAdmin';
import vehiclesReducerAdmin from './admin/vehiclesReducerAdmin';
import settingsReducerAdmin from './admin/settingsReducerAdmin';
  
export default combineReducers({
	auth: authReducer,
	profile: profileReducer, 
	billingInfo: billingInfoReducer,
	vehicle: vehicleReducer,
	vehicleDetails:vehicleDetailsReducer,
	vehicleBids:vehicleBidsReducer,
	errors: errorReducer,
	transactionHistory:transactionHistoryReducer,
	authAdmin: authReducerAdmin,
	users: usersReducerAdmin,
	reportUsers: reportUsersReducerAdmin,
	reports: reportsReducerAdmin,
	settings: settingsReducerAdmin,
	vehicles: vehiclesReducerAdmin,
	userDetails: userDetailsReducer,
	notifications: notificationsReducer,
	savedSearches: userSavedSearchesReducer
});