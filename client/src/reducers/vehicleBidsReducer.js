import { GET_VEHICLE_BIDS 
		} from '../actions/types'; 

const initialState = {}

export default function(state = initialState, action){
	switch(action.type){ 
		case GET_VEHICLE_BIDS:
			return action.payload	 
		default:
			return {
				...state 
			};
	}
}