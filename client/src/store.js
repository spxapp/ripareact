import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

const initialState = {};

const middleware = [thunk];



const devTools = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__() : null
const middlewareApply = process.env.NODE_ENV === 'development' ? 
	compose(
		applyMiddleware(...middleware),
		devTools
	)
	:
	applyMiddleware(...middleware);

const store = createStore(
	rootReducer, 
	initialState, 
	middlewareApply
);

export default store;