import axios from "axios";

import {
  GET_ERRORS,
  SELL_A_VEHICLE,
  GET_ALL_ACTIVE_VEHICLES,
  GET_MY_LISTING_VEHICLES,
  GET_MY_BIDS_VEHICLES,
  EXPIRE_VEHICLE_AUCTION,
  BID_VEHICLE_AUCTION,
  GET_TRANSACTION_HISTORY,
  CHANGE_MAIN_IMAGE,
  OUT_BID_STATUS,
  SET_SELECTED_IMAGES,
  UPLOADED_IMAGE,
  CLEAR_VEHICLE_DETAILS,
  UPDATE_VEHICLES
} from "./types";


// Clear Uploaded Image

export const clearselected = () => dispatch => {
  dispatch({
    type: SET_SELECTED_IMAGES,
    payload: []
  });
};


export const clearselectedMob = () => dispatch => {
	dispatch({
		type: SELL_A_VEHICLE,
		payload: {imagesUploaded:0}
	});
  };

// Sell A Vehicle
export const sellAVehicle = vehicleData => (dispatch, getState) => {
  if (vehicleData.step === 3) {
    let selectedImages = [];

    if (
      typeof vehicleData.type !== "undefined" &&
      vehicleData.type === "mobile"
    ) {

        selectedImages.push({ id: 1, progress: 0 });
        dispatch({
          type: SET_SELECTED_IMAGES,
          payload: selectedImages
        });

      axios
        .post("/api/vehicles/upload", vehicleData.fd, {
          onUploadProgress: function(progressEvent) {
            let progress = Math.round(
              (progressEvent.loaded * 100) / progressEvent.total
            );
            progress = progress === 100 ? progress - 5 : progress;
            let selImg = [];
            selImg.splice(0,1,{ id: 0, progress: progress })
              dispatch({
                type: UPLOADED_IMAGE,
                payload: selImg
              });
          }
        })
        .then(res => {
          let selImg = [];
          selImg.splice(0,1,{ id: 0, progress: 100 })
          dispatch({
            type: UPLOADED_IMAGE,
            payload: selImg
          });
          dispatch({
            type: SELL_A_VEHICLE,
            payload: res.data.results
          });
        })
        .catch(err =>
          dispatch({
            type: GET_ERRORS,
            payload: err.response.data
          })
        );
    } else {
      Array.from(vehicleData.images).forEach((file, i) => {
        selectedImages.push({ id: i, progress: 0 });
      });
      dispatch({
        type: SET_SELECTED_IMAGES,
        payload: selectedImages
      });

      Array.from(vehicleData.images).forEach((file, i) => {
        let fd = new FormData();
        fd.append("images", file, file.name);
        fd.append("vehicle_id", vehicleData.vehicle_id);

        axios
          .post("/api/vehicles/upload/", fd, {
            onUploadProgress: function(progressEvent) {
              let progress = Math.round(
                (progressEvent.loaded * 100) / progressEvent.total
              );
              progress = progress === 100 ? progress - 5 : progress;
              if(progress%20===0){
                let selImg = getState().vehicle.selectedImages.slice();
                selImg.splice(i,1,{ id: i, progress: progress })
                  dispatch({
                    type: UPLOADED_IMAGE,
                    payload: selImg
                  });
              }
            }
          })
          .then(res => {
            let selImg = getState().vehicle.selectedImages.slice();
                selImg.splice(i,1,{ id: i, progress: 100 })
                  dispatch({
                    type: UPLOADED_IMAGE,
                    payload: selImg
                  });
            dispatch({
              type: SELL_A_VEHICLE,
              payload: res.data.results
            });
          })
          .catch(err =>
            dispatch({
              type: GET_ERRORS,
              payload: err.response.data
            })
          );
      });
    }
  } else {
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    dispatch({
      type: SELL_A_VEHICLE,
      payload: {}
    });
    axios
      .post("/api/vehicles/sell", vehicleData)
      .then(res => {
        if (res.data.results.step === 4) {
          sessionStorage.clear();
        }
        return dispatch({
          type: SELL_A_VEHICLE,
          payload: res.data.results
        });
      })
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
  }
};

// Change main Image
export const mainImage = vehicleData => dispatch => {
  axios
    .post("/api/vehicles/main_image", vehicleData)
    .then(res =>
      dispatch({
        type: CHANGE_MAIN_IMAGE,
        payload: res.data.results
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// List All Auction Active Vehicles
export const activeVehicles = data => dispatch => {
  axios
    .get("/api/vehicles/all/active")
    .then(res =>
      dispatch({
        type: GET_ALL_ACTIVE_VEHICLES,
        payload: res.data.results
      })
    )
    .catch(err =>{
      if(err.response)
      {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      }
    }
    );
};

// List All Vehicles of current user
export const myListingVehicles = () => dispatch => {
  axios
    .get("/api/vehicles/mylisting")
    .then(res =>
      dispatch({
        type: GET_MY_LISTING_VEHICLES,
        payload: res.data.results
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// get all vehicles where user have bid
export const myBidsVehicles = data => dispatch => {
  axios
    .get("/api/vehicles/mybids")
    .then(res =>
      dispatch({
        type: GET_MY_BIDS_VEHICLES,
        payload: res.data.results
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Filter Vehciles

export const VehiclesFilter = (filters, tab) => dispatch => {
  axios
    .post("/api/vehicles/filters", { filters, tab })
    .then(res => {
      if (tab === 1) {
        dispatch({
          type: GET_MY_LISTING_VEHICLES,
          payload: res.data.results
        });
      } else if (tab === 2) {
        dispatch({
          type: GET_MY_BIDS_VEHICLES,
          payload: res.data.results
        });
      } else {
        dispatch({
          type: GET_ALL_ACTIVE_VEHICLES,
          payload: res.data.results
        });
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Expire Active Vehicles
export const expireVehicle = vehicleData => (dispatch, getState) => {
  let activeVehicles = getState().vehicle.allActive.slice();
  let index = activeVehicles.indexOf(vehicleData)
  if(index>-1)
  {
    activeVehicles.splice(index,1)
      dispatch({
        type: EXPIRE_VEHICLE_AUCTION,
        payload: activeVehicles
      })
  }
  axios.get(`/api/vehicles/get-expired-vehicle/${vehicleData._id}`)
      .then(res =>
        {
          if(res.data)
          {
          let myListing = getState().vehicle.myListing.slice();
          let mylistingsindex = myListing.findIndex((list)=>list._id===vehicleData._id)
          let myBids = getState().vehicle.myBids.slice();
          let myBidsindex = myBids.findIndex((bids)=>bids._id===vehicleData._id)
          if(mylistingsindex>-1)
          {
            myListing.splice(index,1,res.data.vehicle)
          }
          if(myBidsindex>-1)
          {
            myBids.splice(index,1,res.data.vehicle)
          }
            dispatch({
              type: UPDATE_VEHICLES,
              payload: {myBids,myListing}
            })
  
          }
        }
      )
      .catch(err =>{
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      }
  );
  
};

// Vehicle Bidding
export const vehicleBid = vehicleData => dispatch => {
  axios
    .post("/api/vehicles/bid", vehicleData)
    .then(res =>
      dispatch({
        type: BID_VEHICLE_AUCTION,
        payload: res.data.results,
        bidPlaced: res.data.bidPlaced
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const clearVehicle = () => dispatch => {
  dispatch({
    type: CLEAR_VEHICLE_DETAILS
  })
}

export const getCurrentTimer = id => dispatch => {
  console.log("reached action")
  axios
    .get(`/api/vehicles/get-vehicle/${id}`)
    .then(res =>
      console.log("res",res)
    )
    .catch(err =>{
      console.log("errors",err)
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    }
    );
};

// Out Bid Status
export const outBidStatus = vehicleData => dispatch => {
  axios
    .post("/api/vehicles/outbidstatus", vehicleData)
    .then(res =>
      dispatch({
        type: OUT_BID_STATUS,
        payload: res.data.results
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// List All Transactions
export const getTransactionHistory = () => dispatch => {
  axios
    .get("/api/vehicles/transaction_history")
    .then(res =>
      dispatch({
        type: GET_TRANSACTION_HISTORY,
        payload: res.data
      })
    )
    .catch(err =>
      {
        if(err.response)
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      }
    );
};

// auctionStatus
export const auctionStatus = data => dispatch => {
  axios.post("/api/vehicles/auction_status", data);
};

// fiveMinutesNotifications
export const sendFiveMinsNotification = data => dispatch => {
  axios.post("/api/vehicles/five-minutes-notifications", data);
};
