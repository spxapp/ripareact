import axios from 'axios';  

import { GET_ERRORS, 
		 GET_VEHICLE_BIDS
	    } from './types';
 

// get vehicle details
export const getVehicleBids = data => dispatch => {
	  
		axios.post('/api/vehicle/bids', data)
		.then(res => 
			dispatch({
				type: GET_VEHICLE_BIDS,
				payload: res.data
			})
		) 
		.catch(err => 
			{
				if(err.response && err.response.data)
				{
					dispatch({
						type: GET_ERRORS,
						payload: err.response.data
					})	
				}
			}
		);  
	
};  