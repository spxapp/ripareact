import axios from 'axios';
import isEmpty from '../validation/isEmpty';

import { GET_ERRORS, GET_NOTIFICATIONS } from './types';
 
// Update credit card
export const getNotifications = () => dispatch => {
  		
  	  
	axios.get('/api/notifications/get')
		.then(res =>  
			dispatch({
				type: GET_NOTIFICATIONS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: !isEmpty(err.response)?err.response.data:err
			})
		);  
};


// Update credit card
export const updateNotifications = () => dispatch => {
  		
  	  
	axios.get('/api/notifications/update')
		.then(res =>  
			dispatch({
				type: GET_NOTIFICATIONS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};