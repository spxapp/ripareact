import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode'

import { GET_ERRORS,SET_CURRENT_USER,REGISTRATION_SUCCESSFULL, CHANGE_PASSWORD, FORGOT_PASSWORD, USER_NOTIFICATIONS } from './types';

// Register User
export const registerUser = (userData, history) => dispatch =>{
	 
	axios.post('/api/users/register', userData)
		.then(res => {
			dispatch({
				type: REGISTRATION_SUCCESSFULL,
				payload: res.data
			})
		}) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

// Login - Get User Token

export const loginUser = userData => dispatch => {
	axios.post('/api/users/login', userData)
		.then(res => {
			// Save to local storage
			const { token } = res.data;
			// Set token to local storage
			window.localStorage.setItem('jwtTokenFront', token);
			// Set token to Auth header
			setAuthToken(token);
			// Decode token to get user data
			const decoded = jwt_decode(token);
			// Set current user
			dispatch(setCurrentUser(decoded));

			
		}) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

//  Get Current User 

export const getUser = () => dispatch => {
	axios.get('/api/users/get')
		.then(res => {

			dispatch({
				type: SET_CURRENT_USER,
				payload: res.data
			})
			
		}) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};


//set logged in user

export const setCurrentUser = (decoded) => {
	return {
		type: SET_CURRENT_USER,
		payload: decoded
	}
}

// Log User out

export const logoutUser = () => dispatch => {
	// Remove Token from window.localStorage
	window.localStorage.removeItem('jwtTokenFront');
	window.localStorage.removeItem('notification');
	// Remove auth header for future requests
	setAuthToken(false);
	// set current user {} which will set isAuthenticated to false
	dispatch(setCurrentUser({}));
}


// Change Password
export const changePassword = (userData) => dispatch =>{
	 
	axios.post('/api/users/change-password', userData)
		.then(res => {
			dispatch({
				type: CHANGE_PASSWORD,
				payload: res.data.results
			})
		}) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};


// forget Password
export const forgetPassword = (userData) => dispatch =>{
	 
	axios.post('/api/users/forget-password', userData)
	.then(res => {
		dispatch({
			type: FORGOT_PASSWORD,
			payload: res.data
		})
	}) 
	.catch(err => 
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})	
	);
};




// Update User Notifications
export const allowUserNotifications = (notifications) => dispatch =>{
  	 
	
	axios.post('/api/users/allow-user-notifications', notifications)
		.then(res => 
			{
				dispatch({
					type: USER_NOTIFICATIONS,
					payload: res.data
				})	
			}
		) 
		.catch(err => 
			{
			if(err.response)
			{
				dispatch({
					type: GET_ERRORS,
					payload: err.response.data
				})
			}
		}
		);  
};