import axios from 'axios';

import { GET_ERRORS, SEARCH_FILTERS } from './types';
 
// Save User Search Filters

export const saveUserSearchFilters = (filters) => dispatch => {
	  
	axios.post('/api/profile/save-search-filters', filters)
	.then(res => 
	{ 
		dispatch({
			type: SEARCH_FILTERS,
			payload: res.data.results
		}) 
	}) 
	.catch(err => {
		if(err.response)
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})
	}	
	);  

};  

// get User Search Filters

export const getUserSearchFilters = () => dispatch => {
	  
	axios.get('/api/profile/get-search-filters')
	.then(res => 
	{ 
		dispatch({
			type: SEARCH_FILTERS,
			payload: res.data.results
		}) 
	}) 
	.catch(err => 
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})	
	);  

};  


// delete User Search Filters

export const deleteUserSearchFilters = (filters) => dispatch => {
	  
	axios.post('/api/profile/delete-search-filter', filters)
	.then(res => 
	{ 
		dispatch({
			type: SEARCH_FILTERS,
			payload: res.data.results
		}) 
	}) 
	.catch(err => 
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})	
	);  

};
 

// Update User Search Filters

export const updateUserSearchFilters = (filters) => dispatch => {
	  
	axios.post('/api/profile/update-search-filters', filters)
	.then(res => 
	{ 
		dispatch({
			type: SEARCH_FILTERS,
			payload: res.data.results
		}) 
	}) 
	.catch(err => 
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})	
	);  

};  