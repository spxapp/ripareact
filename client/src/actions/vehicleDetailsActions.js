import axios from 'axios';  

import { GET_ERRORS, 
		 GET_VEHICLE_DETAILS
	    } from './types';
 

// get vehicle details
export const getVehicleDetails = data => dispatch => {
	  
		axios.post('/api/vehicles/getdetails', data)
		.then(res => 
			{
				dispatch({
					type: GET_VEHICLE_DETAILS,
					payload: res.data
				})
			}
		) 
		.catch(err =>
			{
				if(err.response){
					dispatch({
						type: GET_ERRORS,
						payload: err.response.data
					})	
				}
			}
			
		);  
	
};  

// get vehicle details
export const vehicleAcceptedRejected = data => dispatch => {
	  
	axios.post('/api/vehicles/accepted_rejected', data)
	.then(res => 
		dispatch({
			type: GET_VEHICLE_DETAILS,
			payload: res.data
		})
	) 
	.catch(err => 
		dispatch({
			type: GET_ERRORS,
			payload: err.response.data
		})	
	);  

};  