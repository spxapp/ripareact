import axios from 'axios';

import { GET_ERRORS, LOAD_PROFILE,SET_CURRENT_USER, GET_BILLING_INFO, SETUP_PROFILE_STEPS, SET_SEARCH_FILTER } from './types';

import setAuthToken from '../utils/setAuthToken';



// Load Existing Profile
export const loadProfile = file => dispatch =>{
   
	axios.get('/api/profile/')
		.then(res =>
			{
			if(res.data.user && res.data.step!=0)
			{
				dispatch({
					type: SET_SEARCH_FILTER,
					payload:res.data.user.isSavedSearch
				})
			}
			dispatch({
				type: LOAD_PROFILE,
				payload: res.data
			})
		}	
		) 
		.catch(err => 
			{	
				if(err.response.data==='Unauthorized'){
					window.localStorage.removeItem('jwtTokenFront');
					window.localStorage.removeItem('notification');
					// Remove auth header for future requests
					setAuthToken(false);
					// set current user {} which will set isAuthenticated to false
					dispatch({
						type: SET_CURRENT_USER,
						payload: {}
					})
				}

				dispatch({
					type: GET_ERRORS,
					payload: err.response.data
				})
			}
		);  
};

// Update Existing Profile
export const updateProfile = profile => dispatch =>{
   
	axios.post('/api/profile/update', profile)
		.then(res => 
			dispatch({
				type: LOAD_PROFILE,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};



// Setup Profile Pic
export const setupProfile1 = file => dispatch =>{
  		
  	const fd = new FormData();	

  	fd.append('image', file, file.name);
   
	axios.post('/api/profile/setup_profile_1', fd)
		.then(res => 
			dispatch({
				type: SETUP_PROFILE_STEPS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};



// skip Profile Step1
export const skipProfileStep1 = () => dispatch =>{
   
  axios.post('/api/profile/skip_profile_1')
	  .then(res => 
		  dispatch({
			  type: SETUP_PROFILE_STEPS,
			  payload: res.data
		  })	
	  ) 
	  .catch(err => 
		  dispatch({
			  type: GET_ERRORS,
			  payload: err.response.data
		  })
	  );  
};



// Update existing profile
export const setupProfile2 = formData => dispatch => {
  		
  	  
	axios.post('/api/profile/setup_profile_2', formData)
		.then(res =>  
			dispatch({
				type: SETUP_PROFILE_STEPS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};


// Create credit card
export const setupProfile3 = token => dispatch => {
  		
  	  
	axios.post('/api/profile/setup_profile_3', token)
		.then(res =>  
			dispatch({
				type: SETUP_PROFILE_STEPS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};

// Update credit card
export const changeCard = token => dispatch => {
  		
	axios.post('/api/profile/update_card', token)
		.then(res =>  
			dispatch({
				type: SETUP_PROFILE_STEPS,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};

// Get Billing Info
export const getBillingInfo = Customer => dispatch =>{
  	 
   
	axios.post('/api/profile/billing_info', Customer)
		.then(res => 
			dispatch({
				type: GET_BILLING_INFO,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};


// Save Notifications Token
export const saveNotificationsToken = (tokenData) => dispatch =>{
  	 
   
	axios.post('/api/profile/save-notifications-token', tokenData)
		.then(res => 
			dispatch({
				type: LOAD_PROFILE,
				payload: res.data
			})	
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);  
};