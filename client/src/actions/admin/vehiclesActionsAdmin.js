import axios from 'axios'; 

import { GET_ERRORS, GET_ALL_VEHICLES, GET_VEHICLE_BIDS } from '../types';
  
export const getAllVehicles = file => dispatch => {

	axios.get('/api/admin/vehicles/all')
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_VEHICLES,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const getVehicleBid = id => dispatch => {

	axios.get(`/api/admin/vehicles/get-bids/${id}`)
		.then(res => {
			// get all users
			dispatch({
				type: GET_VEHICLE_BIDS,
				payload: res.data
			})
		}
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};
export const suspendUser = id => dispatch => {

	axios.get(`/api/admin/vehicles/suspend/${id}`)
		.then(res => {
			// get all users
			dispatch({
				type: GET_VEHICLE_BIDS,
				payload: res.data
			})
		}
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};