import axios from 'axios'; 

import { GET_ERRORS, GET_ALL_REPORT_USERS } from '../types';
  
export const getAllReportUsers = file => dispatch => {

	axios.get('/api/admin/report-users/all')
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_REPORT_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const activeReportUserAction = (userData) => dispatch => {

	axios.post('/api/admin/report-users/activeuser', userData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_REPORT_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const addReportUser = (userData) => dispatch => {

	axios.post('/api/admin/report-users/register', userData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_REPORT_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const updateReportUser = (userData) => dispatch => {

	axios.post('/api/admin/report-users/updateuser', userData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_REPORT_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const deleteReportUser = (userID) => dispatch => {
	axios.delete(`/api/admin/report-users/deleteuser/${userID}`)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_REPORT_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};