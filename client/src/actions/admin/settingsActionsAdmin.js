import axios from 'axios'; 

import { GET_ERRORS, GET_ADMIN_SETTINGS } from '../types';
  
export const getAdminSettings = () => dispatch => {

	axios.get('/api/admin/settings/')
		.then(res => 
			// get all users
			dispatch({
				type: GET_ADMIN_SETTINGS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const updateAdminSettings = (settingsData) => dispatch => {

	axios.post('/api/admin/settings/update', settingsData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ADMIN_SETTINGS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};