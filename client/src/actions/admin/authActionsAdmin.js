import axios from 'axios';
import setAuthTokenAdmin from '../../utils/admin/setAuthTokenAdmin';
import jwt_decode from 'jwt-decode'

import { GET_ERRORS,SET_CURRENT_USER_ADMIN } from '../types';
 
// Login - Get User Token

export const loginUserAdmin = userData => dispatch => {
	axios.post('/api/admin/users/login', userData)
		.then(res => {
			// Save to local storage
			const { token } = res.data;
			// Set token to local storage
			localStorage.setItem('jwtTokenAdmin', token);
			// Set token to Auth header
			setAuthTokenAdmin(token);
			// Decode token to get user data
			const decoded = jwt_decode(token);
			// Set current user
			dispatch(setCurrentUserAdmin(decoded));

			
		}) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};


//set logged in user

export const setCurrentUserAdmin = (decoded) => {
	return {
		type: SET_CURRENT_USER_ADMIN,
		payload: decoded
	}
}


// Log User out

export const logoutUserAdmin = () => dispatch => {
	// Remove Token from localStorage
	localStorage.removeItem('jwtTokenAdmin');
	// Remove auth header for future requests
	setAuthTokenAdmin(false);
	// set current user {} which will set isAuthenticated to false
	dispatch(setCurrentUserAdmin({}));
}