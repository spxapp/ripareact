import axios from 'axios'; 

import { GET_ERRORS, GET_ALL_USERS } from '../types';
  
export const getAllUsers = file => dispatch => {

	axios.get('/api/admin/users/all')
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const activeUserAction = (userData) => dispatch => {

	axios.post('/api/admin/users/activeuser', userData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const updateUser = (userData) => dispatch => {

	axios.post('/api/admin/users/updateuser', userData)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};

export const deleteUser = (userID) => dispatch => {
	axios.delete(`/api/admin/users/deleteuser/${userID}`)
		.then(res => 
			// get all users
			dispatch({
				type: GET_ALL_USERS,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};