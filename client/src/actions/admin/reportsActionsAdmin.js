import axios from 'axios'; 

import { GET_ERRORS, GET_REPORT_DATA } from '../types';
  
export const getReportData = (filter) => dispatch => {

	axios.post('/api/admin/reports/all', filter)
		.then(res => 
			// get report data
			dispatch({
				type: GET_REPORT_DATA,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};


  
export const getReportBranchData = (filter) => dispatch => {

	axios.post('/api/admin/reports/branch', filter)
		.then(res => 
			// get report data
			dispatch({
				type: GET_REPORT_DATA,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};


export const getReportUserData = (filter) => dispatch => {

	axios.post('/api/admin/reports/user', filter)
		.then(res => 
			// get report data
			dispatch({
				type: GET_REPORT_DATA,
				payload: res.data
			})
			
		) 
		.catch(err => 
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})	
		);
};