import axios from 'axios';

import {GET_ERRORS, GET_USER_DETAILS} from './types';

// get user details by id
export const getUserDetails = userData => dispatch => {

				axios
								.post('/api/users/getdetails', userData)
								.then(res => {
												console.log("respons eof sending the user=======>", res)
												dispatch({type: GET_USER_DETAILS, payload: res.data})
								})
								.catch(err => dispatch({type: GET_ERRORS, payload: err.response.data}));
};