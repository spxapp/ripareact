import openSocket from 'socket.io-client';
import keys from '../config/keys'; 

const  socket = openSocket(keys.socketUrl);
function bidUpdate(cb) { 
  socket.on('bidUpdate', bid => cb(bid));
  socket.emit('bidUpdate',bid);
}

function liveAuction(cb) { 
  socket.on('liveAuction', () => cb());
  socket.emit('liveAuction');
} 

function timer(cb) { 
  socket.on('timer', () => cb());
  socket.emit('timer');
} 
export { bidUpdate, liveAuction, timer  };