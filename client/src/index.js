import React from "react";
import ReactDOM from "react-dom"; 

import App from "./App"; 
import { initializeFirebase } from './push_notifications';


import "./assets/css/core.css";
 
const startApp = () => {
  
  ReactDOM.render( 

    <App />
    ,
    document.getElementById("main")
  );

}

if(window.cordova) {
  document.addEventListener('deviceready', ()=>{
    // window.cordova.plugins.backgroundMode.enable();
    startApp()
  }, false);
} else {
  startApp();
}  

initializeFirebase();