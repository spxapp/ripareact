const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const VehiclesSchema = new Schema({

	user: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}, 
	registration_number: {
		type: String 
	},
	vin: {
		type: String,
	},
	year: {
		type : String
	}, 
	make: {
		type : String
	},
	model: {
		type : String
	},  
	km:{
		type : Number
	}, 
	transmission:{
		type : String
	}, 
	nz_new:{
		type : Boolean
	},
	location:{
		type : String
	},
	power_train:{
		type : String
	},
	vehicle_type:{
		type : String
	},
	colour:{
		type : String
	},
	engine:{
		type : String
	},
	registration_expire:{
		type : String
	},
	wof_expire:{
		type : String
	},
	step:{
		type: Number
	},
	active: {
		type: Boolean
	},
	expired: {
		type: Boolean
	}, 
	images:{
		type: Array
	},
	main_photo:{
		type: String
	},
	current_bid: {
		type: Number
	},
	expiry_date: {
		type: Date
	},
	accepted_date: {
		type: Date
	},
	accepted:{
		type:Boolean 
	},
	email_click:{
		type:Boolean 
	},
	accepted_bid: {
		type: Schema.Types.ObjectId,
		ref: 'bids'
	},
	add_details:{
		type:String
	},
	seller_invoice:{
		type:String
	},
	buyer_invoice:{
		type:String
	},
	fuel_type:{
		type:String
	},
	five_minutes_sent:{
		type: Boolean,
	},
	date: {
		type: Date,	
		default: Date.now
	},

});

module.exports = Vehicles = mongoose.model('vehicles', VehiclesSchema);