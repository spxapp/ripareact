const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const NotificationsSchema = new Schema({

	userTo: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}, 
	userFrom: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}, 
	type: {
		type: String 
	},
	vehicle: {
		type: Schema.Types.ObjectId,
		ref: 'vehicles'
	},
	price: {
		type : String
	},  
	read:{
		type: Boolean
	},
	date: {
		type: Date,	
		default: Date.now
	},

});

module.exports = Notifications = mongoose.model('notifications', NotificationsSchema);