const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSavedSearchSchema = new Schema({

	name: {
		type: String,	
		required: true
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	},
    filterData: {
		
        type: Object,	
		required: true
    },
	date: {
		type: Date,	
		default: Date.now
	}, 
});

module.exports = UserSavedSearch = mongoose.model('UserSavedSearch', UserSavedSearchSchema);