const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({

	name: {
		type: String,	
		required: true
	},
	email: {
		type: String,	
		required: true
	},
	password: {
		type: String,	
		required: true
	},  
	dealer_number: {
		type : String
	},  
	branch: {
		type : String
	},
	credit_limit: {
		type: Number,	
		required: false,
		default:0,
	}, 
	date: {
		type: Date,	
		default: Date.now
	},
	status: {
		type: Boolean,	
		default: false
	},
	seller_feedback:{
		type:Number
	},
	buyer_feedback:{
		type:Number
	},
	notifications:{
		type:Boolean,
		default: false
	},
	notifications_token: {
		type: String,
		default: null
	},
	web_notification_token: {
		type: String,
		default: null
	},
	ios_notification_token: {
		type: String,
		default: null
	},
	virtual_credit: {
		type:Number,
		default:0
	},
	suspended:{
		type:Boolean,
		default: false
	},
	archived:{
		type:Boolean,
		default: false
	},
	isSavedSearch:{
		type:Boolean,
		default:false
	}
});

module.exports = User = mongoose.model('users', UserSchema);