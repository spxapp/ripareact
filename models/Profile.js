const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ProfileSchema = new Schema({

	user: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}, 
	step: {
		type: Number,
		default: 0 
	},
	profile_pic: {
		type: String,
	},
	address: {
		type : String
	}, 
	phone: {
		type : String
	},
	company: {
		type : String
	},  
	customer_id:{
		type: String
	}, 
	date: {
		type: Date,	
		default: Date.now
	}

});

module.exports = Profile = mongoose.model('profile', ProfileSchema);