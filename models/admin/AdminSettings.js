const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const AdminSettingsSchema = new Schema({
 
	credit_limit: {
		type: String,	
		required: true
	}, 
	admin_email: {
		type: String,	
		required: true
	}, 
	date: {
		type: Date,	
		default: Date.now
	},

});

module.exports = AdminSettings = mongoose.model('adminsettings', AdminSettingsSchema);