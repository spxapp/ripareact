const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const AdminUserSchema = new Schema({
	name: {
		type: String,	
		required: true
	},
	username: {
		type: String,	
		required: true
	},
	password: {
		type: String,	
		required: true
	}, 
	role:{
		type: Number,
		default:1	
	},  
	dealer_number: {
		type : String
	},
	active: {
		type : Boolean,
		default:true
	},
	date: {
		type: Date,	
		default: Date.now
	} 
});

module.exports = AdminUser = mongoose.model('adminusers', AdminUserSchema);