const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const BidsSchema = new Schema({

	user: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	}, 
	vehicle: {
		type: Schema.Types.ObjectId,
		ref: 'vehicles'
	}, 
	bid_price: {
		type: Number,
		default: 0
	},
	date: {
		type: Date,	
		default: Date.now
	},

});

module.exports = Bids = mongoose.model('bids', BidsSchema);