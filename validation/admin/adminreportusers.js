const Validator = require('validator');
const isEmpty   = require('../isEmpty');

module.exports = function adminReportUsers(data){
	
	let errors = {};
    
    data.name           = !isEmpty(data.name) ? data.name : '';
    data.dealer_number  = !isEmpty(data.dealer_number) ? data.dealer_number : '';
    data.username       = !isEmpty(data.username) ? data.username : ''; 
	data.password       = !isEmpty(data.password) ? data.password : ''; 
    
    if(Validator.isEmpty(data.name)){
		errors.name = 'Company name is required';
    }
    if(Validator.isEmpty(data.dealer_number)){
		errors.dealer_number = 'Dealer number is required';
    }
    
	if(Validator.isEmpty(data.username)){
		errors.username = 'Username is required';
	}
 	 
	if(Validator.isEmpty(data.password)){
		errors.password = 'Password is required';
	}
	 
	return {
		errors,
		isValid: isEmpty(errors)
	}
}