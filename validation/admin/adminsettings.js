const Validator = require('validator');
const isEmpty   = require('../isEmpty');

module.exports = function validateSettingsInputAdmin(data){
	
	let errors = {};
 
	data.credit_limit     = !isEmpty(data.credit_limit) ? data.credit_limit : '';
  	data.admin_email     = !isEmpty(data.admin_email) ? data.admin_email : '';
  	 
	if(Validator.isEmpty(data.credit_limit)){
		errors.credit_limit = 'Credit Limit is required';
	}
 	
 	if(Validator.isEmpty(data.admin_email)){
		errors.admin_email = 'Admin Email is required';
	}
	 
	return {
		errors,
		isValid: isEmpty(errors)
	}
}