const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateVehicleInput(data){
	
	let errors = {};

	/*if(data.step===1)
	{

		data.registration_number   = !isEmpty(data.registration_number) ? data.registration_number : '';

		if(Validator.isEmpty(data.registration_number)){
			errors.registration_number = 'Registration number is required';
		}
 
	}else 
	
	*/
	if(data.step===2)
	{
		data.registration_number   = !isEmpty(data.registration_number) ? data.registration_number : '';
		data.vin   = !isEmpty(data.vin) ? data.vin : '';
		data.year   = !isEmpty(data.year) ? data.year : '';
		data.make   = !isEmpty(data.make) ? data.make : '';
		data.model   = !isEmpty(data.model) ? data.model : '';
		data.km   = !isEmpty(data.km) ? data.km.toString() : '';
		data.transmission   = !isEmpty(data.transmission) ? data.transmission : '';
		data.nz_new   = !isEmpty(data.nz_new) ? data.nz_new : '';
		data.location   = !isEmpty(data.location) ? data.location : '';
		data.power_train   = !isEmpty(data.power_train) ? data.power_train : '';
		data.fuel_type   = !isEmpty(data.fuel_type) ? data.fuel_type : ''; 
		data.vehicle_type   = !isEmpty(data.vehicle_type) ? data.vehicle_type : '';
		data.colour   = !isEmpty(data.colour) ? data.colour : '';
		data.engine   = !isEmpty(data.engine) ? data.engine : ''; 
		data.registration_expire   = !isEmpty(data.registration_expire) ? data.registration_expire : ''; 
		data.wof_expire   = !isEmpty(data.wof_expire) ? data.wof_expire : ''; 

		/*if(Validator.isEmpty(data.registration_number)){
			errors.registration_number = 'Registration number is required';
		}

		if(!Validator.isLength(data.vin, { max:17 })){
			errors.vin = 'Vin number is 17 numbers maximum';
		}

		if(Validator.isEmpty(data.vin)){
			errors.vin = 'Vin Field is required';
		}*/

		if(!Validator.isLength(data.vin, { max:17 })){
			errors.vin = 'Vin number is 17 numbers maximum';
		}
 
		if(Validator.isEmpty(data.year)){
			errors.year = 'Year Field is required';
		}

		if(data.year<1900){
			errors.year = 'Year cannot be less than 1900';
		}

		if(Validator.isEmpty(data.year)){
			errors.year = 'Year Field is required';
		}

		if(Validator.isEmpty(data.make)){
			errors.make = 'Make Field is required';
		}

		if(Validator.isEmpty(data.model)){
			errors.model = 'Model Field is required';
		}

		if(!Validator.isEmpty(data.engine)  && !Validator.isNumeric(data.engine)){
			errors.engine = 'Engine Field should be a Numeric Value';
		} 

		
		if(!Validator.isEmpty(data.km) && !Validator.isNumeric(data.km)){
			errors.km = 'Kilometers Field should be a Numeric Value';
		}
		
		
		/*
		if(Validator.isEmpty(data.km)){
			errors.km = 'Kilometers Field is required';
		}
		*/	
		/*if(Validator.isEmpty(data.transmission)){
			errors.transmission = 'Transmission Field is required';
		}*/
		/*
		if(Validator.isEmpty(data.nz_new)){
			errors.nz_new = 'NZ New Field is required';
		}

		if(Validator.isEmpty(data.location)){
			errors.location = 'Location Field is required';
		}

		if(Validator.isEmpty(data.power_train)){
			errors.power_train = 'Power Train Field is required';
		}

		if(Validator.isEmpty(data.fuel_type)){
			errors.fuel_type = 'Fuel Type Field is required';
		} 

		if(Validator.isEmpty(data.vehicle_type)){
			errors.vehicle_type = 'Body Style Field is required';
		}

		if(Validator.isEmpty(data.colour)){
			errors.colour = 'Colour Field is required';
		}

		if(Validator.isEmpty(data.engine)){
			errors.engine = 'Engine Field is required';
		} 

		if(Validator.isEmpty(data.registration_expire)){
			errors.registration_expire = 'Registration Expires Field is required';
		} 

		if(Validator.isEmpty(data.wof_expire)){
			errors.wof_expire = 'WOF Expires Field is required';
		} */
 
	}
 
	 

	return {
		errors,
		isValid: isEmpty(errors)
	}
}