const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateUpdateProfile(data){
	
	let errors = {};

    data.name           = !isEmpty(data.name) ? data.name : '';
    data.address        = !isEmpty(data.address) ? data.address : '';
    data.phone          = !isEmpty(data.phone) ? data.phone : '';
    data.email          = !isEmpty(data.email) ? data.email : '';
    data.company        = !isEmpty(data.company) ? data.company : '';
	data.dealer_number  = !isEmpty(data.dealer_number) ? data.dealer_number : ''; 
 	 
	
	if(!Validator.isLength(data.name, { min:3, max:30 })){
		errors.name = 'Name should be in 3 to 30 charaters';
	}

	if(Validator.isEmpty(data.name)){
		errors.name = 'Name field is required';
    }
    if(Validator.isEmpty(data.address)){
		errors.address = 'Address field is required';
	}

    if(Validator.isEmpty(data.phone)){
		errors.phone = 'Phone field is required';
    }  
 	
 	if(!Validator.isEmail(data.email)){
		errors.email = 'Invalid Email address';
	}

	if(Validator.isEmpty(data.email)){
		errors.email = 'Email field is required';
    }
    
    if(Validator.isEmpty(data.company)){
		errors.company = 'Company field is required';
    }  
 	
	if(!Validator.isLength(data.dealer_number, { min:7, max:7 })){
		errors.dealer_number = 'Dealer Number is not valid';
	} 

	if(! /^M[0-9]{6}$/i.test(data.dealer_number)){
		errors.dealer_number = 'Dealer Number is not valid';
	} 

	if(Validator.isEmpty(data.dealer_number)){
		errors.dealer_number = 'Dealer Number is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}
}