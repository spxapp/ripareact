const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateChangePassword(data){
	
	let errors = {};

	data.current_password      = !isEmpty(data.current_password) ? data.current_password : '';
	data.new_password     = !isEmpty(data.new_password) ? data.new_password : '';
	data.confirm_password  = !isEmpty(data.confirm_password) ? data.confirm_password : '';
    
    
    if(Validator.isEmpty(data.current_password)){
		errors.current_password = 'Current Password field is required';
    } 
    
	if(!Validator.isLength(data.new_password, { min:6, max:30 })){
		errors.new_password = 'New Password must be atleast 6 characters';
    } 
    
    if(Validator.isEmpty(data.new_password)){
		errors.new_password = 'New Password field is required';
	} 
	
	if(!Validator.equals(data.new_password, data.confirm_password)){
		errors.confirm_password = 'New Password must match with confirm password';
	}

	if(Validator.isEmpty(data.confirm_password)){
		errors.confirm_password = 'Confirm Password field is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}
}