const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateProfileInput(data){
	
	let errors = {};

	data.address      = !isEmpty(data.address) ? data.address : '';
	data.phone     = !isEmpty(data.phone) ? data.phone : '';
	data.company  = !isEmpty(data.company) ? data.company : ''; 
 	 
	 
	if(Validator.isEmpty(data.address)){
		errors.address = 'Address field is required';
	}
 	
 	if(Validator.isEmpty(data.phone)){
		errors.phone = 'Phone field is required';
	}

	if(Validator.isEmpty(data.company)){
		errors.company = 'Company field is required';
	}
 	  

	return {
		errors,
		isValid: isEmpty(errors)
	}
}