const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateRegisterInput(data){
	
	let errors = {};

	data.name      = !isEmpty(data.name) ? data.name : '';
	data.email     = !isEmpty(data.email) ? data.email : '';
	data.password  = !isEmpty(data.password) ? data.password : '';
	data.password2 = !isEmpty(data.password2) ? data.password2 : '';
	data.dealer_number = !isEmpty(data.dealer_number) ? data.dealer_number : '';
 	 
	
	if(!Validator.isLength(data.name, { min:3, max:30 })){
		errors.name = 'Name should be in 3 to 30 charaters';
	}

	if(Validator.isEmpty(data.name)){
		errors.name = 'Name field is required';
	}
 	
 	if(!Validator.isEmail(data.email)){
		errors.email = 'Invalid Email address';
	}

	if(Validator.isEmpty(data.email)){
		errors.email = 'Email field is required';
	}
 	 
	 
	if(!Validator.isLength(data.password, { min:6, max:30 })){
		errors.password = 'Password must be atleast 6 characters';
	} 

	if(Validator.isEmpty(data.password)){
		errors.password = 'Password field is required';
	}
	
	if(!Validator.equals(data.password, data.password2)){
		errors.password2 = 'Password must match with confirm password';
	}

	if(Validator.isEmpty(data.password2)){
		errors.password2 = 'Confirm Password field is required';
	}

	if(!Validator.isLength(data.dealer_number, { min:7, max:7 })){
		errors.dealer_number = 'Dealer Number is not valid';
	} 

	if(! /^M[0-9]{6}$/i.test(data.dealer_number)){
		errors.dealer_number = 'Dealer Number is not valid';
	} 

	if(Validator.isEmpty(data.dealer_number)){
		errors.dealer_number = 'Dealer Number is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}
}