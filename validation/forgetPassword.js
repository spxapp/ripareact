const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateForgetPasswordInput(data){
	
	let errors = {};
  
	data.email        = !isEmpty(data.email) ? data.email : '';    
	 
 	if(!Validator.isEmail(data.email)){
		errors.email = 'Invalid Email address';
	}

	if(Validator.isEmpty(data.email)){
		errors.email = 'Email field is required';
	}

	if(typeof data.password !== 'undefined'){

		if(!Validator.isLength(data.password, { min:6, max:30 })){
			errors.password = 'Password must be atleast 6 characters';
		} 
	
		if(Validator.isEmpty(data.password)){
			errors.password = 'Password field is required';
		}
		
		if(!Validator.equals(data.password, data.password2)){
			errors.password2 = 'Password must match with confirm password';
		}
	
		if(Validator.isEmpty(data.password2)){
			errors.password2 = 'Confirm Password field is required';
		}

	}
 
	return {
		errors,
		isValid: isEmpty(errors)
	}
}