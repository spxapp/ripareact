const Validator = require('validator');
const isEmpty   = require('./isEmpty');

module.exports = function validateVehicleBidInput(data){
	
	let errors = {};
 
	data.bid_price = !isEmpty(data.bid_price) ? data.bid_price : '';

	if(Validator.isEmpty(data.bid_price)){
		errors.bid_price = 'Enter Bid Price';
	}
    
	return {
		errors,
		isValid: isEmpty(errors)
	}
}