const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const fileUpload = require('express-fileupload');
const path = require('path');

var cors = require('cors')

const users = require('./routes/api/users');
const profile = require('./routes/api/profile'); 
const vehicles = require('./routes/api/vehicles');
const vehicleBids = require('./routes/api/vehicleBids'); 
const notifications = require('./routes/api/notifications');

const adminusers = require('./routes/api/admin/adminusers');
const adminreportusers = require('./routes/api/admin/adminreportusers');
const adminreports = require('./routes/api/admin/adminreports');
const adminvehicles = require('./routes/api/admin/adminvehicles');
const adminsettings = require('./routes/api/admin/adminsettings');

const cronJob = require('./utils/cron'); 
const cronRejectBid = require('./utils/cronRejectBid');

const app = express();  
 
app.use(cors());
 
var http = require('http').Server(app);
var io = require('socket.io')(http);

require('./config/socketio')(io);
require('./routes/api/sockets/vehicle.socket').register(io);

global.__basedir = __dirname;

// Body Parser
app.use(bodyParser.urlencoded({extended: true, limit: '200mb'}));
app.use(bodyParser.json()); 
 
app.use(fileUpload()); 
 
 
// DB Config
const db = require('./config/keys').mongoURI; 

// Connecting to DB

mongoose
	.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() => console.log('MongoDB Connected'))
	.catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);	 	  

// Passport Config
require('./config/admin/passportAdmin')(passport);	 
	

// User Routes

app.use('/api/users', users);
app.use('/api/profile', profile); 
app.use('/api/vehicles', vehicles); 
app.use('/api/vehicle/bids', vehicleBids);
app.use('/api/notifications', notifications); 

app.use('/api/admin/users', adminusers); 
app.use('/api/admin/report-users', adminreportusers); 
app.use('/api/admin/reports', adminreports); 
app.use('/api/admin/vehicles', adminvehicles); 
app.use('/api/admin/settings', adminsettings);


io.on('connection', function(client){
   client.on('liveAuction', function(){
   	io.sockets.emit('liveAuction')
   });	
});

// Server Static files
if(process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging'){
	//Set static folder
	app.use(express.static('client/build'));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

 
const port = process.env.PORT || 5000; 


http.listen(port, () => console.log(`Server is Running on port ${port}`));

cronJob();
cronRejectBid();
