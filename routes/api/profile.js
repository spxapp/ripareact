const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

const keys = require("../../config/keys");
const stripe = require("stripe")(keys.StripeKey);

const AWS = require("aws-sdk");
const fs = require("fs");
const fileType = require("file-type");
const bluebird = require("bluebird");
const multiparty = require("multiparty");

// Load Profile model
const Profile = require("../../models/Profile");
const Vehicles = require("../../models/Vehicles");

// Load User model
const User = require("../../models/Users");
const UserSavedSearch = require("../../models/UserSavedSearch"); 

const validateProfileInput = require("../../validation/profile");
const validateUpdateProfile = require("../../validation/updateProfile");

const sharp = require('sharp');
 
// @route   GET api/profile @desc    Get current logged in profile @access
// Private

router.get(
  "/",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const errors = {};
 

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        if (profile) {
          User.findOne({ _id: profile.user }).then(async user => {
            const user_clone = JSON.parse(JSON.stringify(user));

            var user_sell_vehilces = await Vehicles.find({
              user: user._id,
              active: false,
              expired: true
            }).countDocuments();

            var seller_feedback =
              typeof user.seller_feedback !== "undefined"
                ? user.seller_feedback
                : 0;

            if (seller_feedback < 0) {
              seller_feedback = 0;
            }

            if (seller_feedback > 100) {
              seller_feedback = 100;
            }

            if (user_sell_vehilces == 0 && seller_feedback == 0) {
              seller_feedback = 100;
            }

            const user_addon = Object.assign(user_clone, {
              sell_rating: user_sell_vehilces + " (" + seller_feedback + "%)",
              buy_rating: "0",
              buy_rating_red: "(0)"
            });

            const profile_clone = JSON.parse(JSON.stringify(profile));

            profile_clone.user = user_addon;

            res.json(profile_clone);
          });
        } else {
          res.json({ step: 0 });
        }
      })
      .catch(err => res.status(404).json(err));
  }
);


// @route   POST api/profile/update @desc    Get current logged in profile @access
// Private

router.post(
  "/update",
  passport.authenticate("user-role", { session: false }),
  (req, res) => { 

    const { errors, isValid } = validateUpdateProfile(req.body); 

    if(!isValid){
      return res.status(400).json(errors);
    }

    const name = req.body.name;
    const address = req.body.address;
    const phone = req.body.phone;
    const email = req.body.email;
    const company = req.body.company;
    const dealer_number = req.body.dealer_number.toLowerCase();

    User.findOne({ email: email, 
      _id: { 
        $nin: [req.user.id]
      }
    }).then(user => {
      if (user) {
        errors.email = "Email address cannnot be changed with the existing account.";
        return res.status(400).json(errors);
      }
    });
     

    Profile.findOneAndUpdate({ user: req.user.id },
      {$set: {
        address,
        phone,
        company
      }},{ new: true })
      .then(profile => {
        if (profile) {  
          User.findOneAndUpdate({ _id: profile.user },
            {$set: {
              name,
              email,
              dealer_number
            }},{ new: true }).then(async user => {   
            const user_clone = JSON.parse(JSON.stringify(user));

            var user_sell_vehilces = await Vehicles.find({
              user: user._id,
              active: false,
              expired: true
            }).countDocuments();

            var seller_feedback =
              typeof user.seller_feedback !== "undefined"
                ? user.seller_feedback
                : 0;

            if (seller_feedback < 0) {
              seller_feedback = 0;
            }

            if (seller_feedback > 100) {
              seller_feedback = 100;
            }

            if (user_sell_vehilces == 0 && seller_feedback == 0) {
              seller_feedback = 100;
            }

            const user_addon = Object.assign(user_clone, {
              sell_rating: user_sell_vehilces + " (" + seller_feedback + "%)",
              buy_rating: "0",
              buy_rating_red: "(0)",
              account_updated: true
            });

            const profile_clone = JSON.parse(JSON.stringify(profile));

            profile_clone.user = user_addon;

            res.json(profile_clone);
          });
        } else {
          res.json({ step: 0 });
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   POST api/profile/setup_profile_1 @desc    Profile Photo Upload
// @access  Public configure the keys for accessing AWS
AWS.config.update({
  accessKeyId: "AKIAIVCB326MI5F3QU7A",
  secretAccessKey: "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"
});

// configure AWS to work with promises
AWS.config.setPromisesDependency(bluebird);

// create S3 instance
const s3 = new AWS.S3();

// abstracts function to upload a file returning a promise
const uploadFile = (buffer, name, type) => {
  const params = {
    ACL: "public-read",
    Body: buffer,
    Bucket: "ripa-car-app",
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };
  return s3.upload(params).promise();
};


router.post(
  "/setup_profile_1",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
  
    Profile.findOne({ user: req.user.id })
      .then(async profile => {
        if (!profile) {
          let imageFile = req.files.image;
          let file = `/uploads/profile-pics/${req.user.id}_${
            req.files.image.name
          }`;
          const buffer = imageFile.data;
					const type = fileType(buffer);
				  const timestamp = Date.now().toString();
					const fileName = `uploads/profile-pics/${timestamp}-lg`;
          let data = await sharp(buffer).rotate().resize(800, 800, {
            fit: 'inside',
          }).toBuffer();
          const upload_data = uploadFile(data, fileName, type)
            .then(response => {
              const newProfile = new Profile({
                user: req.user.id,
                profile_pic: response.Location,
                step: 1
              });
              newProfile.save().then(profile => {
                Profile.findOne({ user: req.user.id }).then(
                  newInsertedProfile => {
                    return res.json(newInsertedProfile);
                  }
                );
              });
          
            })
            .catch(error => {
              return res.status(500).send(error);
            });
        } else {
          let imageFile = req.files.image;
          let file = `/uploads/profile-pics/${req.user.id}_${
            req.files.image.name
          }`;
          const buffer = imageFile.data;
					const type = fileType(buffer);
				  const timestamp = Date.now().toString();
					const fileName = `uploads/profile-pics/${timestamp}-lg`;
          let data = await sharp(buffer).rotate().resize(800, 800, {
            fit: 'inside',
          }).toBuffer();
          const upload_data = uploadFile(data, fileName, type)
            .then(response => {
              Profile.findOneAndUpdate(
                {
                  user: req.user.id
                },
                {
                  $set: {
                    profile_pic: response.Location
                  }
                }
              ).then(newInsertedProfile => {
                Profile.findById(newInsertedProfile._id).then(profile => {
                  return res.json(profile);
                });
              });
            })
            .catch(error => {
              return res.status(500).send(error);
            });
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

router.post(
  "/skip_profile_1",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        if (!profile) {
          const newProfile = new Profile({ user: req.user.id, step: 1 });

          newProfile.save().then(profile => {
            Profile.findOne({ user: req.user.id }).then(newInsertedProfile => {
              return res.json(newInsertedProfile);
            });
          });
        } else {
          Profile.findOne({ user: req.user.id }).then(newInsertedProfile => {
            return res.json(newInsertedProfile);
          });
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   POST api/profile/setup_profile_2 @desc    Profile Photo Upload
// @access  Public

router.post(
  "/setup_profile_2",
  passport.authenticate("user-role", { session: false }),
  (req, res) => { 
  
    const { errors, isValid } = validateProfileInput(req.body); 

    if(!isValid){
      return res.status(400).json(errors);
    }
 
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        if (profile) {
          Profile.findOneAndUpdate(
            {
              user: req.user.id
            },
            {
              $set: {
                step: 2,
                address: req.body.address,
                phone: req.body.phone,
                company: req.body.company 
              }
            },
            { new: true }
          ).then(profile => res.json(profile));
        } else {
          errors.noprofile = "Profile dont exist.";
          return res.status(404).json(errors);
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   POST api/profile/setup_profile_2 @desc    Profile Photo Upload
// @access  Public

router.post(
  "/setup_profile_3",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {

    
    const token = req.body.token;

    const errors = {};

    Profile.findOne({ user: req.user.id })
      .then(async profile => {
        if (profile) {
          
          let UserData = await User.findOne({_id: req.user.id}); 
          stripe.customers
            .create({
              description: "Added credit card",
              email: UserData.email,
              source: token.id
            })
            .then(customer => {
              //console.log(customer);
              Profile.findOneAndUpdate(
                {
                  user: req.user.id
                },
                {
                  $set: {
                    step: 3,
                    customer_id: customer.id
                  }
                },
                { new: true }
              ).then(async profile => {
                const profile_clone = JSON.parse(JSON.stringify(profile));

                const profile_addon = Object.assign(profile_clone, {
                  addedCard: 1,
                  user:req.user
                });
 
                res.json(profile_addon);
              });
            }, err => res.json({ status:false, error: err.message }));
        } else {
          errors.noprofile = "Profile doest exist.";
          return res.status(404).json(errors);
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

router.post(
  "/update_card",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const token = req.body.token;

    const errors = {};

    Profile.findOne({ user: req.user.id })
      .then( async profile => {
        if (profile) {

          let UserData = await User.findOne({_id: req.user.id});

          stripe.customers
            .update(profile.customer_id,{
              email: UserData.email,
              description: "Updated credit card",
              source: token.id
            })
            .then(customer => {
              Profile.findOneAndUpdate(
                {
                  user: req.user.id
                },
                {
                  $set: {
                    customer_id: customer.id
                  }
                },
                { new: true }
              ).then(async profile => {
                const profile_clone = JSON.parse(JSON.stringify(profile));

                const profile_addon = Object.assign(profile_clone, {
                  cardUpdated: true
                });
 
                res.json(profile_addon);
              });
            });
        } else {
          errors.noprofile = "Profile doest exist.";
          return res.status(404).json(errors);
        }
      })
      .catch(err => res.status(404).json(err));
  }
);

router.post(
  "/billing_info",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const cid = req.body.customer_id;

    if (cid) {
      stripe.customers
        .retrieve(cid)
        .then(ress => {
          res.json(ress.sources.data);
        })
        .catch(err => res.status(404).json(err));
    } else {
      res.status(404).json("no billing info");
    }
  }
);

router.post(
  "/save-search-filters",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const userId = req.user.id;
    const filterName = req.body.filterName;
    const filterData = req.body.filterData;

    const newSavedSearch = new UserSavedSearch({
      name: filterName,
      user: userId,
      filterData: filterData
    });

    newSavedSearch.save().then(async newSavedSearch => {

      await User.findOneAndUpdate({_id:req.user.id}, {
        $set:{
          isSavedSearch: true
        }
      });

      UserSavedSearch.find({ user: req.user.id })
        .sort({ _id: -1 })
        .then(UserSavedSearches => {
          return res.json({ results: UserSavedSearches });
        });
    });
  }
);


router.post(
  "/update-search-filters",
  passport.authenticate("user-role", { session: false }),
  
  async (req, res) => { 
    const filterId = req.body.filterId;
    const filterName = req.body.filterName;
    const filterData = req.body.filterData;
  

    await UserSavedSearch.findOneAndUpdate({_id:filterId}, {
      $set:{
        name: filterName, 
        filterData: filterData
      }
    });

    UserSavedSearch.find({ user: req.user.id })
      .sort({ _id: -1 })
      .then(UserSavedSearches => {
        return res.json({ results: UserSavedSearches });
      }); 
  }
);

router.get(
  "/get-search-filters",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const userId = req.user.id;

    UserSavedSearch.find({ user: req.user.id })
      .sort({ _id: -1 })
      .then(UserSavedSearches => {
        return res.json({ results: UserSavedSearches });
      });
  }
);

router.post(
  "/delete-search-filter",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const userId = req.user.id;
    const filterId = req.body.filterId;

    UserSavedSearch.remove({ user: req.user.id, _id: filterId }).then(() => {
      UserSavedSearch.find({ user: req.user.id })
        .sort({ _id: -1 })
        .then(async UserSavedSearches => { 
          if(UserSavedSearches.length===0 || UserSavedSearches===null)
          {
            await User.findOneAndUpdate({_id:req.user.id}, {
              $set:{
                isSavedSearch: false
              }
            });
          }

          return res.json({ results: UserSavedSearches });
        });
    });
  }
);

router.post(
  "/save-notifications-token",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const userId = req.user.id;
    const token = req.body.token;
    const type = req.body.type;
    let notification_token = "";
    if(type==="ios"){
      notification_token="ios_notification_token";
    }
    else if(type==="web")
    {
      notification_token="web_notification_token";
    }
    else if(type==="android"){
      notification_token="notifications_token";
    }
    var update = { $set : {} };
    update.$set[notification_token] = token;
    User.findOneAndUpdate(
      {
        _id: userId
      },
      update,
      { new: true }
    ).then(user => {
      Profile.findOne({ user: user._id })
        .then(profile => {
          if (profile) {
            User.findOne({ _id: profile.user }).then(async user => {
              const user_clone = JSON.parse(JSON.stringify(user));

              var user_sell_vehilces = await Vehicles.find({
                user: user._id,
                active: false,
                expired: true
              }).countDocuments();

              var seller_feedback =
                typeof user.seller_feedback !== "undefined"
                  ? user.seller_feedback
                  : 0;

              const user_addon = Object.assign(user_clone, {
                sell_rating: user_sell_vehilces + " (" + seller_feedback + "%)",
                buy_rating: "0",
                buy_rating_red: "(0)"
              });

              const profile_clone = JSON.parse(JSON.stringify(profile));

              profile_clone.user = user_addon;

              res.json(profile_clone);
            });
          } else {
            res.json({ step: 0 });
          }
        })
        .catch(err => res.status(404).json(err));
    });
  }
);

module.exports = router;
