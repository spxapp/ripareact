const express = require("express");
const router = express.Router();
const Users = require("../../../models/Users");
const Vehicles = require("../../../models/Vehicles");
const AdminUsers = require("../../../models/admin/AdminUsers");
const keys = require("../../../config/keys");
const passport = require("passport");

const isEmpty = require("../../../validation/isEmpty");

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(keys.SENDGRID_API_KEY);

// @route  POST api/admin/reports/all
// @desc   POST report data
// @access Private

router.post(
  "/all",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    try {
      const user_id = req.user.id;

      const userDetails = await AdminUsers.findOne({ _id: user_id });

      var userDealerNumbers = []

      userDealerNumbers = userDetails.dealer_number.split(",").map((quoted)=>{
        return  quoted.trim()
      }) 

      if (userDetails !== null) { 

        let userList = await Users.aggregate([{ 
          $match:{
            dealer_number: {$in: userDealerNumbers }
          }
          }, 
          { "$group": {
              "_id": "$branch", 
              user : { $first: '$_id' }, 
              name : { $first: '$name' }, 
              branch : { $first: '$branch' },
              dealer_number: {$first: '$dealer_number'} 
            } } 
        ]);
        

        if (userList !== null) {
          const data = await Promise.all(
            userList.map(async user => await branchReport(user))
          );
          res.json({ all: data });
        }
      }
    } catch (error) {
      return res.status(500).send(error);
    }
  }
);
 

// @route  POST api/admin/reports/branch
// @desc   POST report data
// @access Private

router.post(
  "/branch",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    try {
      const user_id = req.user.id;
      const branch = req.body.branch;

      const userDetails = await AdminUsers.findOne({ _id: user_id });

      var userDealerNumbers = []

      userDealerNumbers = userDetails.dealer_number.split(",").map((quoted)=>{
        return  quoted.trim()
      }) 

      if (userDetails !== null) {
  
        let userList = await Users.aggregate([{ 
          $match:{
            dealer_number: {$in: userDealerNumbers },
            branch: branch 
          }
          }, 
          { "$group": {
              "_id": "$_id", 
              user : { $first: '$_id' }, 
              name : { $first: '$name' }, 
              branch : { $first: '$branch' },
              dealer_number: {$first: '$dealer_number'} 
            } } 
        ]);
 

        if (userList !== null) {
          const data = await Promise.all(
            userList.map(async user => await branchReportBranch(user))
          );
          res.json({ all: data });
        }
      }
    } catch (error) {
      return res.status(500).send(error);
    }
  }
);



// @route  POST api/admin/reports/user
// @desc   POST report data
// @access Private

router.post(
  "/user",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    try {
      const user_id = req.user.id;
      const user = req.body.user;


      const vehicleList = await Vehicles.find({user: user, step: 4}) 
  
        if (vehicleList !== null) {
          const data = await Promise.all(
            vehicleList.map(async vehicle => await branchReportUser(vehicle))
          );
          
          res.json({ all: data });
        }
       
    } catch (error) {
      return res.status(500).send(error);
    }
  }
);



const branchReport = async (userData) => { 

  let userList = await Users.distinct("_id", { 
    dealer_number: userData.dealer_number, 
    branch: userData.branch 
  });
  

  let carOffered = await Vehicles.find({
    step: 4,
    user: { $in : userList }
  }).countDocuments();

  let carPurchased = await Vehicles.find({ 
    step: 4,
    user: { $in : userList },
    accepted_bid: {$exists: true},
  }).countDocuments();

  let carIncompletePurchased = await Vehicles.find({ 
    step: 4,
    user: { $in : userList },
    accepted_bid: {$exists: false},
  }).countDocuments();

  let TotalCost = await Vehicles.aggregate([{ 
	  $match:{
			step: 4,
			user: { $in : userList },
			accepted_bid: {$exists: true},
	  }}, 
	  { $group: {_id: null, sum:{$sum: "$current_bid"} } }
  ]);
 

 
    let data = { branch: userData.branch, carOffered, carPurchased, total: 0, carIncompletePurchased };
    
    return data;
  
 
  

};



const branchReportBranch = async (userData) => { 
 

  let carOffered = await Vehicles.find({
    step: 4,
    user: userData.user
  }).countDocuments();

  let carPurchased = await Vehicles.find({ 
    step: 4,
    user: userData.user,
    current_bid: {$gt:1},
    accepted_bid: {$exists: true},
  }).countDocuments();

  let carIncompletePurchased = await Vehicles.find({ 
    step: 4,
    user: userData.user,
    accepted_bid: {$exists: false},
  }).countDocuments();

  let TotalCost = await Vehicles.aggregate([{ 
	  $match:{
			step: 4,
			user: userData.user,
			accepted_bid: {$exists: true},
	  }}, 
	  { $group: {_id: null, sum:{$sum: "$current_bid"} } }
  ]);
 
 

    let data = { id:userData._id, name: userData.name, carOffered, carPurchased, total: 0, carIncompletePurchased };
    
      return data;
 

};


const branchReportUser = async (vehicleData) => { 
  
    var won;

    if(vehicleData.current_bid>1 &&  !isEmpty(vehicleData.accepted_bid)){
      won = 'Y'
    }else if(vehicleData.current_bid>1 && isEmpty( vehicleData.accepted_bid)){
      won = 'Incomplete'
    }else if(vehicleData.current_bid===1){
      won = 'No Bids'
    }

    let data = {
      vehicle:vehicleData.year + " " + vehicleData.make + " " + vehicleData.model, 
      registration_number: vehicleData.registration_number ? vehicleData.registration_number : 'N/A', 
      won:won, 
      amount_bid:vehicleData.current_bid, 
      total: vehicleData.current_bid,
      fee:1
     };
    
      return data;
 

};

router.delete(
  "/deleteuser/:id",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.params.id;
    try {
      await AdminUser.findByIdAndRemove(user_id);
      let adminUsers = await AdminUser.find({ role: 2 }).sort({ _id: -1 });
      return res.json({ all: adminUsers });
    } catch (error) {
      return res.json({ error });
      console.log(error);
    }
  }
);

router.post(
  "/updateuser",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.body._id;
    try {
      await AdminUser.findOneAndUpdate(
        { _id: user_id },
        {
          $set: {
            name: req.body.name,
            dealer_number: req.body.dealer_number.toLowerCase(),
            username: req.body.username
          }
        }
      );

      let adminUsers = await AdminUser.find({ role: 2 }).sort({ _id: -1 });
      return res.json({ all: adminUsers });
    } catch (error) {
      console.log(error);
    }
  }
);

router.post(
  "/activeuser",
  passport.authenticate("admin-role", { session: false }),
  (req, res) => {
    const user_id = req.body.user_id;
    const status = req.body.status;

    AdminUser.findOneAndUpdate(
      { _id: user_id },
      {
        $set: {
          active: status
        }
      },
      { new: true }
    ).then(async user => {
      let adminUsers = await AdminUser.find({ role: 2 }).sort({ _id: -1 });
      return res.json({ all: adminUsers });
    });
  }
);

module.exports = router;
