const express  =   require('express');
const router   =   express.Router(); 
const Vehicles    =   require('../../../models/Vehicles'); 
const Profile = require('../../../models/Profile');
const passport =   require('passport');

// @route  GET api/admin/vehicles/all
// @desc   GET all users
// @access Private

router.get('/all', passport.authenticate('admin-role', {session: false}), (req, res) => {
 
	Vehicles.find().sort({_id:-1})
	.then(vehicles => {
		res.json({all: vehicles});
	})
	.catch(err => {
		console.log(err);
	});
});

router.get('/get-bids/:id', passport.authenticate('admin-role', {session: false}), async (req, res) => {
	let vehicle = await Vehicles.findOne({ _id: req.params.id }).populate('user accepted_bid')
	let sellerprofile = await Profile.findOne({user:vehicle.user._id}).populate('user')
	let winnerProfile={}
	let winningBidder={}
	if(vehicle.accepted_bid){
		winnerProfile = await Profile.findOne({user:vehicle.accepted_bid.user}).populate('user')
		winningBidder={
			name:winnerProfile.user.name,
			location:winnerProfile.address,
			make:vehicle.make,
		}
	}
	let itemListed={
		name:sellerprofile.user.name,
		location:sellerprofile.address,
		make:vehicle.make,
		listed_date:vehicle.date
	}
	Bids.find({vehicle: req.params.id}).sort({_id: -1}).populate('user')
	.then(bids => {
					return res.json({bids: bids,itemListed,winningBidder});
	})
	.catch(err => {
					console.log("No bids yet");
	});
})


 
module.exports = router;  