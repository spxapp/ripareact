const express  =   require('express');
const router   =   express.Router();
const Settings =   require('../../../models/admin/AdminSettings');
const bcrypt   =   require('bcryptjs');
const jwt      =   require('jsonwebtoken');
const keys     =   require('../../../config/keys');
const passport =   require('passport');

// Load Input Validation
const validateSettingsInputAdmin = require('../../../validation/admin/adminsettings');
     
// Fetch the settings

router.get('/', passport.authenticate('admin-role', {session: false}), (req, res) => {
  
	Settings.findOne()
	.then(settings =>{ 
		res.json({settings:settings}); 
	})
	.catch(err=>{
		console.log(err);
	});

}); 


// Update the Settings

router.post('/update', passport.authenticate('admin-role', {session: false}), (req, res) => {
 	
	const { errors, isValid } = validateSettingsInputAdmin(req.body);

	//check validation

	if(!isValid){
		return res.status(400).json(errors);
	}

 	const settings_id = req.body.settings_id?req.body.settings_id:null; 
	const credit_limit = req.body.credit_limit; 
	const admin_email = req.body.admin_email; 

	if(settings_id)
	{
		Settings.findOneAndUpdate(
		{_id: settings_id},
		{
		 $set:{credit_limit: credit_limit, admin_email: admin_email}
		},
		{new: true}
		)
		.then(settings =>{ 
			res.json({settings:settings, updated:true}); 
		})
		.catch(err=>{
			console.log(err);
		});

	}else{
		const adminSettings = new Settings({
				credit_limit: credit_limit,
				admin_email: admin_email 
			});	
 		adminSettings.save()
 		.then(settings =>{ 
			res.json(settings)
		})
	}
	 
	

}); 

 
module.exports = router;  