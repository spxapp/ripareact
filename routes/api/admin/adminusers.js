const express = require("express");
const router = express.Router();
const AdminUser = require("../../../models/admin/AdminUsers");
const Users = require("../../../models/Users");
const Profile = require("../../../models/Profile");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../../config/keys");
const passport = require("passport");

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(keys.SENDGRID_API_KEY);

// Load Input Validation
const validateLoginInputAdmin = require("../../../validation/admin/adminlogin");

// @route   POST /api/admin/users/register
// @desc    Register Users Route
// @access  Public
/*
router.post(
	'/register', (req, res) => { 

	const newUser = new AdminUser({ 
		username: req.body.username, 
		password: req.body.password
	});

	bcrypt.genSalt(10, (err, salt) => {
		bcrypt.hash(newUser.password, salt, (err, hash) => {
			if(err) throw err;
			newUser.password = hash;
			newUser.save()
			.then(user => res.json(user))
			.catch(err => console.log(err));
		})
	})
});
 */
// @route   POST api/users/login
// @desc    Login Users Route
// @access  Public

router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInputAdmin(req.body);

  //check validation

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const username = req.body.username;
  const password = req.body.password;

  // Find user by username
  AdminUser.findOne({ username, active: true }).then(user => {
    //check for user
    if (!user) {
      errors.username = "Invalid or Inactive User";
      return res.status(404).json(errors);
    }

    //check password

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        //user matched

        const payload = {
          id: user.id,
          username: user.username,
          role: user.role,
          name: user.name
        }; // create JWT payload

        //Sign Token

        jwt.sign(payload, keys.secretKey, (err, token) => {
          res.json({
            success: true,
            token: "Bearer " + token
          });
        });
      } else {
        errors.password = "Invalid Password";
        return res.status(400).json(errors);
      }
    });
  });
});

// @route  GET api/admin/users/all
// @desc   GET all users
// @access Private

router.get(
  "/all",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    try {
      let profile = await Profile.find()
        .sort({ _id: -1 })
        .populate("user");
      let users = await Users.find({ archived: false }).sort({ _id: -1 });
      return res.json({ all: profile, users });
    } catch (error) {
      console.log(error);
    }
  }
);

// @route  GET api/admin/users/all
// @desc   GET all users
// @access Private

router.get(
  "/admin/all",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    try {
      let adminUsers = await AdminUser.find().sort({ _id: -1 });
      return res.json({ all: adminUsers });
    } catch (error) {
      console.log(error);
    }
  }
);

router.delete(
  "/deleteuser/:id",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.params.id;
    try {
      await Users.findOneAndUpdate(
        { _id: user_id },
        {
          $set: {
            archived: true
          }
        },
        { new: true }
      );
      let profile = await Profile.find()
        .sort({ _id: -1 })
        .populate("user");
      let users = await Users.find({ archived: false }).sort({ _id: -1 });
      return res.json({ all: profile, users });
    } catch (error) {
      return res.json({ error });
      console.log(error);
    }
  }
);

router.get(
  "/getUser/:id",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.params.id;
    try {
      let profile = await Profile.findOne({ user: user_id }).populate("user");
      let userDetails = {
        ...profile._doc,
        ...profile.user._doc
      };
      return res.json({ profile: userDetails });
    } catch (error) {
      return res.json({ error });
      console.log(error);
    }
  }
);

router.post(
  "/updateuser",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.body.user_id;
    const profile_id = req.body.profile_id;
    try {
      await Users.findOneAndUpdate(
        { _id: user_id },
        {
          $set: {
            status: req.body.status,
            name: req.body.name,
            email: req.body.email,
            notifications: req.body.notification,
            credit_limit: req.body.creditlimit,
            virtual_credit: req.body.virtualcredit,
            suspended: req.body.suspended,
            dealer_number: req.body.dealer.toLowerCase(),
            branch: req.body.branch
          }
        },
        { new: true }
      );
      await Profile.findOneAndUpdate(
        { _id: profile_id },
        {
          $set: {
            step: req.body.steps,
            address: req.body.address,
            company: req.body.company,
            phone: req.body.phone
          }
        },
        { new: true }
      );
      let profile = await Profile.find()
        .sort({ _id: -1 })
        .populate("user");
      let users = await Users.find().sort({ _id: -1 });
      return res.json({ all: profile, users });
    } catch (error) {
      console.log(error);
    }
  }
);

router.post(
  "/suspendUser",
  passport.authenticate("admin-role", { session: false }),
  async (req, res) => {
    const user_id = req.body.user_id;
    console.log("body", req.body);
    try {
      await Users.findOneAndUpdate(
        { _id: user_id },
        {
          $set: {
            suspended: req.body.suspended
          }
        },
        { new: true }
      );
      return res.json({ status: true });
    } catch (error) {
      console.log(error);
    }
  }
);

router.post(
  "/activeuser",
  passport.authenticate("admin-role", { session: false }),
  (req, res) => {
    const user_id = req.body.user_id;
    const status = req.body.status;

    Users.findOneAndUpdate(
      { _id: user_id },
      {
        $set: {
          status: status
        }
      },
      { new: true }
    ).then(user => {
      if (user.status === true) {
        const msg = {
          to: user.email,
          from: "Ripa <info@ripa.app>",
          subject: "Your account registration in Ripa is now approved!",
          templateId: "a6f61e5a-6460-406d-b86d-275d25aa6315",
          substitutions: {
            user: user.name,
            SITE_URL: keys.HOST_PROTOCOL + keys.SITE_URL
          }
        };
        sgMail.send(msg);
      }

      Profile.find()
        .sort({ _id: -1 })
        .populate("user")
        .then(user => {
          let users = Users.find().sort({ _id: -1 });
          return res.json({ all: user, users });
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
);

module.exports = router;
