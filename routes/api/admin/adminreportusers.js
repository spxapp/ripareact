const express  =   require('express');
const router   =   express.Router();
const AdminUser    =   require('../../../models/admin/AdminUsers'); 
const bcrypt   =   require('bcryptjs');
const jwt      =   require('jsonwebtoken');
const keys     =   require('../../../config/keys');
const passport =   require('passport');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(keys.SENDGRID_API_KEY);

// Load Input Validation
const validateLoginInputAdmin = require('../../../validation/admin/adminlogin'); 
const validateAdminReportUsers = require('../../../validation/admin/adminreportusers');
  
// @route   POST /api/admin/users/register
// @desc    Register Users Route
// @access  Public
 
router.post(
	'/register', (req, res) => { 
  
	const { errors, isValid } = validateAdminReportUsers(req.body);

	//check validation 
	if(!isValid){	
		return res.status(400).json(errors);
	}

	const newUser = new AdminUser({
		role:2,
		name: req.body.name,
		dealer_number: req.body.dealer_number.toLowerCase(), 
		username: req.body.username, 
		password: req.body.password
	});

	bcrypt.genSalt(10, (err, salt) => {
		bcrypt.hash(newUser.password, salt, (err, hash) => {
			if(err) throw err;
			newUser.password = hash;
			newUser.save()
			.then(async (user) => {
				let adminUsers = await AdminUser.find({role:2}).sort({_id:-1})
				res.json({all: adminUsers });
			})
			.catch(err => console.log(err));
		})
	})
}); 
    
// @route  GET api/admin/users/all
// @desc   GET all users
// @access Private

router.get('/all', passport.authenticate('admin-role', {session: false}), async(req, res) => {
	
	try { 
		let adminUsers = await AdminUser.find({role:2}).sort({_id:-1})
		return res.json({all: adminUsers });
} catch (error) {
	console.log(error);
}})

router.delete('/deleteuser/:id', passport.authenticate('admin-role', {session: false}), async(req, res) => {
	const user_id=req.params.id;
	try {
		await AdminUser.findByIdAndRemove(user_id) 
		let adminUsers = await AdminUser.find({role:2}).sort({_id:-1})
		return res.json({all: adminUsers });
} catch (error) {
	return res.json({error});
	console.log(error);
}})
	
router.post('/updateuser', passport.authenticate('admin-role', {session: false}), async(req, res) => {
	
	const user_id=req.body._id; 
	try {

		await AdminUser.findOneAndUpdate({_id: user_id},{ $set:{ 
			name: req.body.name,
			dealer_number: req.body.dealer_number.toLowerCase(), 
			username: req.body.username,
		}
		});

		let adminUsers = await AdminUser.find({role:2}).sort({_id:-1})
		return res.json({all: adminUsers });

	} catch (error) {
		console.log(error);
	}

});

router.post('/activeuser', passport.authenticate('admin-role', {session: false}), (req, res) => {
 	
	const user_id = req.body.user_id;
	const status = req.body.status;
	
	AdminUser.findOneAndUpdate(
		{_id:user_id},
		{ $set : {   
			active:status
		}},
		{ new: true } 
	)
	.then( async user=>{
		let adminUsers = await AdminUser.find({role:2}).sort({_id:-1})
		return res.json({all: adminUsers });
	});  

});




 
module.exports = router;  