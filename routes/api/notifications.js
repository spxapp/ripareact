const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport'); 

// Load Notifications model
const Notifications = require('../../models/Notifications'); 

// Load User model
const Users = require('../../models/Users'); 

// Load Vehicles model
const Vehicles = require('../../models/Vehicles');

// @route   GET api/profile/test
// @desc    Test profile route
// @access  Public
 

router.get(
	'/get', passport.authenticate('user-role', {session: false}), (req, res) =>{ 

	const errors = {};

	//console.log(req.user.id);

	Notifications.find({ userTo: req.user.id, read: null}).sort({_id:-1}).limit(10)
		.then( notifications => {  
			 res.json({all:notifications}); 
		})
		.catch(err => res.status(404).json(err));	

}); 

router.get(
	'/update', passport.authenticate('user-role', {session: false}),  (req, res) =>{ 

	const errors = {};

	//console.log(req.user.id);

	Notifications.updateMany({ userTo: req.user.id, read: null},{
		$set:{read:true}
	})
		.then( notifications => { 
			   
			 Notifications.aggregate([
				{
					$match: {userTo: mongoose.Types.ObjectId(req.user.id)}
				},
				{ "$sort": { "_id": -1 } },
				{ "$limit": 20 },
				{
				$unwind: "$vehicle"
				},
				{
				$lookup:
					{
						from: "vehicles",
						localField: "vehicle",
						foreignField: "_id",
						as: "vehicle"
					}
				}
				])
				.then( notifications => { 
				     
					res.json({all:notifications}); 
				 })
				 .catch(err=>{
					return res.status(500).send(err);
				 }) 
	
	 


		})
		.catch(err => {
			Notifications.find({ userTo: req.user.id}).sort({_id:-1}).limit(10)
			 .then( notifications => { 
				 res.json({all:notifications}); 
			 })
		});	

}); 


module.exports = router;