const EventEmitter = require("events");

const VehicleEvent = new EventEmitter();
const events = ['timer','bidUpdate','accept-timer','bidAccept'];

// Set max event listeners (0 == unlimited)
VehicleEvent.setMaxListeners(0);

exports.VehicleEvent = VehicleEvent;
exports.register = register;

function register(io) {
 events.forEach(event => {
   let listner = createListener(event, io);
   VehicleEvent.on(event, listner);
 });
}

function createListener(event, io) {
 return function(data) {
   let room = `vehicle:${data.vehicle_id}`;
   io.sockets.in(room).emit(event, data)
 };
}
