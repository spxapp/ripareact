const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const xmlToJson = require("xml-js");
const request = require("request");

const keys = require("../../config/keys");
const stripe = require("stripe")(keys.StripeKey);

const AWS = require("aws-sdk");
const fs = require("fs");
const fileType = require("file-type");
const bluebird = require("bluebird");
const multiparty = require("multiparty");

const User = require("../../models/Users");
const Profile = require("../../models/Profile");
const Vehicles = require("../../models/Vehicles");
const Notifications = require("../../models/Notifications");
const Bids = require("../../models/Bids");
const UserSavedSearch = require("../../models/UserSavedSearch");

const isEmpty = require("../../validation/isEmpty");

// Load Input Validation
const validateVehicleInput = require("../../validation/vehicle");
const validateVehicleBidInput = require("../../validation/vehicleBid");

const sharp = require('sharp');

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(keys.SENDGRID_API_KEY);

// Soap Client
const SoapClient = require("../../utils/soapclient");

// configure the keys for accessing AWS
AWS.config.update({
  accessKeyId: "AKIAIVCB326MI5F3QU7A",
  secretAccessKey: "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"
});

// configure AWS to work with promises
AWS.config.setPromisesDependency(bluebird);

var io = require('socket.io')();
// create S3 instance
const s3 = new AWS.S3();

// abstracts function to upload a file returning a promise
const uploadFile = (buffer, name, type) => {
  const params = {
    ACL: "public-read",
    Body: buffer,
    Bucket: "ripa-car-app",
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };
  return s3.upload(params).promise();
};

const VehicleEvent = require('./sockets/vehicle.socket').VehicleEvent;
const isRoomExisting = require('./sockets/vehicle.socket').isRoomExisting;


router.post(
  "/sell",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    //console.log(req.body);
    const { errors, isValid } = validateVehicleInput(req.body);
    //check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    if (req.body.step === 2) {
      const registration_number = !isEmpty(req.body.registration_number)
        ? req.body.registration_number
        : "";
      const vin = !isEmpty(req.body.vin) ? req.body.vin : "";
      const year = !isEmpty(req.body.year) ? req.body.year : "";
      const make = !isEmpty(req.body.make) ? req.body.make : "";
      const model = !isEmpty(req.body.model) ? req.body.model : "";
      const starting_price = !isEmpty(req.body.starting_price)
        ? req.body.starting_price
        : "";
      const km = !isEmpty(req.body.km) ? req.body.km : "";
      const transmission = !isEmpty(req.body.transmission)
        ? req.body.transmission
        : "";
      const nz_new = !isEmpty(req.body.nz_new) ? req.body.nz_new : false;
      const location = !isEmpty(req.body.location) ? req.body.location : "";
      const power_train = !isEmpty(req.body.power_train)
        ? req.body.power_train
        : "";
      const vehicle_type = !isEmpty(req.body.vehicle_type)
        ? req.body.vehicle_type
        : "";
      const colour = !isEmpty(req.body.colour) ? req.body.colour : "";
      const engine = !isEmpty(req.body.engine) ? req.body.engine : "";
      const fuel_type = !isEmpty(req.body.fuel_type) ? req.body.fuel_type : "";
      const registration_expire = !isEmpty(req.body.registration_expire)
        ? req.body.registration_expire
        : "";
      const wof_expire = !isEmpty(req.body.wof_expire)
        ? req.body.wof_expire
        : "";
      const data = req.body;
      data["expired"] = false;
      data["nz_new"] = !isEmpty(data["nz_new"]) ? data["nz_new"] : false;

      const newVehicle = new Vehicles({
        user: req.user.id,
        registration_number: registration_number,
        vin: vin,
        year: year,
        step: 2,
        make: make,
        model: model,
        km: km,
        transmission: transmission,
        nz_new: nz_new,
        location: location,
        power_train: power_train,
        vehicle_type: vehicle_type,
        colour: colour,
        engine: engine,
        fuel_type: fuel_type,
        registration_expire: registration_expire,
        wof_expire: wof_expire,
        starting_price: starting_price
      });

      newVehicle.save().then(newVehicle => {
        // startTimer(newVehicle)
        return res.json({
          results: {
            vehicle: newVehicle,
            step: 2
          }
        });
      });
    } else if (req.body.step === 4) {
      try {
        const vehicle_id = req.body.vehicle_id;
        const add_details = req.body.add_details;
        const customer_id = req.body.customer_id;
        const current_user = req.user.id;

        var userDetails = await User.findOne({ _id: current_user });

        // ************ START **************
        // LISTING VEHICLE PAYMENT PROCESSOR
        // *********************************

        var virtual_credits = 0;
        var amount = 1;

        // if there is virtual credit availble in user account
        if (
          typeof userDetails.virtual_credit !== "undefined" &&
          userDetails.virtual_credit > 0
        ) {
          virtual_credits = parseFloat(userDetails.virtual_credit, 2);
        }

        // check if there is virtual credit availble in user account
        if (virtual_credits > 0) {
          // (nested if check) if virtual credit less then the transaction fee amount

          let totalAmountCheck = amount * 1.15;

          if (virtual_credits < totalAmountCheck) {
            // calculate the remaining amount

            let totalAmount = amount * 1.15;

            amount = totalAmount - virtual_credits;
            virtual_credits = 0;

            //update user virtual credit
            await User.findOneAndUpdate(
              { _id: current_user },
              {
                $set: {
                  virtual_credit: virtual_credits
                }
              }
            );

            // deduct remaining amount from stripe
            await stripe.charges.create({
              amount: amount * 100,
              currency: "nzd",
              description: "Listing vehicle",
              customer: customer_id
            });

            const vehicle = await Vehicles.findOneAndUpdate(
              { _id: vehicle_id },
              {
                $set: {
                  active: 1,
                  current_bid: 1,
                  step: 4,
                  add_details: add_details,
                  date: new Date(Date.now()).toISOString()
                }
              }
            );

            // call payment processor
            await vehicleSellProcessor(userDetails, vehicle, amount);

            return res.json({
              results: {
                step: 4,
                vehicleListedMsg: 1,
                vehicle: vehicle
              }
            });
          }
          // (nested if else check) if sufficent virtual credit availble
          else {
            let totalAmount = amount * 1.15;

            virtual_credits = virtual_credits - totalAmount;

            await User.findOneAndUpdate(
              { _id: current_user },
              {
                $set: {
                  virtual_credit: virtual_credits
                }
              }
            );

            const vehicle = await Vehicles.findOneAndUpdate(
              { _id: vehicle_id },
              {
                $set: {
                  active: 1,
                  current_bid: 1,
                  step: 4,
                  add_details: add_details,
                  date: new Date(Date.now()).toISOString()
                }
              }
            );

            // call payment processor
            await vehicleSellProcessor(
              userDetails,
              vehicle,
              amount,
              "Virtual Credits"
            );

            return res.json({
              results: {
                step: 4,
                vehicleListedMsg: 1,
                vehicle: vehicle
              }
            });
          }

          // if there is no virtual credit then charge from custom CC
        } else {
          if (customer_id) {
            let totalAmount = amount * 115;

            await stripe.charges.create({
              amount: totalAmount,
              currency: "nzd",
              description: "Listing vehicle",
              customer: customer_id
            });
          }

          const vehicle = await Vehicles.findOneAndUpdate(
            { _id: vehicle_id },
            {
              $set: {
                active: 1,
                current_bid: 1,
                step: 4,
                add_details: add_details,
                date: new Date(Date.now()).toISOString()
              }
            }
          );

          // call payment processor
          await vehicleSellProcessor(userDetails, vehicle, amount);

          return res.json({
            results: {
              step: 4,
              vehicleListedMsg: 1,
              vehicle: vehicle
            }
          });
        }

        // ************* END ***************
        // LISTING VEHICLE PAYMENT PROCESSOR
        // *********************************
      } catch (err) {
        //console.log(err)
        const errors = {};
        errors.error =
          "Transaction is unable to complete using your current payment method.";
        return res.status(400).json(errors);
      }
    } else {
      const registration_number = req.body.registration_number;

      if (isEmpty(registration_number)) {
        let responseData = {
          step: req.body.step,
          registration_number_empty: true
        };

        return res.json({ results: responseData });
      }

      if (
        process.env.NODE_ENV === "production" ||
        process.env.NODE_ENV === "staging"
        ) {
          try {
          //Authentication Request.
          const soapActionForAuth =
            '"http://schemas.cdi.ltsa.govt.nz/Security/AccessControl/AuthenticateClient"';
          const urlForAuth =
            "http://18.220.175.45:9080/cdpro/webservices/security/accesscontrol.asmx";
          const xmlForAuth = fs.readFileSync(
            "soap/xml/AuthenticateClient.xml",
            "utf-8"
          );

          let { statusCode, body } = await SoapClient(
            urlForAuth,
            soapActionForAuth,
            xmlForAuth
          );

          if (statusCode != 200) {
            return res
              .status(400)
              .json({ errorcode: "NO_TOKEN", error: "Token not generated" });
          }

          const results = xmlToJson.xml2js(body, { compact: true });
          const token =
            results["soap:Envelope"]["soap:Header"]["Security"]["CDIToken"][
              "TokenValue"
            ]["_text"];

          if (!token || typeof token == "undefined" || token == "") {
            return res
              .status(400)
              .json({ errorcode: "NO_TOKEN", error: "Token not generated" });
          }

          //Registration Request.
          const soapActionForRegn =
            '"http://schemas.cdi.ltsa.govt.nz/Registration/Registration/InquireCurrentDetails"';
          const urlForReg =
            "http://18.220.175.45:9080/cdpro/webservices/registration/registration.asmx";

          let xmlForReg = fs.readFileSync(
            "soap/xml/InquireCurrentDetails.xml",
            "utf-8"
          );

          xmlForReg = xmlForReg.replace("[token]", token);
          xmlForReg = xmlForReg.replace("[plate-number]", registration_number);

          let resultReg = await SoapClient(
            urlForReg,
            soapActionForRegn,
            xmlForReg
          );

          if (resultReg.statusCode != 200) {
            let responseData = {
              step: req.body.step,
              registration_number: registration_number,
              regnError: {
                errorCode: "NO_REGN",
                error: "Unable to retrieve registration details"
              }
            };
            return res.status(200).json({
              results: responseData
            });
          }

          const vehicleDetails = xmlToJson.xml2js(resultReg.body, {
            compact: true
          });
          const _inquireCurrentDetailsResponse =
            vehicleDetails["soap:Envelope"]["soap:Body"][
              "InquireCurrentDetailsResponse"
            ];

          if (
            !_inquireCurrentDetailsResponse ||
            typeof _inquireCurrentDetailsResponse == "undefined"
          ) {
            let responseData = {
              step: req.body.step,
              registration_number: registration_number,
              regnError: {
                errorCode: "NO_REGN",
                error: "Unable to retrieve registration details"
              }
            };
            return res.status(200).json({
              results: responseData
            });
          }

          let status =
            _inquireCurrentDetailsResponse["InquireCurrentDetailsResult"][
              "ServiceHeader"
            ]["Status"];

          if (status["_attributes"].Code != "102") {
            let responseData = {
              step: req.body.step,
              registration_number: registration_number,
              regnError: {
                errorCode: "NO_REGN",
                error: "Unable to retrieve registration details"
              }
            };
            return res.status(200).json({
              results: responseData
            });
          }

          const CDISessionToken =
            vehicleDetails["soap:Envelope"]["soap:Header"]["Security"][
              "CDISessionToken"
            ]["_text"];

          //EndSession Request.
          const soapActionForEndSession =
            '"http://schemas.cdi.ltsa.govt.nz/SessionManagement/AdministrationService/EndSession"';
          const urlForEndSession =
            "http://18.220.175.45:9080/cdpro/webservices//SessionManagement/AdministrationService.asmx";

          let xmlForEndSession = fs.readFileSync(
            "soap/xml/EndSession.xml",
            "utf-8"
          );

          xmlForEndSession = xmlForEndSession.replace("[token]", token);
          xmlForEndSession = xmlForEndSession.replace(
            "[CDISessionToken]",
            CDISessionToken
          );

          let endSession = await SoapClient(
            urlForEndSession,
            soapActionForEndSession,
            xmlForEndSession
          );

          const InquireCurrentDetailsResponse =
            vehicleDetails["soap:Envelope"]["soap:Body"][
              "InquireCurrentDetailsResponse"
            ];

          if (
            !InquireCurrentDetailsResponse ||
            typeof InquireCurrentDetailsResponse == "undefined"
          ) {
            let responseData = {
              step: req.body.step,
              registration_number: registration_number,
              regnError: {
                errorCode: "NO_REGN",
                error: "Unable to retrieve registration details"
              }
            };
            return res.status(200).json({
              results: responseData
            });
          }
          const vehicleInfo =
            InquireCurrentDetailsResponse["InquireCurrentDetailsResult"][
              "ResponseBody"
            ]["ResponseDetail"]["Vehicle"];

          if (
            vehicleInfo &&
            vehicleInfo["ReportedStolen"] &&
            vehicleInfo["ReportedStolen"]["_text"] == "true"
          ) {
            return res.status(400).json({
              errorcode: "STOLEN",
              error: "Vehicle is reported as stolen"
            });
          }

          let OdometerReading = 0;

          if (
            vehicleInfo["OdometerReadings"] &&
            vehicleInfo["OdometerReadings"]["OdometerReading"]
          ) {
            OdometerReading =
              vehicleInfo["OdometerReadings"]["OdometerReading"]["Reading"][
                "_text"
              ];
            const OdometerReadingUnit =
              vehicleInfo["OdometerReadings"]["OdometerReading"]["Unit"];

            if (OdometerReadingUnit && OdometerReadingUnit["_text"] == "M") {
              OdometerReading = OdometerReading * 1.6093;
            }
          }

          let wof_expire = "";
          if (
            vehicleInfo["Inspections"] &&
            vehicleInfo["Inspections"]["Inspection"] &&
            vehicleInfo["Inspections"]["Inspection"]["ExpiryDate"]
          ) {
            wof_expire =
              vehicleInfo["Inspections"]["Inspection"]["ExpiryDate"]["_text"];
          }

          let registration_expire = "";
          if (
            vehicleInfo["MvrLicences"] &&
            vehicleInfo["MvrLicences"]["MvrLicence"] &&
            vehicleInfo["MvrLicences"]["MvrLicence"]["ExpiryDate"]
          ) {
            registration_expire =
              vehicleInfo["MvrLicences"]["MvrLicence"]["ExpiryDate"]["_text"];
          }

          let responseData = {
            step: req.body.step,
            registration_number: registration_number,
            vin: vehicleInfo["VIN"] ? vehicleInfo["VIN"]["_text"] : "",
            km: OdometerReading,
            year: vehicleInfo["YearOfManufacture"]
              ? vehicleInfo["YearOfManufacture"]["_text"]
              : "",
            make: vehicleInfo["Make"]
              ? titleCase(vehicleInfo["Make"]["_text"])
              : "",
            model: vehicleInfo["Model"]
              ? titleCase(vehicleInfo["Model"]["_text"])
              : "",
            vehicle_type: vehicleInfo["VIVehicleTypeN"]
              ? titleCase(vehicleInfo["VIVehicleTypeN"]["_text"])
              : "",
            colour: vehicleInfo["BasicColour"]
              ? titleCase(vehicleInfo["BasicColour"]["_text"])
              : "",
            engine: vehicleInfo["CCRating"]
              ? titleCase(vehicleInfo["CCRating"]["_text"])
              : "",
            fuel_type: vehicleInfo["FuelType"]
              ? titleCase(vehicleInfo["FuelType"]["_attributes"]["Description"])
              : "",
            wof_expire: wof_expire,
            registration_expire: registration_expire
          };
          return res.json({ results: responseData });
        } catch (error) {
          console.log("==========");
          console.log(error);
          console.log("==========");
          return res
            .status(400)
            .json({ errorcode: "ERR_NZTA", error: "Problem with the NZTA" });
        }
      } else {
        let responseData = {
          step: req.body.step,
          registration_number: registration_number
        };

        return res.json({ results: responseData });
      }
    }
  }
);


startTimer=(data,req)=>{
  let interval = setInterval(async()=>{
    var eventTime= new Date(data.date).getTime() + (60*60*1000);
    var currentTime = new Date().getTime();
    var diffTime = (eventTime - currentTime)/1000;
    if(parseInt(diffTime, 10)==300){
      console.log("sending five mintutes")
			sendFiveMinuteNotification(data._id);
		}
      VehicleEvent.emit('timer', {
      vehicle_id: data._id,
      time: diffTime
    });
    if(diffTime<=0){
       vehicleExpires(data._id,req)
      clearInterval(interval)
    }
    }, 1000);
  
}

sendFiveMinuteNotification = async(vehicle_id) =>{
  try {
    var vehicle = await Vehicles.findOne({
      _id: vehicle_id,
      five_minutes_sent: { $exists: false },
      // date:{
      //   $lte: new Date(Date.now() - 55 * 60 * 1000) // condition to verify if time is not less then 55 mins
      // }
    });
  
  
    if (vehicle !== null) {
  
      await Vehicles.findOneAndUpdate(
        {
          _id: vehicle_id
        },
        {
          $set: {
            five_minutes_sent: true
          }
        }
      );
  
      var allBidders = await Bids.distinct("user", { vehicle: vehicle_id });
  
      var get_users_token = await User.distinct("notifications_token", {
        _id: {
          $in: [vehicle.user, ...allBidders]
        },
        notifications:true
      });
  
      get_users_token = get_users_token.concat(await User.distinct(
        "web_notification_token",
        {
          _id: {
            $in: [vehicle.user, ...allBidders]
          },
          notifications:true
        }
      ),
      await User.distinct(
        "ios_notification_token",
        {
          _id: {
            $in: [vehicle.user, ...allBidders]
          },
          notifications:true
        }
      ))
  
      // Send Push Notification to seller and bidders
      if (get_users_token !== null) {
        console.log("send five minute notification")
        let vehicle_name =
          vehicle.year + " " + vehicle.make + " " + vehicle.model;
        console.log("last five minutes")
        const req_res = await request({
          url: "https://fcm.googleapis.com/fcm/send",
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization:
              "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
          },
          body: JSON.stringify({
            notification: {
              title: "Ripa - Auction Ending in 5 minutes",
              body:
                "The vehicle: " +
                vehicle_name +
                " auction is ending in 5 minutes.",
              icon: "./logo_192x192.png",
              sound: "sound",
              "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
            },
            data: {
              body: "expired",
              vehicle: vehicle_id
            },
            registration_ids: get_users_token
          })
        });
      }
      
      if (allBidders !== null) {
        const emails = await Promise.all(
          allBidders.map(async user => await bidsEmail(vehicle, user))
        ); 
  
        if (!isEmpty(emails)) {
          sgMail.send(emails);
        }
      }
    }
  } catch (error) {
    console.log("five minute notification error",error)
  }
 
}

startExpireTimer=(data)=>{
  console.log("expired vehilce",data)
  try {
    let interval = setInterval(async()=>{
      var eventTime= new Date(data.expiry_date).getTime() + (1000*60*15);
      var currentTime = new Date().getTime();
      var diffTime = (eventTime - currentTime)/1000;
        VehicleEvent.emit('accept-timer', {
        vehicle_id: data._id,
        time: diffTime
      });
      if(diffTime<=0){
        clearInterval(interval)
      }
      }, 1000);
  } catch (error) {
    console.log("error",error)
  }
  
}

async function endSession(token, CDISessionToken) {
  const soapActionForEndSession =
    '"http://schemas.cdi.ltsa.govt.nz/SessionManagement/AdministrationService/EndSession"';
  const urlForEndSession =
    "http://18.220.175.45:9080/cdpro/webservices//SessionManagement/AdministrationService.asmx";

  let xmlForEndSession = fs.readFileSync("soap/xml/EndSession.xml", "utf-8");

  xmlForEndSession = xmlForEndSession.replace("[token]", token);
  xmlForEndSession = xmlForEndSession.replace(
    "[CDISessionToken]",
    CDISessionToken
  );

  let endSession = await SoapClient(
    urlForEndSession,
    soapActionForEndSession,
    xmlForEndSession
  );
}

function titleCase(str) {
  return str
    .toLowerCase()
    .split(" ")
    .map(function(word) {
      return word.replace(word[0], word[0].toUpperCase());
    })
    .join(" ");
}

// GET ALL LIVE AUCTION VEHICLES
router.get(
  "/all/active",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    //	{ active: true }
    Vehicles.find({ active: true })
      .sort({ _id: 1 })
      .then(vehicles => {
        return res.json({ results: vehicles });
      })
      .catch(err => {
        return res.status(500).send(err);
      });
  }
);

router.get(
  "/get-vehicle/:id",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    //	{ active: true }
    
    Vehicles.findOne({ _id:req.params.id })
      .then(vehicles => {
        console.log("vehicle is -----> ", vehicles)
        if(vehicles.active)
        {
          console.log("calling timer")
          startTimer(vehicles,req)
        }
        if(vehicles.expired)
        { console.log("expired")
          startExpireTimer(vehicles);
        }
        return res.json({ status:true });
      })
      .catch(err => {
        return res.status(500).send(err);
      });
  }
);

router.get(
  "/get-expired-vehicle/:id",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    //	{ active: true }
    Vehicles.findOne({ _id:req.params.id })
      .then(vehicle => {
        return res.json({ vehicle });
      })
      .catch(err => {
        return res.status(500).send(err);
      });
  }
);


// GET MY LISTING
router.get(
  "/mylisting",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    //	{ active: true }
    Vehicles.find({ user: req.user.id, step: 4 })
      .sort({ date: -1 })
      .limit(20)
      .then(vehicles => {
        return res.json({ results: vehicles });
      })
      .catch(err => {
        return res.status(500).send(err);
      });
  }
);

// GET MY BIDS
router.get(
  "/mybids",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    var bids = Bids.find({ user: req.user.id })
      .sort({ _id: -1 })
      .limit(20)
      .then(bids => {
        // Transform matching _id values into an array
        var vehicles = [];
        bids.forEach(function(bid) {
          vehicles.push(bid.vehicle);
        });

        Vehicles.find({
          _id: {
            $in: vehicles
          }
        })
          .sort({ _id: -1 })
          .limit(20)
          .then(vehicles => {
            return res.json({ results: vehicles });
          })
          .catch(err => {
            return res.status(500).send(err);
          });
      });
  }
);

// VEHICLE FILTERS

router.post(
  "/filters",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    let vehicleType = req.body.filters.vehicleType;
    let Mileage = req.body.filters.Mileage;
    let Make = req.body.filters.Make;
    let Model = req.body.filters.Model;
    let Year = req.body.filters.Year;
    let YearEnd = req.body.filters.YearEnd;
    let PriceRange = req.body.filters.PriceRange;
    let nznew = req.body.filters.nznew;
    let tab = req.body.tab;

    var filterQuery = {};
    
    if (vehicleType !== "") {
      if(!Array.isArray(vehicleType))
      {
        vehicleType = vehicleType.split()
      }
      vehicleType.push('');
      filterQuery.vehicle_type = { $in : vehicleType};
    }
    filterQuery.$and=[]
    if (Mileage && Mileage.start!=0 && Mileage.end != 1000000) {
      let start = Mileage.start
      let end = Mileage.end
      filterQuery.$and.push(
        {'km': {$gte: start}},
        {'km':{ $lte: end}}
      )
    }

    if (Make.length) {
      filterQuery.make = { $in  :Make};
    }

    if (Model !== "") {
      filterQuery.model = Model;
    }

    if (Year && YearEnd) {
      filterQuery.$and.push(
        {'year':{$gte:Year}},
        {'year':{$lte:YearEnd}}
      )
    }

    if(filterQuery.$and.length===0)
    {
      delete filterQuery.$and
    }

    if (PriceRange !== "") {
      PriceRange = PriceRange.replace("+", "00");

      filterQuery.current_bid = { current_bid: { $lte: PriceRange } };
    }

    if (tab == 1) {
      filterQuery.user = req.user.id;

      Vehicles.find(filterQuery)
        .sort({ _id: -1 })
        .limit(20)
        .then(vehicles => {
          return res.json({ results: vehicles });
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    } else if (tab == 2) {
      var bids = Bids.find({ user: req.user.id })
        .limit(20)
        .then(bids => {
          // Transform matching _id values into an array
          var vehicles = [];
          bids.forEach(function(bid) {
            vehicles.push(bid.vehicle);
          });

          filterQuery._id = { $in: vehicles };

          Vehicles.find(filterQuery)
            .sort({ _id: -1 })
            .limit(20)
            .then(vehicles => {
              return res.json({ results: vehicles });
            })
            .catch(err => {
              return res.status(500).send(err);
            });
        });
    } else {
      filterQuery['active'] = true;

      Vehicles.find(filterQuery)
        // .sort({ _id: 1 })
        .then(vehicles => {
          return res.json({ results: vehicles });
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    }
  }
);

router.post(
  "/getdetails",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const vehicle_id = req.body.vehicle_id;
    console.log("vehicle_id",vehicle_id)
    Vehicles.findOne({ _id: vehicle_id })
      .then(vehicle => {
        User.findOne({ _id: vehicle.user }).then(async user => {
          var user_latest_bid = await Bids.findOne({
            vehicle: vehicle_id,
            user: req.user.id
          }).sort({ _id: -1 });
          var statusMessage = false;
          var statusType = false;

          if (user_latest_bid !== null) {
            if (req.user.id === vehicle.user) {
              statusMessage = false;
              statusType = false;
            } else {
              if (user_latest_bid.bid_price >= vehicle.current_bid) {
                if (vehicle.expired) {
                  statusMessage = "You've won the auction.";
                  statusType = "success";
                } else {
                  statusMessage = "You are leading";
                  statusType = "success";
                }
              } else {
                if (vehicle.expired) {
                  statusMessage = "You've lost the auction.";
                  statusType = "orange";
                } else {
                  statusMessage = "You've been outbid";
                  statusType = "orange";
                }
              }
            }
          }

          const user_clone = JSON.parse(JSON.stringify(user));
          let soled_vehilces = await Vehicles.find({
            user: user._id,
            active: false,
            expired: true
          }).countDocuments();

          let seller_feedback =
            typeof user.seller_feedback !== "undefined"
              ? user.seller_feedback
              : 0;

          if (seller_feedback < 0) seller_feedback = 0;

          if (seller_feedback > 100) seller_feedback = 100;

          if (soled_vehilces == 0 && seller_feedback == 0)
            seller_feedback = 100;

          const user_addon = Object.assign(user_clone, {
            sell_rating: soled_vehilces + " (" + seller_feedback + "%)",
            buy_rating: "0",
            buy_rating_red: "(0)"
          });

          const vehicle_clone = JSON.parse(JSON.stringify(vehicle));

          vehicle_clone.user = user_addon;

          const vehicle_addon = Object.assign(vehicle_clone, {
            statusMessage: statusMessage,
            statusType: statusType
          });

          let index = vehicle_addon.images.indexOf(vehicle_addon.main_photo);
          vehicle_addon.images.splice(index, 1);
          vehicle_addon.images.splice(0, 0, vehicle_addon.main_photo);

          return res.json(vehicle_addon);
        });
      })
      .catch(err => {
        console.log("error fetching",err)
        return res.status(500).send(err);
      });
  }
);

// outbid status

router.post(
  "/outbidstatus",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const vehicle_id = req.body.vehicle_id;
    const current_bid = req.body.current_bid;
    const listing_user = req.body.listing_user;
    const current_user = req.user.id;

    if (current_user === listing_user) {
      return res.json({ results: false });
    }

    Vehicles.findOne({ _id: vehicle_id })
      .then(async vehicle => {
        var user_latest_bid = await Bids.findOne({
          vehicle: vehicle_id,
          user: current_user
        }).sort({ _id: -1 });

        if (user_latest_bid.bid_price >= current_bid) {
          if (vehicle.expired) {
            var eventTime= new Date(vehicle.expiry_date).getTime() + (1000*60*15);
            var currentTime = new Date().getTime();
            var diffTime = (eventTime - currentTime)/1000;
            if(!vehicle.accepted && diffTime<=0)
            {
              return res.json({
                results: { _id: vehicle_id, message: "The seller has rejected your bid." }
              });
            }
            if(vehicle.accepted && user_latest_bid.user==current_user)
            {
              return res.json({
                results: { _id: vehicle_id, message: "You've won the auction." }
              });
            }
            if(diffTime>0 && vehicle.accepted==undefined)
            {
              return res.json({
                results: { _id: vehicle_id, message: "You are the highest bidder pending approval" }
              });
            }
          } else {
            return res.json({
              results: { _id: vehicle_id, message: "You are leading" }
            });
          }
        } else {
          if (vehicle.expired) {
            return res.json({
              results: { _id: vehicle_id, message: "You've lost the auction." }
            });
          } else {
            return res.json({
              results: { _id: vehicle_id, message: "You've been outbid" }
            });
          }
        }
      })
      .catch(err => {
        return res.json({ results: false });
      });
  }
);

router.post(
  "/upload",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    const vehicle_id = req.body.vehicle_id;

    let imageFiles = req.files.images;

    var images = [];
    var promises = [];

    if (!isEmpty(imageFiles) && typeof imageFiles.length === "undefined") {
      imageFiles = [imageFiles];
    }

    try {
      let promises = await Promise.all(imageFiles.map(async (file, index) => {
        const buffer = file.data;
        const type = fileType(buffer);
        const timestamp = Date.now().toString();
        const fileName = `original/uploads/vehicle-pics/${index}-${timestamp}-lg`;

        let data = await sharp(buffer).rotate().resize(800, 800, {
          fit: 'inside',
        }).toBuffer();
        return uploadFile(data, fileName, type);
      }));
      let data = await Promise.all(promises);
      if (data.length > 0) {
        for (let i = 0; i < data.length; i++) {
          var image = data[i].Location;
          //image = image.replace("original/uploads/vehicle-pics","resized");
          images.push(image);
        }

        images.reverse();

        var mainImage = images[0];

        Vehicles.findOne({ _id: vehicle_id }).then(vehicle => {
          if (!isEmpty(vehicle.main_photo)) {
            mainImage = vehicle.main_photo;
          }

          Vehicles.findOneAndUpdate(
            { _id: vehicle_id },
            {
              $addToSet: {
                images: images
              },
              $set: {
                main_photo: mainImage
              }
            },
            { new: true }
          )
            .then(vehicle => {
              return res.json({
                results: {
                  step: 3,
                  imagesUploaded: 1,
                  vehicle: vehicle
                }
              });
            })
            .catch(error => {
              return res.status(500).send(error);
            });
        });
      }

    } catch (error) {
      console.log("error in uploading", error);
      return res.status(500).send(error);
    }
    
  }
);

// Set main image

router.post(
  "/main_image",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const vehicle_id = req.body.vehicle_id;
    const mainImage = req.body.mainImage;

    Vehicles.findOne({ _id: vehicle_id }).then(vehicle => {
      Vehicles.findOneAndUpdate(
        { _id: vehicle_id },
        {
          $set: {
            main_photo: mainImage
          }
        },
        { new: true }
      )
        .then(vehicle => {
          return res.json({
            results: {
              step: 3,
              imagesUploaded: 1,
              vehicle: vehicle
            }
          });
        })
        .catch(error => {
          return res.status(500).send(error);
        });
    });
  }
);

router.post(
  "/expire",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    try {
      const vehicle_id = req.body.vehicle_id;
      await vehicleExpires(vehicle_id,req)
      return res.json({ results: true });
    } catch (err) {
      return res.status(500).send(err);
    }
  }
);

vehicleExpires = async (id,req)=>{
  try {
    const vehicle_id = id;
      const expiry_date = new Date(Date.now()).toISOString()
 
      
      const vehicle = await Vehicles.findOneAndUpdate(
        { _id: vehicle_id, active: true, date:{
          $lte: new Date(Date.now() - 59 * 60 * 1000) // condition to verify if time is not less then 1 hr
        } },
        {
          $set: {
            active: 0,
            expired: 1,
            expiry_date: expiry_date
          }
        },
        { new: true }
      );
    
      
      if (vehicle === null) {
        const vehicles = await Vehicles.find({ active: true }).sort({ _id: 1 });
        return res.json({ results: vehicles });
      }

      // get winner user
      var last_user_bid = await Bids.findOne({ vehicle: vehicle._id }).sort({
        _id: -1
      });
 

      // vehicle have atleast one bid
      if (last_user_bid !== null) {
        if(vehicle.expiry_date){
          startExpireTimer(vehicle);
        }
        user_latest_bid = last_user_bid.bid_price;
        bidder = last_user_bid.user;

        //get user credit limit first
        var bidder_user = await User.findOne({ _id: bidder._id });

        if (bidder_user.credit_limit >= 0) {
          var new_credit_limit =
            parseInt(bidder_user.credit_limit, 10) +
            parseInt(user_latest_bid, 10);

          // if this user win the auction
          if (vehicle !== null && user_latest_bid === vehicle.current_bid) {
            var update_user = await User.findOneAndUpdate(
              { _id: bidder._id },
              {
                $set: { credit_limit: new_credit_limit }
              }
            );
          }
        }

        var get_bid_user_token = await User.distinct("notifications_token", {
          _id: last_user_bid.user,
          notifications:true
        });

        get_bid_user_token = get_bid_user_token.concat(await User.distinct(
          "web_notification_token",
          {
            _id: last_user_bid.user,
            notifications:true
          }
        ),await User.distinct(
          "ios_notification_token",
          {
            _id: last_user_bid.user,
            notifications:true
          }
        ))

        // Send Notification to the highest bidder
        if (get_bid_user_token !== null) {
          let vehicle_name =
            vehicle.year + " " + vehicle.make + " " + vehicle.model;

          const req_res = await request({
            url: "https://fcm.googleapis.com/fcm/send",
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization:
                "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
            },
            body: JSON.stringify({
              notification: {
                title: "Ripa - " + vehicle_name + " Auction Ended",
                body:
                  "The vehicle: " +
                  vehicle_name +
                  " auction has ended and you are the highest bidder! Congratulations!",
                icon: "./logo_192x192.png",
                sound: "sound",
                "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
              },
              data: {
                body: "expired",
                vehicle: vehicle_id,
                expiry:expiry_date
              },
              registration_ids: get_bid_user_token
            })
          });
        }

        // add notification entry
        const newNotification = new Notifications({
          userTo: last_user_bid.user,
          userFrom: req.user.id,
          type: "auction_ended_buyer",
          vehicle: vehicle._id
        });
        newNotification.save();

        // send email to the winner
        if (bidder_user.email) {
          let vehicle_name =
            vehicle.year + " " + vehicle.make + " " + vehicle.model;
          const msg = {
            to: bidder_user.email,
            from: "Ripa <info@ripa.app>",
            subject:
              "Congratulations,  you were the highest bidder for vehicle: " +
              vehicle_name +
              ".",
            templateId: "f30adf5f-e103-45cc-b0c9-8b761ec87faf",
            substitutions: {
              user: bidder_user.name,
              vehicle: vehicle_name,
              SITE_URL: "https://www.ripa.co.nz/contact/"
            }
          };
          sgMail.send(msg);
        }

        // send email to the loosers

        var allLoosers = await Bids.distinct("user", {
          vehicle: vehicle._id,
          user: {
            $nin: [last_user_bid.user]
          }
        });
        var looserDetails = await Bids.find({
          vehicle: vehicle._id,
          user: {
            $nin: [last_user_bid.user]
          }
        });
        
        looserDetails.map(async(details)=>{
          await User.update({_id: details.user},{ $inc: { credit_limit: details.bid_price}})
        })
        var totalBids = 1;

        if (allLoosers !== null) {
          const loosers = [...allLoosers];

          const emails = await Promise.all(
            loosers.map(async user => await emailLoosers(vehicle, user))
          );

          if (!isEmpty(emails)) {
            sgMail.send(emails);
          }

          totalBids += loosers.length;
        }

        // send notification to seller
        var get_seller_user_token = await User.distinct("notifications_token", {
          _id: vehicle.user,
          notifications:true
        });

        get_seller_user_token = get_seller_user_token.concat(await User.distinct(
          "web_notification_token",
          {
            _id: vehicle.user,
            notifications:true
          }
        ),
        await User.distinct(
          "ios_notification_token",
          {
            _id: vehicle.user,
            notifications:true
          }
        ))

        let vehicle_name =
          vehicle.year + " " + vehicle.make + " " + vehicle.model;

        if (get_seller_user_token !== null) {
          await request({
            url: "https://fcm.googleapis.com/fcm/send",
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization:
                "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
            },
            body: JSON.stringify({
              notification: {
                title: "Ripa - " + vehicle_name + " Auction Ended",
                body:
                  "The vehicle: " +
                  vehicle_name +
                  " auction has ended. It has " +
                  totalBids +
                  " bid/s. You have 15 mins to approve or reject the offer. Tap to see details.",
                icon: "./logo_192x192.png",
                sound: "sound",
                "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
              },
              data: {
                body: "expired",
                vehicle: vehicle_id,
                expiry:expiry_date
              },
              registration_ids: get_seller_user_token
            })
          });
        }

        const newNotificationSeller = new Notifications({
          userTo: vehicle.user,
          userFrom: vehicle.user,
          type: "auction_ended_seller",
          vehicle: vehicle._id
        });
        newNotificationSeller.save();

        var seller_user = await User.findOne({
          _id: vehicle.user
        });

        // send email notification to seller
        const msg = {
          to: seller_user.email,
          from: "Ripa <info@ripa.app>",
          subject: "Ripa - " + vehicle_name + " Auction Ended",
          templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
          substitutions: {
            user: seller_user.name,
            SITE_URL: "https://www.ripa.co.nz/contact/",
            message:
              "The vehicle: " +
              vehicle_name +
              " auction has ended. It has " +
              totalBids +
              " bid/s. You have 15 mins to approve or reject the offer."
          }
        };
        sgMail.send(msg);
      } else {
        var seller_user = await User.findOne({ _id: vehicle.user });
        if (typeof seller_user.seller_feedback !== "undefined") {
          var seller_feedback = parseInt(seller_user.seller_feedback - 1, 10);
        } else {
          var seller_feedback = 100 - 10;
        }
        VehicleEvent.emit('accept-timer', {
          vehicle_id: vehicle._id,
          time: 0
        });
        if (seller_feedback > 100) {
          seller_feedback = 100;
        }

        if (seller_feedback < 0) {
          seller_feedback = 0;
        }

        var update_seller = await User.findOneAndUpdate(
          { _id: vehicle.user },
          {
            $set: { seller_feedback: seller_feedback }
          }
        );

        // send notification to seller
        var get_seller_user_token = await User.distinct("notifications_token", {
          _id: vehicle.user,
          notifications:true
        });

        get_seller_user_token = get_seller_user_token.concat(await User.distinct(
          "web_notification_token",
          {
            _id: vehicle.user,
            notifications:true
          }
        ),
        await User.distinct(
          "ios_notification_token",
          {
            _id: vehicle.user,
            notifications:true
          }
        ))

        let vehicle_name =
          vehicle.year + " " + vehicle.make + " " + vehicle.model;

        if (get_seller_user_token !== null) {
          await request({
            url: "https://fcm.googleapis.com/fcm/send",
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization:
                "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
            },
            body: JSON.stringify({
              notification: {
                title: "Ripa - " + vehicle_name + " Auction Ended",
                body:
                  "The vehicle: " +
                  vehicle_name +
                  " auction has ended. Unfortunately, there are no bids. Please try listing the vehicle again.",
                icon: "./logo_192x192.png",
                sound: "sound",
                "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
              },
              data: {
                body: "expired",
                vehicle: vehicle_id,
                expiry:expiry_date
              },
              registration_ids: get_seller_user_token
            })
          });
        }
        const newNotification = new Notifications({
          userTo: vehicle.user,
          userFrom: vehicle.user,
          type: "auction_ended_seller_no_bids",
          vehicle: vehicle._id
        });
        newNotification.save();

        // send email notification to seller
        const msg = {
          to: seller_user.email,
          from: "Ripa <info@ripa.app>",
          subject: "Ripa - " + vehicle_name + " Auction Ended",
          templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
          substitutions: {
            user: seller_user.name,
            SITE_URL: "https://www.ripa.co.nz/contact/",
            message:
              "The vehicle: " +
              vehicle_name +
              " auction has ended. Unfortunately, there are no bids. Please try listing the vehicle again."
          }
        };
        sgMail.send(msg);
      }

      const vehicles = await Vehicles.find({ active: true }).sort({ _id: 1 });
  } catch (error) {
    console.log("error",error)
  }
}

emailLoosers = async (vehicle, user) => {
  let userDetails = await User.findOne({ _id: user });

  let vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;

  // send email notification to buyer and seller
  return {
    to: userDetails.email,
    from: "Ripa <info@ripa.app>",
    subject: "Sorry, you lost the auction for vehicle: " + vehicle_name + ".",
    templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
    substitutions: {
      user: userDetails.name,
      vehicle: vehicle_name,
      SITE_URL: "https://www.ripa.co.nz/contact/",
      message:
        "The vehicle: " +
        vehicle_name +
        " auction has ended and you lost the auction"
    }
  };
};

// Accepted/Rejected
router.post(
  "/accepted_rejected",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    const vehicle_id = req.body.vehicle_id;
    const accepted_rejected =
      req.body.accepted_rejected == "true" ? true : false;

    let winBid;
    try {
      var last_user_bid = await Bids.findOne({ vehicle: vehicle_id }).sort({bid_price:-1}).limit(1);
      winBid = last_user_bid.id;
    } catch (error) {
      return res.status(500).send(error);
    }

    const vehicle = await Vehicles.findOneAndUpdate(
      { _id: vehicle_id /*active: false, expired: true*/ },
      {
        $set: {
          accepted: accepted_rejected,
          accepted_bid: winBid,
          accepted_date: new Date(Date.now()).toISOString()
        }
      },
      { new: true }
    );

    try {
      if (vehicle !== null) {
        // get winner user
        var last_user_bid = await Bids.findOne({ vehicle: vehicle._id }).sort({bid_price:-1}).limit(1);

        var seller = vehicle.user;
        var buyer = last_user_bid.user;

        if (last_user_bid !== null) {
          //get seller user
          var seller_user = await User.findOne({ _id: seller });
          var buyer_user = await User.findOne({ _id: buyer });

          var buyer_details = await Profile.findOne({ user: buyer });
          var seller_details = await Profile.findOne({ user: seller });

          if (accepted_rejected) {
            //postive feedback

            if (typeof seller_user.seller_feedback !== "undefined") {
              var seller_feedback = parseInt(
                seller_user.seller_feedback + 1,
                10
              );
            } else {
              var seller_feedback = 100;
            }

            var get_bid_user_token = await User.distinct(
              "notifications_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            );

            get_bid_user_token = get_bid_user_token.concat(await User.distinct(
              "web_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ),
            await User.distinct(
              "ios_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ))

            VehicleEvent.emit('bidAccept', {
              user: last_user_bid.user,
              vehicle_id: vehicle._id,
              message: "You've won the auction."
            });

            // Send Notification to the highest bidder
            if (get_bid_user_token !== null) {
              let vehicle_name =
                vehicle.year + " " + vehicle.make + " " + vehicle.model;

              const req_res = await request({
                url: "https://fcm.googleapis.com/fcm/send",
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Authorization:
                    "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
                },
                body: JSON.stringify({
                  notification: {
                    title: "Ripa - " + vehicle_name + " Bid Accepted!",
                    body:
                      "Your bid has been accepted by seller. Tap to view seller contact details.",
                    icon: "./logo_192x192.png",
                    sound: "sound",
                    "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
                  },
                  data: {
                    body: "seller_details",
                    user: seller
                  },
                  registration_ids: get_bid_user_token
                })
              });
            }

            let vehicle_name =
              vehicle.year + " " + vehicle.make + " " + vehicle.model;

            // add notification entry
            const newNotification = new Notifications({
              userTo: last_user_bid.user,
              userFrom: req.user.id,
              type: "auction_accepted",
              vehicle: vehicle._id
            });

            newNotification.save();

            // send email notification to buyer and seller
            const msg = {
              to: buyer_user.email,
              from: "Ripa <info@ripa.app>",
              subject: "Congratulations! " + vehicle_name + " Bid Accepted!",
              templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
              substitutions: {
                user: buyer_user.name,
                seller_name: seller_user.name,
                seller_email: seller_user.email,
                vehicle: vehicle_name,
                SITE_URL: "https://www.ripa.co.nz/contact/",
                message:
                  "Congratulations! The seller of " +
                  vehicle_name +
                  " has accepted your bid. Please find below the contact details of the seller: <br/><br/>" +
                  "Seller Name: " +
                  seller_user.name +
                  "<br/>" +
                  "Seller Email: " +
                  seller_user.email +
                  "<br/>" +
                  "Phone: " +
                  seller_details.phone +
                  "<br/>" +
                  "Company: " +
                  seller_details.company +
                  "<br/>"
              }
            };
            sgMail.send(msg);

            const msg1 = {
              to: seller_user.email,
              from: "Ripa <info@ripa.app>",
              subject:
                "Congratulations! " + vehicle_name + " successfully auctioned!",
              templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
              substitutions: {
                user: seller_user.name,
                buyer_name: buyer_user.name,
                buyer_email: buyer_user.email,
                vehicle: vehicle_name,
                SITE_URL: "https://www.ripa.co.nz/contact/",
                message:
                  "Congratulations! Your vehicle: " +
                  vehicle_name +
                  " has been successfully auctioned. Please find below the contact details of the buyer:<br/><br/>" +
                  "Buyer Name: " +
                  buyer_user.name +
                  "<br/>" +
                  "Buyer Email: " +
                  buyer_user.email +
                  "<br/>" +
                  "Phone: " +
                  buyer_details.phone +
                  "<br/>" +
                  "Company: " +
                  buyer_details.company +
                  "<br/>"
              }
            };
            sgMail.send(msg1);

            // Deduct Winner $1 + gst
            var userDetails = await User.findOne({ _id: buyer });

            var virtual_credits = 0;
            var amount = 1;

            if (
              typeof userDetails.virtual_credit !== "undefined" &&
              userDetails.virtual_credit > 0
            ) {
              virtual_credits = parseFloat(userDetails.virtual_credit);
            }

            
            var paymentType = 'Credit Card';

            // check if the buyer have the virtual credits available
            if (virtual_credits > 0) {
              // check if the buyer virtual credit less or equal to 1

              let totalAmountCheck = amount * 1.15;
              // calculate the remaining amount
              if (virtual_credits < totalAmountCheck) {
                let totalAmount = amount * 1.15;

                amount = totalAmount - virtual_credits;
                virtual_credits = 0;

                const updateUser = User.findOneAndUpdate(
                  { _id: buyer },
                  {
                    $set: {
                      virtual_credit: virtual_credits
                    }
                  }
                ).then(async user => {
                  let profile = await Profile.findOne({ user: buyer });

                  if (profile.customer_id) {
                    await stripe.charges.create({
                      amount: amount * 100,
                      currency: "nzd",
                      description: "Bid Winner charges",
                      customer: profile.customer_id
                    });
                  }
                });
                // else the buyer virtual credit bigger then 1
              } else {
                let totalAmount = amount * 1.15;

                virtual_credits = virtual_credits - totalAmount;

                await User.findOneAndUpdate(
                  { _id: buyer },
                  {
                    $set: {
                      virtual_credit: virtual_credits
                    }
                  }
                );
              }

              paymentType = 'Virtual Credits';

              // buyer doesn't have the virtual credit, deduct from stripe
            } else {
              let profile = await Profile.findOne({ user: buyer });

              if (profile.customer_id) {
                await stripe.charges.create({
                  amount: amount * 115,
                  currency: "nzd",
                  description: "Bid Winner charges",
                  customer: profile.customer_id
                });
              }
            }

            if (userDetails.email) {
              let vehicle_name
              if(vehicle.registration_number){
                vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - " + vehicle.registration_number;
              }
              else{
                vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - No Rego";
              }
      
              let gst = 15;
              let calcGst = parseFloat((amount * gst) / 100, 2);
              let invoice =  "IN_" + Math.floor(Math.random() * (50000 - 5000 + 1)) + 5000;
               await Vehicles.findOneAndUpdate(
                { _id: vehicle._id /*active: false, expired: true*/ },
                {
                  $set: {
                    buyer_invoice:invoice
                  }
                },
                { new: true }
              );
          
      
              const msg = {
                to: userDetails.email,
                from: "Ripa <info@ripa.app>",
                subject: `Invoice for Winning Bid Vehicle - ${vehicle_name}`,
                templateId: "ac8174d2-790a-4035-9050-0c0ba2b15894",
                substitutions: {
                  invoice_id:invoice,
                  invoice_date: new Date()
                    .toString()
                    .replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, "$2-$1-$3"),
                  user_name: userDetails.name,
                  user_email: userDetails.email,
                  vehicle_name: `${vehicle_name} - Winning Bid`,
                  vehicle_price: amount,
                  payment_type: paymentType,
                  vehicle_currency: "NZD",
                  gst_price: `($${calcGst}) ${gst}%`,
                  total_price: parseFloat(amount + calcGst, 2),
                  SITE_URL: "https://www.ripa.co.nz/contact/"
                }
              };
      
              sgMail.send(msg);
            }

          } else {
            //negative feedback

            if (typeof seller_user.seller_feedback !== "undefined") {
              var seller_feedback = parseInt(
                seller_user.seller_feedback - 1,
                10
              );
            } else {
              var seller_feedback = 100 - 10;
            }

            // Deduct seller $1 for rejection and credit to the top bidder
            var sellerDetails = await User.findOne({ _id: seller_user._id });

            var get_bid_user_token = await User.distinct(
              "notifications_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            );

            get_bid_user_token = get_bid_user_token.concat(await User.distinct(
              "web_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ),
            await User.distinct(
              "ios_notification_token",
              {
                _id: last_user_bid.user,
                notifications:true
              }
            ))

            var sellerVirtualCredits = 0;
            var amount = 1;

            if (
              typeof sellerDetails.virtual_credit !== "undefined" &&
              sellerDetails.virtual_credit > 0
            ) {
              sellerVirtualCredits = parseFloat(
                sellerDetails.virtual_credit,
                2
              );
            }
            let totalAmountCheck = amount * 1.15;
            // check if the buyer have the virtual credits available
            var paymentType = 'Credit Card';

            //When rejecting or not accepting bid, the seller must not be charged on it and nothing must be added to the buyer's virtual credit.

            // if (sellerVirtualCredits > 0) {
            //   // check if the buyer virtual credit less or equal to 1
            //   if (sellerVirtualCredits < totalAmountCheck) {
            //     let totalAmount = amount * 1.15;

            //     amount = totalAmount - sellerVirtualCredits;
            //     virtual_credits = 0;

            //     // Deduct from seller and update
            //     User.findOneAndUpdate(
            //       { _id: sellerDetails._id },
            //       {
            //         $set: {
            //           virtual_credit: virtual_credits
            //         }
            //       }
            //     ).then(async user => {
            //       let profile = await Profile.findOne({
            //         user: sellerDetails._id
            //       });

            //       if (profile.customer_id) {
            //         await stripe.charges.create({
            //           amount: amount * 100,
            //           currency: "nzd",
            //           customer: profile.customer_id
            //         });
            //       }
            //     });
            //     // else the seller virtual credit bigger then 1
            //   } else {
            //     let totalAmount = amount * 1.15;

            //     virtual_credits = sellerVirtualCredits - totalAmount;
            //     const updateUser = await User.findOneAndUpdate(
            //       { _id: sellerDetails._id },
            //       {
            //         $set: {
            //           virtual_credit: virtual_credits
            //         }
            //       }
            //     );
            //   }
            //   paymentType = 'Virtual Credits';
            //   // seller doesn't have the virtual credit, deduct from stripe
            // } else {
            //   let profile = await Profile.findOne({ user: sellerDetails._id });

            //   if (profile.customer_id) {
            //     await stripe.charges.create({
            //       amount: amount * 115,
            //       currency: "nzd",
            //       customer: profile.customer_id
            //     });
            //   }
            // }
 
            // if (sellerDetails.email) {
            
            //     let vehicle_name
            //     if(vehicle.registration_number){
            //       vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - " + vehicle.registration_number;
            //     }
            //     else{
            //       vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - No Rego";
            //     }

            //   let gst = 15;
            //   let calcGst = parseFloat((amount * gst) / 100, 2);
      
            //   const msg = {
            //     to: sellerDetails.email,
            //     from: "Ripa <info@ripa.app>",
            //     subject: `Invoice for Reject Bid Vehicle - ${vehicle_name}`,
            //     templateId: "ac8174d2-790a-4035-9050-0c0ba2b15894",
            //     substitutions: {
            //       invoice_id:
            //         "IN_" + Math.floor(Math.random() * (50000 - 5000 + 1)) + 5000,
            //       invoice_date: new Date()
            //         .toString()
            //         .replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, "$2-$1-$3"),
            //       user_name: sellerDetails.name,
            //       user_email: sellerDetails.email,
            //       vehicle_name: `${vehicle_name} - Reject Bid`,
            //       vehicle_price: amount,
            //       payment_type: paymentType,
            //       vehicle_currency: "NZD",
            //       gst_price: `($${calcGst}) ${gst}%`,
            //       total_price: parseFloat(amount + calcGst, 2),
            //       SITE_URL: "https://www.ripa.co.nz/contact/"
            //     }
            //   };
 
      
            //   sgMail.send(msg);
            // }

            // Credit to buyer and update
            // const updateBuyer = await User.findOneAndUpdate(
            //   { _id: buyer },
            //   {
            //     $inc: {
            //       virtual_credit: amount * 1.15
            //     }
            //   }
            // );
            VehicleEvent.emit('bidAccept', {
              user: last_user_bid.user,
              vehicle_id: vehicle._id,
              message: "The seller has rejected your bid."
            });

            let vehicle_name =
              vehicle.year + " " + vehicle.make + " " + vehicle.model;

            // if bidder user has opted for notifications
            if (get_bid_user_token) {
              await request({
                url: "https://fcm.googleapis.com/fcm/send",
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Authorization:
                    "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
                },
                body: JSON.stringify({
                  notification: {
                    title: "Ripa - " + vehicle_name + " Bid Rejected!",
                    body: "Your bid has been rejected by seller.",
                    icon: "./logo_192x192.png",
                    sound: "sound",
                    "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
                  },
                  data: {
                    body: "expired",
                    vehicle: vehicle._id,
                    expiry:vehicle.expiry_date
                  },
                  registration_ids: get_bid_user_token
                })
              });
            }

            // send rejected email notification to buyer and seller
            const msg = {
              to: buyer_user.email,
              from: "Ripa <info@ripa.app>",
              subject: "Sorry, " + vehicle_name + " Bid Rejected!",
              templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
              substitutions: {
                user: buyer_user.name,
                seller_name: seller_user.name,
                seller_email: seller_user.email,
                vehicle: vehicle_name,
                SITE_URL: "https://www.ripa.co.nz/contact/",
                message:
                  "Sorry, The seller of " +
                  vehicle_name +
                  " has rejected your bid."
              }
            };
            sgMail.send(msg);
          }

          if (seller_feedback > 100) {
            seller_feedback = 100;
          }

          if (seller_feedback < 0) {
            seller_feedback = 0;
          }

          var update_seller = await User.findOneAndUpdate(
            { _id: seller },
            {
              $set: { seller_feedback: seller_feedback }
            }
          );
        }
      }

      res.json(vehicle);
    } catch (err) {
      res.json(vehicle);
    }
  }
);

// Send 5 mins notification to the seller and bidders
router.post(
  "/five-minutes-notifications",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    const vehicle_id = req.body.vehicle_id;
    try {
      this.sendFiveMinuteNotification(vehicle_id);
      res.status(200).send("Notification sent");
    } catch (err) {
      res.status(500).json("Invalid vehicle id");
    }
  }
);

bidsEmail = async (vehicle, user) => {
  let userDetails = await User.findOne({ _id: user });

  let vehicle_name = vehicle.year + " " + vehicle.make + " " + vehicle.model;

  // send email notification to buyer and seller
  return {
    to: userDetails.email,
    from: "Ripa <info@ripa.app>",
    subject: "Ripa - " + vehicle_name + " Auction Ending in 5 minutes",
    templateId: "26bfd657-ede1-4282-be69-673a97cbd4a4",
    substitutions: {
      user: userDetails.name,
      vehicle: vehicle_name,
      SITE_URL: "https://www.ripa.co.nz/contact/",
      message:
        "The vehicle: " + vehicle_name + " auction is ending in 5 minutes."
    }
  };
};

router.post(
  "/bid",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    console.log("reached here=====bid")
    const { errors, isValid } = validateVehicleBidInput(req.body);
    //check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const vehicle_id = req.body.vehicle_id;
    const bid_price = req.body.bid_price;
    const userTo = req.body.userTo;

    Vehicles.findOne({ _id: vehicle_id }).then(async vehicle => {
      var currentBid = vehicle.current_bid ? vehicle.current_bid : 1;
      var nextBid = parseInt(currentBid + 100, 10);
      if (bid_price >= nextBid) {
        //get user credit limit first
        var user = await User.findOne({ _id: req.user.id });
        /*
				console.log("=====USER CREDIT LIMIT======");
				console.log(user.credit_limit);
				console.log("==============");
				*/

        // check if user credit limit is 0
        if (user.credit_limit <= 0) {
          errors.bid_price = "Sorry, Your credit limit is zero";
          return res.status(400).json(errors);
        }

        var all_active_vehicles = await Vehicles.distinct("_id", {
          active: true
        });

        // get user latest bid on this vehicle
        var user_latest_bid = await Bids.findOne({
          vehicle: vehicle_id,
          user: req.user.id
        }).sort({ _id: -1 });

        if (user_latest_bid === null) {
          user_latest_bid = 0;
        } else {
          user_latest_bid = user_latest_bid.bid_price;
        }

        /*
				console.log("=====User Latest Bid======");
				console.log(user_latest_bid);
				console.log("==============");
			*/

        var new_credit_limit =
          parseInt(bid_price, 10) - parseInt(user_latest_bid, 10);

        /*
				console.log("=====New Credit limit======");
				console.log(new_credit_limit);
				console.log("==============");
				*/

        if (user.credit_limit >= new_credit_limit) {
          Vehicles.findOneAndUpdate(
            {
              _id: vehicle_id
            },
            {
              $set: {
                current_bid: bid_price
              }
            },
            { new: true }
          )
            .then(vehicle => {
              // add bid entry
              const newBid = new Bids({
                user: req.user.id,
                vehicle: vehicle_id,
                bid_price: bid_price
              });
              console.log("--------bid added------")
              VehicleEvent.emit('bidUpdate', {
                user: req.user.id,
                vehicle_id: vehicle_id,
                bid_price: bid_price
              });
              newBid.save();

              // add notification entry
              const newNotification = new Notifications({
                userTo: userTo,
                userFrom: req.user.id,
                type: "new_bid",
                vehicle: vehicle_id,
                price: bid_price
              });

              newNotification.save();

              Vehicles.find({ active: true })
                .sort({ _id: 1 })
                .then(async vehicles => {
                  User.findByIdAndUpdate(
                    {
                      _id: req.user.id
                    },
                    {
                      credit_limit:
                        req.user.credit_limit - (bid_price - user_latest_bid)
                    },
                    { new: true }
                  ).exec();
                  // get user latest bid on this vehicle
                  var get_all_bid_users = await Bids.distinct("user", {
                    vehicle: vehicle_id
                  });
                
                  var get_all_bid_users_tokens = await User.distinct(
                    "notifications_token",
                    {
                      _id: {
                        $in: get_all_bid_users,
                        $nin: [req.user.id]
                      },
                      notifications:true
                    }
                  );

                  get_all_bid_users_tokens = get_all_bid_users_tokens.concat(await User.distinct(
                    "web_notification_token",
                    {
                      _id: {
                        $in: get_all_bid_users,
                        $nin: [req.user.id]
                      },
                      notifications:true
                    }
                  ),
                  await User.distinct(
                    "ios_notification_token",
                    {
                      _id: {
                        $in: get_all_bid_users,
                        $nin: [req.user.id]
                      },
                      notifications:true
                    }
                  ))
                    console.log("siteurl======",keys.SITE_URL)
                  if (get_all_bid_users_tokens !== null) {
                    let vehicle_name =
                      vehicle.year + " " + vehicle.make + " " + vehicle.model;

                    const req_res = await request({
                      url: "https://fcm.googleapis.com/fcm/send",
                      method: "POST",
                      headers: {
                        "Content-Type": "application/json",
                        Authorization:
                          "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOM" +
                          "Sz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
                      },
                      body: JSON.stringify({
                        notification: {
                          title: "Ripa - " + vehicle_name,
                          body: "You have been outbid!",
                          icon: "./logo_192x192.png",
                          sound: "sound",
                          "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`
                        },
                        data: {
                          body: "outbid",
                          vehicle: vehicle_id
                        },
                        registration_ids: get_all_bid_users_tokens
                      })
                    });
                  }

                  return res.json({ bidPlaced: true, results: vehicles });
                })
                .catch(err => {
                  return res.status(500).send(err);
                });
            })
            .catch(error => {
              return res.status(500).send(error);
            });
        } else {
          errors.bid_price =
            "Sorry, the amount you have bid is greater than your credit limit.";
            errors.vehicle_id=vehicle._id;
          return res.status(400).json(errors);
        }
      } else {
        errors.bid_price = "Bid increase must be greater than $100";
        errors.vehicle_id=vehicle._id;
        return res.status(400).json(errors);
      }
    });
  }
);

// GET Transaction history
router.get(
  "/transaction_history",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    //	{ active: true }
    let vehicles = [];
    try {
      let userVehicles = await Vehicles.find({ user: req.user.id }).populate(
        "accepted_bid"
      );
      userVehicles = userVehicles.map(vehicle => {
        let vehicleStatus = "";
        if (vehicle.accepted) {
          vehicleStatus = "Sold";
        } else {
          vehicleStatus = "Unsold";
        }
        return { ...vehicle._doc, type: "sell", vehicleStatus };
      });
      let userBids = await Bids.find({ user: req.user.id }).populate("vehicle");
      let userBidVehicles = [];
      if (userBids !== null) {
        let userBidIds = userBids.map(bid => bid.id);
        let userBroghtVehicles = await Vehicles.find({
          accepted_bid: { $in: userBidIds }
        }).populate("accepted_bid");
        userBidVehicles = userBroghtVehicles.map(vehicle => {
          return { ...vehicle._doc, type: "bid", vehicleStatus: "Bought" };
        });
      }
      vehicles = [...userVehicles, ...userBidVehicles];
      return res.json(vehicles);
    } catch (error) {
      //console.log("Error: Error in fetchin transasction history...", error);
      return res
        .status(500)
        .send("Error: Error in fetchin transasction history...");
    }
  }
);

// auction status
router.post(
  "/auction_status",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    const vehicle_id = req.body.vehicle;
    const user_id = req.body.user;
    const status = req.body.status;

    const updated_vehicle = await Vehicles.findOneAndUpdate(
      { _id: vehicle_id, email_click: { $exists: false } },
      {
        $set: {
          email_click: true
        }
      }
    );

    if (updated_vehicle !== null) {
      if (status == "yes") {
        //get user feedback
        var buyer_user = await User.findOne({ _id: user_id });

        //postive feedback

        if (typeof buyer_user.buyer_feedback !== "undefined") {
          var buyer_feedback = parseInt(buyer_user.buyer_feedback + 1, 10);
        } else {
          var buyer_feedback = 100;
        }

        if (buyer_feedback > 100) {
          buyer_feedback = 100;
        }

        if (buyer_feedback < 0) {
          buyer_feedback = 0;
        }

        await User.findOneAndUpdate(
          { _id: user_id },
          {
            $set: { buyer_feedback: buyer_feedback }
          }
        );
      } else {
        await User.findOneAndUpdate(
          { _id: user_id },
          {
            $set: {
              suspended: true
            }
          }
        );
      }
    }
  }
);

const vehicleSellProcessor = (
  userDetails,
  vehicle,
  amount,
  paymentType = "Credit Card"
) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (userDetails.email) {

          let vehicle_name
          if(vehicle.registration_number){
            vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - " + vehicle.registration_number;
          }
          else{
            vehicle_name = vehicle.year + " " + vehicle.make + " - " + vehicle.model + " - No Rego";
          }
        let gst = 15;
        let calcGst = parseFloat((amount * gst) / 100, 2);
        let invoice = "IN_" + Math.floor(Math.random() * (50000 - 5000 + 1)) + 5000;
        await Vehicles.findOneAndUpdate(
         { _id: vehicle._id /*active: false, expired: true*/ },
         {
           $set: {
            seller_invoice:invoice
           }
         },
         { new: true }
       );

        const msg = {
          to: userDetails.email,
          from: "Ripa <info@ripa.app>",
          subject: `Invoice for Successful Listing Vehicle - ${vehicle_name}`,
          templateId: "ac8174d2-790a-4035-9050-0c0ba2b15894",
          substitutions: {
            invoice_id: invoice,
            invoice_date: new Date()
              .toString()
              .replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, "$2-$1-$3"),
            user_name: userDetails.name,
            user_email: userDetails.email,
            vehicle_name: `${vehicle_name} - Successful Listing`,
            vehicle_price: amount,
            payment_type: paymentType,
            vehicle_currency: "NZD",
            gst_price: `($${calcGst}) ${gst}%`,
            total_price: parseFloat(amount + calcGst, 2),
            SITE_URL: "https://www.ripa.co.nz/contact/"
          }
        };

        sgMail.send(msg);
      }

      let searchCreteria = [];

      if(vehicle.vehicle_type)
      {
        searchCreteria.push({ "filterData.vehicleType": vehicle.vehicle_type })
      }
      else{
        searchCreteria.push({ "filterData.vehicleType":  ["Convertible","Coupe","Hatchback","Sedan","Station Wagon","RV/SUV","Ute","Van","Other"]})
       
      }

      if(vehicle.make)
      {
        searchCreteria.push({ "filterData.Make": vehicle.make })
      }

      if(vehicle.km)
      {
        searchCreteria.push({
          $and: [
            { "filterData.Mileage.start": { $lte: parseInt(vehicle.km,10) } },
            { "filterData.Mileage.end": { $gt: parseInt(vehicle.km,10) } }
          ]
        })
      }
      else{
        searchCreteria.push({
          $and: [
            { "filterData.Mileage.start": { $lte: 0 } },
            { "filterData.Mileage.end": { $gt: 0 } }
          ]
        })
      }

      if(vehicle.year)
      {
        searchCreteria.push({
          $and: [
            { "filterData.Year": { $lte: parseInt(vehicle.year,10) } },
            { "filterData.YearEnd": { $gte: parseInt(vehicle.year,10) } }
          ]
        })
      } 
       
      if (searchCreteria.length==0) {
        resolve("Worked out");
      }
       console.log("searchCreteria",searchCreteria)
      var findUserSavedSearch = await UserSavedSearch.distinct("user", {
        $and: searchCreteria
      });
      console.log("searchCreteria",findUserSavedSearch)

      
      if (findUserSavedSearch !== null) {
        var userSavedSearch = "";

        if (!findUserSavedSearch instanceof Array) {
          userSavedSearch = [findUserSavedSearch];
        } else {
          userSavedSearch = findUserSavedSearch;
        }
        var get_all_bid_users_tokens = await User.distinct(
          "notifications_token",
          {
            _id: {
              $in: userSavedSearch,
              $nin: [userDetails._id]
            },
            notifications:true
          }
        );

        get_all_bid_users_tokens = get_all_bid_users_tokens.concat(await User.distinct(
          "web_notification_token",
          {
             _id: {
              $in: userSavedSearch,
              $nin: [userDetails._id]
            },
            notifications:true
          }
        ),
        await User.distinct(
          "ios_notification_token",
          {
             _id: {
              $in: userSavedSearch,
              $nin: [userDetails._id]
            },
            notifications:true
          }
        ))

        if (get_all_bid_users_tokens !== null) {
          const req_res = await request({
            url: "https://fcm.googleapis.com/fcm/send",
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Authorization:
                "key=AAAA0xWSq2o:APA91bFFCpXhSwpU_yYuW5xCBC-N_pD_UsDpzIKL0_nrpbWuIF-4UMtdYYKg4ZOMSz0NgIpJA60AleE2ti2216F5YI5rC2tpMPkwzpLOciyjJOIDpwxQj9_dv-DQcVgwgdBFwVitQKLU"
            },
            body: JSON.stringify({
              notification: {
                title: "Ripa - Live Auction",
                body:
                  "A new vehicle matching your preferences has been listed in auction.",
                // "click_action": "https://dev.ripa.app/live-auctions",
                icon: "./logo_192x192.png",
                sound: "sound",
                "click_action": `${ keys.HOST_PROTOCOL + keys.SITE_URL}/`,
              },
              data: {
                body: "new_vehicle",
                vehicle: vehicle._id
              },
              registration_ids: get_all_bid_users_tokens
            })
          });
        }
      }

      resolve("Worked out");
    } catch (err) {
      console.log(err)
      reject(err);
    }
  });

  
};



module.exports = router;
