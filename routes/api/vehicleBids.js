const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Bids = require('../../models/Bids');

const isEmpty = require('../../validation/isEmpty');

router.post('/', passport.authenticate('user-role', {session: false}), (req, res) => {

				const vehicle_id = req.body.vehicle_id;

				Bids
								.find({vehicle: vehicle_id})
								.sort({_id: -1})
								.then(bids => {
												return res.json({bids: bids});
								})
								.catch(err => {
												console.log("No bids yet");
								});

});

module.exports = router;