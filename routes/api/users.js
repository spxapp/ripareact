const express = require("express");
const router = express.Router();
const Users = require("../../models/Users");
const Profile = require("../../models/Profile");
const AdminSettings = require("../../models/admin/AdminSettings");
const Vehicles = require("../../models/Vehicles");
const Bids = require("../../models/Bids");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

// Load Input Validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const validateChangePassword = require("../../validation/changePassword");
const validateForgetPasswordInput = require("../../validation/forgetPassword");


 
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(keys.SENDGRID_API_KEY); 

// @route   GET api/users/test @desc    Test users route @access  Public

router.get(
  "/get",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    Users.findOne({ _id: req.user.id }).then(user => {
      res.json({
        id: user._id,
        email: user.email,
        name: user.name,
        status: user.status,
        credit_limit: user.credit_limit,
        notifications: user.notifications
      });
    });
  }
);

// @route   POST api/users/register @desc    Register Users Route @access Public

router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  //check validation

  if (!isValid) {
    return res.status(400).json(errors);
  }

  Users.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = "Email already exists";
      return res.status(400).json(errors);
    } else {
      AdminSettings.findOne()
        .then(settings => {
          const newUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            credit_limit: settings.credit_limit,
            dealer_number: req.body.dealer_number.toLowerCase()
          });
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                .save()
                .then( async (user) => {
                  
                  // Send Email Notification to admin

                  const adminSettings = await AdminSettings.findOne(); 

                  if (adminSettings !== null) {
                    const msg = {
                      to: adminSettings.admin_email,
                      from: "Ripa <info@ripa.app>",
                      subject:
                        "New user registration: " +
                        user.name +
                        " is waiting for approval",
                      templateId: "458bb7f0-137f-4c7c-832f-c00cc2e7296a",
                      substitutions: {
                        name: user.name,
                        email: user.email,
                        dealer_number: user.dealer_number, 
                        SITE_URL: "https://www.ripa.co.nz/contact/" 
                      }
                    };
                    sgMail.send(msg);
                  }

                  res.json(user)
               
                })
                .catch(err => console.log(err));
            });
          });
        })
        .catch(err => {
          const newUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            dealer_number: req.body.dealer_number.toLowerCase()
          });
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                .save()
                .then( async (user) => 
                  
                 {
                  // Send Email Notification to admin

                  const adminSettings = await AdminSettings.findOne(); 

                  if (adminSettings !== null) {

                    const msg = {

                      to: adminSettings.admin_email,
                      from: "Ripa <info@ripa.app>",
                      subject:
                        "New user registration: " +
                        user.name +
                        " is waiting for approval",
                      templateId: "458bb7f0-137f-4c7c-832f-c00cc2e7296a",
                      substitutions: {
                        name: user.name,
                        email: user.email,
                        dealer_number: user.dealer_number, 
                        SITE_URL: "https://www.ripa.co.nz/contact/" 
                      }
                    };

                    sgMail.send(msg);

                  }

                  res.json(user)
                }
                )
                .catch(err => console.log(err));
            });
          });
        });
    }
  });
});

// @route   POST api/users/login @desc    Login Users Route @access  Public

router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  //check validation

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  Users.findOne({ email,archived:false }).then(user => {
    //check for user
    if (!user) {
      errors.email = "This email does not exist in our system.";
      return res.status(404).json(errors);
    }

    //check password

    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        //user matched

        const payload = {
          id: user.id,
          email: user.email,
          name: user.name,
          status: user.status,
          credit_limit: user.credit_limit,
          notifications: user.notifications
        }; // create JWT payload

        //Sign Token

        jwt.sign(payload, keys.secretKey, (err, token) => {
          res.json({
            success: true,
            token: "Bearer " + token
          });
        });
      } else {
        errors.password = "Invalid Password";
        return res.status(400).json(errors);
      }
    });
  });
});


// @route   POST api/users/forget-password @desc    Login Users Route @access  Public

router.post("/forget-password", (req, res) => {
  const { errors, isValid } = validateForgetPasswordInput(req.body);

  //check validation 
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email; 
  const timestamp = req.body.timestamp;
  const resetPassword = req.body.resetPassword;
 
  // Find user by email
  Users.findOne({ email }).then(user => {
    //check for user
    if (!user) {
      errors.email = "This email does not exist in our system.";
      return res.status(404).json(errors);
    }  

    
    if(resetPassword)
    {
      const password  = req.body.password;
      const password2 = req.body.password2;

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) throw err;
          Users.findOneAndUpdate(
            {
              email: user.email
            },
            {
              $set: {
                password: hash
              }
            }
          ).then(user => {
            const successMsg = 'Password changed successfully!!';
            res.json({ type: 'change-password', successMsg });
          });
        });
      }); 

    }else{

      if (user.email) {
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
        
        const msg = {
  
          to: user.email,
          from: "Ripa <info@ripa.app>",
          subject:
            "Reset Password Request",
          templateId: "3a34bc1a-2344-4ba2-b011-b29245a33d03",
          substitutions: {
            user: user.name,
            WEB_URL: keys.HOST_PROTOCOL + keys.SITE_URL,
            reset_link: Base64.encode('email='+user.email+'&timestamp='+timestamp), 
						SITE_URL: "https://www.ripa.co.nz/contact/" 
          }
        };
  
        sgMail.send(msg);
        return res.json({ type: 'send-reset-link', successMsg: 'A password reset link has been sent to your email.' });
      }

    } 

    
 
  });
});

// @route   GET api/users/current @desc    Return Current User @access  Private

router.get(
  "/current",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      status: req.user.status
    });
  }
);

// getdetails

router.post(
  "/getdetails",
  passport.authenticate("user-role", { session: false }),
  async (req, res) => {
    var user_id = req.body.user_id;
 
    const type = req.body.type;

    if(type==='Buyer'){

      BidUser = await Bids.findOne({_id:user_id}) 

      user_id = BidUser.user;
 
    }

    Users.findOne({ _id: user_id }).then(async user => {
      if (user) {
        var user_sell_vehilces = await Vehicles.find({
          user: user._id,
          active: false,
          expired: true
        }).countDocuments();

        var seller_feedback =
          typeof user.seller_feedback !== "undefined"
            ? user.seller_feedback
            : 0;
        if (seller_feedback < 0) {
          seller_feedback = 0;
        }

        if (seller_feedback > 100) {
          seller_feedback = 100;
        }

        if (user_sell_vehilces == 0 && seller_feedback == 0) {
          seller_feedback = 100;
        }

        Profile.findOne({ user: user._id })
          .then(profile => {
            return res.json({
              user: user,
              profile: profile,
              ratings: {
                sell_rating: user_sell_vehilces + " (" + seller_feedback + "%)",
                buy_rating: "0",
                buy_rating_red: "(0)"
              }
            });
          })
          .catch(err => {
            return res.json({ user: user, profile: {} });
          });
      }
    });
  }
);

// @route   POST api/users/change-password @desc    Change Password Users Route
// @access  Public

router.post(
  "/change-password",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateChangePassword(req.body);

    //check validation

    if (!isValid) {
      return res.status(400).json(errors);
    }

    const userId = req.user.id;
    const current_password = req.body.current_password;
    const new_password = req.body.new_password;

    Users.findOne({ _id: req.user.id })
      .then(user => {
        //check password

        bcrypt.compare(current_password, user.password).then(isMatch => {
          if (isMatch) {
            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(new_password, salt, (err, hash) => {
                if (err) throw err;
                Users.findOneAndUpdate(
                  {
                    _id: req.user.id
                  },
                  {
                    $set: {
                      password: hash
                    }
                  }
                ).then(user => {
                  res.json({ results: user });
                });
              });
            });
          } else {
            errors.current_password = "Invalid Current Password";
            return res.status(400).json(errors);
          }
        });
      })
      .catch(err => {
        res.status(500).json(err);
      });
  }
);

router.post(
  "/allow-user-notifications",
  passport.authenticate("user-role", { session: false }),
  (req, res) => {
    const userId = req.user.id;
    const notifications = req.body.notifications;
    const token = req.body.token;
    const type = req.body.type;
    console.log("request reached",req.body)
    let notification_token = "";
    if(type==="ios"){
      notification_token="ios_notification_token";
    }
    else if(type==="web")
    {
      notification_token="web_notification_token";
    }
    else if(type==="android"){
      notification_token="notifications_token";
    }
    var update = { $set : {} };
    update.$set[notification_token] = token;
    update.$set['notifications'] = notifications;

    User.findOneAndUpdate(
      {
        _id: userId
      },
     update,
      { new: true }
    ).then(user => {
      return res.json({
        id: user._id,
        email: user.email,
        name: user.name,
        status: user.status,
        credit_limit: user.credit_limit,
        notifications: user.notifications
      });
    });
  }
);

module.exports = router;
